<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'scheme' => 'https',
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'pusher' => [
        'beams_instance_id' => '5ff5290c-319d-46b8-b077-84ad0350650c',
//        'beams_secret_key' => 'AAAA_6SqhXg:APA91bHAOiupJR2dzXKuiJgYRMeFPEjlGVzz7skk4BDO5u_qloJb2FdUsPz3hUlECh_DH3LGOnt_EtPVkLXnJGwUcESsfyt_bSblPgIRGFBCINGC4Anlv8I1CGmCePSJWGzf_CXRjRUB',
        'beams_secret_key' => '4080E12EBBEC3A3914D4E1939E52C5E6BBD6DA8BBC3F72481B7F3AD8209A9A61',
    ],

];
