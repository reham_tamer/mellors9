<?php

namespace App\Console;

use App\Console\Commands\AddCycleNumber;
use App\Console\Commands\AddQueueMinuteNumber;
use App\Console\Commands\ScheduleParkTimes;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
          $schedule->command(AddCycleNumber::class)->everyThirtyMinutes();
          $schedule->command(AddQueueMinuteNumber::class)->hourly();
          $schedule->command(ScheduleParkTimes::class)->daily();
          $schedule->command('session:gc')->hourly();


    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
