<?php

namespace App\Console\Commands;

use App\Models\ParkTime;
use App\Models\QueueRideNumber;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AddQueueMinuteNumber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add-queue-rider-hourly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $currentDate = Carbon::now()->toDateString();
        $currentTime = Carbon::now('Asia/Riyadh')->format('H:i');
        ParkTime::with('parks.rides.queues')->where('date', $currentDate)
            ->orWhere(function ($subquery) use ($currentDate, $currentTime) {
                $subquery->where('close_date', $currentDate)
                    ->where('end', '>=', $currentTime);
            })->get()->each(function ($parkTime) {
                Log::info('Processing parkTime ID: ' . $parkTime->id);
//            $array = [];
                foreach ($parkTime->parks->rides as $ride) {
                    $queues = $ride->queues?->where('park_time_id', $parkTime->id)
                        ->whereBetween('start_time', [Carbon::now('Asia/Riyadh')->subHour(), Carbon::now('Asia/Riyadh')]);
                    $totalQueues = $queues?->sum('queue_seconds');

                    $totalQueuesMinutes = $totalQueues / 60;
                    if ($queues->count() > 0) {
                        QueueRideNumber::query()->create([
                            'ride_id' => $ride->id,
                            'minute_count' => $totalQueuesMinutes,
                            'date' => now('Asia/Riyadh')->format('Y-m-d H'),
                            'game_time_id' => $parkTime->gameTimes->where('ride_id', $ride->id)->first()?->id,
                            'avg' => round($totalQueuesMinutes / $queues->count(), 1)
                        ]);
//                        $array[] = array('ride_id' => $ride->name, 'avg_minutes' => $totalQueuesMinutes / $queues->count() / 60, 'date' => now());
                    }


//                if(!empty($array)){
//                    $parkAdmins = User::whereHas('roles', function ($query) {
//                        return $query->where('name', 'Park Admin');
//                    })->whereHas('parks', function ($query) use ($parkTime) {
//                        return $query->where('park_id', $parkTime->park_id);
//                    })->get();
//                    Excel::store(new RideQueueExport($array), 'excels/queues.xlsx', 'public');
//                    Notification::send($parkAdmins, new SendRideQueueHourly());
                }
            });
    }
}
