<?php

namespace App\Console\Commands;

use App\Events\timeSlotNotification;
use App\Models\AvailabilityStoppage;
use App\Models\GameTime;
use App\Models\Park;
use App\Models\ParkTime;
use App\Models\Ride;
use App\Models\RideStoppages;
use App\Models\ScheduleParkTime;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use RakibDevs\Weather\Weather;

class ScheduleParkTimes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule-park-time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $currentDate = Carbon::now('Asia/Riyadh')->toDateString();
        ScheduleParkTime::with('park')->where('start_date', $currentDate)->whereNotNull('parent_id')->get()->each(function ($parkTime) {
            Log::info('Processing Schedule parkTime ID: ' . $parkTime->id);
            try {
                print_r('park time schedule');
                $park = Park::find($parkTime->park_id);
                $city = $park->branches;
                $wt = new Weather();
                $info = $wt->getCurrentByCity($city['name']);

                $start_date = $parkTime->start_date;
                $end_date = $parkTime->end_date;

                $to_time = $parkTime->start;
                $from_time = $parkTime->end;
                $start_timestamp = strtotime("$start_date $to_time");
                $end_timestamp = strtotime("$end_date $from_time");
                $data['duration_time'] = round(abs($start_timestamp - $end_timestamp) / 60, 2) . " minute";
                if ($start_date === Carbon::now()->toDateString()) {
                    $data['general_weather'] = $info->weather[0]->main;
                    $data['description'] = $info->weather[0]->description;
                    $data['temp'] = $info->main->temp;
                    $data['windspeed_avg'] = $info->wind->speed;
                }
                $data['start'] = $parkTime->start;
                $data['end'] = $parkTime->end;
                $data['date'] = $start_date;
                $data['close_date'] = $end_date;
                $data['park_id'] = $parkTime->park_id;
                $oldParkTime = ParkTime::where('date', $data['date'])->where('park_id',$parkTime->park_id)->first();
                if (!$oldParkTime) {
                    $parkTime = ParkTime::create($data);
                    $availablitystoppage = AvailabilityStoppage::where('park_id', $park->id)->where('date', Carbon::now()->toDateString())->get();
                    foreach ($availablitystoppage as $item) {
                        $ride = Ride::where('id', $item->ride_id)->first();
                        $lastStoppage = $ride->rideStoppages()?->where('ride_status', 'stopped')?->latest()?->first();
                        if ($lastStoppage?->parent_id != null) {
                            $parent = $lastStoppage?->parent_id;
                        } else {
                            $parent = $lastStoppage?->id;
                        }
                        RideStoppages::query()->create([
                            'ride_id' => $item->ride_id,
                            'ride_status' => 'stopped',
                            'stopage_sub_category_id' => $item->stopage_sub_category_id,
                            'stopage_category_id' => $item->stopage_category_id,
                            'description' => $item->comment,
                            'date' => Carbon::now()->toDateString(),
                            'time' => Carbon::now()->toTimeString(),
                            'opened_date' => $start_date,
                            'type' => 'all_day',
                            'end_date' => $data['close_date'],
                            'time_slot_end' => $data['end'],
                            'time_slot_start' => $to_time,
                            'down_minutes' => $parkTime->duration_time,
                            'user_id' => $item->user_id,
                            'park_id' => $parkTime->park_id,
                            'zone_id' => $ride->zone_id,
                            'park_time_id' => $parkTime->id,
                            'stoppage_status' => 'working',
                            'parent_id' => $parent
                        ]);

                    }
                    $rides=Ride::where('park_id',$parkTime->park_id)->get();
                    foreach ($rides as $ride) {
                        $list = new GameTime();
                        $list->ride_id = $ride->id;
                        $list->zone_id = $ride->zone_id;
                        $list->no_of_seats = $ride->capacity_one_cycle;
                        $list->park_id = $parkTime->park_id;
                        $list->date = $parkTime->date;
                        $list->park_time_id = $parkTime->id;
                        $list->start = $parkTime->start;
                        $list->end = $parkTime->end;
                        $list->close_date = $parkTime->close_date;
                        $list->save();
                    }
                    $date = [
                        'user_name' => auth()->user()->name,
                        'start' => $data['start'],
                        'end' => $data['end'],
                        'date' => $data['date'],
                        'close_date' => $data['close_date'],
                    ];

                    event(new timeSlotNotification($date));
                }


            } catch (\Exception $e) {
                Log::error('Error processing rides', [
                    'parkTime_id' => $parkTime->id,
                    'error' => $e->getMessage(),
                ]);
            }


        });
    }
}
