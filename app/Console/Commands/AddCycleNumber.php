<?php

namespace App\Console\Commands;

use App\Models\CycleRideNumber;
use App\Models\ParkTime;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AddCycleNumber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add-cycle-rider-hourly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $currentDate = Carbon::now('Asia/Riyadh')->toDateString();
        $currentTime = Carbon::now('Asia/Riyadh')->format('H:i');

        ParkTime::with('parks.rides.cycle')->where('date', $currentDate)
            ->orWhere(function ($subquery) use ($currentDate, $currentTime) {
                $subquery->where('close_date', $currentDate)
                    ->where('end', '>=', $currentTime);
            })->get()->each(function ($parkTime) {
                Log::info('Processing parkTime ID: ' . $parkTime->id);
//            $array = [];
                try {
                    foreach ($parkTime->parks->rides as $ride) {
                        $cyclesByHour = $ride->cycle
                            ->where('park_time_id', $parkTime->id)
                            ->whereBetween('start_time', [Carbon::now('Asia/Riyadh')->subHour(), Carbon::now('Asia/Riyadh')]);
                        $totalRidersByHour = $cyclesByHour->sum('riders_count')
                            + $cyclesByHour->sum('number_of_disabled')
                            + $cyclesByHour->sum('number_of_vip')
                            + $cyclesByHour->sum('number_of_ft');

                        if ($cyclesByHour->count() > 0) {
                            CycleRideNumber::query()->create([
                                'ride_id' => $ride->id,
                                'rider_count' => $totalRidersByHour,
                                'date' => now('Asia/Riyadh')->format('Y-m-d H'),
                                'game_time_id' => $parkTime->gameTimes->where('ride_id', $ride->id)->first()?->id,
                                'cycle_count' => $cyclesByHour->count(),
                            ]);
                        }


//                    $array[] = [
//                        'ride_id' => $ride->name,
//                        'rider_count' => $totalRiders,
//                        'date' => now('Asia/Riyadh')->format('Y-m-d H'),
//                    ];
                    }
                } catch (\Exception $e) {
                    Log::error('Error processing rides', [
                        'parkTime_id' => $parkTime->id,
                        'error' => $e->getMessage(),
                    ]);
                }

//            if (!empty($array)) {
//                $parkAdmins = User::whereHas('roles', function ($query) {
//                    return $query->where('name', 'Park Admin');
//                })->whereHas('parks', function ($query) use ($parkTime) {
//                    return $query->where('park_id', $parkTime->park_id);
//                })->get();
//
//                $filename = 'excels/cycles/' . $parkTime->id . '_' . now('Asia/Riyadh') . '.xlsx';
//                Excel::store(new RideCycleExport($array), $filename, 'public');
//
//                Notification::send($parkAdmins, new SendRideCycleHourly());
//            }
            });
    }
}
