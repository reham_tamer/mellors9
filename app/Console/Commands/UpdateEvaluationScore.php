<?php

namespace App\Console\Commands;

use App\Models\EvaluationQuestion;
use Illuminate\Console\Command;

class UpdateEvaluationScore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-evaluation-score';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update evaluation score to 5';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        EvaluationQuestion::query()
            ->whereNull('description')
            ->update([
                'max_score' => 5,
            ]);
    }
}
