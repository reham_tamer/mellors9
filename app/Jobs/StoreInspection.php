<?php

namespace App\Jobs;

use App\Events\Api\showNotification;
use App\Events\RideCapacityEvent;
use App\Models\AttendanceLog;
use App\Models\GameTime;
use App\Models\PreopeningList;
use App\Models\Ride;
use App\Models\RideCapacity;
use App\Models\RideCycles;
use App\Models\User;
use App\Notifications\RideCapacityNotifications;
use App\Notifications\ZoneSupervisorNotifications;
use App\Traits\Api\NotificationApi;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class StoreInspection implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(public $validated, public $user, public $list)
    {
    }

    public function handle()
    {
        $supervisors = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Operations/Zone supervisor');
        })->get();

        $preopeningLists = [];
        foreach ($this->validated['inspection_list_id'] as $key => $inspection) {
            $preopeningLists[] = [
                'comment' => $this->validated['comment'][$key] ?? null,
                'inspection_list_id' => $inspection,
                'status' => $this->validated['status'][$key] ?? null,
                'is_checked' => $this->validated['is_checked'][$key] ?? null,
                'preopen_info_id' => $this->list->id,
            ];
        }
        PreopeningList::insert($preopeningLists); // Batch insert

        $ride = Ride::with(['park', 'zone'])->find($this->validated['ride_id']);

        if ($this->validated['lists_type'] == 'preopening') {

            $capacity = RideCapacity::where('date', Carbon::now()->toDateString())
                ->where('ride_id', $this->validated['ride_id'])
                ->first();

            if ($capacity != null) {

                if ($this->validated['number_of_seats'] != $capacity->ride_availablity_capacity) {
                    $capacity->preopening_capacity = $this->validated['number_of_seats'];
                    $capacity->time = Carbon::now()->toTimeString();
                    $capacity->save();
                    $users = User::whereHas('roles', function ($query) {
                        return $query->whereIn('name', ['Super Admin', 'Park Admin']);
                    })->get();
                    foreach ($supervisors as $supervisor) {
                        if (isset($supervisor) && $supervisor->zones?->whereIn('id', $this->validated['zone_id'])) {
                            event(new showNotification($supervisor->id, $this->validated['lists_type'] . ' check list added to ' . $ride?->name, $capacity->created_at, ParkTime($ride?->park_id)?->id, $this->validated['ride_id']));
                        }
                    }

                    $data = [
                        'ride_id' => $this->validated['ride_id'],
                        'title' => 'There was a difference in the number of chairs ' . $ride->name . ' Waiting for confirmation of capacity',
                        'user_id' => $this->user->id,
                    ];
                    foreach ($users as $user) {
                        Notification::send($user, new RideCapacityNotifications($data));
                        event(new RideCapacityEvent($user->id, $data['title'], Carbon::now()->toDateTimeString()));
                    }
                } else {
                    $capacity->preopening_capacity = $this->validated['number_of_seats'];
                    $capacity->final_capacity = $this->validated['number_of_seats'];
                    $capacity->time = Carbon::now()->toTimeString();
                    $capacity->save();
                }
            }
        } else {
            $openDate = GameTime::where('ride_id', $this->validated['ride_id'])->orderBy('date', 'DESC')->latest()->first()?->date;

            if ($openDate) {
                $log = AttendanceLog::query()->where('open_date', $openDate)
                    ->where('ride_id', $this->validated['ride_id'])->where('type', 'login')->latest()->first();
            } else {
                Log::error('dateTime() returned null at attendants log');
                $log = null;
            }
            if ($log) {
                $logStart = \Carbon\Carbon::parse("$log->date_time_start");
                $log->update([
                    'type' => 'logout',
                    'date_time_end' => Carbon::now(),
                    'shift_minutes' => $logStart->diffInMinutes(Carbon::now()),
                ]);
            }
            if (isset($ride)) {
                $ride->current_user_id = null;
                $ride->save();
            } else {
                Log::error('Ride  is null ');
            }

        }
        $gameTime = GameTime::where('ride_id', $this->validated['ride_id'])->orderBy('date', 'DESC')->latest()->first();
        $park_time_id = $gameTime?->park_time_id;
        if ($this->validated['lists_type'] == 'preclosing' && isset($this->validated['duration_seconds']) && $this->validated['duration_seconds'] != null) {

            $rider = RideCycles::where('ride_id', $this->validated['ride_id'])
                ->where('duration_seconds', 0)
                ->where('park_time_id', $park_time_id)
                ?->latest()
                ->first();

            if ($rider) {
                $rider->duration_seconds = $this->validated['duration_seconds'];
                $rider->save();
            }
        }
        /* close opened queue
        if ($this->validated['lists_type'] == 'preclosing') {
                  $lastQueue = Queue::where('ride_id', $this->validated['ride_id'])
                      ->where('park_time_id', $park_time_id)
                      ->latest()
                      ?->first();
                  if ($lastQueue && $lastQueue->queue_seconds == 0) {
                      $startTime = Carbon::parse($lastQueue->start_time);
                      $endTime = Carbon::now();
                      $totalDuration = (new Carbon($endTime))->diffInSeconds(new Carbon($startTime));
                      $lastQueue->update(['queue_seconds' => $totalDuration]);
                  }
              } */
        $zone = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Operations/Zone supervisor');
        })
            ->whereHas('zones', function ($query) use ($ride) {
                return $query->where('zone_id', $ride->zone?->id);
            })
            ->first();

        $data = [
            'title' => $ride->park?->name . ' : ' . $this->validated['lists_type'] . ' check list added to ' . $ride?->name,
            'ride_id' => $ride->id,
            'user_id' => $this->user->id,
        ];
        if ($zone && $this->validated['lists_type'] != 'inspection_list') {
            Notification::send($zone, new ZoneSupervisorNotifications($data));
            $new = new NotificationApi("$zone->id", $data['title']);
            $new->send();
        }

    }
    /*    public function middleware()
       {
           return [
               (new RateLimited('inspection'))->dontRelease()
           ];
       } */
}
