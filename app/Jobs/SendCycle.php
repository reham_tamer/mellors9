<?php

namespace App\Jobs;

use App\Models\GameTime;
use App\Models\RideCycles;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendCycle implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(public $validate2)
    {
    }

    public function handle()
    {
        $gameTime = GameTime::where('ride_id', $this->validate2['ride_id'])
            ->latest()
            ->first();
        $this->validate2['game_time_id'] = $gameTime?->id;
        $cycle = RideCycles::query()->create($this->validate2);
        $totalRiders = $this->validate2['riders_count'] + $this->validate2['number_of_disabled'] + $this->validate2['number_of_vip'] + $this->validate2['number_of_ft'];
        event(new \App\Events\TotalRidersEvent($cycle->park_id, $totalRiders));
    }
}
