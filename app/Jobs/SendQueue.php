<?php

namespace App\Jobs;

use App\Models\GameTime;
use App\Models\ParkTime;
use App\Models\Queue;
use App\Models\RideCycles;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class SendQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(public $validate)
    {
    }


    public function handle()
    {
        $parkTime = ParkTime::find($this->validate['park_time_id']);
        $this->validate['opened_date'] = $parkTime?->date;
        $gameTime = GameTime::where('ride_id', $this->validate['ride_id'])->orderBy('date', 'DESC')->latest()->first();
        $this->validate['game_time_id'] = $gameTime?->id;

        $lastQueue = Queue::where('ride_id', $this->validate['ride_id'])->where('park_time_id', $parkTime->id)->latest()?->first();
        if ($lastQueue && $lastQueue->queue_seconds == 0) {
            $startTime = Carbon::parse($lastQueue->start_time)->toTimeString();
            $endTime = Carbon::now()->toTimeString();
            $totalDuration = (new Carbon($endTime))->diffInSeconds(new Carbon($startTime));
            $lastQueue->update(['queue_seconds' => $totalDuration]);
        }
        Queue::query()->create($this->validate);
        event(new \App\Events\RideQueueEvent($this->validate['ride_id'], 'active'));

    }
}

