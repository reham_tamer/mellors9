<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class RideStoppages extends Model
{
    use HasFactory;

    protected $fillable = [
        'ride_id',
        'park_id',
        'number_of_seats',
        'operator_number',
        'operator_name',
        'ride_status',
        'stopage_sub_category_id',
        'stopage_category_id',
        'ride_notes',
        'date',
        'time',
        'opened_date',
        'date_time',
        'down_minutes',
        'user_id',
        'stoppage_status',
        'type',
        'time_slot_start',
        'time_slot_end','end_date',
        'park_time_id','description','zone_id','parent_id','game_time_id','edited_by_id'
    ];

    public function ride()
    {
        return $this->belongsTo(Ride::class)->withDefault();
    }
    public function park()
    {
        return $this->belongsTo(Park::class)->withDefault();
    }
    public function zone()
    {
        return $this->belongsTo(Zone::class,'zone_id')->withDefault([
            'name'=>'not found'
        ]);

    }
    public function stopageSubCategory()
    {
        return $this->belongsTo(StopageSubCategory::class,'stopage_sub_category_id')->withDefault()->withTrashed();
    }
    public function stopageCategory()
    {
        return $this->belongsTo(StopageCategory::class,'stopage_category_id')->withDefault()->withTrashed();
    }


    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();
    }
    public function parkTime()
    {
        return $this->belongsTo(ParkTime::class,'park_time_id')->withDefault();
    }
    public  function album(){

        return $this->hasMany(rideStoppagesImages::class,'ride_stoppages_id','id');
    }
    public function parent()
    {
        return $this->belongsTo(RideStoppages::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(RideStoppages::class, 'parent_id');
    }

    public function evacuation()
    {
        return $this->hasOne(Evacuation::class, 'stoppage_id', 'id');
    }
    public function UpdatedBy()
    {
        return $this->belongsTo(User::class,'edited_by_id','id');
    }

}
