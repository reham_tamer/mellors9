<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


class ParkTime extends Model
{

    protected $fillable = [
        'start','end','park_id','date','daily_entrance_count','close_date',
        'duration_time','general_comment','general_weather','temp','description','windspeed_avg'
    ];

    public function parks()
    {
        return $this->belongsTo(Park::class,'park_id');

    }

    public function rides()
    {
        return $this->hasMany(Ride::class,'park_id','park_id');

    }

    public function cycles()
    {
        return $this->hasMany(RideCycles::class,'park_time_id','id');

    }
    public function queues()
    {
        return $this->hasMany(Queue::class,'park_time_id','id');

    }
    public function preopeningLists()
    {
        return $this->hasMany(PreopenInfo::class,'park_time_id','id');

    }

    public function healthAndSafetyReports()
    {
        return $this->hasOne(HealthAndSafetyReport::class,'park_time_id','id');

    }
    public function maintenanceReports()
    {
        return $this->hasOne(MaintenanceReport::class,'park_time_id','id');

    }
    public function gameTimes()
    {
        return $this->hasMany(GameTime::class,'park_time_id','id');

    }
    public function evacuations()
    {
        return $this->hasMany(Evacuation::class,'park_time_id','id');

    }
    public function skillGameReports()
    {
        return $this->hasOne(SkillGameReport::class,'park_time_id','id');

    }
    public function techReports()
    {
        return $this->hasOne(TechReport::class,'park_time_id','id');

    }
    public function rideStoppages()
    {
        return $this->hasMany(RideStoppages::class,'park_time_id','id');

    }
    public function rideOpsReport()
    {
        return $this->hasOne(RideOpsReport::class,'park_time_id','id');

    }
    public function attraction()
    {
        return $this->hasOne(Attraction::class,'park_time_id','id');

    }
    public function attractions()
    {
        return $this->hasMany(Attraction::class,'park_time_id','id')->where('date',Carbon::now()->toDateString());

    }

 /*    public static function boot()
    {
        parent::boot();

        static::deleting(function($parkTime) {
            $parkTime->cycles()->delete();
            $parkTime->queues()->delete();
            $parkTime->rideStoppages()->delete();
            $parkTime->rideOpsReport()->delete();
            $parkTime->preopeningLists()->delete();
            $parkTime->techReports()->delete();
            $parkTime->skillGameReports()->delete();
            $parkTime->maintenanceReports()->delete();
            $parkTime->healthAndSafetyReports()->delete();
            $parkTime->gameTimes()->delete();

            // You can also add additional cleanup logic here
        });
    } */

    public function rides2()
    {
        return $this->hasMany(Ride::class);

    }

}
