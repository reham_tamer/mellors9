<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AttendanceLog extends Model
{
    protected $guarded = [];

    public function park()
    {
        return $this->belongsTo(Park::class);
    }
    public function ride()
    {
        return $this->belongsTo(Ride::class,'ride_id');

    }
    public function attendance()
    {
        return $this->belongsTo(Attendance::class);
    }
}
