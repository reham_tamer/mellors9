<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DailyOperationalInfo extends Model
{
    protected $fillable = [
        'daily_operational_id',
        'check_list_question_id',
        'answer',
        'comment',
    ];
    public function question(){
        return $this->belongsTo(CheckListQuestion::class,'check_list_question_id');
    }
}
