<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBranch extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function preopenInfos()
    {
        return $this->hasMany(PreopenInfo::class, 'approve_by_id', 'id');
    }
}
