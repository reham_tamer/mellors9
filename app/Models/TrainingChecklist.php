<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class TrainingChecklist extends Model
{

    protected $guarded = [];
    public function items(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(TrainingChecklistItem::class,'training_checklist_id');
    }

}
