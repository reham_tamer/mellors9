<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Attendance extends Authenticatable
{
    use  HasApiTokens,Notifiable;

    protected $fillable = [
        'name', 'last_name', 'park_id', 'code', 'phone', 'national_id','status'
    ];

    public function park()
    {
        return $this->belongsTo(Park::class);
    }

    public function training_user(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(TrainingUser::class, 'userable');
    }

    public function training_checklist_users(): MorphMany
    {
        return $this->morphMany(TrainingUserChecklist::class, 'userable');
    }
    public function rides(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Ride::class,AttendanceRide::class);
    }
    public function training_rides(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(TrainingUserRide::class);
    }

}
