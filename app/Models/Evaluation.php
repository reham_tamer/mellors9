<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    protected $fillable = ['ride_id', 'park_id', 'operator_id', 'supervisor_id', 'total_score', 'park_time_id','by_user_id',
    'date','time','approve','approve_by_id','approved_at','signature','remarks'];

    public function ride()
    {
        return $this->belongsTo(Ride::class, 'ride_id')->withDefault();
    }

    public function park()
    {
        return $this->belongsTo(Park::class, 'park_id')->withDefault();
    }

    public function operator()
    {
        return $this->belongsTo(User::class, 'operator_id')->withTrashed()->withDefault();
    }

    public function supervisor()
    {
        return $this->belongsTo(User::class, 'supervisor_id')->withTrashed()->withDefault();
    }

    public function park_time()
    {
        return $this->belongsTo(ParkTime::class, 'park_time_id')->withDefault();
    }

    public function lists()
    {
        return $this->hasMany(EvaluationInfo::class, 'evaluation_id', 'id');
    }

    public function addedBy()
    {
        return $this->belongsTo(User::class, 'by_user_id')->withTrashed();
    }
    public function approvedBy()
    {
        return $this->belongsTo(User::class, 'approve_by_id')->withTrashed();
    }
    public function brackets(): Attribute
    {
        $brackets = '';

        switch ($this->total_score) {

            case $this->total_score >= 121 && $this->total_score <= 150:
                $brackets = 'Outstanding';
                break;

            case $this->total_score >= 91 && $this->total_score <= 120:
                $brackets = 'Exceeds';
                break;

            case $this->total_score >= 61 && $this->total_score <= 90:
                $brackets = 'Meets';
                break;

            case $this->total_score >= 31 && $this->total_score <= 60:
                $brackets = 'Improvement';
                break;
            case $this->total_score >= 0 && $this->total_score <= 30:
                $brackets = 'Unsatisfactory';
                break;

        }

        return new Attribute(
            get: fn() => $brackets,
        );

    }
    public function bracketsColor(): Attribute
    {
        $color = '';

        switch ($this->total_score) {

            case $this->total_score >= 121 && $this->total_score <= 150:
                $color = 'success';
                break;

            case $this->total_score >= 91 && $this->total_score <= 120:
                $color = 'primary';
                break;

            case $this->total_score >= 61 && $this->total_score <= 90:
                $color = 'warning';
                break;

            case $this->total_score >= 31 && $this->total_score <= 60:
                $color = 'dark';
                break;
            case $this->total_score >= 0 && $this->total_score <= 30:
                $color = 'danger';
                break;

        }

        return new Attribute(
            get: fn() => $color,
        );
    }

}
