<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class GameTime extends Model
{

    protected $fillable = [
        'start','end','ride_id','date','close_date','park_id','park_time_id','first_status'
        ,'second_status','comment','zone_id','no_of_gondolas','no_of_seats','user_id','verified_by_id','status'
    ];

    public function rides()
    {
        return $this->belongsTo(Ride::class,'ride_id')->withDefault([
            'name'=>'not found'
        ]);

    }
    public function created_by()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function verified_by()
    {
        return $this->belongsTo(User::class, 'verified_by_id', 'id');
    }
    public function parks()
    {
        return $this->belongsTo(Park::class,'park_id')->withDefault([
            'name'=>'not found'
        ]);

    }
    public function zone()
    {
        return $this->belongsTo(Zone::class,'zone_id')->withDefault([
            'name'=>'not found'
        ]);

    }
    public function stoppage_reason(){
        return $this->hasOne(AvailabilityStoppage::class,'game_time_id');
    }

    public function cycles()
    {
        return $this->hasMany(RideCycles::class,'park_time_id','id');

    }
    public function queues()
    {
        return $this->hasMany(Queue::class,'park_time_id','id');

    }
    public function rideStoppages()
    {
        return $this->hasMany(RideStoppages::class,'park_time_id','id');

    }
}
