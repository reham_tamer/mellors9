<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingUserChecklist extends Model
{

    protected $guarded = [];

    public function userable()
    {
        return $this->morphTo();
    }

    public function operators(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(User::class, 'id', 'userable_id');
    }

    public function attendances(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Attendance::class, 'id', 'userable_id');
    }

    public function trainingItems()
    {
        return $this->belongsTo(TrainingChecklistItem::class,'training_checklist_item_id');
    }
}
