<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ZoneInspectionList extends Model
{

    protected $fillable = [
        'zone_id','inspection_list_id','lists_type'
    ];
    public function inspection_list()
    {
        return $this->belongsTo(InspectionList::class, 'inspection_list_id', 'id');
    }
    public function zone()
    {
        return $this->belongsTo(Zone::class, 'zone_id')->withTrashed();
    }
    public function  list()
    {
        return $this->belongsTo(InspectionList::class, 'inspection_list_id', 'id');
    }
}
