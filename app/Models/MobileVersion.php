<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MobileVersion extends Model
{

    protected $fillable = ['type', 'version', 'status'];

    protected $casts = [
        'version' => 'integer',
    ];
}
