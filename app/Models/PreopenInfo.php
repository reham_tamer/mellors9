<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreopenInfo extends Model
{
    protected $guarded = [];
    public function created_by()
    {
        return $this->belongsTo(User::class, 'operator_id', 'id');
    } 
    public function approve_by()
    {
        return $this->belongsTo(User::class, 'approved_by_id', 'id');
    }

    public function ride()
    {
        return $this->belongsTo(Ride::class, 'ride_id', 'id')->withDefault(['name'=>'not found']);
    }
    public function park()
    {
        return $this->belongsTo(Park::class, 'park_id', 'id');
    }
    public function zone()
    {
        return $this->belongsTo(Zone::class, 'zone_id', 'id');
    }
    public function park_time()
    {
        return $this->belongsTo(ParkTime::class, 'park_time_id', 'id');
    }
 
    public function lists()
    {
        return $this->hasMany(PreopeningList::class, 'preopen_info_id', 'id');
    }

}
