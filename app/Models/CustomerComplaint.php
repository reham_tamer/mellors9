<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CustomerComplaint extends Model
{

    protected $fillable = [
        'name','type'
    ];

    public function categories(){
        return $this->hasMany(FeedbackSubCategory::class);
    }


}
