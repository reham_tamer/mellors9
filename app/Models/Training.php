<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{

    protected $fillable = ['training_checklist_id',
        'start_date',
        'end_date',
        'ride_id',
        'park_id',
        'created_by_id',
        'user_type',
        'manager_id','supervisor_id','type'
];

    public function trainingUsers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(TrainingUser::class, 'training_id');
    }

    public function user_checklist(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(TrainingUserChecklist::class, 'training_id');
    }

    public function ride(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Ride::class);
    }

    public function park(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Park::class);
    }

    public function checklist(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(TrainingChecklist::class, 'training_checklist_id');
    }
    public function supervisor()
    {
        return $this->belongsTo(User::class, 'supervisor_id');
    }
    
    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id');
    }


}
