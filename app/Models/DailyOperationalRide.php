<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DailyOperationalRide extends Model
{
    protected $fillable = [
        'daily_operational_id',
        'check_list_question_id',
        'ride_id',
        'comment_ride',
    ];

    public function ride()
    {
        return $this->belongsTo(Ride::class)->withDefault();
    }
}
