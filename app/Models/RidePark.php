<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RidePark extends Model
{
    protected $fillable = [
        'ride_id','park_id'
    ];
}
