<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CycleRideNumber extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function gameTime(){
        return $this->belongsTo(GameTime::class,'game_time_id','id');
    }
    public function ride(){
        return $this->belongsTo(Ride::class,'ride_id','id');
    }
}
