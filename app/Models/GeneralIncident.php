<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GeneralIncident extends Model
{
    protected $guarded = [];
    protected $casts = [
        'value' => 'array',
        'value_2' => 'array',
        'value_3' => 'array',
        'value_4' => 'array',
    ];


    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id', 'id');
    }
    public function ride()
    {
        return $this->belongsTo(Ride::class,'ride_id')->withDefault();
    }
    public function park()
    {
        return $this->belongsTo(Park::class,'park_id')->withDefault();
    }
    public function incidentStatements()
{
    return $this->hasMany(IncidentStatement::class, 'incident_form_id');
}
public  function album(){

    return $this->hasMany(IncidentImage::class,'general_incident_id','id');
}
}
