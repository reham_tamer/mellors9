<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainingChecklistItem extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function training(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(TrainingChecklist::class);
    }
}
