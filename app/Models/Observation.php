<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Observation extends Model
{
    protected $guarded = [];

    public function ride()
    {
        return $this->belongsTo(Ride::class)->withDefault();
    }
    public function department()
    {
        return $this->belongsTo(Department::class,'department_id');
    }
    public function observationCategory()
    {
        return $this->belongsTo(ObservationCategory::class,'observation_category_id');
    }
    public function created_by()
    {
        return $this->belongsTo(User::class,'created_by_id');
    }

}

