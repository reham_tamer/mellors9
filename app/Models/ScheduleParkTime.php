<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScheduleParkTime extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function park()
    {
        return $this->belongsTo(Park::class,'park_id');

    }
}
