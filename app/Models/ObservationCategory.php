<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ObservationCategory extends Model
{
    use HasFactory;
    protected $fillable = ['name'];
}
