<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DailyOperational extends Model
{


    protected $fillable = [
        'user_id',
        'park_id',
        'signature',
        'additional_comment',
        'date',
        'type'
    ];
    public function park()
    {
        return $this->belongsTo(Park::class)->withDefault();
    }
    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();
    }
}
