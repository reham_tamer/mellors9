<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evacuation extends Model
{
    use HasFactory;

    protected $fillable = [
        'time',
        'total_rider',
        'first_rider_off',
        'last_rider_off',
        'abnormal','longer_than_normal','medics_required','civil_defense_involved',
        'vip_guest_involved','customer_service_issue','property_damage',
        'first_aid',
        'evacuation_details',
        'customer_service_gesture',
        'portal_overview',
        'image',
        'park_id',
        'ride_id',
        'game_time_id','park_time_id',
        'stoppage_id',
        'created_by',
        'submit_date',
        'normal'
    ];

    public function park()
    {
        return $this->belongsTo(Park::class,'park_id');

    }
    public function ride()
    {
        return $this->belongsTo(Ride::class,'ride_id');

    }
}
