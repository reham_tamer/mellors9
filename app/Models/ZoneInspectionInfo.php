<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZoneInspectionInfo extends Model
{
    protected $guarded = [];
    public function created_by()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function approve_by()
    {
        return $this->belongsTo(User::class, 'approve_by_id', 'id');
    }
    public function park()
    {
        return $this->belongsTo(Park::class, 'park_id', 'id');
    }
    public function zone()
    {
        return $this->belongsTo(Zone::class, 'zone_id', 'id');
    }


    public function lists()
    {
        return $this->hasMany(ZoneInspection::class, 'zone_inspection_infos_id', 'id');
    }

}
