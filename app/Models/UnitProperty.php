<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UnitProperty extends Model
{
    protected $guarded = [];
    protected $casts = [
        'move_in' => 'array',
        'initial_inspection' => 'array',
        'move_out' => 'array',
    ];

    public function move_in_inspected_by()
    {
        return $this->belongsTo(User::class,'move_in_inspected_by_id')->withDefault();
    }
    public function move_out_inspected_by()
    {
        return $this->belongsTo(User::class,'move_out_inspected_by_id')->withDefault();
    }
    public function initial_inspection_by()
    {
        return $this->belongsTo(User::class,'initial_inspection_by_id')->withDefault();
    }
}
