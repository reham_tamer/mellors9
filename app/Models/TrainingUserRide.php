<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainingUserRide extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function ride()
    {
        return $this->belongsTo(Ride::class,'ride_id');
    }
}
