<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FeedbackSubCategory extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'customer_complaint_id'];
    protected $table = 'feedback_subcategory';

    public function customerComplaint()
    {
        return $this->belongsTo(CustomerComplaint::class,'customer_complaint_id','id');
    }
}
