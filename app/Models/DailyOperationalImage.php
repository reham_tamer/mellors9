<?php

namespace App\Models;
use App\Traits\ImageOperations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DailyOperationalImage extends Model
{
    protected  $table='daily_operationals_images';
    use HasFactory,ImageOperations;
    protected $guarded = [];
}
