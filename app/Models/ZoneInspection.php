<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ZoneInspection extends Model
{
    protected $fillable = [
        'inspection_list_id',
        'status',
        'comment',
        'is_checked',
        'zone_inspection_infos_id'
    ];

    public function inspection_list()
    {
        return $this->belongsTo(InspectionList::class,'inspection_list_id','id');

    }

    public function info()
    {
        return $this->belongsTo(ZoneInspectionInfo::class,'zone_inspection_infos_id');

    }


}
