<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RideZone extends Model
{
    protected $fillable = [
        'ride_id','zone_id'
    ];
}
