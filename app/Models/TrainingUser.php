<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainingUser extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function userable()
    {
        return $this->morphTo();
    }

    public function operators(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(User::class, 'id', 'userable_id');
    }

    public function attendances(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Attendance::class, 'id', 'userable_id');
    }
    public function operator()
    {
        return $this->hasOne(User::class, 'id', 'userable_id');
    }

    public function attendance()
    {
        return $this->hasOne(Attendance::class, 'id', 'userable_id');
    }
    public function getUserAttribute(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->operator() ? $this->operator() : $this->attendance();
    }
    public function training()
    {
        return $this->belongsTo(Training::class);
    }
    public function additions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(TrainingAddition::class, 'training_id', 'training_id');
    }

}
