<?php

namespace App\Traits\Api;

class NotificationApi
{


    public function __construct(public $id, public $title)
    {

    }

    /**
     * @throws \Exception
     */
    public function send()
    {
        $beamsClient = new \Pusher\PushNotifications\PushNotifications(array(
            "instanceId" => "5ff5290c-319d-46b8-b077-84ad0350650c",
            "secretKey" => "4080E12EBBEC3A3914D4E1939E52C5E6BBD6DA8BBC3F72481B7F3AD8209A9A61",
        ));

        $publishResponse = $beamsClient->publishToUsers(
            [$this->id],
            array(
                "fcm" => array(
                    "notification" => array(
                        "title" => "Mellors",
                        "body" => $this->title,
                    )
                ),
                "apns" => array("aps" => array(
                    "alert" => array(
                        "title" => "Mellors",
                        "body" => $this->title
                    )
                )),
            ));
//        $beamsClient->publishToInterests(
//            array("mellors",'notification-'.$this->id), // we pass the user id here
//            array(
//                "fcm" => array(
//                    "notification" => array(
//                        "title" => $this->id,
//                        "body" => $this->title,
//                    )
//                ),
//                "apns" => array("aps" => array(
//                    "alert" => array(
//                        "title" => "Stoppage!!",
//                        "body" => $this->title,
//                    )
//                )),
//
//            ));

//        return $publishResponse;
    }

}
