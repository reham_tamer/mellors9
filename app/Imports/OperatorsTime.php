<?php

namespace App\Imports;

use App\Models\ParkTime;
use App\Models\Ride;
use App\Models\UserLog;
use App\Models\User;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon\Carbon;

class OperatorsTime implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $ride = Ride::find($row['ride_id']);
            if (is_null($ride)) {
                throw ValidationException::withMessages([
                    'Ride' => 'Ride does not exist. Ride ID: ' . $row['ride_id'],
                ]);
            }
            $user = User::where('code', $row['code'])->first();
            if (is_null($user)) {
                throw ValidationException::withMessages([
                    'Operator' => 'Operator does not exist. Operator ID: ' . $row['code'],
                ]);
            }
            $user_id = $user->id;
            $format = 'd/m/Y H:i';
            //dd( $row['start_date_time']);
            $start_date_time = Carbon::createFromFormat($format, $row['start_date_time']);
            $end_date_time = Carbon::createFromFormat($format, $row['end_date_time']);

            $shift_minutes = $end_date_time->diffInMinutes($start_date_time);
            $park_id = $ride->park->id;

            $openDateFormat = 'd/m/Y';
            $open_date = Carbon::createFromFormat($openDateFormat, $row['open_date'])->toDateString();

            $park_id = $ride->park->id;
            $parkTime = ParkTime::query()->where('park_id', $park_id)->where('date', $open_date)->first();
            $shift = (int)$parkTime?->duration_time;
            if ($parkTime && $shift >= $shift_minutes) {
                UserLog::create([
                    'user_id' => $user_id,
                    'ride_id' => $row['ride_id'],
                    'park_id' => $park_id,
                    'date' => $start_date_time->toDateString(),
                    'time' => $start_date_time->toTimeString(),
                    'end_time' => $end_date_time->toDateTimeString(),
                    'open_date' => $open_date,
                    'type' => 'logout',
                    'shift_minutes' => $shift_minutes

                ]);
            }


        }
    }
}
