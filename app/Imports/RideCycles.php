<?php

namespace App\Imports;

use App\Models\Park;
use App\Models\GameTime;
use App\Models\ParkTime;
use App\Models\Ride;
use App\Models\RideStoppages;
use App\Models\StopageSubCategory;
use App\Models\User;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class RideCycles implements ToCollection,WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $ride = Ride::find($row['ride_id']);
            $operator = User::find($row['operator_id']);
            $park= Park::find($row['park_id']);

            if (is_null($ride)) {
                return throw ValidationException::withMessages(['ride' => 'Ride does not exist. Ride Name: ' . $row['ride_name']]);
            }
            if (is_null($operator)) {
                return throw ValidationException::withMessages(['Operator' => 'Operator does not exist. Operator Name: ' . $row['operator_name']]);
            }
            if (is_null($park)) {
                return throw ValidationException::withMessages(['Operator' => 'Park does not exist.Park Name: '.$row['park_name']]);
            }
            $park_time=ParkTime::where('park_id',$park->id)
                         ->where('date', date('Y-m-d', strtotime($row['opened_date'])))->first();
            if (is_null($park_time)) {
            return throw ValidationException::withMessages(['Time slot not found ,pleas add time slot first to park']);
            }            
            $gameTime = GameTime::where('ride_id', $ride->id)->where('park_time_id', $park_time->id)->first();
            $startDateTime = date('Y-m-d', strtotime($row['date'])) . ' ' . date('H:i:s', strtotime($row['time']));
            $ft_price=$ride->ride_price_ft;
            $ride_price=$ride->ride_price;
            $sale=$row['num_of_ft']?? 0 *$ft_price + $row['seats_filled']*$ride_price ;
            \App\Models\RideCycles::create([
                'ride_id' => $ride->id ?? 1,
                'zone_id' => $ride->zone_id ?? null,
                'user_id' => $operator->id?? null,
                'park_id'=>$park->id??null,
                'park_time_id'=>$park_time->id??null,
                'game_time_id'=>$gameTime->id??null,
                'opened_date' => date('Y-m-d', strtotime($row['opened_date'])),
                'start_time' => $startDateTime,
                "riders_count" => $row['seats_filled'],
                "number_of_ft" => $row['num_of_ft'] ?? 0,
                "number_of_vip" => $row['num_of_vip'] ?? 0,
                "number_of_disabled" => $row['num_of_disabled'] ?? 0,
                "duration_seconds" => $row['cycle_time_minute']*60 ,
                "sales" => $sale ?? 0
            ]);
        }

    }
}
