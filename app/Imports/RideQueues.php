<?php

namespace App\Imports;

use App\Models\Park;
use App\Models\ParkTime;
use App\Models\Ride;
use App\Models\Queue;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class RideQueues implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $ride = Ride::find($row['ride_id']);
            $operator = User::find($row['operator_id']);
            $park= Park::find($row['park_id']);
            if (is_null($ride)) {
                return throw ValidationException::withMessages(['ride' => 'Ride does not exist. Ride Name: ' . $row['ride_name']]);
            }
            if (is_null($operator)) {
                return throw ValidationException::withMessages(['Operator' => 'Operator does not exist. Operator Name: ' . $row['operator_name']]);
            }
            if (is_null($park)) {
                return throw ValidationException::withMessages(['Operator' => 'Park does not exist.Park Name: '.$row['park_name']]);
            }
            $park_time=ParkTime::where('park_id',$park->id)
            ->where('date', date('Y-m-d', strtotime($row['opened_date'])))->first();
            $startDateTime = date('Y-m-d', strtotime($row['date'])) . ' ' . date('H:i:s', strtotime($row['time']));

            Queue::create([
                'ride_id' => $ride->id ?? null,
                'user_id' => $operator->id ?? null,
                'park_time_id'=>$park_time->id??null,
                'opened_date' => date('Y-m-d', strtotime($row['opened_date'])),            
                'park_id' => $park->id ?? null,
                'zone_id' => $ride->zone_id ?? null,
                'start_time' => $startDateTime,
                "queue_minutes" => $row['queue_minutes'],
                "queue_seconds" => $row['queue_seconds'],

            ]);
        }

    }
}
