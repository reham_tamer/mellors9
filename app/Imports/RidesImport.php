<?php

namespace App\Imports;

use App\Models\Game;
use App\Models\GameCategory;
use App\Models\Park;
use App\Models\Ride;
use App\Models\RideZone;
use App\Models\Zone; 
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Collection;

class RidesImport implements ToCollection, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
     //   dd($rows);

        foreach ($rows as $row) {
            $park = Park::find($row['park_id']);
            $zone = Zone::find($row['zone_id']);
            if (is_null($park)) {
                throw ValidationException::withMessages([
                    'park' => 'park does not exist. park ID: ' . $row['park_id'],
                ]);
            }
            if (is_null($zone)) {
                throw ValidationException::withMessages([
                    'zone' => 'zone does not exist. zone ID: ' . $row['zone_id'],
                ]);
            }
            if (!empty($row['asset_number']) && Ride::where('asset_number', $row['asset_number'])->exists()) {
                throw ValidationException::withMessages([
                    'asset_number' => 'The asset number already exists: ' . $row['asset_number'],
                ]);
            }
            $ride = Ride::create([
                'name' => $row['game_name'],
                'capacity_one_cycle' => $row['capacity'],
                'game_cat_id' => $ride_cat_id->id ?? 1,
                'park_id' => $row['park_id'] ?? null,
                'zone_id' => $row['zone_id'] ?? null,
                'ride_type_id' => 12,
                'asset_number' => $row['asset_number'],
            ]);
        
            RideZone::create([
                'ride_id' => $ride->id,
                'zone_id' => $row['zone_id'],
            ]);
        }
    }
}
