<?php

namespace App\Imports;

use App\Models\Park;
use App\Models\Attendance;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class Attendances implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $park = Park::find($row['park_id']);
            if (is_null($park)) {
                throw ValidationException::withMessages([
                    'Park' => 'Park does not exist. Park ID: ' . $row['park_id'],
                ]);
            }

            $attendance = Attendance::firstOrNew(['code' => $row['code']]);

            $attendance->name = $row['name'] ?? $attendance->name;
            $attendance->last_name = $row['last_name'] ?? $attendance->last_name;
            $attendance->park_id = $row['park_id'] ?? $attendance->park_id;
            $attendance->phone = $row['phone'] ?? $attendance->phone;
            $attendance->national_id = $row['national_id'] ?? $attendance->national_id;
            $attendance->save();
        }
    }
}
