<?php

namespace App\Notifications;

use Illuminate\Broadcasting\Channel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Storage;

class SendRideCycleHourly extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $action;

    public function __construct()
    {
//        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {

        $path = asset('excels/cycles.xlsx');
        return (new MailMessage)
            ->subject('Report Of Rides Cycles')
            ->line(__('base.hello', ['username' => $notifiable->name]))
            ->view('emails.ride_cycle', ['path' => $path]);
    }

}
