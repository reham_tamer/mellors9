<?php

namespace App\Events\Api;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class StoppageApplication implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(public $id, public $title, public $ride_id)
    {
    }


    public function broadcastOn()
    {
        return new Channel('User.Notifications.' . $this->id);

    }

    public function broadcastAs()
    {
        return 'show-stoppage';
    }


    public function broadcastWith()
    {
        return [
            'data' => [
                'id' => $this->id,
                'action' => $this->title,
                'ride_id' => $this->ride_id,
            ],
        ];
    }


}
