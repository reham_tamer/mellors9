<?php

namespace App\Events\Api;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class QueueEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(public $id, public $title, public $action,public $ride_id)
    {
    }


    public function broadcastOn()
    {
        return new Channel('User.Notifications.' . $this->id);

    }

    public function broadcastAs()
    {
        return 'show-queue';
    }


    public function broadcastWith()
    {
        return [
            'data' => [
//                'id' => $this->id,
                'title' => $this->title,
                'action' => $this->action,
                'ride_id' => $this->ride_id,
            ],
        ];
    }


}
