<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class DailyCheckListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment' => 'nullable|array',
            'comment.*' => 'nullable',
            'ride_id' => 'nullable|array',
            'ride_id.*' => 'nullable|exists:rides,id',
            'additional_comment' => 'nullable',
            'check_list_question_id' => 'required|array',
            'check_list_question_id.*' => 'nullable|exists:check_list_questions,id',
            'answer' => 'nullable|array',
            'answer.*' => 'required',
            'signature' => 'required|image',
            'park_id' => 'required|exists:parks,id',
            'comment_ride' => 'nullable|array',
            'type'=>'required',
            'img_comment' => 'nullable|array',
            'images.*' => 'nullable',
        ];
    }
}
