<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class ObservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ride_id' => 'required|exists:rides,id',
            'date' => 'required',
            'snag' => 'nullable|string',
            'image' => 'nullable|image',
            'department_id' => 'required|exists:departments,id',
            'observation_category_id' => 'required|exists:observation_categories,id',
        ];
    }
}
