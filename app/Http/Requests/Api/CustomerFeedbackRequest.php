<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class CustomerFeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment' => 'required',
            'ride_id' => 'required|exists:rides,id',
            'date' => 'required',
            'type' => 'required',
            'image' => 'nullable|array',
            'image*' => 'nullable|array|mimes:jpeg,png,jpg,gif|image',
            'zone_id' => 'required|exists:zones,id',
            'park_id' => 'required|exists:parks,id',
            'complaint_id' => 'nullable|exists:customer_complaints,id',
            'feedback_subcategory_id' => 'nullable|exists:feedback_subcategory,id',
            'age' => 'nullable|numeric',
            'kind' => 'nullable|string',
            'client_involved_name' => 'nullable|string',
        ];
    }
}
