<?php

namespace App\Http\Requests\Dashboard\Ride;

use Illuminate\Foundation\Http\FormRequest;

class RideCycleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'ride_id' => 'required',
            'park_time_id' => 'required',
            'riders_count' => 'required',
            'number_of_vip' => 'nullable',
            'number_of_ft' => 'nullable',
            'number_of_disabled' => 'nullable',
            'duration_seconds' => 'required',
            'sales' => 'required',
            'start_time' => 'required',
        ];

        // If the request method is PATCH, override some rules
        if ($this->getMethod() == 'PATCH') {
            $rules = [
                'riders_count' => 'required',
                'duration_seconds' => 'required',
                'start_time' => 'required',
                'number_of_vip' => 'nullable',
                'number_of_ft' => 'nullable',
                'number_of_disabled' => 'nullable',
                'sales' => 'required',
            ];
        }

        return $rules;
    }

}
