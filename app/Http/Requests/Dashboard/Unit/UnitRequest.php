<?php

namespace App\Http\Requests\Dashboard\Unit;

use Illuminate\Foundation\Http\FormRequest;

class UnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules= [
            'unit_no' => 'required',
            'address' => 'required',
            'owner' => 'required',
            'city' => 'nullable',
        ];
        if ($this->getMethod() == 'PATCH') {
            $rules = [
                'name'=>'nullable',
                'address'=>'nullable',
                'owner'=>'nullable',
            ];
        }
        return $rules;

    }
}
