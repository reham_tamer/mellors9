<?php

namespace App\Http\Requests\Dashboard\Accident;

use Illuminate\Foundation\Http\FormRequest;

class EvacuationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'time' => 'required',
            'total_rider' => 'required',
            'first_rider_off' => 'nullable',
            'last_rider_off' => 'nullable',
            'abnormal' => 'nullable',
            'normal' => 'nullable',
            'longer_than_normal' => 'nullable',
            'medics_required' => 'nullable',
            'civil_defense_involved' => 'nullable',
            'vip_guest_involved' => 'nullable',
            'customer_service_issue' => 'nullable',
            'property_damage' => 'nullable',
            'first_aid' => 'nullable',
            'evacuation_details' => 'nullable',
            'customer_service_gesture' => 'nullable',
            'portal_overview' => 'nullable',
            'image' => 'nullable|image',
            'stoppage_id' => 'nullable|exists:ride_stoppages,id',
            'park_id' => 'nullable|exists:parks,id',
        ];

        return $rules;
    }
}
