<?php

namespace App\Http\Requests\Dashboard\Accident;

use Illuminate\Foundation\Http\FormRequest;

class IncidentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
            $rules = [
                'department_id'=>'required',
                'date'=>'required',
                'type_of_event'=>'required',
                'harm'=>'required',
                'name'=>'required',
                'position'=>'required',
                'address'=>'required',
                'phone'=>'nullable',
                'description'=>'nullable',
                'details'=>'nullable',
                'investigation'=>'required',
                'reportable'=>'required',
                'investigation_level'=>'required',
                'report_reference_no'=>'required',
                'ride_id'=>'nullable',
                'park_id'=>'required',
                'text'=>'nullable',
                'age'=>'nullable',
                'gender'=>'nullable',
                'area_of_injury'=>'required',
                'other_of_area_of_injury'=>'nullable',
                'book'=>'required',
                'signature'=>'nullable',
 //               'images'=>'nullable|array',
                'images.*' => 'nullable',
                'comment.*'=>'nullable',
                'zone_id'=>'nullable|exists:zones,id',
            ];

        return $rules;

    }
}
