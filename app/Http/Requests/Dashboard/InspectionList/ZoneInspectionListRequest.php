<?php

namespace App\Http\Requests\Dashboard\InspectionList;

use Illuminate\Foundation\Http\FormRequest;

class ZoneInspectionListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules= [
            'zone_id' => 'required',
            'inspection_list_id'=>'required'
        ];
        if ($this->getMethod() == 'PATCH') {
            $rules = [
                'zone_id'=>'nullable',
                'inspection_list_id'=>'nullable',

            ];
        }
        return $rules;

    }
}
