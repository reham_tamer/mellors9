<?php

namespace App\Http\Requests\Dashboard\Attendance;

use Illuminate\Foundation\Http\FormRequest;

class AttendanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|string',
            'last_name'=>'required|string',
            'national_id'=>'required',
            'code'=>'required|unique:attendances,code,'.$this->attendance?->id,
            'park_id'=>'required|exists:parks,id',
            //'ride_id'=>'array',
            'status'=>'required'

        ];
    }
}
