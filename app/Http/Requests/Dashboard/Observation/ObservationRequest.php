<?php

namespace App\Http\Requests\Dashboard\Observation;

use Illuminate\Foundation\Http\FormRequest;

class ObservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
        'ride_id'=>'required',
        'snag'=>'required',
        'date_reported'=>'required',
        'observation_category_id'=>'required',
        'department_id'=>'required', 
        'image'=>'nullable',
    ];
    if ($this->getMethod() == 'PATCH') {
        $rules = [
            'date_resolved'=>'required',
            'maintenance_feedback'=>'required',
            'rf_number'=>'nullable',
            'reported_on_tech_sheet'=>'nullable',
            'date_reported'=>'nullable',
            'snag'=>'nullable',
            'observation_category_id'=>'nullable',
            'department_id'=>'nullable',

        ];
    }

        return $rules;

    }
}
