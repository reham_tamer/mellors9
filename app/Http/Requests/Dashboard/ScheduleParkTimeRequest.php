<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class ScheduleParkTimeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'park_id' => 'nullable',
            'start_date' => 'required|after:today',
            'start' => 'nullable',
            'end' => $this->start_date < $this->end_date ? 'nullable' : 'nullable|after:start',
            'end_date' => 'required|after:start_date',
        ];


        return $rules;

    }
}
