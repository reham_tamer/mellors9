<?php

namespace App\Http\Requests\Dashboard\User;

use Illuminate\Foundation\Http\FormRequest;

class OperatorTimeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'park_id' => 'required|exists:parks,id',
            'ride_id' => 'required|exists:rides,id',
            'open_date' => 'required|date',
            'date_time_start' => 'required', 
            'date_time_end' => 'required|after:date_time_start',
            'user_id'=>'required'
        ];
    }
}
