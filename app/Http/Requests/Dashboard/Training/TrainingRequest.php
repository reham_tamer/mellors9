<?php

namespace App\Http\Requests\Dashboard\Training;

use Illuminate\Foundation\Http\FormRequest;

class TrainingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
        public function rules()
        {
            return [
                'start_date' => 'required|date',
                'end_date' => 'nullable|date',
                'park_id' => 'required|exists:parks,id',
                'ride_id' => 'nullable|exists:rides,id',
                'user_type' => 'required',
                'training_checklist_id' => 'required',
                'user_ids' => 'required|array',  
                'manager_id' => 'required|exists:users,id',  
                'supervisor_id' => 'required|exists:users,id'  
            ];
        
        

        return $rules;
    }
}
