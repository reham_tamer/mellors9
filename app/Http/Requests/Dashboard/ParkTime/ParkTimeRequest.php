<?php

namespace App\Http\Requests\Dashboard\ParkTime;

use Illuminate\Foundation\Http\FormRequest;

class ParkTimeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
// dd($this->date < $this->close_date );
        $rules = [
            'park_id' => 'nullable',
            'date' => 'nullable',
            'start' => 'nullable',
            'end' => $this->date < $this->close_date ? 'nullable' : 'nullable|after:start',
//            'end' => 'required',
            'daily_entrance_count' => 'nullable',
            'close_date' => 'required|after_or_equal:date',

        ];


        return $rules;

    }
}
