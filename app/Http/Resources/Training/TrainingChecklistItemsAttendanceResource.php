<?php

namespace App\Http\Resources\Training;

use App\Models\Attendance;
use App\Models\TrainingUserChecklist;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class TrainingChecklistItemsAttendanceResource extends JsonResource
{

    public function toArray($request)
    {
        $attendance = Attendance::where('code',request()->code)->first();
        $checkDate = TrainingUserChecklist::where('userable_type','App\Models\Attendance')->where('userable_id',$attendance->id)
            ->where('training_id',request()->training?->id)->where('training_checklist_item_id',$this->id)->first();
        return [
            'id' => $this->id,
            'name_en' => $this->name_en,
            'name_ar' => $this->name_ar,
            'date' => $checkDate?->date,
            'trainee_signature' => $checkDate?->trainee_signature? Storage::disk('s3')->temporaryUrl('images/'.baseName($checkDate?->trainee_signature),now()->addWeek())  :  null,

        ];
    }
}
