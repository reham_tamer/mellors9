<?php

namespace App\Http\Resources\Training\TrainingAdmin;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class TrainingAdminAdditionResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'date' => $this->date,
            'trainee_signature' => $this->trainee_signature  ? Storage::disk('s3')->temporaryUrl('images/'.baseName($this->trainee_signature),now()->addWeek()) : null,
            'trainer_comment' => $this->trainer_comment,
            'addition' => $this->addition,
        ];
    }
}
