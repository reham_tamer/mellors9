<?php

namespace App\Http\Resources\Training\TrainingAdmin;
use Illuminate\Http\Resources\Json\JsonResource;

class TrainingAdminChecklistTypeResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'training_id' => $this->training_id,
            'user_type' => $this->userable_type == 'App\Models\User' ? 'operator' : 'attendance' ,
            'user_id' => $this->userable_id,
            'checklist_type' => TrainingAdminChecklistResource::collection($this->training->checklist->items->unique('type')),
            'additions' => TrainingAdminAdditionResource::collection($this->additions->where('userable_id',$this->userable_id)->where('userable_type',$this->userable_type))
        ];
    }
}
