<?php

namespace App\Http\Resources\Training\TrainingAdmin;

use App\Models\TrainingUserChecklist;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class TrainingAdminChecklistItemsResource extends JsonResource
{

    public function toArray($request)
    {
        $checkDate = TrainingUserChecklist::where('userable_type',request()->training->userable_type)
            ->where('userable_id',request()->training->userable_id)
            ->where('training_id',request()->training?->training_id)
            ->where('training_checklist_item_id',$this->id)
            ->first();
        return [
            'id' => $this->id,
            'name_en' => $this->name_en,
            'name_ar' => $this->name_ar,
            'date' => $checkDate?->date,
            'trainee_signature' => $checkDate?->trainee_signature? Storage::disk('s3')->url('images/' .$checkDate?->trainee_signature)  :  null,
//            'trainee_signature' => $checkDate?->trainee_signature? Storage::disk('s3')->url('images/'.baseName($checkDate?->trainee_signature))  :  null,

            'trainer_comment' => $checkDate?->trainer_comment

        ];
    }
}
