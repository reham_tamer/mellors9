<?php

namespace App\Http\Resources\Training\TrainingAdmin;

use Illuminate\Http\Resources\Json\JsonResource;

class TrainingAdminUserResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_name' => $this->userable_type == 'App\Models\User' ? $this->operator->name : $this->attendance?->name,
            'user_type' => $this->userable_type == 'App\Models\User' ? 'operator' : 'attendance',
            'is_user_signature' => (bool)$this->user_signature
        ];
    }
}
