<?php

namespace App\Http\Resources\Training\EvacuationTraining;

use App\Models\TrainingUserChecklist;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class TrainingAdminChecklistItemsResource extends JsonResource
{
    public function toArray($request)
    {
        $checkDate = TrainingUserChecklist::where('userable_type', request()->training->userable_type)
            ->where('userable_id', request()->training->userable_id)
            ->where('training_id', request()->training?->training_id)
            ->where('training_checklist_item_id', $this->id)
            ->first();
        return [
            'id' => $this->id,
            'name_en' => $this->name_en,
            'name_ar' => $this->name_ar,
            'checklist_date' => $checkDate?->date,
            'checklist_trainee_signature' => $checkDate ? Storage::disk('s3')->temporaryUrl('images/'.baseName($checkDate->trainee_signature),now()->addWeek())  : null,
            'checklist_trainer_signature' => $checkDate ?  Storage::disk('s3')->temporaryUrl('images/'.baseName($checkDate->trainer_signature),now()->addWeek())  : null,
        ];
    }
}
