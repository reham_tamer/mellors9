<?php

namespace App\Http\Resources\Training\EvacuationTraining;

use App\Models\TrainingUserRide;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class TrainingRidesResource extends JsonResource
{

    public function toArray($request)
    {
        $training = TrainingUserRide::query()->where('attendance_id', request()->training->userable_id)
            ->where('training_id',request()->training->training_id)
            ->where('ride_id', $this->id)->first();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'ride_trainee_signature' => $training ?  Storage::disk('s3')->temporaryUrl('images/'.baseName($training?->trainee_signature),now()->addWeek())   :null,
            'ride_trainer_signature' => $training ?  Storage::disk('s3')->temporaryUrl('images/'.baseName($training?->trainer_signature),now()->addWeek())   : null,
            'ride_date' => $training?->date,
        ];
    }
}
