<?php

namespace App\Http\Resources\Training\EvacuationTraining;

use Illuminate\Http\Resources\Json\JsonResource;

class TrainingResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'park' => $this->park?->name,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'checklist' => $this->checklist?->name,
        ];
    }
}
