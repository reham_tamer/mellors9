<?php

namespace App\Http\Resources\Training\EvacuationTraining;

use App\Models\Attendance;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class TrainingAdminChecklistTypeResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'training_id' => $this->training_id,
            'user_type' => $this->userable_type == 'App\Models\User' ? 'operator' : 'attendance',
            'attendance_id' => $this->userable_id,
            'attendance_signature' => $this->user_signature ? Storage::disk('s3')->temporaryUrl('images/' . baseName($this->user_signature), now()->addWeek()) : null,
            'user_name' => Attendance::find($this->userable_id)?->name,
            'checklist_type' => TrainingAdminChecklistResource::collection($this->training->checklist->items->unique('type')),
            'rides' => TrainingRidesResource::collection($this->training->park->rides),
        ];
    }
}
