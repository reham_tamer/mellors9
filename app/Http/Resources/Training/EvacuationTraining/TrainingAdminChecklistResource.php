<?php

namespace App\Http\Resources\Training\EvacuationTraining;

use App\Models\TrainingChecklistItem;
use Illuminate\Http\Resources\Json\JsonResource;

class TrainingAdminChecklistResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'type' => $this->type,
            'checklist_items' => TrainingAdminChecklistItemsResource::collection(TrainingChecklistItem::where('type', $this->type)->where('training_checklist_id', $this->training_checklist_id)->get())
        ];
    }
}
