<?php

namespace App\Http\Resources\Training;

use App\Http\Resources\Training\TrainingAdmin\TrainingAdminAdditionResource;
use App\Models\Attendance;
use App\Models\RideInspectionList;
use App\Models\TrainingAddition;
use App\Models\TrainingUserChecklist;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Cache;

class TrainingShowResource extends JsonResource
{

    public function toArray($request)
    {
        $ckecklist = Cache::remember('preopening' . $this->id, 60, function ()  { //week
            // Fetch the collection from the database if not cached
            return TrainingChecklistResource::collection($this->checklist->items->unique('type'));
        });
        if(request()->code ){
            $attendance = Attendance::where('code',request()->code)->first();

            $query = TrainingAddition::where('training_id',$this->id)->where('userable_type','App\Models\Attendance')->where('userable_id',$attendance->id)->get();
        }else{
            $query = TrainingAddition::where('training_id',$this->id)->where('userable_type','App\Models\User')->where('userable_id',auth()->id())->get();

        }
        return [
            'id' => $this->id,
            'ride' => $this->ride?->name,
            'park' => $this->park?->name,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'checklist' => $this->checklist?->name,
            'checklist_type' => $ckecklist,
            'additions' => TrainingAdminAdditionResource::collection($query)

        ];
    }
}
