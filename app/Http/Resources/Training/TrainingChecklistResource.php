<?php

namespace App\Http\Resources\Training;

use App\Models\TrainingChecklistItem;
use Illuminate\Http\Resources\Json\JsonResource;

class TrainingChecklistResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'type' => $this->type,
            'checklist_items' => request()->code ? TrainingChecklistItemsAttendanceResource::collection(TrainingChecklistItem::where('type', $this->type)->where('training_checklist_id', $this->training_checklist_id)->get()) :
                TrainingChecklistItemsOperatorResource::collection(TrainingChecklistItem::where('type', $this->type)->where('training_checklist_id', $this->training_checklist_id)->get())
        ];
    }
}
