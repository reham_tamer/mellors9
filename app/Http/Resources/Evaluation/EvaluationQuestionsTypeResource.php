<?php

namespace App\Http\Resources\Evaluation;

use App\Models\EvaluationQuestion;
use Illuminate\Http\Resources\Json\JsonResource;

class EvaluationQuestionsTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [

            'question_title' => $this->type,
            'questions' => EvaluationQuestionsResource::collection(EvaluationQuestion::where('evaluation_to', 'operator')->where('type', $this->type)->get())

        ];
    }
}
