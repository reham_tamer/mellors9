<?php

namespace App\Http\Resources\User;

use App\Http\Resources\User\ParkResource;
use App\Http\Resources\User\Ride\QueueResource;
use App\Http\Resources\User\ZoneResource;
use App\Models\RideStoppages;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class AttendanceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */

    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'name' => $this->attendance?->name,
            'code' => $this->attendance?->code,
            'park' => ParkResource::make($this->park),
            'status' => $this->type == 'login' ?  'active': 'inactive',
        ];


        return $data;
    }

}
