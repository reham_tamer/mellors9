<?php

namespace App\Http\Resources\User;


use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class ParksWithoutTimeTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => $this->type, 
            'count' => $this->park->zonesLists?->where('type', $this->type)
                ->filter(function ($zone) {
                    return Carbon::parse($zone->opened_date)->toDateString() === Carbon::today()->toDateString();
                })
                ->count(),
               
                ];
    }
}
