<?php

namespace App\Http\Resources\User\Ride;

use Illuminate\Http\Resources\Json\JsonResource;

class PreopeningListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'inspection_list' => InspectionResource::make($this->inspection_list),
            'status' =>$this->status,
            'is_checked' =>$this->is_checked,
            'comment' =>$this->comment,
            'zone_id' =>$this->info?->zone_id,
            'park_id' =>$this->info?->park_id,
            'opened_date' =>$this->info?->opened_date,
            'lists_type' =>$this->info?->type,
            'date_time' =>$this->created_at,

        ];
    }
}
