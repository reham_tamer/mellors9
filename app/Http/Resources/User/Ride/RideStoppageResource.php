<?php

namespace App\Http\Resources\User\Ride;

use App\Http\Resources\User\ParkResource;
use App\Http\Resources\User\ZoneResource;
use App\Models\Evacuation;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class RideStoppageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $status = false;
        if($this->stopage_sub_category_id == 12 || $this->stopage_sub_category_id== 13){
            $status = true;
        }

        $evac = Evacuation::where('ride_id', $this->id)
                            ->where('park_time_id', $this->park_time_id)
                            ->first();
        $hasEvac = (bool)$evac;

        $data = [
            'id' => $this->id,
            'ride_status' => $this->ride_status,
            'stopage_sub_category_id' => $this->stopage_sub_category_id,
            'stopage_sub_category_name' => $this->stopageSubCategory?->name,
            'date' => $this->date,
            'opened_date' => $this->opened_date,
            'type' => $this->type,
            'description' => $this->description,
            'stoppage_time' => $this->down_minutes,
//            'ride_notes' => $this->ride_notes,
            'time_slot_start' => $this->time_slot_start,
            'stopage_category_id' => $this->stopage_category_id,
            'stopage_category_name' => $this->stopageCategory?->name,
            'date_time' => $this->date .' '.$this->time_slot_start,
            'make_evacuation' => $status,
            'hasEvac' => $hasEvac,
            'ride_type' => RideTypeResource::make($this->ride?->ride_type),


        ];

         return $data;
    }
}
