<?php

namespace App\Http\Resources\User\Ride;

use App\Models\Ride;
use Illuminate\Http\Resources\Json\JsonResource;

class RideCycleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $ride = Ride::find($this->ride_id);
        return [
            'id' => $this->id,
            'ride_id' => $this->ride_id,
            'park_time_id' => $this->park_time_id,
            'number_of_vip' => $this->number_of_vip,
            'number_of_ft' => $this->number_of_ft ?? 0,
            'number_of_disabled' => $this->number_of_disabled ?? 0,
            'riders_count' => $this->riders_count,
            'duration_seconds' => $this->duration_seconds,
            'pressure_time' => $this->pressure_time,
            'cycle_count' => $ride->cycle()
                ->where(function ($query) {
                    $query->whereBetween('start_time', [ParkTime($this->park_id)?->date, ParkTime($this->park_id)?->close_date])
                        ->orWhereDate('start_time', ParkTime($this->park_id)?->date);
                })->count()
        ];


    }
}
