<?php

namespace App\Http\Resources\User\Ride;

use App\Http\Resources\User\ParkResource;
use App\Models\GameTime;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;
use App\Http\Resources\User\ZoneResource;

class RideMainPageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */

    public function toArray($request)
    {
        if ($this->rideStoppages?->last()?->type == 'all_day' && $this->rideStoppages?->last()->ride_status == 'stopped') {
            $status = 'Closed';
        } elseif ($this->rideStoppages?->last()?->ride_status == 'stopped') {
            $status = 'Stopped';
        } else {
            $status = 'Active';
        }
        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'status' => $status,
            'zone' => ZoneResource::make($this->zone),

        ];

//        $stoppageNewDate = $this->rideStoppages?->where( 'ride_status', 'stopped' )->first();
//        $stoppageLastDate = $this->rideStoppages?->where( 'ride_status', 'stopped' )->last();
        $rideTime = $this->rideStoppages?->where('park_time_id', ParkTime($this->park_id)?->id);
        $lastStoppage = $this->rideStoppages->last();
        $preopining = $this->preopening_lists?->where('type', 'preopening')?->whereBetween('opened_date', [ParkTime($this->park_id)?->date, ParkTime($this->park_id)?->close_date])?->first();

        if ($rideTime->isNotEmpty() && $rideTime?->where('stopage_category_id', 5)->first() != null && $lastStoppage?->ride_status == 'active') {
            $data['color'] = 'yellow';
        } elseif (!$preopining) {
            $data['color'] = 'gray';
        } elseif ($lastStoppage?->ride_status == 'active') {
            $data['color'] = 'green';
        } elseif ($lastStoppage?->type == 'time_slot' && $lastStoppage?->ride_status == 'stopped' || $lastStoppage?->ride_status == 'closed' && $lastStoppage?->ride_status == 'time_slot') {
            $data['color'] = 'red';
        } elseif ($lastStoppage?->ride_status == 'stopped' && $lastStoppage?->type == 'all_day' || $lastStoppage?->ride_status == 'closed' && $lastStoppage?->type == 'all_day') {
            $data['color'] = 'red';
        } else {
            $data['color'] = 'green';
        }


        $queues = $this->queue
            ? $this->queue()->where('park_time_id', ParkTime($this->park_id)?->id)->latest()
                ->first()
            : null;
        $data['queues'] = QueueResource::make($queues);

//        if ( $stoppageLastDate && $stoppageLastDate?->date < dateTime()?->date && dateTime() != null ) {
//            addNewDateStappage( $stoppageNewDate, $this );
//        }
        $gametime = GameTime::where('ride_id', $this->id)->where('park_time_id', ParkTime($this->park_id)?->id)
            ->first();

        $data['seat_availability'] = $this->number_of_seats > 0 ? round($gametime?->no_of_seats / $this->number_of_seats, 1) * 100 . '%' : 0 . '%';
        $data['ride_type'] = RideTypeResource::make($this->ride_type);

        return $data;
    }

}
