<?php

namespace App\Http\Resources\User\Ride;

use App\Http\Resources\User\ParkResource;
use App\Http\Resources\User\UserResource;
use App\Http\Resources\User\ZoneResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class RideInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'capacity_one_cycle' => (int) (($this->capacity?->final_capacity) ?? $this->times?->first()?->no_of_seats),
            'one_cycle_duration_seconds' => $this->one_cycle_duration_seconds,
            'ride_cycle_mins' => $this->ride_cycle_mins,
            'ride_price' => $this->ride_price,
            'number_of_seats' => (int) (($this->capacity?->final_capacity) ?? $this->times?->first()?->no_of_seats),
            'theoretical_number' => $this->theoretical_number,
            'ride_price_ft' => $this->ride_price_ft,
            'minimum_height_requirement' => $this->minimum_height_requirement,
            'ride_cat' => $this->ride_cat,
            'no_of_gondolas' => (int) (($this->times?->first()?->no_of_gondolas) ?? $this->no_of_gondolas),
            'ride_type' => RideTypeResource::make($this->ride_type),
            'zone' => ZoneResource::make($this->zone),
            'park' => ParkResource::make($this->park),
            'status' => $this->rideStoppages?->last()->ride_status ?? 'active',
            'status_type' => $this->rideStoppages?->last()?->type,

        ];
        $queues = $this->queue
            ? $this->queue()?->where('park_time_id',ParkTime($this->park_id)?->id)->latest()
                ->first()
            : null;


        $riders = $this->cycle
            ? $this->cycle()
//                ->where('duration_seconds', '!=', 0)
                ->where('park_time_id',ParkTime($this->park_id)?->id)
            : null;

        $cycles = $this->cycle
            ? $this->cycle()
                ->where('park_time_id',ParkTime($this->park_id)?->id)
            : null;
        $seats = $this->finalCapacity
            ? $this->finalCapacity()
                ->where(function ($query) {
                    $query->whereBetween('date', [ParkTime($this->park_id)?->date, ParkTime($this->park_id)?->close_date]);
//                        ->orWhereDate('date', dateTime()?->date);
                })
            : null;
        $user = User::where('id',$this->current_user_id)->first();
//        if (!$user){
//            $user = $this->users()?->whereHas('roles', function ($query) {
//                return $query->whereIn('name', ['Operations/Operator']);
//            })->latest()->first();
//        }
        $preopining = $this->preopening_lists?->where('type', 'preopening')?->whereBetween('opened_date', [ParkTime($this->park_id)?->date, ParkTime($this->park_id)?->close_date])?->first();
      //  $inspaction = true ;
        $inspaction =
           $preopining ? $preopining?->lists?->pluck('is_checked')?->toArray() : [];
        if ($inspaction != null && in_array(null, $inspaction) || in_array('no', $inspaction)) {
            $inspaction = false;
        } elseif (empty($inspaction)) {
            $inspaction = false;
        } else {
           $inspaction = true;
        }


        $closing = $this->preopening_lists?->where('type', 'preclosing')?->whereBetween('opened_date', [ParkTime($this->park_id)?->date, ParkTime($this->park_id)?->close_date])?->first();
        $preclosing = $closing ? $closing->lists?->pluck('is_checked')?->toArray() : [];

        if (in_array(null, $preclosing) || in_array('no', $preclosing)) {
            $preclosing = false;
        } elseif (empty($preclosing)) {
            $preclosing = false;
        } else {
            $preclosing = true;
        }


        $allStopagges = $this->rideStoppages()?->where('park_time_id',ParkTime($this->park_id)?->id)?->where('ride_status', 'stopped');
        $stoppageNewDate = $allStopagges?->first();
        $stoppageLastDate = $allStopagges?->latest()->first();


        if ($stoppageLastDate && $stoppageLastDate?->stoppage_status == 'pending' && $stoppageLastDate?->parent_id == null && $allStopagges->count() == 1) {
            $stoppageStartTime = Carbon::now();
            $date = Carbon::now()->toDateString();
            $stoppageParkTimeEnd = Carbon::parse("$date $stoppageLastDate->time_slot_start");
            $down_minutes = $stoppageParkTimeEnd->diffInMinutes($stoppageStartTime);


        } else {

            $down_minutes = $this->rideStoppages()?->where('park_time_id',ParkTime($this->park_id)?->id)->sum('down_minutes');
        }

        $data['queues'] = QueueResource::make($queues);
        $data['last_cycle_time'] = $cycles?->get()?->last()? $cycles?->get()?->last()?->start_time : null;
        $data['last_cycle_duration'] = $cycles?->get()?->last()? $cycles?->get()?->last()?->duration_seconds : null;

        $data['queues_count'] = $this->queues()?->where('park_time_id',ParkTime($this->park_id)?->id)->count();

        $data['total_riders'] = $riders?->sum('number_of_vip') + $riders?->sum('number_of_disabled') + $riders?->sum('riders_count') + $riders?->sum('number_of_ft');
        $data['stoppage_minutes'] = $down_minutes;
        $data['stoppage_count'] = $this->rideStoppages()?->where('park_time_id',ParkTime($this->park_id)?->id)?->count();
        $data['cycle_count'] = $cycles?->count();
        $data['user'] = UserResource::make($user);
        $data['isPreopeningChecked'] = $inspaction;
        $data['isPreclosingChecked'] = $preclosing;
       // $data['isPreopeningRequested'] = true ;
        $data['isPreopeningRequested'] = $preopining != null;
        $data['isPreclosingRequested'] = $closing != null;
        $data['final_capacity'] = $seats?->orderBy('id','desc')?->first()?->final_capacity != null ? true : false;
        $data['pressure_time'] = $cycles?->get()?->last()? $cycles?->get()?->last()?->pressure_time : null;
        $data['ride_status_number'] = $cycles?->get()?->last()? $cycles?->get()?->last()?->number_status : null ;
        $data['stoppage_type'] = $stoppageLastDate?->type ?? null;




        if ($stoppageLastDate && $stoppageLastDate?->date < ParkTime($this->park_id)?->date && ParkTime($this->park_id) != null) {
            addNewDateStappage($stoppageNewDate, $this);
        }
        return $data;
    }

}
