<?php

namespace App\Http\Resources\User\Ride;

use App\Models\ParkTime;
use App\Models\RideCapacity;
use Illuminate\Http\Resources\Json\JsonResource;

class RideCapacityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $time = ParkTime::where('park_id', $this->park_id)->latest()->first();
        $first = RideCapacity::where('ride_id', $this->ride_id)->where(function ($query) use($time){
            $query->whereBetween('date', [$time?->date, $time?->close_date]);
        })->first();
        return [
            'id' => $this->id,
            'date' => $this->date,
            'ride_id ' => $this->ride_id ,
            'ride_availablity_capacity' => $first?->ride_availablity_capacity,
            'preopening_capacity' => $this->preopening_capacity,
            'final_capacity' => $this->final_capacity,
        ];
    }
}
