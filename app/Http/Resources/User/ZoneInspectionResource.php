<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class ZoneInspectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->inspection_list?->id,
            'name' => $this->inspection_list?->name,
            'comment' =>$this->comment,
            'status' =>$this->status,
            'is_checked' => $this->is_checked == 1,
        ];
    }
}
