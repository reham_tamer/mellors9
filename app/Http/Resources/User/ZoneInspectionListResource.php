<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class ZoneInspectionListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
         return [


            'type' => $this?->type,
            'park' => ParkResource::make($this->park),
            'zone' => ZoneResource::make($this?->zone),
            'image' => $this?->image,
            'date' => $this?->opened_date,
            'inspection' => ZoneInspectionResource::collection($this->lists)

        ];
    }
}
