<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class ZoneListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->zone_id,
            'name' => $this->zone?->name,
            'type' => ParksWithoutTimeTypeResource::collection(
                     $this->park?->zonesLists()
                    ->where('zone_id', $this->zone?->id)
                    ->whereDate('opened_date', Carbon::now()->toDateString())
                    ->get()
                    ->unique('type')
            ),
            'daily_lists' => DailyListResource::collection($this->park->zonesLists?->where('zone_id', $this->zone?->id)
                             ->where('type','daily')
                            ->filter(function ($zone) {
                             return Carbon::parse($zone->opened_date)->toDateString() === Carbon::today()->toDateString();
            }))
              ,
        ];
    }
}
