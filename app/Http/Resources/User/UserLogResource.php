<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name' => $this->user?->name,
            'ride' => $this->ride?->name,
            'park' => $this->park?->name,
            'shift_minutes' => $this->shift_minutes,
            'login_time' => $this->time,
        ];
    }
}
