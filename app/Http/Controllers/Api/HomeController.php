<?php

namespace App\Http\Controllers\Api;

use App\Events\Api\showNotification;
use App\Http\Controllers\Controller;
use App\Http\Resources\User\InspectionResource;
use App\Http\Resources\User\ParksWithoutTimeResource;
use App\Http\Resources\User\TimeResource;
use App\Http\Resources\User\ZoneInspectionListResource;
use App\Http\Resources\User\ZoneInspectionResource;
use App\Models\Park;
use App\Models\ParkTime;
use App\Models\ZoneInspectionInfo;
use App\Models\ZoneInspectionList;
use App\Traits\Api\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Pusher\PusherException;

class HomeController extends Controller
{
    use ApiResponse;

    protected function home()
    {
        if (auth()->user()->hasRole('Super Admin') || auth()->user()->hasRole('Client')) {
            $parks = Park::pluck('id');
        } else {
            $parks = auth()->user()->parks->pluck('id');
        }
        $currentDate = Carbon::now()->toDateString();
        $currentTime = Carbon::now()->format('H:i');

        $times = ParkTime::whereIn('park_id', $parks)
            ->where(function ($query) use ($currentDate, $currentTime) {
                $query->where('date', $currentDate)->orWhere('close_date', $currentDate)->where('end', '>=', $currentTime);
            })
            ->get();

        if (auth()->user()->hasRole('Park Admin') || auth()->user()->hasRole('Operations/Zone supervisor')) {
            $park_time = ParkTime::whereIn('park_id', $parks)->orderBy('id', 'DESC')
                ->where(function ($query) {
                    $query->where('date', Carbon::now()->toDateString())
                        ->orWhere('date', Carbon::now()->subDay()->toDateString());
                })
                ->get()->groupBy('park_id');
            $times = [];
            foreach ($park_time as $group) {
                $times[] = $group[0];
            }
        }

        $zones = ZoneInspectionInfo::query()->whereIn('park_id', $parks)->where('opened_date', Carbon::now()->toDateString())
            ->pluck('park_id')->toArray();

        $parks = Park::whereIn('id', $zones)->get();
//        dd($parks);

        $this->body['times'] = TimeResource::collection($times);
        $this->body['without_times'] = ParksWithoutTimeResource::collection($parks);

        return self::apiResponse(200, __('home page'), $this->body);
    }

    protected function zoneInspectionInfo(Request $request)
    {
        $request->validate([
            'zone_id' => 'required|exists:zones,id',
            'type' => 'required',
            'daily_id'=>'nullable'
        ]);
        $inspects = ZoneInspectionInfo::where('zone_id', $request->zone_id)->where('type', $request->type)
            ->where('opened_date', Carbon::now()->toDateString())
            ->get();
            if ($request->daily_id) {
                $list = ZoneInspectionInfo::find($request->daily_id);
               // dd( $list);
               $this->body['zone_inspection_info'] = ZoneInspectionResource::collection($list->lists ?? []);
               return self::apiResponse(200, __('inspections'), $this->body);
            }

        $this->body['zone_inspection_info'] = ZoneInspectionListResource::collection($inspects ?? []);
        return self::apiResponse(200, __('inspections'), $this->body);
    }

    /**
     * @throws PusherException
     * @throws \Exception
     */
    protected function event()
    {
        $data = [
            'title' => 'tttttttttt',
            'ride_id' => '1',
            'user_id' => Auth::user()->id,
        ];
        event(new showNotification($data['user_id'], 'event_queue', Carbon::now(), dateTime()?->id, 1));

        //        Notification::send(Auth::user(), new TestNotifications($data));
        //
        //
        //        $beamsClient = new \Pusher\PushNotifications\PushNotifications(array(
        //            "instanceId" => "5ff5290c-319d-46b8-b077-84ad0350650c",
        //            "secretKey" => "4080E12EBBEC3A3914D4E1939E52C5E6BBD6DA8BBC3F72481B7F3AD8209A9A61",
        //        ));
        //
        //
        //        $publishResponse = $beamsClient->publishToInterests(
        ////            array("mellors"),
        //            array("mellors",'notification-'.auth()->user()->id), // we pass the user id here
        //            array(
        //                "fcm" => array(
        //                    "notification" => array(
        //                        "title" => "Hi!1",
        //                        "body" => 'test'
        //                    )
        //                ),
        //                "apns" => array("aps" => array(
        //                    "alert" => array(
        //                        "title" => "Hi!",
        //                        "body" => 'test'
        //                    )
        //                )),
        //                "web" => array(
        //                    "notification" => array(
        //                        "title" => "Hi!",
        //                        "body" => 'test'
        //                    )
        //                )
        //            ));
        //        return  $publishResponse;

        return self::apiResponse(200, __('event send successfully'), []);
    }

    /**
     * @throws \Exception
     */
    protected function event2(Request $request)
    {
        //        dd($request->user);
        $data = [
            'title' => 'tttttttttt',
            'ride_id' => '1',
            'user_id' => Auth::user()->id,
        ];

        $beamsClient = new \Pusher\PushNotifications\PushNotifications([
            'instanceId' => '5ff5290c-319d-46b8-b077-84ad0350650c',
            'secretKey' => '4080E12EBBEC3A3914D4E1939E52C5E6BBD6DA8BBC3F72481B7F3AD8209A9A61',
        ]);

        $publishResponse = $beamsClient->publishToUsers(
            [$request->user],
            [
                'fcm' => [
                    'notification' => [
                        'title' => 'Hi!',
                        'body' => 'test',
                    ],
                ],
                'apns' => [
                    'aps' => [
                        'alert' => [
                            'title' => 'Hi!!',
                            'body' => 'test',
                        ],
                    ],
                ],
                'web' => [
                    'notification' => [
                        'title' => 'Hi!!!!',
                        'body' => 'test',
                    ],
                ],
            ],
        );
        return $publishResponse;

        return self::apiResponse(200, __('event send successfully'), []);
    }
}
