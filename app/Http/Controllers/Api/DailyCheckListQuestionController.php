<?php

namespace App\Http\Controllers\Api;

use App\Events\Api\showNotification;
use App\Events\Api\StoppageApplication;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CustomerFeedbackRequest;
use App\Http\Requests\Api\DailyCheckListRequest;
use App\Http\Requests\Api\ObservationRequest;
use App\Http\Requests\Api\UpdateInspectionsRequest;
use App\Http\Resources\ComplaintResource;
use App\Http\Resources\User\Ride\ObservationCategoryResource;
use App\Http\Resources\User\Ride\PreopeningListResource;
use App\Http\Resources\User\Ride\QuestionsChecklistResource;
use App\Http\Resources\User\Ride\QuestionsResource;
use App\Http\Resources\User\Ride\RideCapacityResource;
use App\Http\Resources\User\Zone\ZoneResource;
use App\Models\Attraction;
use App\Models\AttractionInfo;
use App\Models\CheckListQuestion;
use App\Models\CustomerComplaint;
use App\Models\CustomerFeedbackImage;
use App\Models\CustomerFeedbacks;
use App\Models\DailyOperational;
use App\Models\DailyOperationalInfo;
use App\Models\DailyOperationalRide;
use App\Models\Department;
use App\Models\GameTime;
use App\Models\GeneralQuestion;
use App\Models\Observation;
use App\Models\ObservationCategory;
use App\Models\ParkTime;
use App\Models\PreopeningList;
use App\Models\Ride;
use App\Models\RideCapacity; 
use App\Models\User;
use App\Models\DailyOperationalImage;
use App\Notifications\StoppageNotifications;
use App\Notifications\UserNotifications;
use App\Traits\Api\ApiResponse;
use App\Traits\Api\NotificationApi;
use App\Traits\ImageOperations;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

class DailyCheckListQuestionController extends Controller
{
    use ApiResponse,ImageOperations;


    protected function storeDailyCheckList(DailyCheckListRequest $request)
    {
        $validate = $request->validated();
        $validate['user_id'] = \auth()->id();
        $validate['date'] = Carbon::now();
        if (!empty($validate['signature'])) {
            $path = Storage::disk('s3')->put('images', $validate['signature']);
            $validate['signature'] = $path;
        }
        $dailyOperational = DailyOperational::query()->create($validate);
        foreach ($validate['check_list_question_id'] as $key => $question) {
            DailyOperationalInfo::query()->create([
                'daily_operational_id' => $dailyOperational->id,
                'check_list_question_id' => $question,
                'answer' => $validate['answer'][$key] ?? 0,
                'comment' => $validate['comment'][$key] ?? null,
            ]);
        }
        if (isset($validate['ride_id'])) {

            foreach ($validate['ride_id'] as $k => $ride) {
                foreach ($ride as $key => $item) {
                    if ($item === null) {
                        continue;
                    }
                    DailyOperationalRide::query()->create([
                        'daily_operational_id' => $dailyOperational->id,
                        'check_list_question_id' => $key,
                        'ride_id' => $item,
                        'comment_ride' => $validate['comment_ride'][$k][$key] ?? null,
                    ]);
                }
            }
        }
        if ($request->has('images') && is_array($request->images)) {
            foreach ($request->images as $key => $image) {
                $path = Storage::disk('s3')->put('images', $image);
                DailyOperationalImage::create(['image' => $path, 'daily_operationals_id' => $dailyOperational->id ,'comment' => $validate['img_comment'][$key] ?? null]);
            }
        }

        return self::apiResponse(200, __('daily checkList successfully'), []);
    }


    protected function questions()
    {
        $questionsAm = CheckListQuestion::query()->where('type', 'am')->get();
        $questionsBm = CheckListQuestion::query()->where('type', 'pm')->get();
        $this->body['questions_am'] = QuestionsChecklistResource::collection($questionsAm);
        $this->body['questions_pm'] = QuestionsChecklistResource::collection($questionsBm);
        $this->body['park_id'] = auth()->user()->parks?->first()?->id;

        return self::apiResponse(200, __('questions'), $this->body);
    }


}
