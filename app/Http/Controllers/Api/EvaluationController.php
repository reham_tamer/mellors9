<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Evaluation\EvaluationQuestionsTypeResource;
use App\Http\Resources\Evaluation\EvaluationQuestionsTypeSupervisorResource;
use App\Http\Resources\User\UserResource;
use App\Http\Resources\User\BreakerResource;
use App\Http\Resources\User\UserZoneResource;
use App\Models\Evaluation;
use App\Models\EvaluationInfo;
use App\Models\EvaluationQuestion;
use App\Models\User;
use App\Models\UserZone;
use Illuminate\Support\Carbon;
use App\Models\UserPark;
use App\Traits\Api\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EvaluationController extends Controller
{
    use ApiResponse;

    protected function questions()
    {
        $this->body['all_questions'] = EvaluationQuestionsTypeResource::collection(EvaluationQuestion::where('evaluation_to', 'operator')->get()->unique('type'));
        return self::apiResponse(200, __('questions'), $this->body);
    }

    protected function storeEvaluation(Request $request)
    {
        $validated = $request->validate([
            'ride_id' => 'nullable|exists:rides,id',
            'park_id' => 'required|exists:parks,id',
            'park_time_id' => 'nullable|exists:park_times,id',
            'operator_id' => 'required_without:supervisor_id|exists:users,id',
            'supervisor_id' => 'required_without:operator_id|exists:users,id',
            'question_id' => 'required|array',
            'question_id.*' => 'exists:evaluation_questions,id',
            'comment' => 'nullable|array',
            'score' => 'required|array',
            'signature' => 'nullable|image',
            'remarks' => 'nullable|string',
        ]);
        $evaluation = Evaluation::query()->create($validated);
        $evaluation['by_user_id'] = auth()->id();
        $evaluation['date'] = Carbon::now()->toDateString();
        $evaluation['time'] = Carbon::now()->toTimeString();
        $score = 0;
        foreach ($validated['question_id'] as $key => $question) {
            EvaluationInfo::query()->create([
                'evaluation_id' => $evaluation->id,
                'evaluation_question_id' => $question,
                'comment' => $validated['comment'][$key] ?? null,
                'score' => $validated['score'][$key] ?? 0,
            ]);
            $eval = EvaluationQuestion::query()->find($question);
            if($eval && $eval->description == null){
                $score += $validated['score'][$key] ?? 0;
            }
        }
        $evaluation->update(['total_score' => $score]);
        if (!empty($validated['signature'])) {
            $path = Storage::disk('s3')->put('images', $validated['signature']);
            $evaluation->update(['signature' => $path]);
        }

        return self::apiResponse(200, __('store evaluation successfully'), []);

    }

    protected function getSupervisorByPark(Request $request)
    {
        $request->validate([
            'park_id' => 'required|exists:parks,id',
        ]);
        $users = UserPark::query()->where('park_id', $request->park_id)->
        whereHas('users.roles', function ($query) {
            return $query->where('name', 'Operations/Zone supervisor');
        })->pluck('user_id');
        $users = User::query()->whereIn('id', $users)->get();
        $this->body['supervisors'] = UserZoneResource::collection($users);
        return self::apiResponse(200, __('store evaluation successfully'),$this->body);

    }
    protected function getbreakerByZone(Request $request)
    {
        $request->validate([
            'zone_id' => 'required|exists:zones,id',
        ]);
        $users = UserZone::query()->where('zone_id', $request->zone_id)->
        whereHas('users.roles', function ($query) {
            return $query->where('name', 'Breaker');
        })->pluck('user_id');
        $users = User::query()->whereIn('id', $users)->get();

        $this->body['breakers'] = BreakerResource::collection($users);
        return self::apiResponse(200, __('Breakers By Zone'),$this->body);

    }

    protected function questionsSupervisors()
    {
        $this->body['all_questions'] = EvaluationQuestionsTypeSupervisorResource::collection(EvaluationQuestion::where('evaluation_to', 'supervisor')->get()->unique('type'));
        return self::apiResponse(200, __('questions'), $this->body);
    }


}
