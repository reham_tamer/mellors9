<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\AttendanceResource;
use App\Models\Attendance;
use App\Models\AttendanceLog;
use App\Models\GameTime;
use App\Models\Ride;
use App\Traits\Api\ApiResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    use ApiResponse;

    protected function read(Request $request)
    {
        $request->validate([
            'code' => 'required',
            'ride_id' => 'required|exists:rides,id',
            'park_time_id' => 'required|exists:park_times,id',

        ]);
        if (dateTime() == null) {
            return self::apiResponse(400, __('time slot not found'));
        }
        $attendance = Attendance::query()->where('code', $request->code)->first();
        if (!$attendance) {
            return self::apiResponse(400, __('Attendance not found'));
        }
        $log = AttendanceLog::query()->where('attendance_id', $attendance->id)->where('open_date', dateTime()->date)
            ->where('ride_id', $request->ride_id)->where('type', 'login')->first();

        if ($log) {
            $logStart = Carbon::parse("$log->date_time_start");
            $log->update([
                'type' => 'logout',
                'date_time_end' => Carbon::now(),
                'shift_minutes' => $logStart->diffInMinutes(Carbon::now()),
            ]);
            $message = __('Removed attendance successfully');
        } else {
            AttendanceLog::query()->create([
                'type' => 'login',
                'attendance_id' => $attendance->id,
                'date_time_start' => Carbon::now(),
                'ride_id' => $request->ride_id,
                'park_id' => dateTime()->park_id,
                'open_date' => dateTime()->date,
            ]);
            $message = __('Added successfully');
        }

        return self::apiResponse(200, $message, []);
    }

    protected function get_attendance(Request $request)
    {
        $request->validate([
            'ride_id' => 'required|exists:rides,id',
            'park_time_id' => 'required|exists:park_times,id',
        ]);

        if (dateTime() == null) {
            return self::apiResponse(400, __('time slot not found'));
        }
        $attendances = AttendanceLog::query()->where('open_date', dateTime()->date)
            ->where('ride_id', $request->ride_id)->orderBy('id', 'DESC')->get()->unique('attendance_id');
        $this->body['attendances'] = AttendanceResource::collection($attendances);

        return self::apiResponse(200, __('attendants'), $this->body);

    }

    protected function chooseRide($id)
    {
        $ride = Ride::query()->findOrFail($id);
        $currentDate = Carbon::now()->toDateString();
        $currentTime = Carbon::now()->format('H:i');
        $time = GameTime::where('ride_id', $id)
            ->where(function ($query) use ($currentDate, $currentTime) {
                $query->where('date', $currentDate)
                    ->orWhere('close_date', $currentDate)->where('end', '>=', $currentTime);
            })
            ->first();
        if (!$time) {
            return self::apiResponse(400, __('park not open yet, please try in another time'), []);
        }
        $log = AttendanceLog::query()->where('attendance_id', auth()->id())
            ->where('type', 'login')->latest()->first();

        if ($log) {
            $logStart = Carbon::parse("$log->date_time_start");
            $log->update([
                'type' => 'logout',
                'date_time_end' => Carbon::now(),
                'shift_minutes' => $logStart->diffInMinutes(Carbon::now()),
            ]);
        }
        AttendanceLog::query()->create([
            'type' => 'login',
            'attendance_id' => auth()->id(),
            'date_time_start' => Carbon::now(),
            'ride_id' => $id,
            'park_id' => $ride->park_id,
            'open_date' => $time?->date,
        ]);


        return self::apiResponse(200, __('login with ride'), []);

    }

    protected function logout()
    {

        $log = AttendanceLog::query()->where('attendance_id', auth()->id())->where('type', 'login')->first();
        if ($log) {
            $logStart = Carbon::parse("$log->date_time_start");
            $log->update([
                'type' => 'logout',
                'date_time_end' => Carbon::now(),
                'shift_minutes' => $logStart->diffInMinutes(Carbon::now()),
            ]);
        }
        auth()->user('sanctum')->tokens()->delete();
        $this->message = __('Logged out successfully');
        return self::apiResponse(200, $this->message, []);
    }


}
