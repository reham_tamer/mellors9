<?php

namespace App\Http\Controllers\Api;

use App\Events\Api\showNotification;
use App\Events\Api\StoppageApplication;
use App\Events\StoppageEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SubmitStoppageRequest;
use App\Http\Requests\Api\UpdateStoppageRequest;
use App\Http\Resources\User\Ride\RideResource;
use App\Http\Resources\User\Ride\RideStoppageResource;
use App\Models\AttendanceLog;
use App\Models\GameTime;
use App\Models\ParkTime;
use App\Models\Ride;
use App\Models\RideCycles;
use App\Models\RideStoppages;
use App\Models\User;
use App\Models\UserLog;
use App\Notifications\StoppageNotifications;
use App\Traits\Api\ApiResponse;
use App\Traits\Api\NotificationApi;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class RideStoppageController extends Controller
{
    use ApiResponse;

    protected function addRideStoppage(SubmitStoppageRequest $request)
    {
        $validate = $request->validated();
        $park_time = dateTime();
        if ($park_time == null) {
            return self::apiResponse(200, __('not found time slot'), []);
        }
        $park_time = ParkTime::findOrFail($validate['park_time_id']);

        $rider = RideCycles::where('ride_id', $validate['ride_id'])
            ->where('park_time_id', $validate['park_time_id'])
            ?->latest()
            ->first();
        if ($rider && $rider?->duration_seconds == 0) {
            $now = Carbon::now();
            $cycleStart = Carbon::parse("$rider->start_time");
            $rider->duration_seconds = $cycleStart->diffInSeconds($now);
            $rider->save();
        }
        $validate['user_id'] = \auth()->user()->id;
        $validate['ride_status'] = 'stopped';
        $open = $park_time?->date;
        if ($validate['type'] == 'arcad_all_day') {
            $validate['type'] =  'all_day';
        }
        if ($validate['type'] == 'all_day') {
            $validate['stoppage_status'] = 'working';
            //  $validate['down_minutes'] = (int) $park_time->duration_time;
            // for type untill end of shift calculate down time from stoppage start time to time slot end time
            $stoppageStartTime = Carbon::now();
            $stoppageParkTimeEnd = Carbon::parse("$park_time->close_date $park_time->end");
            $validate['down_minutes'] = $stoppageParkTimeEnd->diffInMinutes($stoppageStartTime);
            $last = RideStoppages::query()
                ->where('ride_id', $validate['ride_id'])
                ->where('park_time_id', $park_time->id)
                ->where('ride_status', 'stopped')
                ->first();
            if ($last && $last?->type == 'all_day') {
                return self::apiResponse(400, __('You cannot add another stoppage ,this ride stopped until the end of shift'));
            }
        } else {
            $stoppageStartTime = Carbon::now();
            $stoppageParkTimeEnd = Carbon::parse("$park_time->close_date $park_time->end");
            $validate['down_minutes'] = $stoppageParkTimeEnd->diffInMinutes($stoppageStartTime);
        }
        $validate['opened_date'] = $open;
        $validate['end_date'] = $park_time->close_date;
        $validate['time_slot_end'] = $park_time->end;
        $validate['time'] = \Carbon\Carbon::now()->toTimeString();
        $gameTime = GameTime::where('ride_id', $validate['ride_id'])
            ->orderBy('date', 'DESC')
            ->latest()
            ->first();
        $validate['game_time_id'] = $gameTime?->id;
        $stoppage = RideStoppages::query()->create($validate);
        $ride = Ride::find($validate['ride_id']);

        if (isset($validate['cycle_duration_seconds']) && $validate['cycle_duration_seconds'] != null) {
            $ride->cycle->last()->update(['duration_seconds' => $validate['cycle_duration_seconds'] / 60]);
        }
        event(new \App\Events\RideStatusEvent($validate['ride_id'], 'stopped', $stoppage->stopageSubCategory?->name));

        $data = [
            'title' => $stoppage->park?->name . ' : ' . $stoppage->ride?->name . ' ' . 'stopped due to ' . $stoppage->stopageSubCategory->name,
            'ride_id' => $validate['ride_id'],
            'time_id' => dateTime()?->id,
            'user_id' => Auth::user()->id,
        ];
        $adminParkRoles = ['Maintenance/Manager', 'Technical Services/Manager', 'Management/Operations Manager', 'Management/CEO', 'Management/General Manager', 'Client', 'Park Admin'];

        $all = User::whereHas('roles', function ($query) use ($adminParkRoles) {
            return $query->whereIn('name', $adminParkRoles);
        })
            ->whereHas('parks', function ($query) use ($ride) {
                return $query->where('park_id', $ride->park_id);
            })
            ->get();

        foreach ($all as $user) {
            Notification::send($user, new StoppageNotifications($data));
            $new = new NotificationApi("$user->id", $data['title']);
            $new->send();
        }

        $zone = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Operations/Zone supervisor');
        })
            ->whereHas('zones', function ($query) use ($ride) {
                return $query->where('zone_id', $ride->zone->id);
            })
            ->first();

        $operation = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Operations/Operator');
        })
            ->whereHas('rides', function ($query) use ($ride) {
                return $query->where('ride_id', $ride->id);
            })
            ->first();

        if ($zone && auth()->user()->id != $zone->id) {
            Notification::send($zone, new StoppageNotifications($data));
            $new = new NotificationApi("$zone->id", $data['title']);
            $new->send();
        }

        if ($operation && auth()->user()->id != $operation->id) {
            Notification::send($operation, new StoppageNotifications($data));
            $new = new NotificationApi("$operation->id", $data['title']);
            $new->send();
        }
        $admin = User::whereHas('roles', function ($query) use ($adminParkRoles) {
            return $query->where('name', 'Super Admin');
        })->first();

        if ($admin) {
            event(new StoppageEvent($admin->id, $data['title'], $stoppage->created_at, dateTime()?->id, $data['ride_id']));
            Notification::send($admin, new StoppageNotifications($data));
        }
        $zoneManger = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Operations/Zones Manager');
        })
            ->whereHas('zones', function ($query) use ($validate) {
                return $query->where('zone_id', $validate['zone_id']);
            })
            ->first();
        if ($zoneManger) {
            Notification::send($zoneManger, new StoppageNotifications($data));
        }
        //attendance
        $attendanceLogs = AttendanceLog::query()->where('open_date', dateTime()->date)
            ->where('ride_id', $ride->id)->where('type', 'login')->get();

        if ($attendanceLogs) {
            foreach ($attendanceLogs as $attendanceLog) {
                $logStart = \Carbon\Carbon::parse("$attendanceLog->date_time_start");
                $attendanceLog->update([
                    'type' => 'logout',
                    'date_time_end' => Carbon::now(),
                    'shift_minutes' => $logStart->diffInMinutes(Carbon::now()),
                    'is_stopped' => 1
                ]);
            }
        }
        // operator
        $operator = UserLog::query()->where('open_date', dateTime()->date)
            ->where('ride_id', $ride->id)->where('type', 'login')->latest()->first();
        if ($operator) {
            $startTime = \Carbon\Carbon::parse("$operator->date $operator->time");
            $endTime = \Carbon\Carbon::now();
            $shift_minutes = $startTime->diffInMinutes($endTime);
            $operator->update(['type' => 'logout',
                'shift_minutes' => $shift_minutes >= 1 ? $shift_minutes : 0, 'end_time' => $endTime, 'is_stopped' => 1]);
        }
        return self::apiResponse(200, __('stoppage added successfully'), []);
    }

    protected function reopen(Request $request)
    {
        $validate = $request->validate([
            'ride_id' => 'required|exists:rides,id',
            'park_time_id' => 'required|exists:park_times,id',
            'time' => 'required',
        ]);
        $park_time = dateTime();
        if ($park_time == null) {
            return self::apiResponse(200, __('not found time slot'), []);
        }
        $time = $validate['time']; // stoppage end time
        $ride = Ride::find($validate['ride_id']);
        $last = $ride->rideStoppages->last();

        $park_time = ParkTime::findOrFail($validate['park_time_id']);
        $stoppageStartTime = Carbon::parse("$last->date $last->time_slot_start"); //get stoppage start date and time

        $stoppageEndDate = Carbon::now()->toDateString();
        $stoppageEnd = Carbon::parse("$stoppageEndDate $time"); //stoppage end date and time
        $stoppageParkTimeEnd = Carbon::parse("$park_time->date $time");
        $last->down_minutes = $stoppageEnd->diffInMinutes($stoppageStartTime);
        $last->ride_status = 'active';
        $last->stoppage_status = 'done';
        $last->end_date = $stoppageEndDate; // store stoppage real end date
        $last->time_slot_end = $time;

        $last->save();

        $ride
            ->rideStoppages()
            ?->where('ride_status', 'stopped')
            ?->update(['ride_status' => 'active', 'stoppage_status' => 'done']);
        $ride = Ride::find($validate['ride_id']);

        $rider = RideCycles::where('ride_id', $validate['ride_id'])
            ->where('park_time_id', $validate['park_time_id'])
            ?->latest()
            ->first();
        if ($rider) {
            $rider->number_status = 0;
            $rider->save();
        }

        $this->body['ride'] = RideResource::make($ride);
        event(new \App\Events\RideStatusEvent($validate['ride_id'], 'active', $last->stopageSubCategory?->name));

        $data = [
            'title' => $ride->park?->name . ' : ' . $ride?->name . ' ' . 'has active status',
            'ride_id' => $validate['ride_id'],
            'time_id' => dateTime()?->id,
            'user_id' => Auth::user()->id,
        ];

        $adminParkRoles = ['Maintenance/Manager', 'Technical Services/Manager', 'Management/Operations Manager', 'Management/CEO', 'Management/General Manager', 'Client', 'Park Admin'];
        $all = User::whereHas('roles', function ($query) use ($adminParkRoles) {
            return $query->whereIn('name', $adminParkRoles);
        })
            ->whereHas('parks', function ($query) use ($ride) {
                return $query->where('park_id', $ride->park_id);
            })
            ->get();

        foreach ($all as $user) {
            event(new StoppageEvent($user->id, $data['title'], $last->created_at, dateTime()?->id, $data['ride_id']));

            Notification::send($user, new StoppageNotifications($data));
            $new = new NotificationApi("$user->id", $data['title']);
            $new->send();
        }

        $operator = User::where('id', $ride->rideStoppages?->last()?->user_id)->first();
        if ($operator && \auth()->user()->id != $operator->id) {
            $new = new NotificationApi("$operator->id", $data['title']);
            $new->send();
            Notification::send($operator, new StoppageNotifications($data));
            event(new StoppageApplication($operator->id, 'stoppage_status_changed', $ride->id));
        }
        $admin = User::whereHas('roles', function ($query) use ($adminParkRoles) {
            return $query->where('name', 'Super Admin');
        })->first();

        if ($admin) {
            event(new StoppageEvent($admin->id, $data['title'], $last->created_at, dateTime()?->id, $data['ride_id']));
            Notification::send($admin, new StoppageNotifications($data));
        }

        $zone = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Operations/Zone supervisor');
        })
            ->whereHas('zones', function ($query) use ($ride) {
                return $query->where('zone_id', $ride->zone->id);
            })
            ->first();

        if ($zone && auth()->user()->id != $zone->id) {
            Notification::send($zone, new StoppageNotifications($data));
            $new = new NotificationApi("$zone->id", $data['title']);
            $new->send();
        }
        $zoneManger = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Operations/Zones Manager');
        })
            ->whereHas('zones', function ($query) use ($ride) {
                return $query->where('zone_id', $ride->zone_id);
            })
            ->first();
        if ($zoneManger) {
            Notification::send($zoneManger, new StoppageNotifications($data));
        }
        $attendanceLogs = AttendanceLog::query()->where('open_date', dateTime()->date)
            ->where('ride_id', $ride->id)->where('type', 'logout')->where('is_stopped', 1)->get();

        if ($attendanceLogs) {
            foreach ($attendanceLogs as $attendance) {
                $attendance->update(['is_stopped' => 0]);
                AttendanceLog::query()->create([
                    'type' => 'login',
                    'attendance_id' => $attendance->attendance_id,
                    'date_time_start' => \Carbon\Carbon::now(),
                    'ride_id' => $ride->id,
                    'park_id' => dateTime()->park_id,
                    'open_date' => dateTime()->date,
                ]);
            }
        }
        // operator
        $operator = UserLog::query()->where('open_date', dateTime()->date)
            ->where('ride_id', $ride->id)->where('type', 'logout')->where('is_stopped', 1)->latest()->first();
        if ($operator) {
            $operator->update(['is_stopped' => 0]);
            UserLog::query()->create([
                'type' => 'login',
                'user_id' => $operator->user_id,
                'date' => \Carbon\Carbon::now()->toDateString(),
                'time' => \Carbon\Carbon::now()->toTimeString(),
                'ride_id' => $ride->id,
                'park_id' => dateTime()->park_id,
                'open_date' => dateTime()->date,
            ]);
        }

        return self::apiResponse(200, __('update ride status successfully'), $this->body);
    }

    protected function getRideStoppages($id)
    {
        $ride = Ride::query()->find($id);
        if (!$ride) {
            return self::apiResponse(200, __('ride not found'), []);
        }
        $rideStoppages = $ride->rideStoppagesPage ?? collect(); // Provide an empty collection if $ride->rideStoppages is null

        $stoppage = $rideStoppages->where('park_time_id', ParkTime($ride->park_id)?->id);

        if (dateTime() == null) {
            $this->body['ride_stoppage'] = [];
        }
        $this->body['ride_stoppage'] = RideStoppageResource::collection($stoppage);
        return self::apiResponse(200, __('get ride stoppage'), $this->body);
    }

    protected function UpdateRideStoppages(UpdateStoppageRequest $request)
    {
        $validate = $request->validated();

        $park_time = dateTime();
        if ($park_time == null) {
            return self::apiResponse(200, __('not found time slot'), []);
        }
        $adminParkRoles = ['Maintenance/Manager', 'Technical Services/Manager', 'Management/Operations Manager', 'Management/CEO', 'Management/General Manager', 'Client', 'Park Admin'];
        foreach ($validate['id'] as $key => $id) {
            $stoppage = RideStoppages::find($id);
            $ride = Ride::find($stoppage->ride_id);
            $time = $validate['time'][$key];
            $stoppageStartTime = Carbon::parse("$park_time->date $stoppage->time_slot_start");
            $stoppageParkTimeEnd = Carbon::parse("$park_time->date $time");
            $stoppage->down_minutes = $stoppageParkTimeEnd->diffInMinutes($stoppageStartTime);
            $stoppage->ride_status = $validate['status'][$key];
            $stoppage->stoppage_status = $validate['status'][$key] == 'active' ? 'done' : 'pending';
            $stoppage->time_slot_end = $time;
            $stoppage->save();

            $data = [
                'title' => $stoppage->park?->name . ' : ' . $stoppage->ride?->name . ' ' . 'has ' . $validate['status'][$key] . ' status',
                'ride_id' => $stoppage->ride_id,
                'time_id' => dateTime()?->id,
                'user_id' => Auth::user()->id,
            ];

            event(new StoppageApplication(\auth()->id(), 'stoppage_status_changed', $ride->id));

            $all = User::whereHas('roles', function ($query) use ($adminParkRoles) {
                return $query->whereIn('name', $adminParkRoles);
            })
                ->whereHas('parks', function ($query) use ($ride) {
                    return $query->where('park_id', $ride->park_id);
                })
                ->get();
            foreach ($all as $user) {
                Notification::send($user, new StoppageNotifications($data));
                event(new StoppageEvent($user->id, $data['title'], $stoppage->created_at, dateTime()?->id, $data['ride_id']));
                $new = new NotificationApi("$user->id", $data['title']);
                $new->send();
            }

            $admin = User::whereHas('roles', function ($query) use ($adminParkRoles) {
                return $query->where('name', 'Super Admin');
            })->first();
            if ($admin) {
                event(new StoppageEvent($admin->id, $data['title'], $stoppage->created_at, dateTime()?->id, $data['ride_id']));
                Notification::send($admin, new StoppageNotifications($data));
            }
            $zone = User::whereHas('roles', function ($query) {
                return $query->where('name', 'Operations/Zone supervisor');
            })
                ->whereHas('zones', function ($query) use ($ride) {
                    return $query->where('zone_id', $ride->zone->id);
                })
                ->first();

            if ($zone && auth()->user()->id != $zone->id) {
                Notification::send($zone, new StoppageNotifications($data));
                $new = new NotificationApi("$zone->id", $data['title']);
                $new->send();
            }
            $operation = User::whereHas('roles', function ($query) {
                return $query->where('name', 'Operations/Operator');
            })
                ->whereHas('rides', function ($query) use ($ride) {
                    return $query->where('ride_id', $ride->id);
                })
                ->first();

            if ($operation && auth()->user()->id != $operation->id) {
                event(new StoppageApplication($operation->id, 'stoppage_status_changed', $ride->id));
                Notification::send($operation, new StoppageNotifications($data));
                $new = new NotificationApi("$operation->id", $data['title']);
                $new->send();
            }
            $zoneManger = User::whereHas('roles', function ($query) {
                return $query->where('name', 'Operations/Zones Manager');
            })
                ->whereHas('zones', function ($query) use ($stoppage) {
                    return $query->where('zone_id', $stoppage->zone_id);
                })
                ->first();

            if ($zoneManger) {
                Notification::send($zoneManger, new StoppageNotifications($data));
            }
        }

        return self::apiResponse(200, __('update ride stoppage'), []);
    }

    protected function UpdateStoppageCategory(Request $request)
    {
        $validate = $request->validate([
            'id' => 'required|exists:ride_stoppages,id',
            'stopage_category_id' => 'required|exists:stopage_categories,id',
            'stopage_sub_category_id' => 'required|exists:stopage_sub_categories,id',
            'type' => 'required',
            'description' => 'nullable',
            'ride_id' => 'required',
        ]);
        $stoppage = RideStoppages::find($validate['id']);
        // if ($validate['type'] == 'all_day') {
        $last = RideStoppages::query()
            ->where('ride_id', $validate['ride_id'])
            ->where('park_time_id', $stoppage->park_time_id)
            ->first();
        if ($last->type == 'all_day') {
            return self::apiResponse(400, __('You cannot add another stoppage ,this ride stopped until the end of shift'));
        }
        // }
        $stoppage->update([
            'stopage_category_id' => $validate['stopage_category_id'],
            'stopage_sub_category_id' => $validate['stopage_sub_category_id'],
            'type' => $validate['type'],
            'description' => $validate['description'] ?? null,
        ]);
        if ($validate['type'] == 'all_day') {
            $stoppage->update(['stoppage_status' => 'working']);
        }
        $stoppage = RideStoppages::find($validate['id']);
        $operators = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Operations/Operator');
        })->get();
        foreach ($operators as $operator) {
            if (isset($operator) && $operator->rides?->where('id', $stoppage->ride_id)?->first() != null) {
                event(new StoppageApplication($operator->id, 'stoppage_status_changed', $stoppage->ride_id));
                event(new showNotification($operator->id, 'stoppage_status_changed', Carbon::now()->toDateTimeString(), dateTime()?->id, $stoppage->ride_id));
            }
        }
        //        $zone = User::whereHas('roles', function ($query) {
        //            return $query->where('name', 'Operations/Zone supervisor');
        //        })->whereHas('parks', function ($query) use ($stoppage) {
        //            return $query->where('park_id', $stoppage->park_id);
        //        })->first();
        //        if (isset($zone) && \auth()->id() != $zone->id) {
        //            event(new StoppageApplication($zone->id, 'stoppage_status_changed', $stoppage->ride_id));
        //            event(new showNotification($zone->id, 'stoppage_status_changed', Carbon::now()->toDateTimeString(), dateTime()?->id, $stoppage->ride_id));
        //        }
        return self::apiResponse(200, __('update stoppage categories successfully!'), []);
    }

    protected function addAnotherStoppage(Request $request)
    {
        $validate = $request->validate([
            'ride_id' => 'required|exists:rides,id',
            'park_time_id' => 'required|exists:park_times,id',
            'time_slot_start' => 'required',
            'date' => 'required',
            'stopage_category_id' => 'required|exists:stopage_categories,id',
            'stopage_sub_category_id' => 'required|exists:stopage_sub_categories,id',
            'park_id' => 'required|exists:parks,id',
            'zone_id' => 'required|exists:zones,id',
            'type' => 'nullable',
            'description' => 'nullable',
        ]);
        $park_time = dateTime();
        if ($park_time == null) {
            return self::apiResponse(400, __('not found time slot'), []);
        }
        $ride = Ride::find($validate['ride_id']);
        $time = $validate['time_slot_start'];

        $stoppageParkTimeEnd = Carbon::now();
        $last = $ride->rideStoppages->where('park_time_id', $validate['park_time_id'])->last();
        if (!$last) {
            return self::apiResponse(400, __('not found old stoppage'), []);
        }
        $stoppageParkTimeOldStart = Carbon::parse("$last->date $last->time_slot_start");

        $park_time = ParkTime::query()->findOrFail($validate['park_time_id']);
        $last->down_minutes = $stoppageParkTimeEnd->diffInMinutes($stoppageParkTimeOldStart);
        $last->ride_status = 'active';
        $last->stoppage_status = 'done';
        $last->time_slot_end = $time;
        $last->save();
        if ($validate['type'] == 'arcad_all_day') {
            $validate['type'] =  'all_day';
        }
        $validate['user_id'] = \auth()->user()->id;
        $validate['ride_status'] = 'stopped';
        $validate['stoppage_status'] = 'pending';
        $validate['type'] = isset($validate['type']) && $validate['type'] != null ? $validate['type'] : 'time_slot';
        $validate['down_minutes'] = (int)$park_time->duration_time;
        $validate['opened_date'] = $park_time?->date;
        $validate['end_date'] = $park_time?->close_date;
        $validate['time_slot_end'] = $park_time->end;
        $validate['time_slot_start'] = $time;
        // $validate['time_slot_start'] = $park_time->start;
        $validate['parent_id'] = $last?->parent_id != null ? $last?->parent_id : $last?->id;
        $gameTime = GameTime::where('ride_id', $validate['ride_id'])
            ->orderBy('date', 'DESC')
            ->latest()
            ->first();
        $validate['game_time_id'] = $gameTime?->id;
        $stoppage = RideStoppages::query()->create($validate);

        event(new \App\Events\RideStatusEvent($validate['ride_id'], 'stopped', $stoppage->stopageSubCategory?->name));

        $data = [
            'title' => $stoppage->park?->name . ' : ' . $stoppage->ride?->name . ' ' . 'stopped due to' . $stoppage->stopageSubCategory->name,
            'ride_id' => $validate['ride_id'],
            'time_id' => dateTime()?->id,
            'user_id' => Auth::user()->id,
        ];

        $zone = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Operations/Zone supervisor');
        })
            ->whereHas('zones', function ($query) use ($ride) {
                return $query->where('zone_id', $ride->zone_id);
            })
            ->first();

        if ($zone) {
            Notification::send($zone, new StoppageNotifications($data));
            $new = new NotificationApi("$zone->id", $data['title']);
            $new->send();
        }
        $adminParkRoles = ['Maintenance/Manager', 'Technical Services/Manager', 'Management/Operations Manager', 'Management/CEO', 'Management/General Manager', 'Client', 'Park Admin'];
        $all = User::whereHas('roles', function ($query) use ($adminParkRoles) {
            return $query->whereIn('name', $adminParkRoles);
        })
            ->whereHas('parks', function ($query) use ($ride) {
                return $query->where('park_id', $ride->park_id);
            })
            ->get();
        foreach ($all as $user) {
            Notification::send($user, new StoppageNotifications($data));
            $new = new NotificationApi("$user->id", $data['title']);
            $new->send();
        }
        return self::apiResponse(200, __('stoppage added successfully'), []);
    }
}
