<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Training\EvacuationTraining\TrainingAdminChecklistTypeResource;
use App\Http\Resources\Training\EvacuationTraining\TrainingResource;
use App\Http\Resources\Training\TrainingAdmin\TrainingAdminUserResource;
use App\Models\Attendance;
use App\Models\Training;
use App\Models\TrainingUser;
use App\Models\User;
use App\Traits\Api\ApiResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class TrainingEvacuationController extends Controller
{
    use ApiResponse;


    protected function trainingAdmin()
    {
        $user = Auth::user();
        $role = $user->roles->first()?->name == 'Management/Operations Manager' || $user->roles->first()?->name == 'Operations/Zone supervisor';
        if (!$role) {
            return self::apiResponse(200, __('you dont have permission to see this page'), []);
        }
        $user = Auth::user();
        $trainings = Training::query()->where('type', 'evacuation')
            ->whereActiveApp(1)
            ->where(function ($q) use ($user) {
                $q->where('manager_id', $user->id)->orWhere('supervisor_id', $user->id)
                    ->orWhere('trainer_id', $user->id);
            })->get();

        $this->body['evacuation_trainings'] = TrainingResource::collection($trainings);
        $this->body['role'] = $user->roles->first()?->name;
        return self::apiResponse(200, __('trainings'), $this->body);

    }

    protected function trainingAdminShow(Training $training)
    {
        $user = Auth::user();

        $trainings = $training->trainingUsers;
//            ?->whereNotNull('user_signature');
//        if ($user->roles->first()?->name == 'Management/Operations Manager') {
//            $trainings = $trainings->whereNull('operation_manager_signature')
//                ->whereNotNull('supervisor_signature');
//        } else {
//            $trainings = $trainings->whereNull('supervisor_signature');
//        }

        $this->body['evacuation_trainings'] = TrainingAdminUserResource::collection($trainings);
        return self::apiResponse(200, __('trainings'), $this->body);

    }

    protected function trainingAdminShowUser(TrainingUser $training, Request $request)
    {
        $request->training = $training;
        $this->body['evacuation_training'] = TrainingAdminChecklistTypeResource::make($training);
        return self::apiResponse(200, __('trainings'), $this->body);
    }


    /**
     * @param array $validate
     * @return \Illuminate\Http\JsonResponse
     */
    protected function adminChecklistUsersStore(Request $request)
    {
        $validate = $request->validate([
            'training_checklist_item_id' => 'required|array|exists:training_checklist_items,id',
            'checklist_date' => 'nullable|array',
            'training_id' => 'required|exists:trainings,id',
            'attendance_id' => 'required|exists:attendances,id',
            'checklist_trainee_signature' => ['nullable', 'array'],
            'checklist_trainer_signature' => 'nullable|array',

            'ride_id' => 'nullable|array|exists:rides,id',
            'ride_trainee_signature' => 'nullable|array',
            'ride_trainer_signature' => 'nullable|array',
            'ride_date' => 'nullable|array',
            'ride_instructed' => 'nullable|array',
            'attendance_signature' => 'nullable',
        ]);
        $attendance = Attendance::find($request->attendance_id);
        if (isset($validate['training_checklist_item_id']) && $validate['training_checklist_item_id'] != null) {
            foreach ($validate['training_checklist_item_id'] as $item) {
                isset($validate['checklist_trainee_signature'][$item]) ? $validate['checklist_trainee_signature'][$item] = Storage::disk('s3')->put('images', $validate['checklist_trainee_signature'][$item]) : null;
                isset($validate['checklist_trainer_signature'][$item]) ? $validate['checklist_trainer_signature'][$item] = Storage::disk('s3')->put('images', $validate['checklist_trainer_signature'][$item]) : null;

                if (isset($validate['checklist_trainee_signature'][$item])) {
                    $attendance->training_checklist_users()->updateOrCreate(
                        [
                            'training_id' => $validate['training_id'],
                            'training_checklist_item_id' => $item,
                        ],
                        [
                            'trainee_signature' => $validate['checklist_trainee_signature'][$item],
                            'date' => $validate['checklist_date'][$item],
                        ]);
                }
                if (isset($validate['checklist_trainer_signature'][$item])) {
                    $attendance->training_checklist_users()->updateOrCreate(
                        [
                            'training_id' => $validate['training_id'],
                            'training_checklist_item_id' => $item,
                        ],
                        [
                            'trainer_signature' => $validate['checklist_trainer_signature'][$item],
                            'date' => $validate['checklist_date'][$item],
                        ]);
                }
                if (isset($validate['checklist_date'][$item])) {
                    $attendance->training_checklist_users()->updateOrCreate(
                        [
                            'training_id' => $validate['training_id'],
                            'training_checklist_item_id' => $item,
                        ],
                        [
                            'date' => $validate['checklist_date'][$item],
                        ]);
                }
            }
        }
        if (isset($validate['ride_id']) && $validate['ride_id'] != null) {

            foreach ($validate['ride_id'] as $item) {
                isset($validate['ride_trainee_signature'][$item]) ? $validate['ride_trainee_signature'][$item] = Storage::disk('s3')->put('images', $validate['ride_trainee_signature'][$item]) : null;
                isset($validate['ride_trainer_signature'][$item]) ? $validate['ride_trainer_signature'][$item] = Storage::disk('s3')->put('images', $validate['ride_trainer_signature'][$item]) : null;
                if (isset($validate['ride_trainee_signature'][$item])) {
                    $attendance->training_rides()->updateOrCreate([
                        'ride_id' => $item,
                        'training_id' => $validate['training_id'],
                    ],
                        [
                            'trainee_signature' => $validate['ride_trainee_signature'][$item] ?? null,
                            'date' => $validate['ride_date'][$item] ?? null,
                            'instructed' => $validate['ride_instructed'][$item] ?? null,
                        ]);
                }
                if (isset($validate['ride_trainer_signature'][$item])) {
                    $attendance->training_rides()->updateOrCreate([
                        'ride_id' => $item,
                        'training_id' => $validate['training_id'],
                    ],
                        [
                            'trainer_signature' => $validate['ride_trainer_signature'][$item] ?? null,
                            'date' => $validate['ride_date'][$item] ?? null,
                            'instructed' => $validate['ride_instructed'][$item] ?? null,

                        ]);
                }
                if (isset($validate['ride_date'][$item]) || isset($validate['ride_instructed'][$item])) {
                    $attendance->training_rides()->updateOrCreate([
                        'ride_id' => $item,
                        'training_id' => $validate['training_id'],
                    ],
                        [
                            'date' => $validate['ride_date'][$item] ?? null,
                            'instructed' => $validate['ride_instructed'][$item] ?? null,

                        ]);
                }

            }
        }
        $user = Attendance::class;
        $training = TrainingUser::query()->where('training_id', $request->training_id)
            ->where('userable_type', $user)->where('userable_id', $request->attendance_id)->first();

        if ($request->attendance_signature) {
            $path = Storage::disk('s3')->put('images', $request->attendance_signature);
            $training->update(['user_signature' => $path, 'user_date' => Carbon::now()->toDateString()]);
        }


        return self::apiResponse(200, __('Store training checklist successfully'), []);
    }

    protected function trainingAdminStoreSignature(Request $request)
    {
        $request->validate([
            'training_id' => 'required|exists:trainings,id',
            'attendance_id' => 'required|exists:attendances,id',
            'signature' => 'required|image',
        ]);

        $user = Attendance::class;
        $training = TrainingUser::query()->where('training_id', $request->training_id)
            ->where('userable_type', $user)->where('userable_id', $request->attendance_id)->first();

        if (isset($request->signature) && $request->signature) {
            $auth = Auth::user();

            $path = Storage::disk('s3')->put('images', $request->signature);
            if ($auth->roles->first()?->name == 'Management/Operations Manager') {
                $training->update(['operation_manager_signature' => $path, 'operation_manager_date' => Carbon::now()->toDateString()]);
            } elseif ($auth->roles->first()?->name == 'Operations/Zone supervisor') {
                $training->update(['supervisor_signature' => $path, 'supervisor_date' => Carbon::now()->toDateString()]);
            } else {
                return self::apiResponse(200, __('you dont have permission'), []);
            }
        }
        return self::apiResponse(200, __('signature send successfully'), []);
    }


}

