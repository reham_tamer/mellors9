<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Dashboard\Accident\EvacuationRequest;
use App\Http\Requests\Dashboard\Accident\IncidentRequest;
use App\Http\Resources\User\DepartmentResource;
use App\Models\Department;
use App\Models\Evacuation;
use App\Models\GameTime;
use App\Models\IncidentImage;
use App\Models\Park;
use App\Models\ParkTime;
use App\Models\GeneralIncident;
use App\Models\RideCapacity;
use App\Models\RideStoppages;
use App\Models\User;
use App\Notifications\StoppageNotifications;
use App\Notifications\UserNotifications;
use App\Traits\ImageOperations;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use App\Traits\Api\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Accident\InvestigationRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

class HealthSaftyController extends Controller
{
    use ApiResponse,ImageOperations;

    protected function incident(IncidentRequest $request)
    {
        $data = $request->validated();
//        dd($data);
//        $data = Arr::except($data, ['images', 'signature']);

        $gameTime = null;
        if (isset($data['ride_id'])) {
            $gameTime = GameTime::where('ride_id', $data['ride_id'])
                                 ->where('date',$data['date'])->first(); 
        }

        $parkTime = ParkTime::where('park_id', $data['park_id'])
                              ->where('date',$data['date'])->first(); 
        if (!empty($request->signature)) {
            $path = Storage::disk('s3')->put('images', $request->signature);
            $signature = $path;
        }

        $incidentData = [
            'type' => 'incident',
            'status' => 'pending',
            'park_id' => $data['park_id'],
            'date' => $data['date'],
            'created_by_id' => auth()->user()->id,
            'value' => $data,
            'park_time_id' => $parkTime?->id,
            'signature' => $signature ?? null,
            'zone_id' => $data['zone_id'] ?? null,
        ];

        if (isset($data['ride_id'])) {
            $incidentData['ride_id'] = $data['ride_id'];
            $incidentData['game_time_id'] = $gameTime?->id;
        }
        $incident = GeneralIncident::create($incidentData);
        if ($request->has('images') && is_array($request->images)) {
            foreach ($request->images as $key => $image) {
                $path = Storage::disk('s3')->put('images', $image);
                IncidentImage::create(['image' => $path, 'general_incident_id' => $incident->id ,'comment' => $data['comment'][$key] ?? null]);
            }
        }

        $parkAdmins = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Park Admin');
        })->whereHas('parks', function ($query) use ($data) {
            return $query->where('park_id', $data['park_id']);
        })->get();

        $notificationData = [
            'title' => 'New Incident added (' . $data['type_of_event'] . ')',
            'ride_id' => $data['ride_id'] ?? null,
            'user_id' => Auth::user()->id,
            'time_id' => dateTime()?->id,
        ];

        foreach ($parkAdmins as $user) {
            Notification::send($user, new UserNotifications($notificationData));
        }

        return self::apiResponse(200, __('incident'), []);
    }

    protected function investigation(InvestigationRequest $request)
    {
        $data = $request->validated();
        $gameTime = GameTime::where('ride_id', $data['ride_id'])
                             ->where('date',$data['date'])->first(); 

        $parkTime = ParkTime::where('park_id', $data['park_id'])
                            ->where('date',$data['date'])->first(); 

        $list = GeneralIncident::create([
            'type' => 'investigation',
            'park_id' => $data['park_id'],
            'ride_id' => $data['ride_id'],
            'status' => 'pending',
            'date' => $data['date'],
            'created_by_id' => auth()->user()->id,
            'value' => $data,
            'game_time_id' => $gameTime?->id,
            'park_time_id' => $parkTime?->id,
        ]);
        if (!empty($data['signature_image'])) {
            $path = Storage::disk('s3')->put('images', $data['signature_image']);
            $list->update(['image' => $path]);
        }
        return self::apiResponse(200, __('investigation'), []);
    }

    protected function departments()
    {
        $depaertments = Department::all();
        $this->body['depaertments'] = DepartmentResource::collection($depaertments);
        return self::apiResponse(200, __('depaertments'), $this->body);
    }

    protected function evacuation(EvacuationRequest $request)
    {
        $data = $request->validated();
        if (isset($data['normal']) && ($data['normal']) == 1) {
            $data['normal'] = 1;
        }
        if (isset($data['abnormal']) && ($data['abnormal']) == 1) {
            $data['abnormal'] = 1;
        }
        if (isset($data['longer_than_normal']) && ($data['longer_than_normal']) == 1) {
            $data['longer_than_normal'] = 1;
        }
        if (isset($data['medics_required']) && ($data['medics_required']) == 1) {
            $data['medics_required'] = 1;
        }
        if (isset($data['civil_defense_involved']) && ($data['civil_defense_involved']) == 1) {
            $data['civil_defense_involved'] = 1;
        }
        if (isset($data['vip_guest_involved']) && ($data['vip_guest_involved']) == 1) {
            $data['vip_guest_involved'] = 1;
        }
        if (isset($data['customer_service_issue']) && ($data['customer_service_issue']) == 1) {
            $data['customer_service_issue'] = 1;
        }
        if (isset($data['property_damage']) && ($data['property_damage']) == 1) {
            $data['property_damage'] = 1;
        }
        if (isset($data['stoppage_id']) && $data['stoppage_id'] != null) {
            $stoppage = RideStoppages::find($data['stoppage_id']);
            $gameTime = GameTime::where('ride_id', $stoppage->ride_id)
                ->where('park_time_id', $stoppage->park_time_id)
                ->first();
            $data['ride_id'] = $stoppage->ride_id;
            $data['park_id'] = $stoppage->park_id;
            $data['game_time_id'] = $gameTime?->id;
            $data['park_time_id'] = $stoppage->park_time_id;
            $data['submit_date'] = $stoppage->date;
        } elseif (isset($request['park_id']) && $request['park_id'] != '') {
            $lastParkTime = ParkTime::where('park_id', $request['park_id'])
                ->orderBy('id', 'desc')
                ->first();
            $data['submit_date'] = $lastParkTime?->date;
            $data['park_id'] = $request['park_id'];
            $data['park_time_id'] = $lastParkTime?->id;
        }
        $data['created_by'] = auth()->id();
        $evacuation = Evacuation::query()->create($data);
        if (!empty($validate['image'])) {
            $path = Storage::disk('s3')->put('images', $validate['image']);
            $evacuation->update(['image' => $path]);
        }
        return self::apiResponse(200, __('add evacuation successfully'), []);
    }


}
