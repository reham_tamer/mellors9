<?php

namespace App\Http\Controllers\Api;

use App\Events\Api\showNotification;
use App\Events\Api\StoppageApplication;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CustomerFeedbackRequest;
use App\Http\Requests\Api\ObservationRequest;
use App\Http\Requests\Api\UpdateInspectionsRequest;
use App\Http\Resources\ComplaintResource;
use App\Http\Resources\User\Ride\ObservationCategoryResource;
use App\Http\Resources\User\Ride\PreopeningListResource;
use App\Http\Resources\User\Ride\QuestionsResource;
use App\Http\Resources\User\Ride\RideCapacityResource;
use App\Http\Resources\User\Zone\ZoneResource;
use App\Models\Attraction;
use App\Models\AttractionInfo;
use App\Models\CustomerComplaint;
use App\Models\CustomerFeedbackImage;
use App\Models\CustomerFeedbacks;
use App\Models\Department;
use App\Models\GameTime;
use App\Models\GeneralQuestion;
use App\Models\Observation;
use App\Models\ObservationCategory;
use App\Models\ParkTime;
use App\Models\PreopeningList;
use App\Models\Ride;
use App\Models\RideCapacity;
use App\Models\User;
use App\Notifications\StoppageNotifications;
use App\Notifications\UserNotifications;
use App\Traits\Api\ApiResponse;
use App\Traits\Api\NotificationApi;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

class SupervisorController extends Controller
{
    use ApiResponse;

    protected function home()
    {
        $user = Auth::user();
        if (dateTime() != null) {
            $user->zones = $user->zones->where('park_id', dateTime()?->park_id);
        }
        // dd($user->zones);
        $this->body['zones'] = ZoneResource::collection($user->zones);
        return self::apiResponse(200, __('home page supervisor'), $this->body);
    }

    protected function preopeningList($id)
    {
        $ride = Ride::find($id);
        if (!$ride) {
            return self::apiResponse(200, __('ride not found'), []);
        }
        $ridePre = $ride->preopening_lists ?? collect(); // Provide an empty collection if $ride->preopening_lists is null

        $pre = $ridePre
            ?->where('type', 'preopening')
            ?->whereBetween('opened_date', [dateTime()?->date, dateTime()?->close_date])
            ?->first();

        if ($pre == null || $pre?->lists->isEmpty()) {
            return self::apiResponse(200, __('not found'), []);
        }
        $this->body['preopening_list'] = PreopeningListResource::collection($pre->lists);
        return self::apiResponse(200, __('preopening list'), $this->body);
    }

    protected function storeCustomerFeedback(CustomerFeedbackRequest $request)
    {
        $validate = $request->validated();
        $ride = Ride::find($validate['ride_id']);
        $is_skill_game = $ride->ride_type?->name;

        if ($is_skill_game == 'Skill Game') {
            $validate['is_skill_game'] = 'skill_game';
        }

        $validated = Arr::except($validate, 'image');
        $gameTime = GameTime::where('ride_id', $request->input('ride_id'))->orderBy('date', 'DESC')->latest()->first();
        $validated['game_time_id'] = $gameTime?->id;
        $validated['user_id'] = \auth()->id();
        $cs = CustomerFeedbacks::query()->create($validated);
        if (!empty($validate['image'])) {
            $this->Images($validate['image'], new CustomerFeedbackImage(), ['customer_feedback_id' => $cs->id]);
        }

        return self::apiResponse(200, __('send customer feedback successfully'), []);
    }

    private function Images($images, $model, $item)
    {
        foreach ($images as $image) {
            $path = Storage::disk('s3')->put('images', $image);
            $model->create(['image' => $path] + $item);
        }
    }

    protected function preclosingList($id)
    {
        $ride = Ride::find($id);
        if (!$ride) {
            return self::apiResponse(200, __('ride not found'), []);
        }
        $gameTime = GameTime::where('ride_id', $id)->orderBy('date', 'DESC')->latest()->first();
        $park_time_id = $gameTime?->park_time_id;

        $ridePre = $ride->preopening_lists ?? collect();

        $pre = $ridePre
            ?->where('type', 'preclosing')
            ?->where('park_time_id', $park_time_id)
            ?->first();

        if ($pre == null || $pre?->lists->isEmpty()) {
            return self::apiResponse(200, __('not found'), []);
        }
        $this->body['preclosing_list'] = PreopeningListResource::collection($pre->lists);
        return self::apiResponse(200, __('preclosing list'), $this->body);
    }

    protected function updateInspectionList(UpdateInspectionsRequest $request)
    {
        $validate = $request->validated();
        foreach ($validate['id'] as $key => $id) {
            $inpection = PreopeningList::find($id);
            $inpection->info?->update(['approved_by_id' => \auth()->user()->id]);
            $inpection->update([
                'is_checked' => $validate['is_checked'][$key] ?? null,
                'status' => $validate['status'][$key] ?? null,
                'comment' => $validate['comment'][$key] ?? null,
            ]);
        }
        $user = User::find($inpection->info?->operator_id);

        $data = [
            'title' => 'Preopening checklist for Ride ' . $inpection->info->ride?->name . ' submitted. Please review',
            'ride_id' => $inpection->info->ride_id,
            'user_id' => Auth::user()->id,
            'time_id' => dateTime()?->id,
        ];

        if ($user) {
            Notification::send($user, new StoppageNotifications($data));
            $new = new NotificationApi("$user->id", $data['title']);
            $new->send();
        }

        return self::apiResponse(200, __('Preopening checklist submitted successfully'), []);
    }

    protected function observationCategory()
    {
        $this->body['categories'] = ObservationCategoryResource::collection(ObservationCategory::query()->get());
        return self::apiResponse(200, __('categories'), $this->body);
    }

    protected function observation(ObservationRequest $request)
    {
        $validate = $request->validated();
        $data = [
            'ride_id' => $validate['ride_id'],
            'date_reported' => $validate['date'],
            'snag' => $validate['snag'],
            'department_id' => $validate['department_id'],
            'observation_category_id' => $validate['observation_category_id'],
        ];
        $ride = Ride::find($validate['ride_id']);
        $data['park_id'] = $ride->park_id;
        $data['zone_id'] = $ride->zone_id;
        $data['created_by_id'] = auth()->id();
        $gameTime = GameTime::where('ride_id', $data['ride_id'])
            ->orderBy('date', 'DESC')
            ->latest()
            ->first();
        $data['game_time_id'] = $gameTime?->id;
        $observation = Observation::query()->create($data);
        if (!empty($validate['image'])) {
            $path = Storage::disk('s3')->put('images', $validate['image']);
            $observation->update(['image' => $path]);
        }
        $id = $observation->park_id != null ? $observation->park_id : $observation->ride?->park_id;
        if ($id != null) {
            $department = Department::find($data['department_id']);

            if ($department->name == 'Maintenance') {
                $roles = ['Maintenance/Supervisor', 'Maintenance/Manager', 'Maintenance/Director', 'Park Admin'];
            } elseif ($department->name == 'Health &safety') {
                $roles = ['Health & safety/Supervisor', 'Health & safety/Manager', 'Health & safety/Director'];
            } elseif ($department->name == 'Technical') {
                $roles = ['Technical Services/Engineer', 'Technical Services/Manager', 'Technical Services/Director'];
            } elseif ($department->name == 'Client') {
                $roles = ['Client'];
            } else {
                $roles = ['Park Admin'];
            }
            $users = User::whereHas('roles', function ($query) use ($roles) {
                return $query->whereIn('name', $roles);
            })
                ->whereHas('parks', function ($query) use ($id) {
                    return $query->where('park_id', $id);
                })
                ->get();
            $data = [
                'title' => 'New observations added to ' . $ride->name,
                'ride_id' => $ride->_id,
                'user_id' => Auth::user()->id,
            ];
            foreach ($users as $user) {
                Notification::send($user, new UserNotifications($data));
            }
        }

        return self::apiResponse(200, __('Observation added successfully'), []);
    }

    protected function questions()
    {
        $questions = GeneralQuestion::get();
        $this->body['questions'] = QuestionsResource::collection($questions);

        return self::apiResponse(200, __('questions'), $this->body);
    }

    protected function addAttraction(Request $request)
    {
        $request->validate([
            'ride_id' => 'required|exists:rides,id',
            'park_time_id' => 'required|exists:park_times,id',
            'question_id' => 'required|array|exists:general_questions,id',
            'status' => 'required|array',
            'note' => 'nullable|array',
            'corrective_action' => 'nullable|array',
        ]);

        $ride = Ride::findOrFail($request->ride_id);
        $park_time = ParkTime::findOrFail($request->park_time_id);

        $attraction = Attraction::create([
            'ride_id' => $request->ride_id,
            'park_time_id' => $request->park_time_id,
            'date' => $park_time->date,
            'zone_id' => $ride->zone_id,
            'park_id' => $park_time->park_id,
            'datetime' => Carbon::now(),
            'created_by_id' => auth()->id(),
        ]);
        foreach ($request->question_id as $key => $value) {
            AttractionInfo::create([
                'attraction_id' => $attraction->id,
                'general_question_id' => $value,
                'status' => $request->status[$key] ?? null,
                'note' => $request->note[$key] ?? null,
                'corrective_action' => $request->corrective_action[$key] ?? null,
            ]);
        }
        $user = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Park Admin');
        })
            ->whereHas('parks', function ($q) use ($park_time) {
                return $q->where('park_id', $park_time->park_id);
            })
            ->first();

        $data = [
            'title' => $ride->park?->name . ' : ' . 'Attraction Audit check list added to ' . $ride->name,
            'ride_id' => $request->ride_id,
            'user_id' => Auth::user()->id,
        ];
        if ($user) {
            Notification::send($user, new UserNotifications($data));
            event(new showNotification($user->id, $data['title'], Carbon::now(), $park_time->id, $data['ride_id']));
        }

        return self::apiResponse(200, __('Attraction list added successfully'), []);
    }

    protected function getComplaint()
    {
        $complaints = CustomerComplaint::all();
        $this->body['complaints'] = ComplaintResource::collection($complaints);
        return self::apiResponse(200, __('show complaints'), $this->body);
    }

    protected function getCapacity($id)
    {
        if (!dateTime()) {
            return self::apiResponse(200, __('not found time slot'), []);
        }
        $items = RideCapacity::where('ride_id', $id)
            ->where(function ($query) {
                $query->whereBetween('date', [dateTime()->date, dateTime()->close_date]);
            })
            ?->get();

        if ($items) {
            $this->body['rides_capacity'] = RideCapacityResource::collection($items);
        } else {
            $this->body['rides_capacity'] = [];
        }

        return self::apiResponse(200, __('home page supervisor'), $this->body);
    }

    protected function updateRideCapacity(Request $request)
    {
        $request->validate([
            'id' => 'required|exists:ride_capacities,id',
            'final_capacity' => 'required',
        ]);

        $capacity = RideCapacity::find($request->id);
        if ($request->final_capacity > $capacity->ride_availablity_capacity) {
            return self::apiResponse(400, __('number of seats can not be bigger of availability capacity'), []);
        } else {
            $capacity->update(['final_capacity' => $request->final_capacity]);
            return self::apiResponse(200, __('update final capacity successfully'), []);
        }
    }

    protected function changeCapacityBySupervisor(Request $request)
    {
        $request->validate([
            'ride_id' => 'required|exists:rides,id',
            'park_id' => 'required|exists:parks,id',
            'final_capacity' => 'required',
        ]);
        $lastParkTime = ParkTime::where('park_id', $request->park_id)
            ->orderBy('id', 'desc')
            ->first();

        RideCapacity::query()->create([
            'final_capacity' => $request->final_capacity,
            'park_id' => $request->park_id,
            'ride_id' => $request->ride_id,
            'date' => $lastParkTime?->date ?? now()->toDateString(),
            'time' => Carbon::now()->toTimeString(),
        ]);

        return self::apiResponse(200, __('Final capacity changed successfully'), []);
    }
}
