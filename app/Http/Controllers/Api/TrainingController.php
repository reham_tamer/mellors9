<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Training\TrainingAdmin\TrainingAdminChecklistTypeResource;
use App\Http\Resources\Training\TrainingAdmin\TrainingAdminUserResource;
use App\Http\Resources\Training\TrainingResource;
use App\Http\Resources\Training\TrainingShowResource;
use App\Models\Attendance;
use App\Models\Training;
use App\Models\TrainingAddition;
use App\Models\TrainingUser;
use App\Models\TrainingUserChecklist;
use App\Models\User;
use App\Traits\Api\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class TrainingController extends Controller
{
    use ApiResponse;

    protected function operatorChecklist()
    {
        $trainings = Training::query()->whereUserType('operator')
            ->whereActiveApp(1)
            ->whereHas('trainingUsers.operators', function ($q) {
                $q->where('userable_id', auth()->id())->whereNull('user_signature');
            })->get();

        $this->body['trainings'] = TrainingResource::collection($trainings);
        return self::apiResponse(200, __('trainings'), $this->body);
    }

    protected function operatorChecklistShow(Training $training)
    {
        $training = Training::query()->find($training->id);
        $this->body['training'] = TrainingShowResource::make($training);
        return self::apiResponse(200, __('training'), $this->body);
    }

    protected function operatorChecklistStore(Request $request)
    {
        $validate = $request->validate([
            'training_checklist_item_id' => 'required|array|exists:training_checklist_items,id',
            'date' => 'required|array',
            'training_id' => 'required|exists:trainings,id',
            'signature' => 'nullable|image',
            'trainee_signature.*' => 'nullable|image',
        ]);

        auth()->user()->training_checklist_users()->delete();
        $trainingChecklistData = [];
        foreach ($validate['training_checklist_item_id'] as $key => $item) {
            $trainingChecklistData[] = [
                'training_checklist_item_id' => $item,
                'date' => $validate['date'][$key],
                'training_id' => $validate['training_id'],
                'trainee_signature' => Storage::disk('s3')->put('images', $validate['trainee_signature'][$key]) ?? null,
                'userable_id' => auth()->id(),
                'userable_type' => User::class
            ];
        }

        // Perform batch insertion
        auth()->user()->training_checklist_users()->insert($trainingChecklistData);
        return $this->extracted($validate, 'App\Models\User', auth()->user());
    }

    protected function attendanceChecklist(Request $request)
    {
        $request->validate([
            'code' => 'required|exists:attendances,code'
        ]);
        $attendance = Attendance::where('code', $request->code)->first();
        $trainings = Training::query()->where('type', 'training')->whereUserType('attendance')->whereActiveApp(1)
            ->whereHas('trainingUsers.attendances', function ($q) use ($attendance) {
                $q->where('userable_id', $attendance->id)->whereNull('user_signature');
            })->get();

        $this->body['trainings'] = TrainingResource::collection($trainings);
        return self::apiResponse(200, __('trainings'), $this->body);
    }

    protected function attendanceChecklistShow(Training $training, Request $request)
    {
        $request->validate([
            'code' => 'required|exists:attendances,code'
        ]);
        $training = Training::query()->find($training->id);
        $this->body['training'] = TrainingShowResource::make($training);
        return self::apiResponse(200, __('training'), $this->body);
    }

    protected function attendanceChecklistStore(Request $request)
    {
        $validate = $request->validate([
            'training_checklist_item_id' => 'required|array|exists:training_checklist_items,id',
            'date' => 'required|array',
            'training_id' => 'required|exists:trainings,id',
            'code' => 'required|exists:attendances,code',
            'signature' => 'nullable|image',
            'trainee_signature.*' => 'nullable|image',
        ]);
        $attendance = Attendance::whereCode($request->code)->first();
        $attendance->training_checklist_users()->delete();
        $trainingChecklistData = [];
        foreach ($validate['training_checklist_item_id'] as $key => $item) {
//            $filePath = 'images/' . basename($validate['trainee_signature'][$key]);

            $signature = isset($validate['trainee_signature'][$key]) ?  Storage::disk('s3')->put('images', $validate['trainee_signature'][$key]) : null;

            $trainingChecklistData[] = [
                'training_checklist_item_id' => $item,
                'date' => $validate['date'][$key],
                'training_id' => $validate['training_id'],
                'trainee_signature' => $signature,
                'userable_id' => $attendance->id,
                'userable_type' => Attendance::class
            ];
        }
        $attendance->training_checklist_users()->insert($trainingChecklistData);


        return $this->extracted($validate, 'App\Models\Attendance', $attendance);
    }

    protected function trainingAdmin()
    {
        $user = Auth::user();
        $role = $user->roles->first()?->name == 'Management/Operations Manager' || $user->roles->first()?->name == 'Operations/Zone supervisor'
                               || $user->roles->first()?->name =='Health & safety/Supervisor' || $user->roles->first()?->name =='Maintenance/Supervisor';
        if (!$role) {
            return self::apiResponse(200, __('you dont have permission to see this page'), []);
        }
        $user = Auth::user();
        $trainings = Training::query()->where('type', 'training')
            ->whereActiveApp(1)
            ->whereHas('trainingUsers', function ($q) {
//                $q->whereNotNull('user_signature');
            })->where(function ($q) use ($user) {
                $q->where('manager_id', $user->id)->orWhere('supervisor_id', $user->id);
            })->get();

        $this->body['trainings'] = TrainingResource::collection($trainings);
        return self::apiResponse(200, __('trainings'), $this->body);

    }

    protected function trainingAdminShow(Training $training)
    {
        $user = Auth::user();

        $trainings = $training->trainingUsers;
//            ?->whereNotNull('user_signature');
        if ($user->roles->first()?->name == 'Management/Operations Manager') {
            $trainings = $trainings->whereNull('operation_manager_signature')
                ->whereNotNull('supervisor_signature');
        } else {
            $trainings = $trainings->whereNull('supervisor_signature');
        }

        $this->body['trainings'] = TrainingAdminUserResource::collection($trainings);
        return self::apiResponse(200, __('trainings'), $this->body);

    }

    protected function trainingAdminShowUser(TrainingUser $training)
    {
        $this->body['trainings'] = TrainingAdminChecklistTypeResource::make($training);
        return self::apiResponse(200, __('trainings'), $this->body);
    }

    protected function trainingAdminStoreSignature(Request $request)
    {
        $request->validate([
            'training_id' => 'required|exists:trainings,id',
            'user_type' => 'required',
            'user_id' => 'required',
            'signature' => 'required|image',
        ]);

        $user = $request->user_type == 'operator' ? User::class : Attendance::class;
        $training = TrainingUser::query()->where('training_id', $request->training_id)
            ->where('userable_type', $user)->where('userable_id', $request->user_id)->first();

        if (isset($request->signature) && $request->signature) {
            $auth = Auth::user();

            $path = Storage::disk('s3')->put('images', $request->signature);
            if ($auth->roles->first()?->name == 'Management/Operations Manager') {
                $training->update(['operation_manager_signature' => $path]);
            } elseif ($auth->roles->first()?->name == 'Operations/Zone supervisor') {
                $training->update(['supervisor_signature' => $path]);
            } else {
                return self::apiResponse(200, __('you dont have permission'), []);
            }
        }
        return self::apiResponse(200, __('signature send successfully'), []);
    }

    /**
     * @param array $validate
     * @return \Illuminate\Http\JsonResponse
     */
    protected function extracted(array $validate, $type, $user): \Illuminate\Http\JsonResponse
    {
        if (isset($validate['signature']) && $validate['signature']) {
            $signature = TrainingUser::query()->where('training_id', $validate['training_id'])
                ->where('userable_type', $type)->where('userable_id', $user->id)->first();
            if (!$signature) {
                return self::apiResponse(404, __('user not found'), []);
            }
            $path = Storage::disk('s3')->put('images', $validate['signature']);
            $signature->update(['user_signature' => $path]);

        }
        return self::apiResponse(200, __('Store training checklist successfully'), []);
    }

    public function addComment(Request $request)
    {
        $validate = $request->validate([
            'training_checklist_item_id' => 'required|array|exists:training_checklist_items,id',
            'training_id' => 'required|exists:trainings,id',
            'user_type' => 'required|array',
            'user_id' => 'required|array',
            'trainer_comment' => 'required|array',
        ]);

        foreach ($validate['training_checklist_item_id'] as $key => $item) {
            $type = $validate['user_type'][$key] == 'operator' ? 'App\Models\User' : 'App\Models\Attendance';
             TrainingUserChecklist::query()->where('training_checklist_item_id', $item)
                ->where('userable_id', $validate['user_id'][$key])
                ->where('userable_type', $type)
                ->where('training_id', $validate['training_id'])
                 ->update(['trainer_comment'=> $validate['trainer_comment'][$key]]);
        }
        return self::apiResponse(200, __('Add comments successfully'), []);

    }
    public function addAdditions(Request $request)
    {
        $validate = $request->validate([
            'training_id' => 'required|exists:trainings,id',
            'user_type' => 'required',
            'user_id' => 'required',
            'addition' => 'required|array',
            'date' => 'nullable|array',
            'trainee_signature.*' => 'nullable|image',
            'trainer_comment' => 'required|array',
        ]);
        $type = $validate['user_type'] == 'operator' ? 'App\Models\User' : 'App\Models\Attendance';

        TrainingAddition::where('userable_id',$validate['user_id'])->where('userable_type',$type)
            ->where('training_id',$validate['training_id'])->delete();

        foreach ($validate['trainer_comment'] as $key => $item) {
               $signature = isset($validate['trainee_signature'][$key]) ?  Storage::disk('s3')->put('images', $validate['trainee_signature'][$key]) : null;
               TrainingAddition::query()->create([
                   'trainee_signature' => isset($validate['trainee_signature'][$key]) ? $signature : null,
                   'userable_id' => $validate['user_id'],
                   'userable_type' => $type,
                   'training_id' => $validate['training_id'],
                   'trainer_comment' => $item,
                   'addition' => $validate['addition'][$key],
                   'date' => $validate['date'][$key] ?? null,
               ]);
        }

        return self::apiResponse(200, __('Add Additions successfully'), []);

    }

}

