<?php

namespace App\Http\Controllers\Api;

use App\Events\Api\showNotification;
use App\Events\Api\StoppageApplication;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CustomerFeedbackRequest;
use App\Http\Requests\Api\DailyCheckListRequest;
use App\Http\Requests\Api\ObservationRequest;
use App\Http\Requests\Api\UpdateInspectionsRequest;
use App\Http\Resources\ComplaintResource;
use App\Http\Resources\User\Ride\ObservationCategoryResource;
use App\Http\Resources\User\Ride\PreopeningListResource;
use App\Http\Resources\User\Ride\QuestionsChecklistResource;
use App\Http\Resources\User\Ride\QuestionsResource;
use App\Http\Resources\User\Ride\RideCapacityResource;
use App\Http\Resources\User\Zone\ZoneResource;
use App\Http\Resources\User\InspectionResource;
use App\Http\Requests\Api\InspectionsRequest;
use App\Http\Requests\Api\ZoneInspectionsRequest;

use App\Models\Zone;
use App\Models\CheckListQuestion;
use App\Models\CustomerComplaint;
use App\Models\CustomerFeedbackImage;
use App\Models\CustomerFeedbacks;
use App\Models\DailyOperational;
use App\Models\DailyOperationalInfo;
use App\Models\DailyOperationalRide;
use App\Models\Department;
use App\Models\ZoneInspectionInfo;
use App\Models\GeneralQuestion;
use App\Models\Observation;
use App\Models\ObservationCategory;
use App\Models\ParkTime;
use App\Models\PreopeningList;
use App\Models\Ride;
use App\Models\ZoneInspection;
use App\Models\ZoneInspectionList;
use App\Models\User;
use App\Notifications\StoppageNotifications;
use App\Notifications\UserNotifications;
use App\Traits\Api\ApiResponse;
use App\Traits\Api\NotificationApi;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class ZoneCheckListController extends Controller
{
    use ApiResponse;

    protected function home()
    {
        $user = Auth::user();
        $this->body['zones'] = ZoneResource::collection($user->zones);
        return self::apiResponse(200, __('home page'), $this->body);
    }
    protected function zonePreopening($id)
    {
        $inspects = ZoneInspectionList::where('zone_id', $id)->where('lists_type', 'preopening')->get();
        $this->body['inspections'] = InspectionResource::collection($inspects);
        return self::apiResponse(200, __('inspections'), $this->body);
    }

    protected function zonePreclosing($id)
    {
        $inspects = ZoneInspectionList::where('zone_id', $id)->where('lists_type', 'preclosing')->get();
        $this->body['inspections'] = InspectionResource::collection($inspects);
        return self::apiResponse(200, __('inspections'), $this->body);
    }

    protected function zoneInspectionList($id)
    {
        $inspects = ZoneInspectionList::where('zone_id', $id)->where('lists_type', 'inspection')->get();
        $this->body['inspections'] = InspectionResource::collection($inspects);
        return self::apiResponse(200, __('inspections'), $this->body);
    }
    protected function zoneDailyList($id)
    {
        $inspects = ZoneInspectionList::where('zone_id', $id)->where('lists_type', 'daily')->get();
        $this->body['inspections'] = InspectionResource::collection($inspects);
        return self::apiResponse(200, __('inspections'), $this->body);
    }
    protected function storeZoneInspection(ZoneInspectionsRequest $request)
    {
        $validate = $request->validated();

        if ($validate['lists_type'] !== 'daily') {
            $info = ZoneInspectionInfo::query()
                ->where('zone_id', $validate['zone_id'])
                ->where('type', $validate['lists_type'])
                ->where('opened_date', Carbon::now()->toDateString())
                ->first();
        
            if ($info) {
                return self::apiResponse(200, __('you already send your data'), []);
            }
        }
        
        $zone=Zone::find($validate['zone_id']);
        $park_id =$zone->park_id;
 
        \DB::beginTransaction();

        $list = ZoneInspectionInfo::query()->create([
            'type' => $validate['lists_type'],
            'zone_id' => $validate['zone_id'],
            'park_id' => $park_id,
            'opened_date' => Carbon::now()->toDateString(),
            'user_id' => \auth()->user()->id,
        ]);
        if (!empty($validate['image'])) {
            $path = Storage::disk('s3')->put('images', $validate['image']);
            $list->update(['image' => $path]);
        }
        foreach ($validate['inspection_list_id'] as $key => $inspection) {
            ZoneInspection::query()->create([
                'comment' => $validate['comment'][$key] ?? null,
                'inspection_list_id' => $inspection,
                'status' => $validate['status'][$key] ?? null,
                'is_checked' => $validate['is_checked'][$key] ?? null,
                'zone_inspection_infos_id' => $list->id,
            ]);
        }
        DB::commit();

        return self::apiResponse(200, __('inspections created successfully'), []);
    }
     

}
