<?php

namespace App\Http\Controllers\Api;

use App\Events\Api\QueueEvent;
use App\Events\Api\showNotification;
use App\Events\RideCapacityEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\InspectionsRequest;
use App\Http\Requests\Api\SubmitCycleRequest;
use App\Http\Requests\Api\SubmitQueuesRequest;
use App\Http\Requests\Api\UpdateCycleDurationRequest;
use App\Http\Requests\Api\UpdateCycleRequest;
use App\Http\Requests\Dashboard\Attendance\AttendanceRequest;
use App\Http\Resources\User\InspectionResource;
use App\Http\Resources\User\Ride\QueueInfoResource;
use App\Http\Resources\User\Ride\RideCycleResource;
use App\Http\Resources\User\Ride\RideInfoResource;
use App\Http\Resources\User\Ride\RideQueueResource;
use App\Http\Resources\User\Ride\RideResource;
use App\Http\Resources\User\Ride\StoppageResource;
use App\Http\Resources\User\TimeSlotResource;
use App\Http\Resources\User\UserLogResource;
use App\Jobs\SendCycle;
use App\Jobs\SendQueue;
use App\Jobs\StoreInspection;
use App\Models\Attendance;
use App\Models\AttendanceLog;
use App\Models\GameTime;
use App\Models\MobileVersion;
use App\Models\Park;
use App\Models\ParkTime;
use App\Models\PreopenInfo;
use App\Models\PreopeningList;
use App\Models\Queue;
use App\Models\Ride;
use App\Models\RideCapacity;
use App\Models\RideCycles;
use App\Models\RideInspectionList;
use App\Models\RideStoppages;
use App\Models\StopageCategory;
use App\Models\User;
use App\Models\UserLog;
use App\Traits\Api\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
class RideController extends Controller
{
    use ApiResponse;

    protected function home()
    {
        $user = Auth::user();

        $this->body['rides'] = RideResource::collection($user->rides);
        return self::apiResponse(200, __('home page'), $this->body);
    }
    protected function versions(Request $request)
    {
        $request->validate([
            'device_type' => 'nullable',
            'build_no' => 'nullable',
        ]);
        $this->body['ios'] = MobileVersion::where('type','ios')->where('status',1)->pluck('version')->toArray() ;
        $this->body['android'] = MobileVersion::where('type','android')->where('status',1)->pluck('version')->toArray() ;
        return self::apiResponse(200, __('mobile versions'), $this->body);
    }

    protected function ride($id)
    {
        $ride = Ride::find($id);

        if (!$ride) {
            return self::apiResponse(404, __('not found ride'), []);
        }
        $this->body['ride'] = RideInfoResource::make($ride);

        return self::apiResponse(200, __('home page'), $this->body);
                                             /* ->header("Cache-Control","max-age=300, public") */ //5 minutes
    }

    protected function ridePreopening($id)
    {
//        $inspects = RideInspectionList::where('ride_id', $id)->where('lists_type', 'preopening')->get();
        $inspects = Cache::remember('preopening_' . $id, 10000, function () use ($id) { //week
            // Fetch the collection from the database if not cached
            return RideInspectionList::where('ride_id', $id)->where('lists_type', 'preopening')->get();
        });

        $this->body['inspections'] = InspectionResource::collection($inspects);
        return self::apiResponse(200, __('inspections'), $this->body);
    }

    protected function ridePreclosing($id)
    {
        $inspects = Cache::remember('preclosing_' . $id, 10000, function () use ($id) { //week
            // Fetch the collection from the database if not cached
            return RideInspectionList::where('ride_id', $id)->where('lists_type', 'preclosing')->get();
        });
        $this->body['inspections'] = InspectionResource::collection($inspects);


        return self::apiResponse(200, __('inspections'), $this->body)->header("Cache-Control","max-age=10000, public");
    }

    protected function rideInspectionList($id)
    {
        $inspects = Cache::remember('inspection_' . $id, 10000, function () use ($id) { //week
            // Fetch the collection from the database if not cached
            return RideInspectionList::where('ride_id', $id)->where('lists_type', 'inspection_list')->get();
        });
        $this->body['inspections'] = InspectionResource::collection($inspects);
        return self::apiResponse(200, __('inspections'), $this->body)->header("Cache-Control","max-age=10000, public");
    }

    protected function storeInspection(InspectionsRequest $request)
    {
        $validate = $request->validated();

        if ($validate['lists_type'] == 'preclosing') {
            $gameTime = GameTime::where('ride_id', $validate['ride_id'])->orderBy('date', 'DESC')->latest()->first();
            $park_time_id = $gameTime?->park_time_id;
        } else {
            $park_time_id = $validate['park_time_id'];
        }
        $info = PreopenInfo::query()
            ->where('ride_id', $validate['ride_id'])
            ->where('park_time_id', $park_time_id)
            ->where('type', $validate['lists_type'])
            ->first();
        if ($info) {
            return self::apiResponse(200, __('you already send your data'), []);
        }
        $parkTime = ParkTime::find($park_time_id);
        $validate['opened_date'] = $parkTime?->date;
        $list = PreopenInfo::query()->create([
            'type' => $validate['lists_type'],
            'park_time_id' => $park_time_id ?? null,
            'zone_id' => $validate['zone_id'],
            'park_id' => $validate['park_id'],
            'ride_id' => $validate['ride_id'],
            'opened_date' => $validate['opened_date'],
            'operator_id' => \auth()->user()->id,
        ]);
        if (!empty($validate['image'])) {
            $path = Storage::disk('s3')->put('images', $validate['image']);
            $list->update(['image' => $path]);
        }
        $capacity = RideCapacity::where('date', Carbon::now()->toDateString())
            ->where('ride_id', $validate['ride_id'])
            ->first();
        if ($validate['lists_type'] == 'preopening' && $capacity != null) {
            if ($validate['number_of_seats'] != $capacity->ride_availablity_capacity) {
                if ($validate['number_of_seats'] > $capacity->ride_availablity_capacity) {
                    return self::apiResponse(400, __('number of seats can not be bigger of availability capacity'), []);
                }
            }
        }
        Ride::query()->where('id',$validate['ride_id'])->update([
            'test_cycles_count' => $validate['test_cycles_count']??null,
            'test_riders_count' => $validate['test_riders_count']??null,
        ]);

        $user = \auth()->user();
        $validate = Arr::except($validate, 'image');

        dispatch(new StoreInspection($validate, $user, $list));

        return self::apiResponse(200, __('inspections created successfully'), []);
    }

    protected function rideStatus()
    {
        $stoppages = StopageCategory::query()->get();
        $this->body['stoppages_categories'] = StoppageResource::collection($stoppages);

        return self::apiResponse(200, __('stoppages categories'), $this->body);
    }

    protected function addCycle(SubmitCycleRequest $request)
    {
        $validate = $request->validated();

        $ride = Ride::find($validate['ride_id']);

        $firstCycle = RideCycles::where('ride_id', $validate['ride_id'])
            ->where('park_time_id', ParkTime($ride->park_id)?->id)
            ->first();
        $validate['user_id'] = \auth()->user()->id;
        $validate['sales'] = $validate['number_of_ft'] * $ride->ride_price_ft + $validate['riders_count'] * $ride->ride_price;
        $parkTime = ParkTime::find($validate['park_time_id']);
        $validate['opened_date'] = $parkTime?->date;
        if ($ride->ride_type_id == 2) {
            //batch

            if (!$firstCycle) {
                //first
                $validate['duration_seconds'] = 0;
                dispatch(new SendCycle($validate));
            } else {
                $rider = RideCycles::where('ride_id', $validate['ride_id'])
                    ->where('duration_seconds', 0)
                    ->where('park_time_id', ParkTime($ride->park_id)?->id)
                    ?->latest()
                    ->first();
                if ($rider) {
                    $rider->duration_seconds = $validate['duration_seconds'];
                    //                    $rider->pressure_time = $validate['pressure_time'] ?? null;
                    $rider->save();
                }
                $validate['number_status'] = 1;
                $validate['duration_seconds'] = 0;
                dispatch(new SendCycle($validate));
            }
        } else {
            if (!$firstCycle) { //first
                dispatch(new SendCycle($validate));


                /*  $parktime = ParkTime($ride->park_id);
                 $start_time = Carbon::parse($validate['start_time']);
                 $parkDateTime = Carbon::parse("$parktime->date $parktime->start"); */
                /*    $duration = $parkDateTime->diffInSeconds($start_time);
                 $validate['duration_seconds'] = $duration; */
                // $validate['start_time'] = $parkDateTime;

                //                dispatch(new SendCycle($validate));
                //
            } else {
                $validate['number_status'] = 1;
                dispatch(new SendCycle($validate));
            }
        }

        return self::apiResponse(200, __('create ride cycle successfully'), []);
    }

    protected function getCycle(Request $request)
    {
        $validate = $request->validate([
            'ride_id' => 'required|exists:rides,id',
        ]);
        $ride = Ride::find($validate['ride_id']);
        $cycle = RideCycles::query()
            ->where('ride_id', $validate['ride_id'])
            ->where('park_time_id', ParkTime($ride->park_id)?->id)
            ->latest()
            ->first();
        if ($cycle) {
            $this->body['cycle_count'] = 1;
        } else {
            $this->body['cycle_count'] =
                $ride
                    ->cycle()
                    ->where('park_time_id', ParkTime($ride->park_id)?->id)
                    ->count() ?? 0;
        }
        return self::apiResponse(200, __('ride cycle'), $this->body);
    }

    protected function updateCycleCount(UpdateCycleRequest $request)
    {
        $validate = $request->validated();
        $cycle = RideCycles::query()->find($validate['id']);
        $ride = Ride::query()->find($cycle->ride_id);
        $validate['sales'] = $validate['number_of_ft'] * $ride->ride_price_ft + $validate['riders_count'] * $ride->ride_price;
        $cycle->update($validate);
        $this->body['cycle'] = RideCycleResource::make($cycle);

        $totalRiders = $cycle->riders_count + $cycle->number_of_disabled + $cycle->number_of_vip + $cycle->number_of_ft;
        $newRiders = $validate['number_of_ft'] + $validate['riders_count'] + $validate['number_of_vip'] + $validate['number_of_disabled'];
        if ($newRiders > $totalRiders) {
            $total = $newRiders - $totalRiders;
            event(new \App\Events\TotalRidersEvent($cycle->park_id, $total));
        }
        return self::apiResponse(200, __('update ride cycle successfully'), $this->body);
    }

    protected function updateCycleDuration(UpdateCycleDurationRequest $request)
    {
        $validate = $request->validated();
        $cycle = RideCycles::query()->find($validate['id']);
        $cycle->update(['duration_seconds' => $validate['duration_seconds']]);
        $this->body['cycle'] = RideCycleResource::make($cycle);

        return self::apiResponse(200, __('update ride cycle duration seconds successfully'), $this->body);
    }

    protected function addQueues(SubmitQueuesRequest $request)
    {
        $validate = $request->validated();
        $validate['user_id'] = \auth()->user()->id;
//        dispatch(new SendQueue($validate));
        $parkTime = ParkTime::find($validate['park_time_id']);
        $validate['opened_date'] = $parkTime?->date;
        $gameTime = GameTime::where('ride_id', $validate['ride_id'])->orderBy('date', 'DESC')->latest()->first();
        $validate['game_time_id'] = $gameTime?->id;

        $lastQueue = Queue::where('ride_id', $validate['ride_id'])->where('park_time_id', $parkTime->id)->latest()?->first();
        if ($lastQueue && $lastQueue->queue_seconds == 0) {
            $startTime = Carbon::parse($lastQueue->start_time);
            $endTime = Carbon::now();
            $totalDuration = (new Carbon($endTime))->diffInSeconds(new Carbon($startTime));
            $lastQueue->update(['queue_seconds' => $totalDuration]);
        }
        $queue = Queue::query()->create($validate);
        event(new \App\Events\RideQueueEvent($validate['ride_id'], 'active'));
        $this->body['queue'] = RideQueueResource::make($queue);


        //        $parkTime = ParkTime::find($validate['park_time_id']);
        //        $validate['opened_date'] = $parkTime?->date;
        //        $gameTime = GameTime::where('ride_id', $validate['ride_id'])->orderBy('date', 'DESC')->latest()->first();
        //        $validate['game_time_id'] = $gameTime?->id;
        //
        //        $lastQueue = Queue::where('ride_id', $validate['ride_id'])->where('park_time_id', $parkTime->id)->latest()?->first();
        //        if ($lastQueue && $lastQueue->queue_seconds == 0) {
        //            $lastQueue->update(['queue_seconds' => 2000]);
        //            Log::info("queue_old", [$lastQueue]);
        //
        //        }
        //        $queue = Queue::query()->create($validate);
        //        $this->body['queue'] = RideQueueResource::make($queue);
        //
        //        event(new \App\Events\RideQueueEvent($validate['ride_id'], 'active'));

        //        $data = [
        //            'title' =>  $ride->name .' queue starting now',
        //            'ride_id' => $ride->id,
        //            'action' => 'add_queue_to_ride',
        //            'user_id' => Auth::user()->id,
        //
        //        ];
        //         $adminParkRoles = ['Maintenance/Manager',
        //             'Technical Services/Manager', 'Management/Operations Manager',
        //             'Management/CEO', 'Management/General Manager', 'Client', 'Park Admin','Super Admin','Operations/Operator'
        //         ];
        //         $all = User::whereHas('roles', function ($query) use ($adminParkRoles) {
        //             return $query->whereIn('name', $adminParkRoles);
        //         })->whereHas('parks', function ($query) use ($ride) {
        //             return $query->where('park_id', $ride->park_id);
        //         })->get();

        //         foreach ($all as $user){
        //             event(new showNotification($user->id, 'event_queue', Carbon::now(), dateTime()?->id, $queue->ride_id));

        // //            event(new QueueEvent($user->id, $data['title'], $data['action'], $ride->id));
        // //            $new = new NotificationApi("$user->id", $data['title']);
        // //            $new->send();
        // }
        //         $supervisor = User::whereHas('roles', function ($query) {
        //             return $query->where('name', 'Operations/Zone supervisor');
        //         })->whereHas('zones', function ($query) use ($ride) {
        //             return $query->where('zone_id', $ride->zone->id);
        //         })->first();

        //         if($supervisor){
        //             event(new showNotification($supervisor->id, 'event_queue', Carbon::now(), dateTime()?->id, $queue->ride_id));
        // //            $new = new NotificationApi("$supervisor->id", 'queue starting now for ride ' .$queue->ride?->name);
        // //            $new->send();
        //         }
        return self::apiResponse(200, __('queue added successfully'), $this->body);
    }

    protected function stopQueues(Request $request)
    {
        $validate = $request->validate([
            //            'id' => 'required|exists:queues,id',
            //            'queue_seconds' => 'nullable',
            'ride_id' => 'required|exists:rides,id',
            'park_time_id' => 'required||exists:park_times,id',
        ]);

        $lastQueue = Queue::where('ride_id', $validate['ride_id'])
            ->where('park_time_id', $validate['park_time_id'])
            ->where('queue_seconds', 0)
            ->latest()
            ?->first();
        if ($lastQueue) {
            $startTime = Carbon::parse($lastQueue->start_time);
            $endTime = Carbon::now();
            $totalDuration = (new Carbon($endTime))->diffInSeconds(new Carbon($startTime));
            $lastQueue->update(['queue_seconds' => $totalDuration]);
            $this->body['queue'] = RideQueueResource::make($lastQueue);
            event(new \App\Events\RideQueueEvent($lastQueue->ride_id, 'not-active'));
        } else {
            $this->body['queue'] = null;
        }

        //         $adminParkRoles = ['Maintenance/Manager',
        //             'Technical Services/Manager', 'Management/Operations Manager',
        //               'Management/CEO', 'Management/General Manager', 'Client', 'Park Admin','Super Admin'
        //         ];
        //         $all = User::whereHas('roles', function ($query) use ($adminParkRoles) {
        //             return $query->whereIn('name', $adminParkRoles);
        //         })->whereHas('parks', function ($query) use ($ride) {
        //             return $query->where('park_id', $ride->park_id);
        //         })->get();

        //         foreach ($all as $user){
        //             event(new showNotification($user->id, 'event_queue', Carbon::now(), dateTime()?->id, $queue->ride_id));

        // //            event(new showNotification($user->id, 'The queue has ended successfully for ride ' .$queue->ride?->name, Carbon::now(), dateTime()?->id, $queue->ride_id));
        // //            $new = new NotificationApi("$user->id", 'The queue has ended successfully for ride ' .$queue->ride?->name);
        // //            $new->send();
        //         }
        //         $ride = Ride::find($queue->ride_id);

        //         $supervisor = User::whereHas('roles', function ($query) {
        //             return $query->where('name', 'Operations/Zone supervisor');
        //         })->whereHas('zones', function ($query) use ($ride) {
        //             return $query->where('zone_id', $ride->zone->id);
        //         })->first();

        //         if($supervisor){
        //             event(new showNotification($user->id, 'event_queue', Carbon::now(), dateTime()?->id, $queue->ride_id));
        // //            event(new showNotification($supervisor->id, 'The queue has ended successfully for ride ' .$queue->ride?->name, Carbon::now(), dateTime()?->id, $queue->ride_id));
        // //            $new = new NotificationApi("$supervisor->id", 'The queue has ended successfully for ride ' .$queue->ride?->name);
        // //            $new->send();
        //         }
        return self::apiResponse(200, __('The queue has ended successfully'), $this->body);
    }


    protected function timeSlot()
    {
        $time = dateTime();
        $user = \auth()->user();
        $this->body['time_slot'] = null;
        $this->body['user_app'] = [
            'app_version' => $user->app_version,
            'app_build' => $user->app_build,
            'platform_type' => $user->platform_type,
            'device_model' => $user->device_model,
            'device_size' => $user->device_size ,
            'device_version' => $user->device_version ,
        ];
        if (!$time) {
            return self::apiResponse(200, __('not found time slot'), $this->body);
        }
        $this->body['time_slot'] = TimeSlotResource::make($time);

        return self::apiResponse(200, __('time slot info'), $this->body);
    }


    protected function getQueues($id)
    {
        $ride = Ride::find($id);
        if (!$ride) {
            return self::apiResponse(200, __('not found ride'), $this->body);
        }
        $queues = $ride
            ->queue()
            ?->where('park_time_id', ParkTime($ride->park_id)?->id)
            ->get();

        $this->body['queues'] = QueueInfoResource::collection($queues);
        return self::apiResponse(200, __('queues'), $this->body);
    }

//    protected function userLog($user)
//    {
//        $role = $user->roles->first()?->name == 'Operations/Operator';
//        $log = UserLog::where('user_id', $user->id)
//            ->latest()
//            ->first();
//        if ($role && $log && $log->type == 'login') {
//            $startTime = Carbon::parse($log->time);
//            $finishTime = Carbon::now()->toTimeString();
//            $data['ride_id'] = $user->rides[0]->id;
//            $data['shift_hours'] = $startTime->diffInHours($finishTime);
//            $log->update(['shift_hours' => $data['shift_hours'], 'type' => 'preclosing']);
//            $stoppageAllDay = RideStoppages::query()
//                ->where('ride_id', $log->ride_id)
//                ->where('type', 'all_day')
//                ->where('park_time_id', ParkTime($log->park_id)?->id)
//                ->first();
//            if ($stoppageAllDay) {
//                $log->update(['type' => 'off', 'shift_hours' => 0, 'stoppage_hours' => 0, 'end_time' => Carbon::now()->toTimeString()]);
//            } else {
//                $stoppages = RideStoppages::query()
//                    ->where('ride_id', $log->ride_id)
//                    ->where('park_time_id', ParkTime($log->park_id)?->id)
//                    ->sum('down_minutes');
//                $stoppagesHours = $stoppages / 60;
//                $totalHours = $data['shift_hours'] - $stoppagesHours;
//                $log->update(['type' => 'logout', 'stoppage_hours' => $stoppagesHours >= 1 ? $stoppagesHours : 0, 'total_hours' => $totalHours >= 1 ? $totalHours : 0, 'end_time' => Carbon::now()->toTimeString()]);
//            }
//        }
//    }

    protected function userLogDay(Request $request)
    {
        Log::info('user Log Day');

        $validate = $request->validate([
            'park_id' => 'required|exists:parks,id',
            'date' => 'required|date',
            'user_id' => 'required|exists:users,id',
            'key' => 'required',
        ]);
        if ($validate['key'] != 'mellors-api-2661555') {
            return self::apiResponse(401, __('wrong key value'), []);
        }

        $log = UserLog::query();
        if ($request->user_id != null) {
            $log = $log->where('user_id', $request->user_id);
        }
        if ($request->park_id != null) {
            $log = $log->where('park_id', $request->park_id);
        }
        if ($request->date != null) {
            $log = $log->where('open_date', $request->date);
        }

        $this->body['operation_minutes'] = $log?->whereIn('type', ['logout', 'off', 'preclosing'])?->sum('shift_minutes') . ' M';
        return self::apiResponse(200, __('users log'), $this->body);
    }

    protected function allParkUsersLog(Request $request)
    {
        Log::info('all Park Users Log');

        $validate = $request->validate([
            'park_id' => 'required|exists:parks,id',
            'date' => 'required|date',
            'key' => 'required',
        ]);
        if ($validate['key'] != 'mellors-api-2661555') {
            return self::apiResponse(401, __('wrong key value'), []);
        }

        $log = UserLog::query();

        if ($request->park_id != null) {
            $log = $log->where('park_id', $request->park_id);
        }
        if ($request->date != null) {
            $log = $log->where('open_date', $request->date);
        }

        $logDetails = $log->select('user_id', 'ride_id', DB::raw('SUM(shift_minutes) as total_shift_minutes'))
            ->groupBy('user_id', 'ride_id')
            ->get();

        $result = [];
        foreach ($logDetails as $detail) {
            $userId = $detail->user_id;
            $rideId = $detail->ride_id;
            $shiftMinutes = $detail->total_shift_minutes;

            if (!isset($result[$userId])) {
                $result[$userId] = [
                    'user_id' => $userId,
                    'rides' => []
                ];
            }

            $result[$userId]['rides'][] = [
                'ride_id' => $rideId,
                'shift_minutes' => $shiftMinutes
            ];
        }

        $this->body['log_details'] = array_values($result);

        return self::apiResponse(200, __('users log'), $this->body);
    }

    protected function allParkAttendancesLog(Request $request)
    {
        Log::info('all Park Attendances Log');

        $validate = $request->validate([
            'park_id' => 'required|exists:parks,id',
            'date' => 'required|date',
            'key' => 'required',
        ]);
        if ($validate['key'] != 'mellors-api-2661555') {
            return self::apiResponse(401, __('wrong key value'), []);
        }

        $log = AttendanceLog::query();

        if ($request->park_id != null) {
            $log = $log->where('park_id', $request->park_id);
        }
        if ($request->date != null) {
            $log = $log->where('open_date', $request->date);
        }

        $logDetails = $log->select('attendance_id', 'ride_id', DB::raw('SUM(shift_minutes) as total_shift_minutes'))
            ->groupBy('attendance_id', 'ride_id')
            ->get();

        $result = [];
        foreach ($logDetails as $detail) {
            $attendanceId = $detail->attendance_id;
            $rideId = $detail->ride_id;
            $shiftMinutes = $detail->total_shift_minutes;

            if (!isset($result[$attendanceId])) {
                $result[$attendanceId] = [
                    'attendant_id' => $attendanceId,
                    'rides' => []
                ];
            }

            $result[$attendanceId]['rides'][] = [
                'ride_id' => $rideId,
                'shift_minutes' => $shiftMinutes
            ];
        }

        $this->body['log_details'] = array_values($result);

        return self::apiResponse(200, __('users log'), $this->body);
    }

    protected function attendanceLogDay(Request $request)
    {
        Log::info('attendance Log Day');

        $validate = $request->validate([
            'park_id' => 'required|exists:parks,id',
            'date' => 'required|date',
            'attendant_id' => 'required|exists:attendances,id',
            'key' => 'required',
        ]);
        if ($validate['key'] != 'mellors-api-2661555') {
            return self::apiResponse(401, __('wrong key value'), []);
        }

        $log = AttendanceLog::query();
        if ($request->attendant_id != null) {
            $log = $log->where('attendance_id', $request->attendant_id);
        }

        if ($request->park_id != null) {
            $log = $log->where('park_id', $request->park_id);
        }

        if ($request->date != null) {
            $log = $log->where('open_date', $request->date);
        }
        $this->body['attendant_minutes'] = $log?->where('type', 'logout')?->sum('shift_minutes') .' M';
        return self::apiResponse(200, __('attendants log'), $this->body);
    }

    protected function parks(Request $request)
    {
        Log::info('parks');
        $validate = $request->validate([
            'key' => 'required',
        ]);

        if ($validate['key'] != 'mellors-api-2661555') {
            return self::apiResponse(401, __('wrong key value'), []);
        }
        $parks = Park::query()->select('id', 'name')->get();


        $this->body['parks'] = $parks;

        return self::apiResponse(200, __('parks info'), $this->body);
    }

    protected function operators(Request $request)
    {
        Log::info('operators');

        $validate = $request->validate([
            'key' => 'required',
        ]);

        if ($validate['key'] != 'mellors-api-2661555') {
            return self::apiResponse(401, __('wrong key value'), []);
        }
        $users = User::whereHas('roles', function ($query) {
            $query->whereIn('name', ['Operations/Operator', 'Breaker']);
        })->select('name', 'id','code')->get();


        $this->body['operators'] = $users;
        return self::apiResponse(200, __('operators info'), $this->body);
    }

    protected function attendants(Request $request)
    {
        Log::info('attendants');

        $validate = $request->validate([
            'key' => 'required',
        ]);

        if ($validate['key'] != 'mellors-api-2661555') {
            return self::apiResponse(401, __('wrong key value'), []);
        }
        $attendances = Attendance::select('name', 'id','code')
            ->get();

        $this->body['attendants'] = $attendances;
        return self::apiResponse(200, __('attendants info'), $this->body);
    }

    protected function rides_by_park(Request $request, $park_id)
    {
        Log::info('rides_by_park');
        $validate = $request->validate([
            'key' => 'required',
        ]);
        $park = Park::query()->find($park_id);
        if (!$park) {
            return self::apiResponse(401, __('park not found'), []);
        }

        if ($validate['key'] != 'mellors-api-2661555') {
            return self::apiResponse(401, __('wrong key value'), []);
        }

        $rides = Ride::query()
            ->where('park_id', $park_id)
            ->select('id', 'name','restraint_level')
            ->get();
        $this->body['rides'] = $rides;
        return self::apiResponse(200, __('rides'), $this->body);
    }

    protected function getLastQueue(Request $request)
    {
        $validate = $request->validate([
            'ride_id' => 'required|exists:rides,id',
        ]);
        if (dateTime() == null) {
            return self::apiResponse(400, __('time slot not found'));

        }
        $queue = Queue::query()
            ->where('ride_id', $validate['ride_id'])
            ->where('park_time_id', dateTime()->id)
            ->where('queue_seconds', 0)
            ->latest()
            ->first();
        if ($queue) {
            $this->body['queue'] = RideQueueResource::make($queue);
        } else {
            $this->body['queue'] = null;
        }
        return self::apiResponse(200, __('last queue'), $this->body);
    }

    protected function assignRide(Request $request)
    {
        $validate = $request->validate([
            'ride_id' => 'required|exists:rides,id',
        ]);
        $ride = Ride::query()->find($validate['ride_id']);
        $ride->current_user_id = \auth()->id();
        $ride->save();

        return self::apiResponse(200, __('send successfully'), []);
    }

}
