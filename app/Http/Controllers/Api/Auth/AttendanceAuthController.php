<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Attendance\AttendanceLoginRequest;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Resources\User\AttendanceLoginResource;
use App\Http\Resources\User\NotificationResource;
use App\Http\Resources\User\Ride\RideInfoResource;
use App\Http\Resources\User\Ride\RideResource;
use App\Http\Resources\User\UserResource;
use App\Models\Attendance;
use App\Models\AttendanceLog;
use App\Models\ParkTime;
use App\Models\RideStoppages;
use App\Models\UserLog;
use App\Traits\Api\ApiResponse;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AttendanceAuthController extends Controller
{
    use ApiResponse;

    public function login(AttendanceLoginRequest $request)
    {
        $validated = $request->validated();
        $user = Attendance::whereCode($validated['code'])->whereNationalId($validated['national_id'])->first();

        if ($user) {
            if ($user->active == 0) {
                return self::apiResponse(400, __('you dont have permission'));
            }
            $log = AttendanceLog::query()->where('attendance_id', auth()->id())->where('type', 'login')->first();
            if ($log) {
                $logStart = Carbon::parse("$log->date_time_start");
                $log->update([
                    'type' => 'logout',
                    'date_time_end' => Carbon::now(),
                    'shift_minutes' => $logStart->diffInMinutes(Carbon::now()),
                ]);
            }
            $this->message = __('login successfully');
            $this->body['attendance'] = AttendanceLoginResource::make($user);
            $this->body['rides'] = RideResource::collection($user->rides);
            $this->body['accessToken'] = $user->createToken('user-token')->plainTextToken;
            return self::apiResponse(200, $this->message, $this->body);
        } else {
            $this->message = __('auth failed');
            return self::apiResponse(400, $this->message);
        }

    }





}
