<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Resources\User\NotificationResource;
use App\Http\Resources\User\Ride\RideInfoResource;
use App\Http\Resources\User\UserResource;
use App\Models\ParkTime;
use App\Models\Ride;
use App\Models\RideStoppages;
use App\Models\UserLog;
use App\Traits\Api\ApiResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use ApiResponse;

    public function login(LoginRequest $request)
    {
        $validated = $request->validated();
        if (filter_var($validated['username'], FILTER_VALIDATE_EMAIL)) {
            $cred = ['email' => $validated['username'], 'password' => $validated['password']];
        } else {
            $cred = ['name' => $validated['username'], 'password' => $validated['password']];
        }
        if (Auth::attempt($cred)) {

            $user = Auth::user();
            if (!$user || $user->status == 0) {
                return self::apiResponse(400, __('you dont have permission'));
            }
            $data = Arr::except($validated,'password');
            $data = Arr::except($data,'username');
            $user->update($data);

            $role = $user->roles->first()?->name == 'Operations/Operator' || $user->roles->first()?->name == 'Breaker';

            if ($role) {
                if ($user->rides?->first() && isset($user->rides[0])) {
                    $this->body['ride'] = RideInfoResource::make($user->rides[0]);
                }
//                $this->userLog($user);
            }

            $this->message = __('login successfully');
            $this->body['user'] = UserResource::make($user);

            $this->body['accessToken'] = $user->createToken('user-token')->plainTextToken;
            return self::apiResponse(200, $this->message, $this->body);
        } else {
            $this->message = __('auth failed');
            return self::apiResponse(400, $this->message);
        }

    }

    public function logout()
    {
//        $user = auth()->user('sanctum');
//        $role = in_array($user->roles?->first()?->name, ['Operations/Operator', 'Breaker']);
//        $log = UserLog::where('user_id', $user->id)->latest()->first();
//        if ($role && $log && $log->type == 'login' && $log->shift_hours == 0) {
//            $stoppageAllDay = RideStoppages::query()->where('ride_id', $log->ride_id)->where('type', 'all_day')
//                ->where('park_time_id', ParkTime($log->park_id)?->id)->first();
//            if ($stoppageAllDay) {
//                $log->update(['type' => 'off', 'shift_minutes' => 0, 'end_time' => Carbon::now(), 'park_id' => $user->rides->first()?->park_id]);
//            } else {
//                $parks = auth()->user()->parks->pluck('id')->toArray();
//                $park = ParkTime::whereIn('park_id', $parks)->latest()->first();
//                if ($log->open_date == null) {
//                    $log->update(['open_date' => $park?->date]);
//                }
//                $logDate = Carbon::parse("$log->date $log->time");
//                $parkDate = Carbon::parse("$park?->date $park?->start");
//
//                $result = $logDate->gt($parkDate);
//                if ($result) {
//                    $startTime = Carbon::parse("$log->date $log->time");
//                } else {
//                    $startTime = Carbon::parse("$park?->date $park?->start");
//                }
//                if (Carbon::parse("$park?->close_date $park?->end")->gt(Carbon::now())) {
//                    $endTime = Carbon::now();
//                } else {
//                    $endTime = Carbon::parse("$park?->close_date $park?->end");
//                }
//                if ($log && $log->open_date == null) {
//                    $time = ParkTime::whereIn('park_id', $parks)->orderBy('id', 'DESC')->first();
//                    $log->update(['open_date' => $time->date]);
//                }
//                $shift_minutes = $startTime->diffInMinutes($endTime);
//                $log->update(['type' => 'logout',
//                    'shift_minutes' => $shift_minutes >= 1 ? $shift_minutes : 0, 'end_time' => $endTime
//                    , 'park_id' => $user->rides->first()?->park_id]);
//            }
//        }
        auth()->user('sanctum')->tokens()->delete();
        $this->message = __('Logged out successfully');
        return self::apiResponse(200, $this->message, []);

    }

    protected function notifications()
    {
        $this->body['notifications'] = NotificationResource::collection(auth()->user()->notifications()?->paginate(10));
        $this->body['count_of_pages'] = auth()->user()->notifications()?->paginate(10)?->lastPage();
        return self::apiResponse(200, 'notifications', $this->body);

    }

    private function userLog($user)
    {
        $userTimezone = $user->time_zone;
        $currentTime = Carbon::now();
        $currentTime->setTimezone($userTimezone);

        $data = ['user_id' => $user->id, 'shift_minutes' => 0,
            'type' => 'login', 'date' => $currentTime->toDateString(), 'time' => $currentTime->toTimeString()];
        $data['ride_id'] = $user->rides->first()?->id;
        $data['park_id'] = $user->rides->first()?->park_id;
        $data['open_date'] = dateTime()?->date;
        $last = UserLog::query()->where('user_id', $user->id)->where('type', 'login')
            ->where('open_date', dateTime()?->date)->first();
        if (!$last) {
            UserLog::query()->create($data);
        }
    }

    public function timeLogin(Request $request)
    {
        $user = \auth()->user();
        $userTimezone = $user->time_zone;
        $currentTime = Carbon::now();
        $currentTime->setTimezone($userTimezone);

        $data = ['user_id' => $user->id, 'shift_minutes' => 0,
            'type' => 'login', 'date' => $currentTime->toDateString(), 'time' => $currentTime->toTimeString()];
        $data['ride_id'] = $request->ride_id ?? $user->rides->first()?->id;
        $ride = Ride::find($data['ride_id']);
        $data['park_id'] = $ride?->park_id;
        $data['open_date'] = dateTime()?->date ?? Carbon::now()->toDateString();
        $last = UserLog::query()->where('user_id', $user->id)->where('type', 'login')
            ->where('open_date', dateTime()?->date)->first();
        if (!$last) {
            UserLog::query()->create($data);
        }
        return self::apiResponse(200, __('save time successfully'), []);
    }
    public function timeLogout()
    {
        $user = auth()->user('sanctum');
        $role = in_array($user->roles?->first()?->name, ['Operations/Operator', 'Breaker']);
        $log = UserLog::where('user_id', $user->id)->latest()->first();
        if ($role && $log && $log->type == 'login' && $log->shift_hours == 0) {
            $stoppageAllDay = RideStoppages::query()->where('ride_id', $log->ride_id)->where('type', 'all_day')
                ->where('park_time_id', ParkTime($log->park_id)?->id)->first();
            if ($stoppageAllDay) {
                $log->update(['type' => 'off', 'shift_minutes' => 0, 'end_time' => Carbon::now(), 'park_id' => $user->rides->first()?->park_id]);
            } else {
                $parks = auth()->user()->parks->pluck('id')->toArray();
                $park = ParkTime::whereIn('park_id', $parks)->latest()->first();
                if ($log->open_date == null) {
                    $log->update(['open_date' => $park?->date]);
                }
                $logDate = Carbon::parse("$log->date $log->time");
                $parkDate = Carbon::parse("$park?->date $park?->start");

                $result = $logDate->gt($parkDate);
                if ($result) {
                    $startTime = Carbon::parse("$log->date $log->time");
                } else {
                    $startTime = Carbon::parse("$park?->date $park?->start");
                }
                if (Carbon::parse("$park?->close_date $park?->end")->gt(Carbon::now())) {
                    $endTime = Carbon::now();
                } else {
                    $endTime = Carbon::parse("$park?->close_date $park?->end");
                }
                if ($log && $log->open_date == null) {
                    $time = ParkTime::whereIn('park_id', $parks)->orderBy('id', 'DESC')->first();
                    $log->update(['open_date' => $time->date]);
                }
                $shift_minutes = $startTime->diffInMinutes($endTime);
                if($shift_minutes > 1000){
                    $shift_minutes = 0;
                }
                $log->update(['type' => 'logout',
                    'shift_minutes' => $shift_minutes >= 1 ? $shift_minutes : 0, 'end_time' => $endTime
                    , 'park_id' => $user->rides->first()?->park_id]);
            }
        }
        return self::apiResponse(200, __('save time successfully'), []);
    }

}
