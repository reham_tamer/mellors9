<?php

namespace App\Http\Controllers\Admin;


use App\Models\User;
use App\Notifications\UserNotifications;
use Carbon\Carbon;
use App\Models\Park;
use App\Models\IncidentImage;
use App\Models\Ride;
use App\Models\Zone;
use App\Models\IncidentStatement;
use App\Models\Accident;
use App\Models\GameTime;
use App\Models\ParkTime;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Models\GeneralIncident;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Accident\AccidentRequest;
use App\Http\Requests\Dashboard\Accident\IncidentRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use App\Traits\ImageOperations;

class GeneralIncidentController extends Controller
{
    use ImageOperations;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'open incident index';
        $action->save(); 


        if (auth()->user()->hasRole('Super Admin')) {
            $userparks = Park::pluck('name', 'id');
            $parks = Park::pluck('id');
        } else {
            $userparks = auth()->user()->parks->pluck('name', 'id');
            $parks = auth()->user()->parks->pluck('id');
        }

        $startDate = dateTime()?->date;
        $endDate = dateTime()?->close_date;
        $startTime = dateTime()?->start;
        $endTime = dateTime()?->end;
//dd( $endTime);

        if ($startDate && $endDate && $startTime && $endTime) {
            $startDateTime = "{$startDate} {$startTime}";
            $endDateTime = "{$endDate} {$endTime}";
            //   dd($startDateTime);

            $items = GeneralIncident::where('type', 'incident')->whereIn('park_id', $parks)
                ->where(function ($query) use ($startDateTime, $endDateTime) {
                    $query->whereBetween('date', [$startDateTime, $endDateTime]);
                })
                ->get();
//dd( $items);
            return view('admin.general_incident.index', compact('items', 'userparks'));
        } else {
            $items = GeneralIncident::where('type', 'incident')->whereIn('park_id', $parks)
                ->whereDate('date', Carbon::now()->toDateString())->get();
//dd($items);
            return view('admin.general_incident.index', compact('items', 'userparks'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
            $zones = Zone::pluck('name', 'id')->all();
            $rides = Ride::pluck('name', 'id')->all();

        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
            $zones = auth()->user()->zones->pluck('name', 'id')->all();
            $rides = auth()->user()->rides->pluck('name', 'id')->all();
        }
        $departments = Department::pluck('name', 'id')->all();
        return view('admin.general_incident.add', compact('departments', 'parks', 'zones', 'rides'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(IncidentRequest $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add incident';
        $action->save(); 

        $data = $request->validated();
        $incident = GeneralIncident::create([
            'type' => 'incident',
            'status' => 'pending',
            'date' => $data['date'],
            'created_by_id' => auth()->user()->id,
            'value' => $data
        ]);
        if ($request['choose'] == 'ride') {
            $incident->ride_id = $data['ride_id'];
            $incident->park_id = $data['park_id'];
            $gameTime = GameTime::where('ride_id', $data['ride_id'])->where('date',$data['date'])->first();
            $incident->game_time_id = $gameTime?->id;
        }
        if ($request['choose'] == 'park') {
            $incident->park_id = $data['park_id'];
            $incident->ride_id = null;
            $incident->text = $data['text'];
        }
        $parkTime = ParkTime::where('park_id', $data['park_id'])->where('date',$data['date'])->first();
        $incident->park_time_id = $parkTime?->id;
        $incident->save();
        if ($request->has('images')) {
            $this->Gallery($request, new IncidentImage(), ['general_incident_id' => $incident->id]);
        }
        $parkAdmins = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Park Admin');
        })->whereHas('parks', function ($query) use ($data) {
            return $query->where('park_id', $data['park_id']);
        })->get();
        $data = [
            'title' => 'New Incident added (' . $data['type_of_event'] . ')',
            'ride_id' => $data['ride_id'],
            'user_id' => Auth::user()->id,
            'time_id' => dateTime()?->id
        ];
        foreach ($parkAdmins as $user) {
            Notification::send($user, new UserNotifications($data));
        }
        alert()->success('Accident Incident  Report Added successfully !');
        return redirect()->route('admin.incident.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
            $zones = Zone::pluck('name', 'id')->all();
            $rides = Ride::pluck('name', 'id')->all();

        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
            $zones = auth()->user()->zones->pluck('name', 'id')->all();
            $rides = auth()->user()->rides->pluck('name', 'id')->all();
        }
        $departments = Department::pluck('name', 'id')->all();
        $accident = GeneralIncident::find($id);
        $album = $accident->album;

        return view('admin.general_incident.edit', compact('accident', 'album','departments', 'parks', 'zones', 'rides'));

    }

    public function show($id)
    {
        $departments = Department::pluck('name', 'id')->all();
        $accident = GeneralIncident::find($id);
        $images = IncidentImage::where('general_incident_id', $id)->get();

        return view('admin.general_incident.show', compact('accident', 'departments','images'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(IncidentRequest $request, $id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update incident form no. '.$id;
        $action->save(); 

        // dd($request);
        $data = $request->validated();
        $incident = GeneralIncident::findOrFail($id);
        $incident->update([
            'value' => $request->validated(),
            'date' => $data['date']
        ]);
        if ($request['choose'] == 'ride') {
            $incident->ride_id = $data['ride_id'];
            $incident->park_id = $data['park_id'];
        }
        if ($request['choose'] == 'park') {
            $incident->park_id = $data['park_id'];
            $incident->ride_id = null;
            $incident->text = $data['text'];
        }
        $incident->save();
        if ($request->has('images')) {
            $this->Gallery($request, new IncidentImage(), ['general_incident_id' => $incident->id]);
        }
        alert()->success('Accident / incident updated successfully !');
        return redirect()->route('admin.incident.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete incident no. '.$id;
        $action->save(); 

        $accident = GeneralIncident::find($id);
        if ($accident) {
            $incident_statements = IncidentStatement::where('incident_form_id', $id)->first();
            if ($incident_statements & $incident_statements != null) {
                $incident_statements->delete();
            }
            $accident->delete();
            alert()->success('Incident Report deleted successfully');
            return back();
        }
        alert()->error('Incident Report not found');
        return redirect()->route('admin.incident.index');
    }

    public function getImage(Request $request)
    {
        $x = $request->trCount;
        return view('admin.general_incident.append_images', compact('x'));
    }

    public function getZones(Request $request)
    {
        $html = '<option value=""> Select Zone</option>';

        $parks = Zone::where('park_id', $request->bark_id)->get();
        foreach ($parks as $park) {
            $html .= '<option value="' . $park->id . '">' . $park->name . '</option>';
        }
        return response()->json(['html' => $html]);
    }

    public function getRides(Request $request)
    {
        $html = '<option value="">Select Ride</option>';
        $parks = Park::find($request->park_id);

        foreach ($parks->rides as $ride) {
            $html .= '<option value="' . $ride->id . '">' . $ride->name . '</option>';
        }
        return response()->json(['html' => $html]);
    }

    public function approve($id)
    {
        $rsr = GeneralIncident::find($id);
        $rsr->status = 'approved';
        $rsr->approve_by_id = \auth()->user()->id;
        $rsr->save();
        alert()->success('Incident form Approved successfully !');
        return redirect()->back();
    }

    public function approve_director($id)
    {
        $rsr = GeneralIncident::find($id);
        $rsr->director_approve = 'approved';
        $rsr->approve_by_id = \auth()->user()->id;
        $rsr->save();
        alert()->success('Incident form Approved successfully !');
        return redirect()->back();
    }

    public function search(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'search on incident';
        $action->save(); 

        $items = [];

        $date = $request->query('date');
        $park_id = $request->query('park_id');
        //dd($date );
        if (auth()->user()->hasRole('Super Admin')) {
            $userparks = Park::pluck('name', 'id');
         } else {
            $userparks = auth()->user()->parks->pluck('name', 'id');
         }
        if ($park_id != null) {
            $timeSlot = ParkTime::where('date', $date)->where('park_id', $park_id)->first();
            $parks = auth()->user()->parks->pluck('id');
            if ($timeSlot != null) {
                $startDate = $timeSlot->date;
                $endDate = $timeSlot->close_date;
                $startTime = $timeSlot->start;
                $endTime = $timeSlot->end;

                $startDateTime = "{$startDate} {$startTime}";
                $endDateTime = "{$endDate} {$endTime}";

                $items = GeneralIncident::where('type', 'incident')->where('park_id', $park_id)
                    ->where(function ($query) use ($startDate, $endDate) {
//                        $query->whereBetween('date', [$startDateTime, $endDateTime]);
                        $query->whereDate('date', $startDate)->orWhereDate('date',$endDate);
                    })
                    ->get();
            }
            //  dd($items);
            return view('admin.general_incident.index', compact('items', 'userparks'));
        } else {
            $items = GeneralIncident::where('type', 'incident')->whereDate('date', $date)->get();
        }
        // $items=[];
        return view('admin.general_incident.index', compact('items', 'userparks'));
    }
}


