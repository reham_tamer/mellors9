<?php

namespace App\Http\Controllers\Admin;

use App\Models\HealthAndSafetyReport;
use App\Models\MaintenanceRideStatusReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\InspectionList\PreopeningListRequest;
use App\Models\InspectionList;
use App\Models\MaintenanceReport;
use App\Models\Park;
use App\Models\ParkTime;
use App\Models\PreopeningList;
use App\Models\RedFlag;
use App\Models\Ride;
use App\Models\RideStoppages;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class MaintenanceReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasRole('Super Admin')){
            $parks=Park::pluck('name','id')->all();
        }else{
            $parks=auth()->user()->parks->pluck('name','id')->all();
        }       return view('admin.reports.duty_report',compact('parks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
    }
    
    public function add_maintenace_report($park_id,$park_time_id)
    {
        $rides=Ride::where('park_id',$park_id)->get();
        return view('admin.maintenance_reports.add',compact('rides','park_id','park_time_id'));
    }

    public function edit_maintenance_report($park_time_id)
    {
        $items=MaintenanceReport::where('park_time_id',$park_time_id)->get();
        $ride_status_items=MaintenanceRideStatusReport::where('park_time_id',$park_time_id)->get();
        $redFlags=RedFlag::query()->where('park_time_id',$park_time_id)->where('type','maintenance')->get();
        return view('admin.maintenance_reports.edit',compact('items','ride_status_items','park_time_id','redFlags'));
    }
    public function update_request(Request $request)
    {
/*         dd($request->all());
 */
      $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update  maintenance report ';
        $action->save();

        DB::transaction(function () use ($request) {
            MaintenanceRideStatusReport::where('park_time_id', $request->park_time_id)->delete();
            RedFlag::where('park_time_id', $request->park_time_id)->where('type', 'maintenance')->delete();
            MaintenanceReport::where('park_time_id', $request->park_time_id)->delete();
    
            $timeSlot = ParkTime::findOrFail($request->park_time_id);
            $maintenanceReports = [];
            $rideStatuses = [];
            $redFlags = [];
    
            foreach ($request->question as $key => $value) {
                $maintenanceReports[] = [
                    'question' => $value,
                    'answer' => $request->answer[$key],
                    'comment' => $request->comment[$key],
                    'park_time_id' => $request->park_time_id,
                    'date' => $timeSlot->date,
                    'user_id' => auth()->user()->id,
                ];
            }
                foreach ($request->ride_id as $key => $value) {
                $rideStatuses[] = [
                    'ride_id' => $value,
                    'status' => $request->status[$key],
                    'comment' => $request->ride_comment[$key],
                    'park_time_id' => $request->park_time_id,
                    'date' => $timeSlot->date,
                ];
            }
                foreach ($request->ride as $key => $value) {
                if ($value != null) {
                    $redFlags[] = [
                        'ride' => $value,
                        'issue' => $request->issue[$key],
                        'park_time_id' => $request->park_time_id,
                        'type' => 'maintenance',
                        'date' => $timeSlot->date,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];
                }
            }
            if (!empty($maintenanceReports)) {
                MaintenanceReport::insert($maintenanceReports);
            }
            if (!empty($rideStatuses)) {
                MaintenanceRideStatusReport::insert($rideStatuses);
            }
            if (!empty($redFlags)) {
                RedFlag::insert($redFlags);
            }
        });
    
        // Success alert
        alert()->success('Maintenance Report Updated successfully!');
        return redirect()->route('admin.park_times.index');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'store maintenance report ';
        $action->save(); 

        $dateExists = MaintenanceReport::where([
            ['park_time_id',$request['park_time_id']],
            ['park_id', $request['park_id']]
        ])->first();
        if ($dateExists){
            return response()->json(['error'=>'Maintenance  Report Already Exist !']);
        }
      //dd($request->all());
         $timeSlot = ParkTime::findOrFail($request->park_time_id);
            $maintenanceReports = [];
            $rideStatuses = [];
            $redFlags = [];
    
            foreach ($request->question as $key => $value) {
                $maintenanceReports[] = [
                    'question' => $value,
                    'answer' => $request->answer[$key],
                    'comment' => $request->comment[$key],
                    'park_time_id' => $request->park_time_id,
                    'date' => $timeSlot->date,
                    'user_id' => auth()->user()->id,
                ];
            }
                foreach ($request->ride_id as $key => $value) {
                $rideStatuses[] = [
                    'ride_id' => $value,
                    'status' => $request->status[$key],
                    'comment' => $request->ride_comment[$key],
                    'park_time_id' => $request->park_time_id,
                    'date' => $timeSlot->date,
                ];
            }
                foreach ($request->ride as $key => $value) {
                if ($value != null) {
                    $redFlags[] = [
                        'ride' => $value,
                        'issue' => $request->issue[$key],
                        'park_time_id' => $request->park_time_id,
                        'type' => 'maintenance',
                        'date' => $timeSlot->date,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];
                }
            }
            if (!empty($maintenanceReports)) {
                MaintenanceReport::insert($maintenanceReports);
            }
            if (!empty($rideStatuses)) {
                MaintenanceRideStatusReport::insert($rideStatuses);
            }
            if (!empty($redFlags)) {
                RedFlag::insert($redFlags);
            }
        return response()->json(['success'=>'Maintenance Report Added successfully']);

//        alert()->success('Preopening List Added successfully !');
//        return redirect()->back();
    }
    public function search(Request $request){
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'search maintenance report ';
        $action->save(); 

        $park_id=$request->input('park_id');
        $date=$request->input('date');
        $parkTime = ParkTime::query()
        ->where('park_id',$request->input('park_id'))
        ->Where('date', $request->input('date'))
        ->first();
        if (auth()->user()->hasRole('Super Admin')){
            $parks=Park::pluck('name','id')->all();
        }else{
            $parks=auth()->user()->parks->pluck('name','id')->all();
        }
        if($parkTime){
            $maintenance['a'] = MaintenanceReport::query()->where('park_time_id',$parkTime->id)
            ->where('question','Are all rides working for operations?')->first();
            $maintenance['b'] = MaintenanceReport::query()->where('park_time_id',$parkTime->id)
            ->where('question','Any concerns found during routine maintenace?')->first();
            $maintenance['c'] = MaintenanceReport::query()->where('park_time_id',$parkTime->id)
            ->where('question','Any new routine preventative maintenance checks added to Opera?')->first();
            $maintenance['d'] = MaintenanceReport::query()->where('park_time_id',$parkTime->id)
            ->where('question','Any issues with Maintenance App?')->first();
            $maintenance['e'] = MaintenanceReport::query()->where('park_time_id',$parkTime->id)
            ->where('question','Any evacuations during operation?')->first();
            $maintenance['f'] = MaintenanceReport::query()->where('park_time_id',$parkTime->id)
            ->where('question','Additionl comments?')->first();
            
        $maintenanceRideStatus=MaintenanceRideStatusReport::query()->where('park_time_id',$parkTime->id)->get();
        $redFlags=RedFlag::query()->where('park_time_id',$parkTime->id)->where('type','maintenance')->get();
        return view('admin.reports.duty_report', compact('maintenance','parks','redFlags','maintenanceRideStatus','park_id','date','parkTime'));
    }else
        return view('admin.reports.duty_report', compact('parks','park_id','date'));
    }
 
    public function show($id)
    {
        $items=MaintenanceReport::where('zone_id',$id)->get();

        return view('admin.preopening_lists.index',compact('items'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item=MaintenanceReport::find($id);
        return view('admin.maintenance_reports.edit',compact('item'));

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MaintenanceReport $maintenanceReport)
    {
        $maintenanceReport->update($request->all());
        $maintenanceReport->user_id=auth()->user()->id;
        $maintenanceReport->save();
        alert()->success('Maintenance Report updated successfully !');
        return redirect()->route('admin.park_times.index');    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete maintenance report ';
        $action->save(); 
        $items=MaintenanceReport::where('park_time_id',$id)->get();
            if ($items){
                foreach($items as $item){
                $item->delete();
            }
                alert()->success('This  Maintenance Report  deleted successfully');
            return back();
        }
        alert()->error('This Maintenance Report  not found');
        return redirect()->route('admin.park_times.index');
    }

    public function cheackMaintenance(Request $request)
    {
        $item=MaintenanceReport::where('park_time_id',$request->park_time_id)->first();
/*         dd($item);
 */        return response()->json(['item' => $item]);
    }
}
