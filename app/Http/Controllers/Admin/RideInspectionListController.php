<?php

namespace App\Http\Controllers\Admin;

use App\Models\UserPark;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\InspectionList\RideInspectionListRequest;
use App\Models\InspectionList;
use App\Models\Ride;
use Illuminate\Http\Request;
use App\Models\RideInspectionList;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;

class RideInspectionListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            if (auth()->user()->hasRole('Super Admin')) {
                $rides = Ride::all();
                $ride_inspect = Ride::all()->pluck('id');
            } else {
                $parks = auth()->user()->parks->pluck('id');
                // return $zones;
                $rides = Ride::wherein('park_id', $parks)->get();
                $ride_inspect = Ride::wherein('park_id', $parks)->pluck('id');
            }
            $inspection_data_exist = RideInspectionList::where('lists_type', 'inspection_list')
                ->wherein('ride_id', $ride_inspect)->distinct()->pluck('ride_id')->toArray();

            $preopening_data_exist = RideInspectionList::where('lists_type', 'preopening')
                ->wherein('ride_id', $ride_inspect)->distinct()->pluck('ride_id')->toArray();

            $preclosing_data_exist = RideInspectionList::where('lists_type', 'preclosing')
                ->wherein('ride_id', $ride_inspect)->distinct()->pluck('ride_id')->toArray();
            return Datatables::of($rides)
                ->addIndexColumn()
                ->addColumn('name', function ($row) {
                    return $row->name;
                })
                 ->addColumn('park_name', function ($row) {
                    return $row->park ? $row->park->name : '';
                })
                ->addColumn('inspection_lists', function ($row) use ($inspection_data_exist) {
                    if (in_array($row->id, $inspection_data_exist)) {
                        $btn = '<a href="' . url('edit_ride_inspection_lists/' . $row->id) . '">
                                        <button type="button" class="edit btn btn-success">
                                        <i class="fa fa-edit"></i> Edit
                                        </button>
                                    </a>';

                    } else {
                        $btn = ' <a href="' . url('add_ride_inspection_lists/' . $row->id) . '">
                                    <button type="button" class="add btn btn-info">
                                    <i class="fa fa-plus"></i> Add
                                    </button>
                                    </a>';//                    }else{

                    }

                    return $btn;
                })
                ->addColumn('preopening_lists', function ($row) use ($preopening_data_exist) {
                    if (in_array($row->id, $preopening_data_exist)) {
                        $btn = '<a href="' . url('edit_ride_preopen_elements/' . $row->id) . '">
                                        <button type="button" class="edit btn btn-success">
                                        <i class="fa fa-edit"></i> Edit
                                        </button>
                                    </a>';

                    } else {
                        $btn = ' <a href="' . url('add_ride_preopen_elements/' . $row->id) . '">
                                    <button type="button" class="add btn btn-info">
                                    <i class="fa fa-plus"></i> Add
                                    </button>
                                    </a>';//                    }else{

                    }

                    return $btn;
                })
                ->addColumn('preclosing_lists', function ($row) use ($preclosing_data_exist) {
                    if (in_array($row->id, $preclosing_data_exist)) {
                        $btn = '<a href="' . url('edit_ride_preclose_elements/' . $row->id) . '">
                                        <button type="button" class="edit btn btn-success">
                                        <i class="fa fa-edit"></i> Edit
                                        </button>
                                    </a>';

                    } else {
                        $btn = ' <a href="' . url('add_ride_preclose_elements/' . $row->id) . '">
                                    <button type="button" class="add btn btn-info">
                                    <i class="fa fa-plus"></i> Add
                                    </button>
                                    </a>';//                    }else{

                    }

                    return $btn;
                })

                ->rawColumns(['name','park_name', 'inspection_lists', 'preopening_lists', 'preclosing_lists'])
                ->make(true);
        }

        return view('admin.ride_inspection_lists.index_datatable');
    }

    public function create()
    {

    }

    public function add_ride_inspection_lists($ride_id)
    {
        $inspection_list = InspectionList::where('type', 'inspection')->get();
        return view('admin.ride_inspection_lists.add', compact('inspection_list', 'ride_id'));
    }

    public function edit_ride_inspection_lists($ride_id)
    {
        $ride = Ride::find($ride_id);
        $list = RideInspectionList::where('ride_id', $ride_id)
            ->where('lists_type', 'inspection_list')->pluck('inspection_list_id')->toArray();
        $inspection_list = InspectionList::where('type', 'inspection')->get();
        $rides = Ride::pluck('name', 'id')->toArray();
        return view('admin.ride_inspection_lists.edit', compact('inspection_list', 'ride', 'list', 'ride_id'));

    }


    public function store(RideInspectionListRequest $request)
    {
        
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add Ride Inspection Elements ';
        $action->save(); 

        $ride = Ride::find($request['ride_id']);
        $inspectionListIds = $request['inspection_list_id'];
        $ride->inspection_list()->attach($inspectionListIds, ['lists_type' => 'inspection_list']);
        alert()->success('Ride Inspection Elements Added Successfully !');
        return redirect()->route('admin.ride_inspection_lists.index');
    }


    public function edit($id)
    {


    }


    public function update_ride_inspection_lists(Request $request, $ride_id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update Ride Inspection Elements ';
        $action->save();

        RideInspectionList::where('ride_id', $request->ride_id)->where('lists_type', 'inspection_list')->delete();
        $ride = Ride::find($request['ride_id']);
        $inspectionListIds = $request['inspection_list_id'];
        $ride->inspection_list()->attach($inspectionListIds, ['lists_type' => 'inspection_list']);
        alert()->success('Ride Inspection List Updated Successfully !');
        return redirect()->route('admin.ride_inspection_lists.index');
    }

    public function update(RideInspectionListRequest $request, RideInspectionList $rideInspectionList)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete Ride Inspection Elements ';
        $action->save();

        $inspection_list = InspectionList::find($id);
        if ($inspection_list) {

            $inspection_list->delete();
            alert()->success('Inspection element deleted successfully');
            return back();
        }
        alert()->error('department not found');
        return redirect()->route('admin.inspection_lists.index');
    }

    public function cheackRideInspectionList(Request $request)
    {
        $item = RideInspectionList::where('ride_id', $request->ride_id)->first();
        /*         dd($item);
         */
        return response()->json(['item' => $item]);
    }
}
