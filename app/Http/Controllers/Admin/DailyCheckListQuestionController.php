<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\DailyCheckListRequest;
use App\Models\CheckListQuestion;
use App\Models\Park;

use App\Models\DailyOperational;
use App\Models\DailyOperationalInfo;
use App\Models\DailyOperationalRide;
use App\Models\DailyOperationalImage;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Traits\ImageOperations;

class DailyCheckListQuestionController extends Controller
{
    use ImageOperations;

    public function index()
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        $items = DailyOperational::where('type', 'am')->whereDate('date', Carbon::now()->toDateString())->get();

        return view('admin.daily_am_check.index', compact('items', 'parks'));
    }

    public function indexPm()
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        $items = DailyOperational::where('type', 'pm')->whereDate('date', Carbon::now()->toDateString())->get();

        return view('admin.daily_pm_check.index', compact('items', 'parks'));
    }


    public function addAmCheck()
    {
        $questions = CheckListQuestion::where('type', 'am')->get();

        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }

        return view('admin.daily_am_check.add', compact('questions', 'parks'));
    }

    public function addPmCheck()
    {
        $questions = CheckListQuestion::where('type', 'pm')->get();

        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }

        return view('admin.daily_pm_check.add', compact('questions', 'parks'));
    }


    protected function store(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add daily operational checklist';
        $action->save(); 

        // dd($request);
        $path = '';
        if (!empty($request['signature'])) {
            $path = Storage::disk('s3')->put('images', $request['signature']);
        }
        $dailyOperational = DailyOperational::create([
            'type' => $request->form_type,
            'park_id' => $request->park_id,
            'date' => $request->date,
            'user_id' => auth()->id(),
            'additional_comment' => $request->additional_comment,
            'signature' => $path

        ]);
        foreach ($request->element_ids as $key => $question) {
            DailyOperationalInfo::query()->create([
                'daily_operational_id' => $dailyOperational->id,
                'check_list_question_id' => $question,
                'answer' => $request['status'][$key] ?? 0,
                'comment' => $request['comment'][$key] ?? null,
            ]);
        }
        $question = CheckListQuestion::where('is_rides_question', 1)->where('type', $request->form_type)->first();

        foreach ($request['ride_id'] as $key => $ride) {
            if ($ride === null) {
                continue;
            }
            DailyOperationalRide::query()->create([
                'daily_operational_id' => $dailyOperational->id,
                'check_list_question_id' => $question->id,
                'ride_id' => $ride,
                'comment_ride' => $request['ride_comment'][$key] ?? null,
            ]);
        }
        if ($request->has('images')) {
            $this->Gallery($request, new DailyOperationalImage(), ['daily_operationals_id' => $dailyOperational->id]);
        }


        return response()->json(['success' => ' List Added successfully']);
    }

    public function getImage(Request $request)
    {
        $x = $request->trCount;
        return view('admin.daily_pm_check.append_images', compact('x'));
    }


    protected function editPmList()
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update pm daily operational checklist';
        $action->save(); 

        $item = DailyOperational::findOrFail($id);
        $items = DailyOperationalInfo::where('daily_operational_id', $id)->get();
        $rides = DailyOperationalRide::where('daily_operational_id', $id)->get();
        return view('admin.daily_pm_check.edit', compact('items', 'item', 'rides', 'id'));
    }

    public function edit($id)
    {
        $item = Attraction::findOrFail($id);
        $items = AttractionInfo::where('attraction_id', $id)->get();
        return view('admin.general_questions.edit', compact('items', 'id'));

    }

    public function showAmList($id)
    {
        $list = DailyOperational::findOrFail($id);
        $items = DailyOperationalInfo::where('daily_operational_id', $id)->get();
        $rides = DailyOperationalRide::where('daily_operational_id', $id)->get();
        $images = DailyOperationalImage::where('daily_operationals_id', $id)->get();

        return view('admin.daily_am_check.show', compact('items', 'list', 'rides', 'id', 'images'));
    }

    public function showPmList($id)
    {
        $list = DailyOperational::findOrFail($id);
        $items = DailyOperationalInfo::where('daily_operational_id', $id)->get();
        $rides = DailyOperationalRide::where('daily_operational_id', $id)->get();
        $images = DailyOperationalImage::where('daily_operationals_id', $id)->get();
        return view('admin.daily_pm_check.show', compact('items', 'list', 'rides', 'id', 'images'));
    }

    public function update(Request $request, $id)
    {
        AttractionInfo::where('attraction_id', $id)->delete();
        foreach ($request->question_id as $key => $value) {
            AttractionInfo::create([
                'attraction_id' => $id,
                'general_question_id' => $value,
                'status' => $request->status[$key],
                'note' => $request->note[$key],
                'corrective_action' => $request->corrective_action[$key],
            ]);
        }
        alert()->success('Update Audit Check List successfully !');
        return redirect()->back();


    }

    public function amSearch(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'search on am daily operational checklist';
        $action->save();

        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->toArray();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        $date = $request->input('date');
        $park_id = $request->input('park_id');
        $items = DailyOperational::where('type', 'am')->where('park_id', $park_id);
        if ($date) {
            $items = $items->whereDate('date', $date);
        }
        $items = $items->get();
        return view('admin.daily_am_check.ajax', compact('items', 'parks'));
    }

    public function pmSearch(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'search on pm daily operational checklist';
        $action->save();

        // dd($request);
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->toArray();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        $date = $request->input('date');
        $park_id = $request->input('park_id');
        $items = DailyOperational::where('type', 'pm')->where('park_id', $park_id);
        //   dd ($items);

        if ($date) {
            $items = $items->whereDate('date', $date);
        }
        $items = $items->get();
        // dd ($items);
        return view('admin.daily_pm_check.ajax', compact('items', 'parks'));
    }


}
