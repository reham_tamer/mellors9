<?php

namespace App\Http\Controllers\Admin;

use App\Events\StoppageEvent;
use App\Events\RideStatusEvent;
use App\Http\Controllers\Controller;
use App\Imports\RidesStoppageImport;
use App\Models\Ride;
use App\Models\RideStoppages;
use App\Models\rideStoppagesImages;
use App\Models\StopageCategory;
use App\Models\StopageSubCategory;
use App\Models\User;
use App\Traits\ImageOperations;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\Dashboard\Ride\RideStoppageRequest;
use App\Http\Requests\Dashboard\Ride\RideStoppageStatusRequest;
use App\Models\ParkTime;
use App\Models\GameTime;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use App\Notifications\StoppageNotifications;


class RideStoppageController extends Controller
{
    use ImageOperations;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasRole('Technical')) {
            $items = RideStoppages::where('ride_status', 'stopped')
                ->where('opened_date', date('Y-m-d'))->get();
        } else {
            $items = RideStoppages::where('ride_status', 'stopped')->orwhere('opened_date', date('Y-m-d'))->get();
        }

        return view('admin.rides_stoppages.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.rides_stoppages.exce_upload');
    }

    public function add_stoppage($ride_id, $park_time_id)
    {
        $users = User::pluck('name', 'id')->toArray();
        $stopage_category = StopageCategory::pluck('name', 'id')->toArray();
        $stopage_sub_category = StopageSubCategory::pluck('name', 'id')->toArray();
        $time_slot = ParkTime::find($park_time_id);
        $date = $time_slot->date;
        return view('admin.rides_stoppages.add', compact('users', 'date', 'ride_id', 'park_time_id', 'stopage_category', 'stopage_sub_category'));

    }

    public function show_stoppages($ride_id, $park_time_id)
    {

        if (auth()->user()->can('showStoppages')) {

            $items = RideStoppages::where(function ($query) use ($park_time_id, $ride_id) {
                $query->where('park_time_id', $park_time_id)
                    ->where('ride_id', $ride_id);
            })->get();
            $stopage_category = StopageCategory::pluck('name', 'id')->toArray();
            $stopage_sub_category = StopageSubCategory::pluck('name', 'id')->toArray();

            return view('admin.rides_stoppages.index', compact('items', 'ride_id', 'park_time_id', 'stopage_category', 'stopage_sub_category'));
        } else
            return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RideStoppageRequest $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add stoppage';
        $action->save();
        // dd($request);
        $validate = $request->validated();

        \DB::beginTransaction();
        $park_time_id = $request->park_time_id;
        $ride_id = $request->ride_id;
        $ride = Ride::findOrFail($ride_id);
        $zone_id = $ride->zone_id;
        $park_time = ParkTime::findOrFail($park_time_id);
        $park_id = $park_time->park_id;
        $opened_date = $park_time->date;
        $duration = $park_time->duration_time;
        $data = $request->validated();
        $data['opened_date'] = $opened_date;
        $data['park_id'] = $park_id;
        $data['zone_id'] = $zone_id;
        $data['user_id'] = auth()->user()->id;
        $gameTime = GameTime::where('ride_id', $ride_id)->where('park_time_id', $park_time_id)->first();
        $data['game_time_id'] = $gameTime?->id;
        $stoppageStartTime = Carbon::parse("$request->date $request->time_slot_start");
        $stoppageEndTime = Carbon::parse("$request->end_date $request->time_slot_end");
        $stoppageParkTimeEnd = Carbon::parse("$park_time->close_date $park_time->end");
        $stoppageParkTimeStart = Carbon::parse("$park_time->date $park_time->start");
        $ride = Ride::find($validate['ride_id']);
        $last = RideStoppages::query()
            ->where('ride_id', $ride_id)
            ->where('park_time_id', $park_time->id)
            ->where('type', 'all_day')
//            ->where('ride_status', 'stopped')
            ->first();
        if ($last) {
            alert()->error('You cannot add another stoppage ,this ride stopped untill the end of shift!');
            return redirect()->back();
        }
        $stoppage = RideStoppages::query()
            ->where('ride_id', $ride_id)
            ->where('park_time_id', $park_time->id)
//            ->where('ride_status', 'stopped')
            ->first();
        if ($data['type'] == 'all_day' && $stoppage) {
            alert()->error('You cannot add stoppage type all day');
            return redirect()->back();
        }
        if ($data['stoppage_status'] == "done") {
            $data['ride_status'] = "active";
            if ($data['type'] == 'all_day') {
                $data['down_minutes'] = $duration;
                $data['end_date'] = $park_time->close_date;
                $data['time_slot_end'] = $park_time->end;
                $data['time_slot_start'] = $park_time->start;
                $data['stoppage_status'] = 'done';
            } else {
                //$stoppageStartTime = \Illuminate\Support\Carbon::now();
                $data['down_minutes'] = $stoppageEndTime->diffInMinutes($stoppageStartTime);
            }
        } else {
            $data['ride_status'] = "stopped";
            if ($data['type'] == 'all_day') {
                if ($request->date === $park_time->date) {
                    $data['down_minutes'] = $duration;
                    $data['end_date'] = $park_time->close_date;
                    $data['time_slot_end'] = $park_time->end;
                    $data['time_slot_start'] = $park_time->start;
                    $data['stoppage_status'] = 'working';

                } else {
                    alert()->error('Stoppage Start Date Is Incorrect !');
                    return redirect()->back();
                }
            } else {

                if ($stoppageStartTime >= $stoppageParkTimeStart) {
                    $data['down_minutes'] = $stoppageParkTimeEnd->diffInMinutes($stoppageStartTime);
                } else {
                    alert()->error('Stoppage Time Is Incorrect !');
                    return redirect()->back();
                }
            }

        }
        $data['time'] = Carbon::now()->toTimeString();
        $stoppage = RideStoppages::create($data);
        $data1 = [
            'title' => $ride?->name . ' ' . 'has stopped ' . ' because of ' . $stoppage->stopageSubCategory->name,
            'ride_id' => $validate['ride_id'],
            'time_id' => $park_time_id,
            'user_id' => Auth::user()->id,

        ];
        $users = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Super Admin');
        })->get();
        $now = Carbon::now()->toDateTimeString();
        foreach ($users as $user) {
            Notification::send($user, new StoppageNotifications($data1));
            event(new StoppageEvent($user->id, $data1['title'], $now, $park_time_id, $validate['ride_id']));
        }
        event(new RideStatusEvent($ride_id, $data['ride_status'], $stoppage->stopageSubCategory?->name));

        if ($request->has('images')) {
            $this->Gallery($request, new rideStoppagesImages(), ['ride_stoppages_id' => $stoppage->id]);
        }
        $zoneManger = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Operations/Zones Manager');
        })->whereHas('zones', function ($query) use ($stoppage) {
            return $query->where('zone_id', $stoppage->zone_id);
        })->first();

        if ($zoneManger) {
            Notification::send($zoneManger, new StoppageNotifications($data1));
        }
        DB::commit();

        alert()->success('Ride Stoppage Added successfully !');

        return redirect()->route('admin.showStoppages', ['ride_id' => $ride_id, 'park_time_id' => $park_time_id]);
    }


    public function uploadStoppagesExcleFile(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'upload  stoppages excel file';
        $action->save();

        Excel::import(new RidesStoppageImport(), $request->file('file'));
        alert()->success('Ride Stoppage Added successfully !');
        return view('admin.rides_stoppages.exce_upload');
    }


    public function edit($id)
    {
        $item = RideStoppages::findOrFail($id);
        $park_time_id = $item->park_time_id;
        $ride_status = $item->ride_status;
        $rides = Ride::pluck('name', 'id')->all();
        $stopage_category = StopageCategory::pluck('name', 'id')->toArray();
        $stopage_sub_category = StopageSubCategory::pluck('name', 'id')->toArray();
        $users = User::pluck('name', 'id')->toArray();
        $album = $item->album;
        return view('admin.rides_stoppages.edit', compact('item', 'park_time_id', 'stopage_category', 'rides', 'stopage_sub_category', 'users', 'album'));

    }

    public function update(RideStoppageRequest $request, $id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update stoppage';
        $action->save();

        $item = RideStoppages::findOrFail($id);
        //  dd($request) ;
        $lastStoppageStatus = $item['ride_status'];

        $ride_id = $item->ride_id;
        $park_time_id = $item->park_time_id;
        $data = $request->validated();
        $park_time = ParkTime::findOrFail($park_time_id);
        $time_slot_end = $request['time_slot_end'];
        $data['ride_status'] = $item['ride_status'];
        $data['park_id'] = $park_time->park_id;
        $data['opened_date'] = $park_time->date;
        $data['park_time_id'] = $request['park_time_id'];
        $time_slot_start = $request['time_slot_start'];
        $stoppageStartDate = $request['date'];
        $stopageEnddate = $request['end_date'];
        $stoppageStartTime = Carbon::parse("$stoppageStartDate $time_slot_start");
        $stoppageParkTimeEnd = Carbon::parse("$park_time->close_date $park_time->end");
        $data['down_minutes'] = $stoppageParkTimeEnd->diffInMinutes($stoppageStartTime);
        $ride = Ride::find($ride_id);
        $users = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Super Admin');
        })->get();

        if ($data['type'] == 'all_day') {
            $last = RideStoppages::query()
                ->where('ride_id', $ride_id)
                ->where('park_time_id', $park_time->id)
                ->where('id', '!=', $id)
//                ->where('ride_status', 'stopped')
                ->first();
            if ($last) {
                alert()->error('You cannot add another stoppage ,this ride stopped until the end of shift!');
                return redirect()->back();
            }
        }else{
            $last = RideStoppages::query()
                ->where('ride_id', $ride_id)
                ->where('park_time_id', $park_time->id)
                ->where('id', '!=', $id)
//                ->where('ride_status', 'stopped')
                ->where('type', 'all_day')
                ->first();
            if ($last) {
                alert()->error('You cannot add another stoppage ,this ride stopped until the end of shift!');
                return redirect()->back();
            }
        }

        $now = Carbon::now()->toDateTimeString();
        if ($data['type'] == 'all_day' && $request['stoppage_status'] == "done") {
            alert()->error('Please change stoppage type to be Time Slot then solve it !');
            return redirect()->back();
        }
        if (\ParkTime($park_time->park_id) && ParkTime($park_time->park_id) != null) {
            if ($item->ride_status == 'stopped' && $request['stoppage_status'] == "working" || $request['stoppage_status'] == "pending" && $item->ride_status == 'stopped') {
                $data1 = [
                    'title' => $ride?->name . ' ' . 'has stoppage status',
                    'ride_id' => $ride_id,
                    'time_id' => $data['park_time_id'],
                    'user_id' => Auth::user()->id,
                ];
                foreach ($users as $user) {
                    Notification::send($user, new StoppageNotifications($data1));
                    event(new StoppageEvent($user->id, $data1['title'], $now, $data['park_time_id'], $ride_id));
                }
            }
            if ($item->ride_status == 'active' && $request['stoppage_status'] == "done" && $lastStoppageStatus != 'active') {
                $data1 = [
                    'title' => $ride?->name . ' ' . 'has  active status',
                    'ride_id' => $ride_id,
                    'time_id' => $data['park_time_id'],
                    'user_id' => Auth::user()->id,
                ];
                foreach ($users as $user) {
                    Notification::send($user, new StoppageNotifications($data1));
                    event(new StoppageEvent($user->id, $data1['title'], $now, $data['park_time_id'], $ride_id));
                }
            }
        }


        if ($data['type'] == 'all_day' && ($request['stoppage_status'] == "working" || $request['stoppage_status'] == "pending" && $data['type'] == 'all_day')) {
            $data['down_minutes'] = $park_time->duration_time;
            $data['ride_status'] = "stopped";
            $data['end_date'] = $park_time->close_date;
            $data['time_slot_start'] = $park_time->start;
            $data['time_slot_end'] = $park_time->end;
            $data['stoppage_status'] = 'working';


        } elseif ($data['type'] == 'time_slot' && $request['stoppage_status'] == "done") {

            $stopageEndTime = Carbon::parse("$stopageEnddate $time_slot_end");
            if ($stopageEndTime <= $stoppageParkTimeEnd) {
                $data['down_minutes'] = $stopageEndTime->diffInMinutes($stoppageStartTime);
                $data['ride_status'] = "active";
                $data['stoppage_status'] = 'done';
                if ($item->parent_id != NULL) {
                    $item->parent()?->update(['ride_status' => 'active', 'stoppage_status' => 'done']);
                    RideStoppages::where('parent_id', $item->parent_id)
                        ->update(['ride_status' => 'active', 'stoppage_status' => 'done']);
                }

            } else {
                alert()->error('Stoppage End Time Is Incorrect !');
                return redirect()->back();
            }
        } else {
            $data['ride_status'] = "stopped";
            $data['time_slot_end'] = $park_time->end;
            $data['end_date'] = $park_time->close_date;
        }
        $data['edited_by_id'] = Auth::user()->id;

        // Update the ride stoppage with the new data
        $item->update($data);
        event(new RideStatusEvent($ride_id, $data['ride_status'], $item->stopageSubCategory?->name));

        if ($request->has('images') && $request->has('images') != null) {
            $this->Gallery($request, new rideStoppagesImages(), ['ride_stoppages_id' => $id]);
        }
        alert()->success('Ride Stoppage Updated successfully !');
        return redirect()->route('admin.showStoppages', ['ride_id' => $ride_id, 'park_time_id' => $request['park_time_id']]);

    }


    public function extend(RideStoppageStatusRequest $request, RideStoppages $rideStoppage)
    {
        $data = $request->validated();
        $oldStoppageData = RideStoppages::findOrFail($request['stoppage_id']);
        $data['park_id'] = $oldStoppageData->park_id;
        //dd($request['stoppage_id']);
        $data['zone_id'] = $oldStoppageData->zone_id;
        $data['ride_id'] = $oldStoppageData->ride_id;
        $park_time = ParkTime::query()->where('date', date('Y-m-d'))->where('park_id', $data['park_id'])->first();
        // dd($park_time);
        if (!($park_time)) {
            alert()->error('Please, Set Time Slot First To Extend This Stoppage !');
            return redirect()->back();
        } else {
            $data['park_time_id'] = $park_time->id;
            $duration = $park_time->duration_time;
            $data['down_minutes'] = $duration;
            $opened_date = $park_time->date;
            $data['opened_date'] = $opened_date;
            if ($oldStoppageData->parent_id !== null) {
                $data['parent_id'] = $oldStoppageData->parent_id;
            } else {
                $data['parent_id'] = $request['stoppage_id'];
            }
            $data['stopage_category_id'] = $request['stopage_category_id'];
            $data['stopage_sub_category_id'] = $request['stopage_sub_category_id'];
            $data['description'] = $request['description'];
            $data['user_id'] = auth()->user()->id;
            $data['time_slot_start'] = $park_time->start;
            $data['date'] = $park_time->date;
            $data['end_date'] = $park_time->close_date;
            $data['time_slot_end'] = $park_time->end;
            $data['ride_status'] = "stopped";
            if ($request['type'] == 'all_day') {
                $data['type'] = 'all_day';
                $data['stoppage_status'] = 'working';
            } elseif ($request['type'] == 'time_slot') {
                $data['type'] = "time_slot";
            }

            $ride = Ride::find($data['ride_id']);
            $now = Carbon::now()->toDateTimeString();

            $data1 = [
                'title' => $ride?->name . ' ' . 'has' . $data['ride_status'] . ' status',
                'ride_id' => $data['ride_id'],
                'time_id' => $data['park_time_id'],
                'user_id' => Auth::user()->id,
            ];
            $users = User::whereHas('roles', function ($query) {
                return $query->where('name', 'Super Admin');
            })->get();
            /*    foreach ($users as $user) {
                   Notification::send($user, new StoppageNotifications($data1));
                   event(new StoppageEvent($user->id, $data1['title'], $now, $data['park_time_id'], $data['ride_id']));
               } */
            $stoppage = RideStoppages::create($data);
        }
        $stoppageSubCategoryName = '';
        if ($data['stopage_sub_category_id']) {
            $stoppageSubCategory = StopageSubCategory::find($data['stopage_sub_category_id']);
            if ($stoppageSubCategory) {
                $stoppageSubCategoryName = $stoppageSubCategory->name;
            }
        }
        /*         event(new RideStatusEvent($data['ride_id'], $data['ride_status'], $stoppageSubCategoryName));
         */
        alert()->success('Stoppage Extended  Successfully !');

        $latestStoppages = RideStoppages::where('park_id', $oldStoppageData->park_id)
            ->where('stoppage_status', '!=', 'done')
            ->select('ride_id', \DB::raw('MAX(created_at) as latest_created_at'))
            ->groupBy('ride_id');

        $stoppages = RideStoppages::select('ride_stoppages.*')
            ->joinSub($latestStoppages, 'latest_stoppages', function ($join) {
                $join->on('ride_stoppages.ride_id', '=', 'latest_stoppages.ride_id')
                    ->on('ride_stoppages.created_at', '=', 'latest_stoppages.latest_created_at');
            })
            ->get();
        $stopage_category = StopageCategory::pluck('name', 'id')->toArray();
        $stopage_sub_category = StopageSubCategory::pluck('name', 'id')->toArray();

        $extended_stoppages = RideStoppages::where('date', '=', Carbon::now()->format('Y-m-d'))
            ->where('stoppage_status', '!=', 'done')->pluck('ride_id');
        if ($stoppages->isNotEmpty()) {
            return view('admin.not_solved_stoppages.index', compact('stoppages', 'extended_stoppages', 'stopage_category', 'stopage_sub_category'));
        } else

            return redirect()->route('admin.park_times.index');
    }


    public function solveStoppage($id, Request $request)
    {

        $oldStoppageData = RideStoppages::findOrFail($id);
        $oldStoppageData->ride_status = 'active';
        $oldStoppageData->stoppage_status = 'done';
        $oldStoppageData->description = $request->description;
        $done = $oldStoppageData->save();
        //  dd( $request);
        if ($oldStoppageData->parent_id != NULL) {
            $oldStoppageData->parent()->update(['ride_status' => 'active', 'stoppage_status' => 'done']);
            RideStoppages::where('parent_id', $oldStoppageData->parent_id)
                ->update(['ride_status' => 'active', 'stoppage_status' => 'done']);
        }
        if ($done) {
            alert()->success('Stoppage Solved Successfully !');
        } else {
            alert()->error('Failed to solve the stoppage.');
        }
        $latestStoppages = RideStoppages::where('park_id', $oldStoppageData->park_id)
            ->where('stoppage_status', '!=', 'done')
            ->select('ride_id', \DB::raw('MAX(created_at) as latest_created_at'))
            ->groupBy('ride_id');

        $stoppages = RideStoppages::select('ride_stoppages.*')
            ->joinSub($latestStoppages, 'latest_stoppages', function ($join) {
                $join->on('ride_stoppages.ride_id', '=', 'latest_stoppages.ride_id')
                    ->on('ride_stoppages.created_at', '=', 'latest_stoppages.latest_created_at');
            })
            ->get();
        $stopage_category = StopageCategory::pluck('name', 'id')->toArray();
        $stopage_sub_category = StopageSubCategory::pluck('name', 'id')->toArray();
        session()->forget('alert.message');
        $extended_stoppages = RideStoppages::where('date', '=', Carbon::now()->format('Y-m-d'))
            ->where('stoppage_status', '!=', 'done')->pluck('ride_id');
        if ($stoppages->isNotEmpty()) {
            return view('admin.not_solved_stoppages.index', compact('stoppages', 'extended_stoppages', 'stopage_category', 'stopage_sub_category'));
        } else
            return redirect()->route('admin.park_times.index');
    }

    public function destroy($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete stoppage';
        $action->save();

        $rideStoppages = RideStoppages::find($id);
        if ($rideStoppages) {
            $rideStoppages->evacuation?->delete();

            $rideStoppages->delete();
            alert()->success('Row deleted successfully');
            return back();
        }
        alert()->error('Row not found');
        return redirect()->route('admin.rides-stoppages.index');
    }

    public function getImage(Request $request)
    {
        $x = $request->trCount;
        return view('admin.rides_stoppages.append_images', compact('x'));
    }

    public function search(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'search on stoppage';
        $action->save();

        $date = $request->input('date');
        $park_time_id = $request->input('park_time_id');
        $time_slot = ParkTime::find($park_time_id);
        $ride_id = $request->input('ride_id');
        $items = RideStoppages::query()
            ->Where('opened_date', $date)->where('ride_id', $ride_id)
            ->get();
        return view('admin.rides_stoppages.index', compact('items', 'park_time_id', 'ride_id'));
    }
}
