<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Queue\QueueRequest;
use App\Models\CycleRideNumber;
use App\Models\Park;
use App\Models\ParkTime;
use App\Models\QueueRideNumber;
use App\Models\User;
use App\Models\Queue;
use App\Models\Ride;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\GameTime;


class QueueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Queue::where('opened_date', date('Y-m-d'))->get();
        return view('admin.queues.index', compact('items'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.queues.exce_upload');

    }

    public function add_queue($ride_id, $park_time_id)
    {
        $users = User::pluck('name', 'id')->toArray();
        return view('admin.queues.add', compact('users', 'ride_id', 'park_time_id'));

    }

    public function show_queues($ride_id, $park_time_id)
    {
        $items = Queue::where('park_time_id', $park_time_id)
            ->where('ride_id', $ride_id)->get();
        return view('admin.queues.index', compact('items', 'ride_id', 'park_time_id'));
    }

    public function uploadQueueExcleFile(Request $request)
    {

        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'upload queue excle file';
        $action->save();

        Excel::import(new \App\Imports\RideQueues(), $request->file('file'));
        alert()->success('Ride Queues Added successfully !');
        return view('admin.queues.exce_upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(QueueRequest $request)
    {
        // return $request;
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add queue';
        $action->save();


        $park_time_id = $request->park_time_id;
        $ride_id = $request->ride_id;
        $ride = Ride::findOrFail($ride_id);
        $zone_id = $ride->zone_id;
        $park_time = ParkTime::findOrFail($park_time_id);
        $park_id = $park_time->park_id;
        $opened_date = $park_time->date;
        $data = $request->validated();
        $data['opened_date'] = $opened_date;
        $data['park_id'] = $park_id;
        $data['zone_id'] = $zone_id;
        $data['user_id'] = auth()->user()->id;
        $gameTime = GameTime::where('ride_id', $ride_id)->where('park_time_id', $park_time_id)->first();
        $data['game_time_id'] = $gameTime?->id;
        Queue::create($data);
        event(new \App\Events\RideQueueEvent($ride_id, 'active'));

        alert()->success('Queue Added Successfully To The Ride !');

        return redirect()->route('admin.showQueues', ['ride_id' => $ride_id, 'park_time_id' => $park_time_id]);

        // return view('admin.queues.index',compact('','ride_id','park_time_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\GameTime $GameTime
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\GameTime $GameTime
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Queue::findOrFail($id);
        $park_time_id = $item->park_time_id;
        $ride_id = $item->ride_id;
        return view('admin.queues.edit', compact('item', 'park_time_id', 'ride_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\GameTime $GameTime
     * @return \Illuminate\Http\Response
     */
    public function update(QueueRequest $request, Queue $queue)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update queue';
        $action->save();

        $park_time_id = $request->park_time_id;
        $ride_id = $request->ride_id;
        $queue->update($request->validated());
        $queue->save();

        alert()->success('Queue updated Successfully !');
        return redirect()->route('admin.showQueues', ['ride_id' => $ride_id, 'park_time_id' => $park_time_id]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\GameTime $GameTime
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete queue ';
        $action->save();


        $queue = Queue::find($id);
        if ($queue) {
            $queue->forceDelete();
            alert()->success('Queue deleted successfully');
            return back();
        }
        alert()->error('Queue not found');
        return redirect()->route('admin.queues.index');
    }

    public function search(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'search on queue ';
        $action->save();

        $ride_id = $request->input('ride_id');
        $park_time_id = $request->input('park_time_id');
        $date = $request->input('date');
        $items = Queue::query()
            ->where('ride_id', $ride_id)
            ->Where('opened_date', $date)
            ->get();
        return view('admin.queues.index', compact('items', 'ride_id', 'park_time_id'));
    }

    protected function queuesTimeSlot()
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'show queue avg ';
        $action->save();

        if (auth()->user()->hasRole('Super Admin') || auth()->user()->hasRole('Client')) {
            $parks = Park::pluck('id');
        } else {
            $parks = auth()->user()->parks->pluck('id');
        }

        $park_time = ParkTime::whereIn('park_id', $parks)->orderBy('id', 'DESC')
            ->where(function ($q) {
                $q->where('date', Carbon::now()->toDateString())->
                orWhere('date', Carbon::now()->subDay()->toDateString());
            })
            ->get()->groupBy('park_id');

        $times = [];
        foreach ($park_time as $group) {
            $times[] = $group[0];
        }
        return view('admin.layout.queues.index', compact('times'));

    }

//    protected function queuesTime($id)
//    {
//        $action = new \App\Models\Action();
//        $action->user_id = auth()->User()->id;
//        $action->action = 'show queues for time slot no. '.$id;
//        $action->save();
//
//
//        $time = ParkTime::findOrFail($id);
//        $rides = $time->rides;
//
//        return view('admin.layout.queues.rides', compact('rides', 'id'));
//    }
//
//    protected function queuesRide($id, $time)
//    {
//        $action = new \App\Models\Action();
//        $action->user_id = auth()->User()->id;
//        $action->action = 'show queue for  ride  no. '.$id;
//        $action->save();
//
//        $ride = Ride::findOrFail($id);
//        $queues = Queue::where('ride_id', $id)->where('park_time_id', $time)->get();
//        $times = [];
//        $seconds = [];
//        foreach ($queues as $key => $queue) {
//            $times[] = Carbon::parse($queue->start_time)->format('g:i a');
//            $seconds[] = $queue->queue_seconds / $queues->count() / 60;
//        }
//        $queues = [
//            'labels' => $times,
//            'data' => $seconds,
//        ];
//
//        return view('admin.layout.queues.ride', compact('queues', 'ride'));
//
//    }
    protected function queuesTime($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'open queue number page for park ';
        $action->save();



        $cycleRecords = QueueRideNumber::whereHas('gameTime', function ($q) use ($id) {
            $q->where('park_time_id', $id);
        })->get();
        $flattenedLastRecords = $cycleRecords
            ->groupBy('ride_id');

        $times = $cycleRecords
            ->groupBy('date')
            ->map->last()
            ->values();

        if (request()->ajax()) {
            return view('admin.layout.queuesRides.ajax_riders', compact('times', 'flattenedLastRecords','cycleRecords','id'));
        }
        return view('admin.layout.queuesRides.single_park', compact('times', 'flattenedLastRecords','cycleRecords','id'));
    }

}
