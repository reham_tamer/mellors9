<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserResource;
use App\Models\Ride;
use App\Models\StopageSubCategory;
use App\Models\FeedbackSubCategory;
use App\Models\User;
use App\Models\Attendance;
use App\Models\UserPark;
use App\Models\Zone;
use App\Models\Action;
use App\Models\TrainingChecklist;
use Illuminate\Http\Request;
use Carbon\Carbon;

class GeneralController extends Controller
{


    public function getParkZones(Request $request)
    {
        $zones = Zone::where("park_id", $request->park_id)->get();

        $html = '<option value="">' . 'Choose Zone' . '</option>';

        foreach ($zones as $zone) {
            $html .= '<option value="' . $zone->id . '">' . $zone->name . '</option>';
        }
        return response()->json(['html' => $html]);
    }

    public function getSubStoppageCategories(Request $request)
    {
        $cities = StopageSubCategory::where("stopage_category_id", $request->stopage_category_id)->get();

        return response()->json(['subCategory' => $cities]);
    }

    public function getFeedbackSubCategories(Request $request)
    {
        $cities = FeedbackSubCategory::where("customer_complaint_id", $request->feedback_category_id)->get();

        return response()->json(['subCategory' => $cities]);
    }

    public function getParkRides(Request $request)
    {
        $html = '';
        $rides = Ride::where('park_id', $request->park_id)->get();
        $html = '<option value="">' . 'Choose Ride' . '</option>';

        foreach ($rides as $ride) {
            $html .= '<option value="' . $ride->id . '">' . $ride->name . '</option>';
        }
        return response()->json(['html' => $html]);
    }

    public function getZonerides(Request $request)
    {
        $html = '';
        $rides = Ride::where('zone_id', $request->zone_id)->get();
        $html = '<option value="">' . 'Choose Ride' . '</option>';

        foreach ($rides as $ride) {
            $html .= '<option value="' . $ride->id . '">' . $ride->name . '</option>';
        }
        return response()->json(['html' => $html]);
    }

    public function getHasSkillgame(Request $request)
    {
        $rides = Ride::where('park_id', $request->park_id)->where('ride_type_id', 1)->get();

        return response()->json(['rides' => $rides]);
    }

    public function clearMessage(Request $request)
    {
        $request->session()->forget('success'); // Change 'success' to your flash message key
        $request->session()->forget('error');
        return response()->json(['message' => 'Flash message cleared']);
    }

    protected function getSupervisorByPark(Request $request)
    {
        $request->validate([
            'park_id' => 'required|exists:parks,id',
        ]);
        $users = UserPark::query()->where('park_id', $request->park_id)->
        whereHas('users.roles', function ($query) {
            return $query->where('name', 'Operations/Zone supervisor');
        })->pluck('user_id');
        $users = User::query()->whereIn('id', $users)->get();
        $html = '<option value="">' . 'Choose Supervisor...' . '</option>';
        foreach ($users as $user) {
            $html .= '<option value="' . $user->id . '">' . $user->name . '</option>';
        }
        return response()->json(['html' => $html]);


    }

    protected function getOperatorByPark(Request $request)
    {
        $request->validate([
            'park_id' => 'required|exists:parks,id',
        ]);
        $users = UserPark::query()->where('park_id', $request->park_id)->
        whereHas('users.roles', function ($query) {
            return $query->where('name', 'Operations/Operator');
        })->pluck('user_id');
        $users = User::query()->whereIn('id', $users)->get();
        $html = '<option value="">' . 'Choose Operator...' . '</option>';
        foreach ($users as $user) {
            $html .= '<option value="' . $user->id . '">' . $user->name . '</option>';
        }
        return response()->json(['html' => $html]);


    }

    public function getTrainingChecklists(Request $request)
    {
        $html = '';
        $lists = TrainingChecklist::where('type', $request->type)->get();
        $html = '<option value="">' . 'Choose Training Checklist Type..' . '</option>';

        foreach ($lists as $list) {
            $html .= '<option value="' . $list->id . '">' . $list->name . '</option>';
        }
        return response()->json(['html' => $html]);
    }

    public function getUsersByType(Request $request)
    {
        $html = '';
        $type = $request->type;
        $users = UserPark::query()
        ->where('park_id', $request->park_id)
        ->pluck('user_id');
         if ($type == 'operator') {
            
            $users = User::query()
                ->whereIn('id', $users)
                ->whereHas('roles', function ($query) {
                    $query->where('name', 'Operations/Operator');
                })
                ->get();
        } 
        elseif ($type == 'attendance') {
            $users = Attendance::where('park_id', $request->park_id)->get();
        }
        elseif($type == 'supervisor') {
            
            $users = User::query()
                ->whereIn('id', $users)
                ->whereHas('roles', function ($query) {
                    $query->where('name', 'Operations/Zone supervisor')
                    ->orwhere('name', 'Operations/Zones Manager');
                })
                ->get();
            }elseif($type == 'manager') {
            
                $users = User::query()
                    ->whereIn('id', $users)
                    ->whereHas('roles', function ($query) {
                        $query->where('name', 'Management/Operations Manager');
                    })
                    ->get();
                }

        // dd($users);

        $html = '<option value="">' . 'Choose Users...' . '</option>';

        foreach ($users as $user) {
            $html .= '<option value="' . $user->id . '">' . $user->name . '</option>';
        }
        return response()->json(['html' => $html]);
    }
    
    public function getListType(Request $request)
    {
        $lists = TrainingChecklist::where("type", $request->list_type)->get();

        $html = '<option value="">' . 'Choose CheckList ' . '</option>';

        foreach ($lists as $list) {
            $html .= '<option value="' . $list->id . '">' . $list->name . '</option>';
        }
        return response()->json(['html' => $html]);
    }
    public function getChecklistType(Request $request)
    {
        $list = TrainingChecklist::find($request->checklist);
        $training_type=$list->training_type;
        return response()->json(['training_type' => $training_type]);
    }

    public function showActions()
    {

        $actions = Action::whereDate('created_at', Carbon::now()->toDateString())->get();

        return view('admin.actions.index', compact('actions'));

    }
    public function actionSearch(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'search on  actions page ';
        $action->save(); 

        $actions = [];
        $date = $request->query('date');
        $actions = Action::whereDate('created_at', '=', $date)->get();
        return view('admin.actions.index', compact('actions'));
        }

}
