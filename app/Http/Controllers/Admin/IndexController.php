<?php

namespace App\Http\Controllers\Admin;

use App\Models\CustomerFeedbacks;
use App\Models\CycleRideNumber;
use App\Models\Observation;
use App\Models\ParkTime;
use App\Models\RideCycles;
use DateTime;
use Illuminate\Support\Carbon;

//use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Park;
use App\Models\Ride;
use App\Models\Zone;
use App\Models\RideStoppages;

use App\Models\StopageSubCategory;
use App\Models\Notification;


class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
//    public function __invoke(Request $request)
//    {
//        if (auth()->user()->can('mainpage')) {
//
//            if (auth()->user()->hasRole('Super Admin')) {
//                $parks = Park::pluck('id');
//            } else {
//                $parks = auth()->user()->parks->pluck('id');
//            }
//            $currentDate = Carbon::now()->toDateString();
//            $currentTime = Carbon::now()->format('H:i');
//
//            //            $park_times = ParkTime::
//            //            where('date', $currentDate)
//            //                    ->orWhere(function ($subquery) use ($currentDate, $currentTime) {
//            //                    $subquery->where('close_date', $currentDate)
//            //                        ->where('end', '>=', $currentTime);
//            //                })->
//            //            whereIn('park_id', $parks)
//            //                ->pluck('id');
//            $park_time1 = ParkTime::whereIn('park_id', $parks)->orderBy('id', 'DESC')->where('date', Carbon::now()->toDateString())->
//            orWhere('date', Carbon::now()->subDay()->toDateString())->get()->groupBy('park_id');
//            //            dd($park_time1);
//            $park_times = [];
//            foreach ($park_time1 as $group) {
//                $park_times[] = $group->first()?->id;
//            }
//            $cycles = DB::table('rides')->whereNull('rides.deleted_at')
//                ->join('ride_cycles', 'rides.id', '=', 'ride_cycles.ride_id')
//                ->join('park_times', 'ride_cycles.park_time_id', '=', 'park_times.id')
//                ->join('parks', 'park_times.park_id', '=', 'parks.id')
//                ->select([
//                    'rides.ride_cat',
//                    'park_times.id as park_time_id',
//                    DB::raw('AVG(ride_cycles.duration_seconds /60) as avg_duration'),
//                    DB::raw(' SUM(COALESCE(ride_cycles.riders_count, 0)) +
//                              SUM(COALESCE(ride_cycles.number_of_disabled, 0)) +
//                              SUM(COALESCE(ride_cycles.number_of_vip, 0)) +
//                              SUM(COALESCE(ride_cycles.number_of_ft, 0)) as total_rider ')
//                ])
//                ->groupBy('rides.ride_cat', 'park_times.id')
//                ->whereIn('park_times.id', $park_times)
//                ->whereIn('parks.id', $parks)
//                ->orderBy('park_times.id')
//                ->get();
//
//            //get queue avg
//            $queues = DB::table('rides')->whereNull('rides.deleted_at')
//                ->join('queues', 'rides.id', '=', 'queues.ride_id')
//                ->join('park_times', 'queues.park_time_id', '=', 'park_times.id')
//                ->join('parks', 'park_times.park_id', '=', 'parks.id')
//                ->select([
//                    'rides.ride_cat',
//                    'park_times.id as park_time_id',
//                    DB::raw('AVG(queues.queue_seconds  / 60) as avg_queue_minutes')
//                ])
//                ->groupBy('rides.ride_cat', 'park_times.id')
//                ->whereIn('park_times.id', $park_times)
//                ->whereIn('parks.id', $parks)
//                ->orderBy('park_times.id')
//                ->get();
//            // dd ($queues);
//            $stoppages = RideStoppages::whereIn('park_time_id', $park_times)
//                ->whereIn('park_id', $parks)->get();
//            //   dd ($stoppages);
//            //get total riders
//             $total_riders = DB::table('rides')->whereNull('rides.deleted_at')
//                 ->join('ride_cycles', 'rides.id', '=', 'ride_cycles.ride_id')
//                 ->join('park_times', 'ride_cycles.park_time_id', '=', 'park_times.id')
//                 ->join('parks', 'park_times.park_id', '=', 'parks.id')
//                 ->select(
//                     'park_times.id',
//                     DB::raw('SUM(COALESCE(ride_cycles.riders_count, 0)) +
//                             SUM(COALESCE(ride_cycles.number_of_disabled, 0)) +
//                             SUM(COALESCE(ride_cycles.number_of_vip, 0)) +
//                             SUM(COALESCE(ride_cycles.number_of_ft, 0)) as total_rider')
//                 )
//                 ->whereIn('park_times.id', $park_times)
//                 ->whereIn('parks.id', $parks)
//                 ->groupBy('park_times.id')
//                 ->orderBy('park_times.id')
//                 ->get()
//                 ->groupBy('id');
//
//            //get ride status
//            $rides = DB::table('rides')->whereNull('rides.deleted_at')
//                ->join('parks', 'parks.id', '=', 'rides.park_id')
//                ->leftJoin('park_times', function ($join) {
//                    $join->on('park_times.park_id', '=', 'parks.id')
//                        ->whereDate('park_times.date', '=', Carbon::now()->toDateString());
//                })->whereNull('rides.deleted_at') // Filter where deleted_at is null
//                ->leftJoin('ride_stoppages', function ($join) {
//                    $join->on('ride_stoppages.ride_id', '=', 'rides.id')
//                        ->where('ride_stoppages.created_at', function ($subquery) {
//                            $subquery->select(DB::raw('MAX(created_at)'))
//                                ->from('ride_stoppages')
//                                ->whereColumn('ride_stoppages.ride_id', 'rides.id');
//                        });
//                })
//                ->select([
//                    'rides.*',
//                    'parks.name as parkName',
//                    'park_times.start',
//                    'park_times.end',
//                    'park_times.date',
//                    'park_times.close_date',
//                    'ride_stoppages.ride_status as stoppageRideStatus',
//                    'ride_stoppages.ride_notes',
//                    'ride_stoppages.type',
//                    'ride_stoppages.stopage_sub_category_id as stoppageSubCategory',
//                    'ride_stoppages.description as rideSroppageDescription',
//                ])
//                ->whereIn('parks.id', $parks)
//                ->get();
//
//            foreach ($rides as $ride) {
//                $now = Carbon::now();
//                $startDateTime = Carbon::parse("$ride->date $ride->start");
//                $endDateTime = Carbon::parse("$ride->close_date $ride->end");
//                if ($ride->stoppageRideStatus != null) {
//                    $ride->available = $ride->stoppageRideStatus;
//
//                    if (!is_null($ride?->stoppageSubCategory)) {
//                        $stoppageSubCategory = StopageSubCategory::find($ride?->stoppageSubCategory);
//                        $ride->stoppageSubCategoryName = $stoppageSubCategory ? $stoppageSubCategory?->name : null;
//                    }
//                } else {
//                    $ride->available = 'active';
//                    $ride->ride_notes = '';
//                    $ride->rideSroppageDescription = '';
//                }
//
//            }
//
////            $times = ParkTime::where('date', $currentDate)
////                ->orWhere(function ($subquery) use ($currentDate, $currentTime) {
////                    $subquery->where('close_date', $currentDate)
////                        ->where('end', '>=', $currentTime);
////                })->whereIn('park_id', $parks)
////                ->get();
//            $park_time = ParkTime::whereIn('park_id', $parks)->orderBy('id', 'DESC')
//                ->where('date', Carbon::now()->toDateString())->
//                orWhere('date', Carbon::now()->subDay()->toDateString())
//                ->get()
//                ->groupBy('park_id');
//            $times = [];
//            foreach ($park_time as $group) {
//                $times[] = $group[0];
//            }
//             return view('admin.layout.home', compact('stoppages', 'rides', 'queues', 'cycles', 'times', 'total_riders' , 'parks'));
//        } else
//            $stoppages = $rides = $queues = $cycles = $times = $parks = [];
//        $total_riders = Null;
//        return view('admin.layout.home', compact('stoppages', 'rides', 'queues', 'cycles', 'times', 'total_riders', 'parks'));
//
//
//    }
    public function __invoke(Request $request)
    {
        if (!auth()->user()->can('mainpage')) {
            return $this->renderEmptyView();
        }

        $parks = $this->getUserParks();
        $parkTimes = $this->getParkTimes($parks);
        $cycles = $this->getRideCycles($parkTimes, $parks);
        $queues = $this->getQueueAverages($parkTimes, $parks);
        $stoppages = $this->getRideStoppages($parkTimes, $parks);
        $totalRiders = $this->getTotalRiders($parkTimes, $parks);
        $rides = $this->getRidesWithStatus($parks);
        $times = $this->getParkTimesForView($parks);

        return view('admin.layout.home', compact(
            'stoppages', 'rides', 'queues', 'cycles', 'times', 'totalRiders', 'parks'
        ));
    }

    private function getUserParks()
    {
        return auth()->user()->hasRole('Super Admin')
            ? Park::pluck('id')
            : auth()->user()->parks->pluck('id');
    }

    private function getParkTimes($parks)
    {
        return ParkTime::whereIn('park_id', $parks)
            ->orderBy('id', 'DESC')
            ->where('date', Carbon::now()->toDateString())
            ->orWhere('date', Carbon::now()->subDay()->toDateString())
            ->get()
            ->groupBy('park_id')
            ->map(function ($group) {
                return $group->first()?->id;
            })
            ->toArray();
    }

    private function getRideCycles($parkTimes, $parks)
    {
        return DB::table('rides')
            ->join('ride_cycles', 'rides.id', '=', 'ride_cycles.ride_id')
            ->join('park_times', 'ride_cycles.park_time_id', '=', 'park_times.id')
            ->join('parks', 'park_times.park_id', '=', 'parks.id')
            ->select([
                'rides.ride_cat',
                'park_times.id as park_time_id',
                DB::raw('AVG(ride_cycles.duration_seconds / 60) as avg_duration'),
                DB::raw('SUM(COALESCE(ride_cycles.riders_count, 0)) +
                     SUM(COALESCE(ride_cycles.number_of_disabled, 0)) +
                     SUM(COALESCE(ride_cycles.number_of_vip, 0)) +
                     SUM(COALESCE(ride_cycles.number_of_ft, 0)) as total_rider')
            ])
            ->whereNull('rides.deleted_at')
            ->whereIn('park_times.id', $parkTimes)
            ->whereIn('parks.id', $parks)
            ->groupBy('rides.ride_cat', 'park_times.id')
            ->orderBy('park_times.id')
            ->get();
    }

    private function getQueueAverages($parkTimes, $parks)
    {
        return DB::table('rides')
            ->join('queues', 'rides.id', '=', 'queues.ride_id')
            ->join('park_times', 'queues.park_time_id', '=', 'park_times.id')
            ->join('parks', 'park_times.park_id', '=', 'parks.id')
            ->select([
                'rides.ride_cat',
                'park_times.id as park_time_id',
                DB::raw('AVG(queues.queue_seconds / 60) as avg_queue_minutes')
            ])
            ->whereNull('rides.deleted_at')
            ->whereIn('park_times.id', $parkTimes)
            ->whereIn('parks.id', $parks)
            ->groupBy('rides.ride_cat', 'park_times.id')
            ->orderBy('park_times.id')
            ->get();
    }

    private function getRideStoppages($parkTimes, $parks)
    {
        return RideStoppages::whereIn('park_time_id', $parkTimes)
            ->whereIn('park_id', $parks)
            ->get();
    }

    private function getTotalRiders($parkTimes, $parks)
    {
        return DB::table('rides')
            ->join('ride_cycles', 'rides.id', '=', 'ride_cycles.ride_id')
            ->join('park_times', 'ride_cycles.park_time_id', '=', 'park_times.id')
            ->join('parks', 'park_times.park_id', '=', 'parks.id')
            ->select(
                'park_times.id',
                DB::raw('SUM(COALESCE(ride_cycles.riders_count, 0)) +
                     SUM(COALESCE(ride_cycles.number_of_disabled, 0)) +
                     SUM(COALESCE(ride_cycles.number_of_vip, 0)) +
                     SUM(COALESCE(ride_cycles.number_of_ft, 0)) as total_rider')
            )
            ->whereNull('rides.deleted_at')
            ->whereIn('park_times.id', $parkTimes)
            ->whereIn('parks.id', $parks)
            ->groupBy('park_times.id')
            ->orderBy('park_times.id')
            ->get()
            ->groupBy('id');
    }

    private function getRidesWithStatus($parks)
    {
        $rides = DB::table('rides')
            ->join('parks', 'parks.id', '=', 'rides.park_id')
            ->leftJoin('park_times', function ($join) {
                $join->on('park_times.park_id', '=', 'parks.id')
                    ->whereDate('park_times.date', '=', Carbon::now()->toDateString());
            })
            ->leftJoin('ride_stoppages', function ($join) {
                $join->on('ride_stoppages.ride_id', '=', 'rides.id')
                    ->where('ride_stoppages.created_at', function ($subquery) {
                        $subquery->select(DB::raw('MAX(created_at)'))
                            ->from('ride_stoppages')
                            ->whereColumn('ride_stoppages.ride_id', 'rides.id');
                    });
            })
            ->select([
                'rides.*',
                'parks.name as parkName',
                'park_times.start',
                'park_times.end',
                'park_times.date',
                'park_times.close_date',
                'ride_stoppages.ride_status as stoppageRideStatus',
                'ride_stoppages.ride_notes',
                'ride_stoppages.type',
                'ride_stoppages.stopage_sub_category_id as stoppageSubCategory',
                'ride_stoppages.description as rideSroppageDescription',
            ])
            ->whereNull('rides.deleted_at')
            ->whereIn('parks.id', $parks)
            ->get();

        foreach ($rides as $ride) {
            $this->setRideAvailability($ride);
        }

        return $rides;
    }

    private function setRideAvailability($ride)
    {
        if ($ride->stoppageRideStatus != null) {
            $ride->available = $ride->stoppageRideStatus;

            if (!is_null($ride->stoppageSubCategory)) {
                $stoppageSubCategory = StopageSubCategory::find($ride->stoppageSubCategory);
                $ride->stoppageSubCategoryName = $stoppageSubCategory ? $stoppageSubCategory->name : null;
            }
        } else {
            $ride->available = 'active';
            $ride->ride_notes = '';
            $ride->rideSroppageDescription = '';
        }
    }

    private function getParkTimesForView($parks)
    {
        return ParkTime::whereIn('park_id', $parks)
            ->orderBy('id', 'DESC')
            ->where('date', Carbon::now()->toDateString())
            ->orWhere('date', Carbon::now()->subDay()->toDateString())
            ->get()
            ->groupBy('park_id')
            ->map(function ($group) {
                return $group->first();
            });
    }

    private function renderEmptyView()
    {
        $stoppages = $rides = $queues = $cycles = $times = $parks = [];
        return view('admin.layout.home', compact('stoppages', 'rides', 'queues', 'cycles', 'times', 'parks'));
    }
    public function statistics()
    {

        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'open  statistics page ';
        $action->save();

        if (auth()->user()->can('statistics')) {
            if (auth()->user()->hasRole('Super Admin')) {
                $parks = Park::pluck('id');
            } else {
                $parks = auth()->user()->parks->pluck('id');
            }

            $park_time = ParkTime::whereIn('park_id', $parks)->where(function ($query) {
                $query->where('date', Carbon::now()->toDateString())
                    ->orWhere('date', Carbon::now()->subDay()->toDateString());
            })->get()->groupBy('park_id');
            $park_times = [];
            foreach ($park_time as $group) {
                $park_times[] = $group[0];
            }
            return view('admin.layout.statistics', compact('park_times'));
        }
    }

    public function statisticPark($id, Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'open  statistics page for park ';
        $action->save();

        $park_time = ParkTime::where('park_id', $id)->where(function ($query) {
            $query->where('date', Carbon::now()->toDateString())
                ->orWhere('date', Carbon::now()->subDay()->toDateString());
        })->first();
        if ($request->date) {
            $park_time = ParkTime::where('park_id', $id)->where('date', $request->date)->first();

        }


        return view('admin.layout.statistics_park', compact('park_time'));
    }

    protected function makeAllRead()
    {
        auth()->user()->notifications()->update(['read_at' => Carbon::now()]);
        return redirect()->back();
    }

    protected function allNotifications()
    {
        $notifications = Notification::where('notifiable_id', auth()->user()->id)->latest()?->get();
        return view('admin.notification.index', compact('notifications'));
    }

    protected function riders()
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'open riders number page  ';
        $action->save();

        if (auth()->user()->hasRole('Super Admin') || auth()->user()->hasRole('Client')) {
            $parks = Park::pluck('id');
        } else {
            $parks = auth()->user()->parks->pluck('id');
        }

        $park_time = ParkTime::whereIn('park_id', $parks)->orderBy('id', 'DESC')->where(function ($q) {
            $q->where('date', Carbon::now()->toDateString())->
            orWhere('date', Carbon::now()->subDay()->toDateString());
        })->get()->groupBy('park_id');

        $times = [];
        foreach ($park_time as $group) {
            $times[] = $group[0];
        }

        return view('admin.layout.riders', compact('times'));
    }

    protected function singlePage($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'open riders number page for park  ';
        $action->save();


        $cycleRecords = CycleRideNumber::whereHas('gameTime', function ($q) use ($id) {
            $q->where('park_time_id', $id);
        })->get();

         $flattenedLastRecords = $cycleRecords
            ->groupBy('ride_id')->sortBy('date');


         $times = $cycleRecords
            ->groupBy('date')
            ->map->last()
            ->values()
             ->sortBy('date');

        if (request()->ajax()) {
            return view('admin.layout.ajax_riders', compact('times', 'flattenedLastRecords','cycleRecords','id'));
        }
        return view('admin.layout.single_park', compact('times', 'flattenedLastRecords','cycleRecords','id'));
    }

    protected function ridersCycles()
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'open cycles number page  ';
        $action->save();


        if (auth()->user()->hasRole('Super Admin') || auth()->user()->hasRole('Client')) {
            $parks = Park::pluck('id');
        } else {
            $parks = auth()->user()->parks->pluck('id');
        }

        $park_time = ParkTime::whereIn('park_id', $parks)->orderBy('id', 'DESC')
            ->where(function ($q) {
                $q->where('date', Carbon::now()->toDateString())->
                orWhere('date', Carbon::now()->subDay()->toDateString());
            })
            ->get()->groupBy('park_id');

        $times = [];
        foreach ($park_time as $group) {
            $times[] = $group[0];
        }

        return view('admin.layout.cyclesRides.riders', compact('times'));
    }

    protected function singlePageRiderCycle($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'open cycles number page for park ';
        $action->save();


        $cycleRecords = CycleRideNumber::whereHas('gameTime', function ($q) use ($id) {
            $q->where('park_time_id', $id);
        })->get();

        $flattenedLastRecords = $cycleRecords
            ->groupBy('ride_id');

        $times = $cycleRecords
            ->groupBy('date')
            ->map->last()
            ->values();

        if (request()->ajax()) {
            return view('admin.layout.cyclesRides.ajax_riders', compact('times', 'flattenedLastRecords','cycleRecords','id'));
        }
        return view('admin.layout.cyclesRides.single_park', compact('times', 'flattenedLastRecords','cycleRecords','id'));
    }


}
