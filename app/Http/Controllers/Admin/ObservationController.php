<?php

namespace App\Http\Controllers\Admin;

use App\Models\Ride;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Observation\ObservationRequest;
use App\Models\Department;
use App\Models\GameTime;
use App\Models\User;
use App\Models\Observation;
use App\Models\ObservationCategory;
use App\Traits\ImageOperations;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;

class ObservationController extends Controller
{
    use ImageOperations;

    public function index(Request $request)
    {
        if ($request->ajax()) {
            if (auth()->user()->hasRole('Super Admin')) {
                $items = Observation::where('date_reported', Carbon::now()->format('Y-m-d'))->get();
            } else {
                $parks = auth()->user()->parks->pluck('id');

                if (auth()->user()->hasRole('Maintenance/Worker') || auth()->user()->hasRole('Maintenance/Supervisor') ||
                    auth()->user()->hasRole('Maintenance/Director') || auth()->user()->hasRole('Maintenance/Manager') ||
                    auth()->user()->hasRole('Maintenance/Chief')) {


                    $items = Observation::where('department_id', 1)->whereIn('park_id', $parks)
                        ->where('date_reported', Carbon::now()->format('Y-m-d'))->get();


                } elseif (auth()->user()->hasRole('Health & safety/Supervisor') || auth()->user()->hasRole('Health & safety/Manager') ||
                    auth()->user()->hasRole('Health & safety/Director')) {

                        $healthSupervisorIds = User::whereHas('roles', function ($query) {
                            return $query->where('name', 'Health & safety/Supervisor');
                        })->whereHas('parks', function ($query) use ($parks) {
                              $query->whereIn('park_id', $parks);
                        })->pluck('id');

                    $items = Observation::where(function ($query) use ($parks, $healthSupervisorIds) {
                            $query->where('department_id', 3)
                                ->whereIn('park_id', $parks)
                                ->orWhereIn('created_by_id', $healthSupervisorIds);
                        })
                        ->where('date_reported', Carbon::now()->format('Y-m-d'))
                        ->get();
                } elseif (auth()->user()->hasRole('Operations/Zones Manager') || auth()->user()->hasRole('Operations/Zone supervisor') ||
                    auth()->user()->hasRole('Operations/Operator')) {

                    $items = Observation::where('department_id', 2)->whereIn('park_id', $parks)
                        ->where('date_reported', Carbon::now()->format('Y-m-d'))->get();

                } elseif (auth()->user()->hasRole('Technical Services/Engineer') || auth()->user()->hasRole('Technical Services/Manager') ||
                    auth()->user()->hasRole('Technical Services/Director')) {

                    $items = Observation::where('department_id', 4)->whereIn('park_id', $parks)
                        ->where('date_reported', Carbon::now()->formate('Y-m-d'))
                        ->get();

                } elseif (auth()->user()->hasRole('Client')) {

                    $items = Observation::where('department_id', 5)->whereIn('park_id', $parks)
                        ->where('date_reported', Carbon::now()->format('Y-m-d'))
                        ->get();
                } else {
                    $items = Observation::whereIn('park_id', $parks)
                        ->where('date_reported', Carbon::now()->format('Y-m-d'))
                        ->get();
                }
            }
            return Datatables::of($items)
                ->addIndexColumn()
                ->addColumn('ride', function ($row) {
                    return $row->ride?->name;
                })
                ->addColumn('department', function ($row) {
                    return $row->department?->name;
                })
                ->addColumn('observationCategory', function ($row) {
                    return $row->observationCategory?->name;
                })
                ->addColumn('created_by', function ($row) {
                    return $row->created_by?->name;
                })
                ->addColumn('image', function ($row) {
                    if ($row->image != null) {
                        return ' <img class="img-preview"
                              src="' . Storage::disk('s3')->temporaryUrl('images/' . baseName($row->image), now()->addMinutes(30)) . '"
                                                 style="height: 50px; width: 50px">';
                    } else {
                        return '';
                    }
                })->addColumn('solve', function ($row) {
                    if (auth()->user()->can('observations-edit')) {
                        if ($row->date_resolved === null) {
                            $btn = ' <a href="' . route('admin.observations.edit', $row->id) . '" class="btn btn-info">Resolve</a>';
                        } else {
                            $btn = 'Solved';
                        }
                    }


                    return $btn;
                })
                ->addColumn('action', function ($row) {
                    $btn = '';
                    if ($row->date_resolved != null) {
                        $btn = ' <a href="' . route('admin.observations.edit', $row->id) . '"
                                               class="btn btn-info">Edit</a>';
                    }
                    if (auth()->user()->can('observations-delete')) {
                        $btn .= ' <a class="btn btn-danger" data-name="observation"
                                           data-url="' . route('admin.observations.destroy', $row->id) . '"
                                           onclick="delete_form(this)">
                                               Delete
                                        </a>';
                    }


                    return $btn;
                })
                ->rawColumns(['ride', 'department', 'observationCategory', 'created_by', 'image', 'solve', 'action'])
                ->make(true);
        }
        return view('admin.observations.index_datatable');
    }


    public function create()
    {
        return view('admin.branches.add');
    }

    public function add_observation($ride_id)
    {
        $departments = Department::pluck('name', 'id');
        $observationCategories = ObservationCategory::pluck('name', 'id');
        return view('admin.observations.add', compact('ride_id', 'departments', 'observationCategories'));
    }

    public function store(ObservationRequest $request)
    {
        //dd($request);
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add observation ';
        $action->save(); 

        $validate = $request->validated();
        $data = Arr::except($validate, 'image');
        $ride = Ride::find($validate['ride_id']);
        $data['park_id'] = $ride->park_id;
        $data['zone_id'] = $ride->zone_id;
        $data['created_by_id'] = auth()->user()->id;
        $gameTime = GameTime::where('ride_id', $validate['ride_id'])->orderBy('date', 'DESC')->latest()->first();
        $data['game_time_id'] = $gameTime?->id;
        $observation = Observation::query()->create($data);
        if (!empty($validate['image'])) {
            $path = Storage::disk('s3')->put('images', $validate['image']);
            $observation->update(['image' => $path]);
        }
        alert()->success('Observation Added successfully !');
        return redirect()->back();
    }


    public function edit($id)
    {
        $observation = Observation::find($id);
        $departments = Department::pluck('name', 'id');
        $observationCategories = ObservationCategory::pluck('name', 'id');

        return view('admin.observations.edit', compact('observation', 'departments', 'observationCategories'));

    }


    public function update(ObservationRequest $request, Observation $observation)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update observation ';
        $action->save(); 

        $validate = $request->validated();
        if (isset($validate['date_resolved']) && $validate['date_resolved'] == 1) {
            $validate['date_resolved'] = Carbon::now()->toDateTimeString();
        }
        $observation->update($validate);
        $observation->save();

        alert()->success('Updated successfully !');
        return redirect()->route('admin.observations.index');
    }

    public function destroy($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete observation ';
        $action->save(); 

        $observation = Observation::find($id);
        if ($observation) {
            $observation->delete();
            alert()->success('Observation deleted successfully');
            return back();
        }
        alert()->error('observation not found');
        return redirect()->route('admin.observations.index');
    }

    public function search(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'search observation ';
        $action->save(); 

        $date = $request->input('date');
        if (auth()->user()->hasRole('Super Admin')) {
            $items = Observation::where('date_reported', $date)->get();
        } else {
            $parks = auth()->user()->parks->pluck('id');

            if (auth()->user()->hasRole('Maintenance/Worker') || auth()->user()->hasRole('Maintenance/Supervisor') ||
                auth()->user()->hasRole('Maintenance/Director') || auth()->user()->hasRole('Maintenance/Manager') ||
                auth()->user()->hasRole('Maintenance/Chief')) {

                $items = Observation::where('department_id', 1)->whereIn('park_id', $parks)
                    ->where('date_reported', $date)->get();

                } elseif (auth()->user()->hasRole('Health & safety/Supervisor') || auth()->user()->hasRole('Health & safety/Manager') ||
                  auth()->user()->hasRole('Health & safety/Director')) {

                
                    $healthSupervisorIds = User::whereHas('roles', function ($query) {
                        return $query->where('name', 'Health & safety/Supervisor');
                    })->whereHas('parks', function ($query) use ($parks) {
                          $query->whereIn('park_id', $parks);
                    })->pluck('id');

                $items = Observation::where(function ($query) use ($parks, $healthSupervisorIds) {
                        $query->where('department_id', 3)
                            ->whereIn('park_id', $parks)
                            ->orWhereIn('created_by_id', $healthSupervisorIds);
                    })
                    ->where('date_reported', Carbon::now()->format('Y-m-d'))
                    ->get();

                    } elseif (auth()->user()->hasRole('Operations/Zones Manager') || auth()->user()->hasRole('Operations/Zone supervisor') ||
                        auth()->user()->hasRole('Operations/Operator')) {

                        $items = Observation::where('department_id', 2)->whereIn('park_id', $parks)
                    ->where('date_reported', $date)->get();

            } elseif (auth()->user()->hasRole('Technical Services/Engineer') || auth()->user()->hasRole('Technical Services/Manager') ||
                auth()->user()->hasRole('Technical Services/Director')) {

                $items = Observation::where('department_id', 4)->whereIn('park_id', $parks)
                    ->where('date_reported', $date)
                    ->get();

            } elseif (auth()->user()->hasRole('Client')) {

                $items = Observation::where('department_id', 5)->whereIn('park_id', $parks)
                    ->where('date_reported', $date)
                    ->get();
            } else {
                $items = Observation::whereIn('park_id', $parks)
                    ->where('date_reported', $date)
                    ->get();
            }
        }
        return view('admin.observations.index', compact('items'));

    }
}

