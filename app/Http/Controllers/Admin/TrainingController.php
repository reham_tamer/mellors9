<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Training\TrainingRequest;
use App\Models\Attendance;
use App\Models\Ride;
use App\Models\TrainingChecklist;
use App\Models\User;
use App\Models\Park;
use App\Models\Training;
use App\Models\UserPark;
use App\Models\Zone;
use App\Models\TrainingUser;
use App\Models\TrainingUserRide;
use App\Models\TrainingUserChecklist;

use Illuminate\Http\Request;
use function Symfony\Component\String\length;


//use DataTables;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $items = Training::get();
            $user_parks = Park::pluck('name', 'id')->all();
            } 
            else {
            $parks = auth()->user()->parks->pluck('id');
            $items = Training::whereIn('park_id', $parks)->get();
            $user_parks = auth()->user()->parks->pluck('name', 'id')->all();
        
        }
        $lists=TrainingChecklist::pluck('id','name')->all();
        return view('admin.trainings.index', compact('items','lists','user_parks'));
    }
    public function search(Request $request)
    {
           // dd($request);
           $items = [];
    
           $park_id = $request->query('park_id');
           $ride_id = $request->query('ride_id');
           $user_type = $request->query('user_type');
           $list_type = $request->query('list_type');
           $date = $request->query('start_date');

          // dd($operator_id );
           if (auth()->user()->hasRole('Super Admin')) {
               $parks = Park::pluck('id')->all();
               $user_parks = Park::pluck('name', 'id');
            } else {
               $parks = auth()->user()->parks->pluck('id');
               $user_parks = auth()->user()->parks->pluck('name', 'id');
            }
           if ($park_id != null) {
              
               $items = Training::where('park_id', $park_id)->where('start_date', $date);
                                   
               if($ride_id  != null){
                      $items->where('ride_id',$ride_id);
               }
                                     
               if($user_type  != null){
                $items->where('user_type',$user_type);
               }

               $items=$items->get();
                 //dd($items);
               $lists=TrainingChecklist::pluck('id','name')->all();

                 return view('admin.trainings.index', compact('items','lists','user_parks'));
                } else {
                      return redirect()->route('admin.training.index');
           }

    }

    public function create()
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $userParks = Park::query()->pluck('id')->toArray();
        } else {
            $userParks = auth()->user()->parks->pluck('id');
        }
        $parks = Park::whereIn('id', $userParks)->pluck('name', 'id')->all();

        return view('admin.trainings.add', compact('parks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TrainingRequest $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add training list ';
        $action->save();

        //  dd($request->validated());
        $training=$request->validated();
        $list = TrainingChecklist::find($training['training_checklist_id']);
        $training_type=$list->training_type;
//dd($training_type);
        $training = Training::query()->create(
        array_merge($training, ['created_by_id' => auth()->user()->id,
                                            'type'=>$training_type]));
        foreach ($request->user_ids as $user_id) {
            if ($request->user_type == 'attendance') {
                $training->trainingUsers()->create([
                    'userable_id' => $user_id,
                    'userable_type' => Attendance::class,
                ]);
            } else {
                $training->trainingUsers()->create([
                    'userable_id' => $user_id,
                    'userable_type' => User::class,
                ]);
            }
        }
        alert()->success('Training List added to users successfully !');

        return redirect()->route('admin.training.index');
    }


    public function edit(Training $training)
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $userParks = Park::query()->pluck('id')->toArray();
        } else {
            $userParks = auth()->user()->parks->pluck('id');
        }
        $parks = Park::whereIn('id', $userParks)->pluck('name', 'id')->all();
        $rides = Ride::where('park_id', $training->park_id)->pluck('name', 'id');
        $checklists = TrainingChecklist::all()->pluck('name', 'id');
        if ($training->user_type == 'operator') {
            $users = User::whereIn('id', $training->trainingUsers->pluck('userable_id'))->pluck('name', 'id');
            $usersItems = User::whereIn('id', $training->trainingUsers->pluck('userable_id'))->pluck('id');

        } else {
            $users = Attendance::whereIn('id', $training->trainingUsers->pluck('userable_id'))->pluck('name', 'id');
            $usersItems = Attendance::whereIn('id', $training->trainingUsers->pluck('userable_id'))->pluck('id');
        }
        $userspark = UserPark::query()
            ->where('park_id', $training->park_id)
            ->pluck('user_id');

        $supervisors = User::query()
            ->whereIn('id', $userspark)
            ->whereHas('roles', function ($query) {
                $query->whereIn('name', [
                    'Operations/Zone supervisor',
                    'Health & safety/Supervisor',
                    'Maintenance/Supervisor'
                ]);          
              })
            ->get()->pluck('name','id');
        $managers = User::query()
            ->whereIn('id', $userspark)
            ->whereHas('roles', function ($query) {
                $query->where('name', 'Management/Operations Manager');
            })
            ->get()->pluck('name','id');


        return view('admin.trainings.edit', compact('parks', 'training', 'rides', 'checklists', 'users', 'usersItems','supervisors','managers'));

    }


    public function update(TrainingRequest $request, Training $training)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update training list ';
        $action->save();
        $validate = $request->validated();

        $training->update($validate);
        $training->trainingUsers()->delete();
        foreach ($request->user_ids as $user_id) {
            if ($request->user_type == 'attendance') {
                $training->trainingUsers()->create([
                    'userable_id' => $user_id,
                    'userable_type' => Attendance::class,
                ]);
            } else {
                $training->trainingUsers()->create([
                    'userable_id' => $user_id,
                    'userable_type' => User::class,
                ]);
            }
        }
        alert()->success('Attendance edited successfully !');
        return redirect()->route('admin.training.index');
    }
    protected function showUsersLists($id)
    {
        $users=TrainingUser::where('training_id',$id)->get();
        return view('admin.trainings.users_list', compact('users'));

    }
    protected function showUserChecklist($user_id,$training_id)
    {
        $training=Training::find($training_id);
        $items=TrainingUserChecklist::where('training_id',$training_id)->where('userable_id',$user_id)->get();
        $trainingUser=TrainingUser::where('training_id',$training_id)->where('userable_id',$user_id)->first();
         if($training->type == 'evacuation'){
            $trainingUserRides=TrainingUserRide::where('training_id',$training_id)->where('attendance_id',$user_id)->get();
         return view('admin.trainings.show_evac_training', compact('items','training','trainingUser','trainingUserRides'));
 
        }
        // dd($items);
        return view('admin.trainings.show', compact('items','training','trainingUser'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(Training $training)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete training list ';
        $action->save();

        if ($training->user_checklist->isNotEmpty()) {
            alert()->error('you cant delete this training!');
            return back();
        } else {
            $training->trainingUsers()->delete();
            $training->delete();
            alert()->success('user deleted successfully !');
            return back();
        }

    }


}
