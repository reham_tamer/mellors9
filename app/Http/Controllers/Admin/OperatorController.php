<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Attendance\AttendanceRequest;
use App\Http\Requests\Dashboard\User\OperatorTimeRequest;
use App\Models\Attendance;
use App\Models\UserPark;
use App\Models\UserLog;
use App\Models\User;
use App\Models\Park;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use function Symfony\Component\String\length;
use Maatwebsite\Excel\Facades\Excel;


//use DataTables;

class OperatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if (auth()->user()->hasRole('Super Admin')) {
                $items = User::query()->whereHas('roles', function ($query) {
                    $query->whereIn('name', ['Operations/Operator', 'Breaker']); 
                })
                ->get();          
            } else {
                $users = UserPark::query()
                ->whereIn('park_id', auth()->user()->parks?->pluck('id')->toArray())
                ->pluck('user_id');

                $items = User::query()
                    ->whereIn('id', $users)
                    ->whereHas('roles', function ($query) {
                        $query->whereIn('name', ['Operations/Operator', 'Breaker']); 
                    })
                    ->get();
            }

            return Datatables::of($items)
                ->addIndexColumn()
                ->addColumn('name', function ($row) {

                    return $row->name;
                })
                ->addColumn('code', function ($row) {

                    return $row->code;
                })
                ->addColumn('park', function ($row) {
                    return $row->parks()?->first()?->name;

                })
                ->addColumn('ride', function ($row) {
                    if($row->roles()?->first()?->name == 'Breaker'){
                        $rides = $row->rides()?->get();
                        if ($rides->isNotEmpty()) {
                            $rideList = '<ul>';
                            foreach ($rides as $ride) {
                                $rideList .= '<li>' . $ride->name . '</li>';
                            }
                            $rideList .= '</ul>';
                            return $rideList;
                        }
                        
                    }
                    else
                    return $row->rides()?->first()?->name;
                })
                ->addColumn('role', function ($row) {
                    return $row->roles()?->first()?->name;

                })
                ->addColumn('status', function ($row) {
                    return $row->status == 1 ? '<sapn class="bg-success text-white" style="padding: 5px">Active</sapn>' : ' <sapn class="bg-danger text-white" style="padding: 5px">Not active</sapn>';

                })
                ->addColumn('add_time', function ($row) {
                    $btn = '';
                    if (auth()->user()->can('users-edit') || auth()->user()->hasRole('Park Admin')) {
                        $btn = ' <a href="' . route('admin.operatorAddTime', $row->id) . '" class="btn btn-warning">Add</a>';
                    }
                    return $btn;
                })
               
                ->rawColumns(['name', 'park','role', 'code','status','ride','add_time'])
                ->make(true);
        }

        return view('admin.operators.index');
    }


    public function create()
    {

    }
    public function addTime($id)
    {
        

        $user_id=$id;
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $userParks = auth()->user()->parks->pluck('id');
            $parks = Park::whereIn('id', $userParks)->pluck('name', 'id')->all();
        }

        return view('admin.operators.add_time', compact('parks','user_id'));
    }

    public function store(OperatorTimeRequest $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add operator time ';
        $action->save(); 

       $validated= $request->validated();
       $start_time = \Carbon\Carbon::parse($validated['date_time_start']);
       $end_time = \Carbon\Carbon::parse($validated['date_time_end']);
       $shift_minutes= $end_time->diffInMinutes($start_time);
      
       $start_date = $start_time->toDateString();
       $start_time = $start_time->toTimeString();
       $end_time = $end_time->toDateTimeString();
       $type='logout';

       UserLog::query()->create([
            'shift_minutes' => $shift_minutes ,
            'type'=>$type,
            'user_id'=> $validated['user_id'],
            'date'=> $start_date,
            'time'=> $start_time ,
            'ride_id'=> $validated['ride_id'],
            'park_id'=> $validated['park_id'], 
            'end_time'=> $end_time, 
            'open_date'=>$validated['open_date'] 
                ]);
        
        alert()->success('Operators Time added successfully !');
        return redirect()->route('admin.operators.index');
    }
    public function uploadOperatorTime()
    {
        return view('admin.operators.exce_time_upload');
    }

    public function uploadOperatorTimeExcleFile(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'upload operator time excle file ';
        $action->save(); 

        $this->validate($request, ['file' => 'required']);
        Excel::import(new \App\Imports\OperatorsTime(), $request->file('file'));
        alert()->success('Operators Time Added successfully !');
        return redirect()->route('admin.operators.index');
    }

    public function edit(Attendance $attendance)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(AttendanceRequest $request, Attendance $attendance)
    {
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(Attendance $attendance)
    {
      
    }

    public function uploadAttendance()
    {
        return view('admin.attendances.exce_upload');
    }

    public function uploadAttendancesExcleFile(Request $request)
    {
        
        $this->validate($request, ['file' => 'required']);
        Excel::import(new \App\Imports\Attendances(), $request->file('file'));
        alert()->success('Attendances Added successfully !');
        return redirect()->route('admin.attendances.index');
    }

   

    public function delete(AttendanceLog $attendanceLog)
    {
        $attendanceLog->delete();
        alert()->success('Deleted Attendances Time successfully !');
        return redirect()->back();
    }

}
