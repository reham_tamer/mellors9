<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\CustomerFeedback\FeedbackSubCategoryRequest;
use App\Models\StopageCategory;
use App\Models\CustomerComplaint;
use App\Models\FeedbackSubCategory;
use Illuminate\Http\Request;

class FeedbackSubCategoryController extends Controller
{
    public function index()
    {
        $items=FeedbackSubCategory::all();
        return view('admin.feedback_sub_category.index',compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $main_categories=CustomerComplaint::pluck('name','id')->toArray();

        return view('admin.feedback_sub_category.add',compact('main_categories'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FeedbackSubCategoryRequest $request)
    {
        FeedbackSubCategory::create($request->validated());
        alert()->success('Feedback sub Category Added successfully !');
        return redirect()->route('admin.feedback_sub_category.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $main_categories=CustomerComplaint::pluck('name','id')->toArray();
        $item=FeedbackSubCategory::findOrFail($id);
        return view('admin.feedback_sub_category.edit',compact('item','main_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FeedbackSubCategoryRequest  $request, $id)
    {
        $item=FeedbackSubCategory::findOrFail($id);
        $item->update($request->validated());
        alert()->success('Feedback Sub Category updated successfully !');
        return redirect()->route('admin.feedback_sub_category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feedback_sub_category=FeedbackSubCategory::find($id);
        if ($feedback_sub_category){
            $feedback_sub_category->delete();
            alert()->success('Feedback SubCategory deleted successfully');
            return back();
        }
        alert()->error('Feed backSubCategory not found');
        return redirect()->route('admin.feedback_sub_category.index');
    }
}
