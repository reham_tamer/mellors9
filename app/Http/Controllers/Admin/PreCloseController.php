<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Accident\AccidentRequest;
use App\Models\PreopeningList;
use App\Models\PreopenInfo;
use App\Models\Park;
use App\Models\Ride;
use Carbon\Carbon;

class PreCloseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }
    public function show_preclose_list($ride_id, $park_time_id)
    {
        
        $list = PreopenInfo::where('park_time_id', $park_time_id)
            ->where('ride_id', $ride_id)->where('type','preclosing')->with('approve_by')
            ->first();

            if(! $list){
                alert()->error('Precolsing not added yet for this ride !');

                return redirect()->back();
            }
        $preopenInfo_id=$list->id;
        $items = PreopeningList::where('preopen_info_id',$preopenInfo_id)->get();
        return view('admin.preclose.show', compact('items', 'list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
    }
    
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccidentRequest $request)
    {
    }
    public function show(AccidentRequest $request)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AccidentRequest $request, Accident $accident)
    {
       
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }
}
