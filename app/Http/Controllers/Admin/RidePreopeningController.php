<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\InspectionList\RideInspectionListRequest;
use App\Models\InspectionList;
use App\Models\Ride;
use Illuminate\Http\Request;
use App\Models\RideInspectionList;
use Carbon\Carbon;

class RidePreopeningController extends Controller
{

    public function index()
    {
    }


    public function create()
    {
    }

       public function add_ride_preopen_elements($ride_id)
    {
        $inspection_list=InspectionList::where('type','preopening')->get();
        return view('admin.ride_preopening.add',compact('inspection_list','ride_id'));
    }

    public function edit_ride_preopen_elements( $ride_id)
    {
        $ride=Ride::find($ride_id);
        $list=RideInspectionList::where('ride_id',$ride_id)
        ->where('lists_type','preopening')->pluck('inspection_list_id')->toArray();

        $inspection_list=InspectionList::where('type','preopening')->get();
        $rides=Ride::pluck('name','id')->toArray();
        return view('admin.ride_preopening.edit',compact('inspection_list','ride','list','ride_id'));

    }

    public function store(RideInspectionListRequest $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add Ride Preopening Elements ';
        $action->save(); 

        $ride = Ride::find($request['ride_id']);
        $inspectionListIds = $request['inspection_list_id'];
        $ride->inspection_list()->attach($inspectionListIds, ['lists_type' => 'preopening']);
         alert()->success('Ride Preopening Elements Added Successfully !');
        return redirect()->route('admin.ride_inspection_lists.index');
    }

    public function update_ride_preopen_elements(Request $request,  $ride_id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add Ride preopening Elements ';
        $action->save(); 

        RideInspectionList::where('ride_id',$request->ride_id)->where('lists_type','preopening')->delete();
        $ride = Ride::find($request['ride_id']);
        $inspectionListIds = $request['inspection_list_id'];
        $ride->inspection_list()->attach($inspectionListIds, ['lists_type' => 'preopening']);
        alert()->success('Ride Preopening Elements Updated Successfully !');
        return redirect()->route('admin.ride_inspection_lists.index');
    }

    public function edit( $id)
    {


    }

    public function update(RideInspectionListRequest $request, RideInspectionList $rideInspectionList)
    {
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }


}
