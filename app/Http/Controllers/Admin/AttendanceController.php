<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Attendance\AttendanceRequest;
use App\Http\Requests\Dashboard\Attendance\AttendanceTimeRequest;
use App\Models\Attendance;
use App\Models\AttendanceLog;
use App\Models\Park;
use App\Models\Ride;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use function Symfony\Component\String\length;
use Maatwebsite\Excel\Facades\Excel;


//use DataTables;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if (auth()->user()->hasRole('Super Admin')) {
                $items = Attendance::query();
            } else {

                $items = Attendance::whereHas('park', function ($query) {
                    return $query->whereIn('park_id', auth()->user()->parks?->pluck('id')->toArray());
                });

            }

            return Datatables::of($items)
                ->addIndexColumn()
                ->addColumn('name', function ($row) {

                    return $row->name;
                })
                ->addColumn('code', function ($row) {

                    return $row->code;
                })
                ->addColumn('park', function ($row) {
                    return $row->park?->name;

                })
                 ->addColumn('status', function ($row) {

                    return $row->status == 1 ? '<sapn class="bg-success text-white" style="padding: 5px">Active</sapn>' : ' <sapn class="bg-danger text-white" style="padding: 5px">Not active</sapn>';
                })
                ->addColumn('add_time', function ($row) {
                    $btn = '';
                    if (auth()->user()->can('users-edit') || auth()->user()->hasRole('Park Admin')|| auth()->user()->hasRole('Training Admin')) {
                        $btn = ' <a href="' . route('admin.addTime', $row->id) . '" class="btn btn-warning">Add</a>';
                    }
                    return $btn;
                })
                ->addColumn('action', function ($row) {
                    $btn = '';

                    if (auth()->user()->can('users-edit') || auth()->user()->hasRole('Park Admin') || auth()->user()->hasRole('Training Admin')) {
                        $btn = ' <a href="' . route('admin.attendances.edit', $row->id) . '" class="btn btn-info">Edit</a>';
                    }


                    if (auth()->user()->can('users-delete')) {
                        $btn .= ' <a class="btn btn-danger" data-name="' . $row->name . '"
                                           data-url="' . route('admin.attendances.destroy', $row->id) . '"
                                           onclick="delete_form(this)">
                                               Delete
                                        </a>';
                    }


                    return $btn;
                })
                ->rawColumns(['name', 'park', 'code', 'status','add_time', 'action'])
                ->make(true);
        }

        return view('admin.attendances.index');
    }


    public function create()
    {

        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $userParks = auth()->user()->parks->pluck('id');
            $parks = Park::whereIn('id', $userParks)->pluck('name', 'id')->all();
        }

        return view('admin.attendances.add', compact('parks'));
    }
    public function addTime($id)
    {
        $user_id=$id;
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $userParks = auth()->user()->parks->pluck('id');
            $parks = Park::whereIn('id', $userParks)->pluck('name', 'id')->all();
        }

        return view('admin.attendances.add_time', compact('parks','user_id'));
    }

    public function storeTime(AttendanceTimeRequest $request)
    {
       $validated= $request->validated();
       $start_time = \Carbon\Carbon::parse($validated['date_time_start']);
       $end_time = \Carbon\Carbon::parse($validated['date_time_end']);

       $shift_minutes= $end_time->diffInMinutes($start_time);
       $type='logout';
       AttendanceLog::query()->create(
        array_merge($validated, ['shift_minutes' => $shift_minutes , 'type'=>$type]));

        alert()->success('Attendance Time added successfully !');
        return redirect()->route('admin.attendances.index');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttendanceRequest $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'Add new attendant';
        $action->save();
 
        $attendance = Attendance::create($request->validated());
       
       // $request->ride_id ? $attendance->rides()->sync($request->ride_id) : '';

        alert()->success('Attendance added successfully !');
        return redirect()->route('admin.attendances.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit(Attendance $attendance)
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
            $rides = Ride::where('park_id',$attendance->park_id)->pluck('name', 'id')->all();
            $attendanceRide = $attendance->rides->pluck('id')->toArray();

        } else {
            $userParks = auth()->user()->parks->pluck('id');
            $parks = Park::whereIn('id', $userParks)->pluck('name', 'id')->all();
            $rides = Ride::where('park_id',$attendance->park_id)->pluck('name', 'id')->all();
            $attendanceRide = $attendance->rides->pluck('id')->toArray();
        }
        return view('admin.attendances.edit')->with([
            'user' => $attendance,
            'parks' => $parks,
            'rides' => $rides,
            'attendanceRide' => $attendanceRide,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(AttendanceRequest $request, Attendance $attendance)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update attendant';
        $action->save();

        $validate = $request->validated();
        $attendance->update($validate);
/*         if (!empty($validate['ride_id'])) {
            $rideIds = array_filter($validate['ride_id'], function ($id) {
                return !empty($id);
            });
    
            if (!empty($rideIds)) {
                $attendance->rides()->sync($rideIds);
            }
        } */
        
        alert()->success('Attendance edited successfully !');
        return redirect()->route('admin.attendances.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(Attendance $attendance)
    {
        $attendance->delete();
        alert()->success('user deleted successfully !');
        return back();
    }

    public function uploadAttendance()
    {
        return view('admin.attendances.exce_upload');
    }

    public function uploadAttendancesExcleFile(Request $request)
    { 
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'upload attendants excle  file';
        $action->save(); 

        $this->validate($request, ['file' => 'required']);
        Excel::import(new \App\Imports\Attendances(), $request->file('file'));
        alert()->success('Attendances Added successfully !');
        return redirect()->route('admin.attendances.index');
    }

    public function uploadAttendanceTime()
    {
        return view('admin.attendances.exce_time_upload');
    }

    public function uploadAttendancesTimeExcleFile(Request $request)
    {
        
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'upload attendants time excle  file';
        $action->save(); 

        $this->validate($request, ['file' => 'required']);
        Excel::import(new \App\Imports\AttendancesTime(), $request->file('file'));
        alert()->success('Attendants Time Added successfully !');
        return redirect()->route('admin.attendances.index');
    }
    public function show_time_update(Request $request)
    {
        
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update  attendants time ';
        $action->save(); 

        $log = AttendanceLog::find($request->id);
        $log->update([
            'date_time_start' => $request->start,
            'date_time_end' => $request->end,
            'shift_minutes' => $request->minutes,
        ]);
        return response()->json();
    }

    public function delete(AttendanceLog $attendanceLog)
    {
        
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete attendant';
        $action->save(); 

        $attendanceLog->delete();
        alert()->success('Deleted Attendances Time successfully !');
        return redirect()->back();
    }

}
