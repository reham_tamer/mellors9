<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\ScheduleParkTimeRequest;
use App\Models\Park;
use App\Models\ParkTime;
use App\Models\ScheduleParkTime;
use Illuminate\Support\Carbon;


class ScheduleParkTimeController extends Controller
{

    public function index()
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('id');
        } else {
            $parks = auth()->user()->parks->pluck('id');
        }

        if (auth()->user()->hasRole('Super Admin')) {
            $items = ScheduleParkTime::whereNull('parent_id')->get();
        } else {
            $items = ScheduleParkTime::whereIn('park_id', $parks)->whereNull('parent_id')->get();
        }


        return view('admin.schedule_park_times.index', compact('parks', 'items'));
    }


    public function create()
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->toArray();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->toArray();
        }
        return view('admin.schedule_park_times.add', compact('parks'));
    }


    public function store(ScheduleParkTimeRequest $request)
    {

        $data = $request->validated();
        $startDate = Carbon::parse($data['start_date']);
        $endDate = Carbon::parse($data['end_date']);
        $dates = [];
        while ($startDate->lte($endDate)) {
            $dates[] = $startDate->toDateString(); // Add the date to the array
            $startDate->addDay(); // Move to the next day
        }
        $parent = ScheduleParkTime::create($data);
        foreach ($dates as $date) {
            ScheduleParkTime::query()->create([
                'start_date' => $date,
                'end_date' => Carbon::parse($date)->addDay(),
                'start' => $data['start'],
                'end' => $data['end'],
                'park_id' => $data['park_id'],
                'parent_id' => $parent->id,
            ]);
        }


        alert()->success('Schedule Time Slot Added successfully');
        return redirect()->route('admin.schedule_park_times.index');
    }


    public function edit($id)
    {
        $time = ScheduleParkTime::find($id);
        $parks = Park::pluck('name', 'id')->all();
        return view('admin.schedule_park_times.edit', compact('parks', 'time'));
    }


    public function update(ScheduleParkTimeRequest $request, ScheduleParkTime $scheduleParkTime)
    {

        $data = $request->validated();
        $scheduleParkTime->update($data);
        ScheduleParkTime::where('parent_id',$scheduleParkTime->id)->delete();
        $startDate = Carbon::parse($data['start_date']);
        $endDate = Carbon::parse($data['end_date']);
        $dates = [];
        while ($startDate->lte($endDate)) {
            $dates[] = $startDate->toDateString(); // Add the date to the array
            $startDate->addDay(); // Move to the next day
        }
        foreach ($dates as $date) {
            ScheduleParkTime::query()->create([
                'start_date' => $date,
                'end_date' => Carbon::parse($date)->addDay(),
                'start' => $data['start'],
                'end' => $data['end'],
                'park_id' => $data['park_id'],
                'parent_id' => $scheduleParkTime->id,
            ]);
        }
        alert()->success('Schedule Park Time Slot Updated successfully !');
        return redirect()->route('admin.schedule_park_times.index');
    }


    public function destroy($id)
    {

        $parkTime = ScheduleParkTime::find($id);
        ScheduleParkTime::where('parent_id',$parkTime->id)->delete();
        $parkTime->delete();
        alert()->success('Schedule Park Time Slot deleted successfully');
        return back();

    }


}

