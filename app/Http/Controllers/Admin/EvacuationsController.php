<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Dashboard\Accident\EvacuationRequest;
use App\Models\Evacuation;
use App\Models\GameTime;
use App\Models\RideStoppages;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Accident\AccidentRequest;
use App\Http\Requests\Dashboard\Accident\InvestigationRequest;
use App\Models\Accident;
use App\Models\Department;
use App\Models\GeneralIncident;
use App\Models\Park;
use App\Models\Zone;
use App\Models\ParkTime;
use App\Models\Ride;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class EvacuationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('id')->all();
            $userparks = Park::pluck('name', 'id')->all();

        } else {
            $parks = auth()->user()->parks->pluck('id');
            $userparks = auth()->user()->parks->pluck('name', 'id');

        }
        $startDate = dateTime()?->date;
        $endDate = dateTime()?->close_date;


        if ($startDate && $endDate) {
            $items = Evacuation::whereIn('park_id', $parks)
                ->where(function ($query) use ($startDate, $endDate) {
                    $query->whereBetween('submit_date', [$startDate, $endDate]);
                })
                ->get();

            return view('admin.evacuation.index', compact('items', 'userparks'));
        } else {
            $items = [];
            return view('admin.evacuation.index', compact('items', 'userparks'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

//        if (auth()->user()->hasRole('Super Admin')) {
//            $parks = Park::pluck('name', 'id')->all();
//            $rides = Ride::pluck('name', 'id')->all();
//
//        } else {
//            $parks = auth()->user()->parks->pluck('name', 'id')->all();
//            $rides = auth()->user()->rides->pluck('name', 'id')->all();
//        }
        $stoppages = RideStoppages::where('date', Carbon::now()->format('Y-m-d'))
            ->where('ride_status', 'stopped')->whereIn('stopage_sub_category_id', [12, 13])->get();

        return view('admin.evacuation.add', compact('stoppages'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(EvacuationRequest $request)
    {

        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add evacuation';
        $action->save(); 

        // dd($request);
        $data = $request->validated();
        // dd($request['park_id']);
        isset($data['normal']) ? $data['normal'] = 1 : '';
        isset($data['abnormal']) ? $data['abnormal'] = 1 : '';
        isset($data['longer_than_normal']) ? $data['longer_than_normal'] = 1 : '';
        isset($data['medics_required']) ? $data['medics_required'] = 1 : '';
        isset($data['civil_defense_involved']) ? $data['civil_defense_involved'] = 1 : '';
        isset($data['vip_guest_involved']) ? $data['vip_guest_involved'] = 1 : '';
        isset($data['customer_service_issue']) ? $data['customer_service_issue'] = 1 : '';
        isset($data['property_damage']) ? $data['property_damage'] = 1 : '';

        if (isset($data['stoppage_id']) && $data['stoppage_id'] != '') {
            //dd($data['stoppage_id']);
            $stoppage = RideStoppages::find($data['stoppage_id']);
            $gameTime = GameTime::where('ride_id', $stoppage->ride_id)->where('park_time_id', $stoppage->park_time_id)->first();
            $data['ride_id'] = $stoppage->ride_id;
            $data['park_id'] = $stoppage->park_id;
            $data['game_time_id'] = $gameTime?->id;
            $data['park_time_id'] = $stoppage->park_time_id;
            $data['submit_date'] = $stoppage->date;
        } else
            if (isset($request['park_id']) && $request['park_id'] != '') {
                $lastParkTime = ParkTime::where('park_id', $request['park_id'])->orderBy('id', 'desc')->first();
                $data['submit_date'] = $lastParkTime?->date;
                $data['park_id'] = $request['park_id'];
                $data['park_time_id'] = $lastParkTime?->id;
            } else if (isset($request['park_time_id']) && $request['park_time_id'] != '') {
                $parkTime = ParkTime::find($request['park_time_id']);
                $data['submit_date'] = $parkTime?->date;
                $data['park_id'] = $parkTime?->park_id;
                $data['park_time_id'] = $request['park_time_id'];
            }
        $data['created_by'] = auth()->id();
        $evacuation = Evacuation::query()->create($data);
        if (!empty($validate['image'])) {
            $path = Storage::disk('s3')->put('images', $validate['image']);
            $evacuation->update(['image' => $path]);
        }
        alert()->success('Add Evacuation successfully !');
        return redirect()->route('admin.evacuations.index');
    }


    public function evacuationStoppages()
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add evacuation due to stoppage';
        $action->save(); 

        $userparks = auth()->user()->parks->pluck('name', 'id');

        $parks = auth()->user()->parks->pluck('id');
        $startDate = dateTime()?->date;
        $endDate = dateTime()?->close_date;

        if ($startDate && $endDate) {

            $items = RideStoppages::where('ride_status', 'stopped')
                ->whereIn('stopage_sub_category_id', [12, 13])
                ->whereIn('park_id', $parks)
                ->where(function ($query) use ($startDate, $endDate) {
                    $query->whereBetween('date', [$startDate, $endDate]);
                })
                ->get();
        } else
            $items = [];

        return view('admin.evacuation.stoppages_index', compact('items'));
    }

    public function addEvacuationRide($id)
    {
        $stoppage = RideStoppages::find($id);

        return view('admin.evacuation.add', compact('stoppage'));
    }

    public function addEvacuationPark($id)
    {
        $park_time_id = $id;

        return view('admin.evacuation.add', compact('park_time_id'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $evacuation = Evacuation::find($id);
        return view('admin.evacuation.edit', compact('evacuation'));

    }

    public function show($id)
    {
        $stoppages = RideStoppages::where('date', Carbon::now()->format('Y-m-d'))
            ->where('ride_status', 'stopped')->whereIn('stopage_sub_category_id', [12, 13])->get();
        $evacuation = Evacuation::find($id);
        return view('admin.evacuation.show', compact('stoppages', 'evacuation'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EvacuationRequest $request, $id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update evacuation';
        $action->save(); 


        $data = $request->validated();
        isset($data['abnormal']) ? $data['abnormal'] = 1 : $data['abnormal'] = null;
        isset($data['normal']) ? $data['normal'] = 1 : $data['normal'] = null;
        isset($data['longer_than_normal']) ? $data['longer_than_normal'] = 1 : $data['longer_than_normal'] = null;
        isset($data['medics_required']) ? $data['medics_required'] = 1 : $data['medics_required'] = null;
        isset($data['civil_defense_involved']) ? $data['civil_defense_involved'] = 1 : $data['civil_defense_involved'] = null;
        isset($data['vip_guest_involved']) ? $data['vip_guest_involved'] = 1 : $data['vip_guest_involved'] = null;
        isset($data['customer_service_issue']) ? $data['customer_service_issue'] = 1 : $data['customer_service_issue'] = null;
        isset($data['property_damage']) ? $data['property_damage'] = 1 : $data['property_damage'] = null;
        $evacuation = Evacuation::findOrFail($id);
        $evacuation->update($data);
        if (!empty($validate['image'])) {
            $path = Storage::disk('s3')->put('images', $validate['image']);
            $evacuation->update(['image' => $path]);
        }

        alert()->success('Evacuations form updated successfully !');
        return redirect()->route('admin.evacuations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete evacuation';
        $action->save(); 

        $evacuation = Evacuation::find($id);
       if ($evacuation) {
            $evacuation->delete();
            alert()->success('Evacuation  deleted successfully');
            return back();
       }
        alert()->error('Evacuation  not found');
        return redirect()->route('admin.evacuation.index');
    }
    public function search(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'search evacuation';
        $action->save(); 


//         dd($request);
        $date = $request->input('date');
        $park_id = $request->input('park_id');
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('id')->all();
            $userparks = Park::pluck('name', 'id')->all();

        } else {
            $parks = auth()->user()->parks->pluck('id');
            $userparks = auth()->user()->parks->pluck('name', 'id');

        }
        $timeSlot = ParkTime::where('date', $date)->where('park_id', $park_id)->first();
        if ($timeSlot != null) {
            $startDate = $timeSlot->date;
            $endDate = $timeSlot->close_date;
            $items = Evacuation::where('park_id', $park_id)
                ->where(function ($query) use ($startDate, $endDate) {
                    $query->whereBetween('submit_date', [$startDate, $endDate]);
                })->get();

            return view('admin.evacuation.index', compact('items', 'userparks'));
        } else {
            $items = [];
            return view('admin.evacuation.index', compact('items', 'userparks'));
        }
    }


    protected function reportEvacuation(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'make  evacuation report';
        $action->save(); 

        $outRang = false;
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();

        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        if ($request->ajax()) {
            $park_id = $request->input('park_id');
            $items = Evacuation::where('park_id', $park_id);


            if ($request->input('ride_id')) {
                $items = $items->where('ride_id', $request->input('ride_id'));
            }
            if ($request->input('from')) {
                $items = $items->where('submit_date','>=', $request->input('from'));
            }
            if ($request->input('to')) {
                $items = $items->where('submit_date','<=', $request->input('to'));
            }
            $datetimeFrom = Carbon::parse($request->input('from'));
            $datetimeTo = Carbon::parse($request->input('to'));
            $interval = $datetimeFrom->diffInDays($datetimeTo);
            if ($interval > 30){
                $outRang = true;
            }
//            dd($outRang);
            $items = $items->get();
            return view('admin.evacuation.ajax', compact('items', 'parks','outRang'));

        }

        return view('admin.evacuation.report', compact('parks'));

    }


}
