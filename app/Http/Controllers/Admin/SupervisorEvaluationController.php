<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Dashboard\Accident\EvaluationRequest;
use App\Models\Evaluation;
use App\Models\EvaluationInfo;
use App\Models\User;
use App\Models\UserPark;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Park;
use App\Models\Zone;
use App\Models\ParkTime;
use App\Models\Ride;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class SupervisorEvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('id')->all();
            $userparks = Park::pluck('name', 'id')->all();

        } else {
            $parks = auth()->user()->parks->pluck('id');
            $userparks = auth()->user()->parks->pluck('name', 'id');

        }

        $items = Evaluation::whereIn('park_id', $parks)->where('supervisor_id', '!=', null)
            ->where('date', Carbon::now()->toDateString())
            ->get();
        return view('admin.supervisor_evaluation.index', compact('items', 'userparks'));


    }

    public function search(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = ' serech on supervisor evaluation report';
        $action->save();


        $items = [];

        $date = $request->query('date');
        $dateTo = $request->query('date_to');

        $park_id = $request->query('park_id');
        $supervisor_id = $request->query('supervisor_id');
        // dd($supervisor_id );
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('id')->all();
            $userparks = Park::pluck('name', 'id');
        } else {
            $parks = auth()->user()->parks->pluck('id');
            $userparks = auth()->user()->parks->pluck('name', 'id');
        }
        if ($park_id != null) {
            $items = Evaluation::where('park_id', $park_id)
                ->where('date', '>=', $date)->whereNotNull('supervisor_id');
            if ($dateTo) {
                $items = $items->where('date', '<=', $dateTo);
            }
            if ($supervisor_id != null) {
                $items->where('supervisor_id', $supervisor_id);
            }
            $items = $items->get();
            //dd($items);
            $users = UserPark::query()->where('park_id', $request->park_id)->
            whereHas('users.roles', function ($query) {
                return $query->where('name', 'Operations/Zone supervisor');
            })->pluck('user_id');
            $users = User::whereIn('id',$users)->pluck('name','id');
            return view('admin.supervisor_evaluation.index', compact('items', 'userparks','users'));
        } else {
            $items = Evaluation::whereIn('park_id', $parks)->where('supervisor_id', '!=', null)
                ->where('date', Carbon::now()->toDateString())
                ->get();
        }
        // $items=[];
        return view('admin.supervisor_evaluation.index', compact('items', 'userparks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(EvacuationRequest $request)
    {

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $evaluation = Evaluation::find($id);
        $users = UserPark::query()->where('park_id', $evaluation->park_id)->
        whereHas('users.roles', function ($query) {
            return $query->where('name', 'Operations/Zone supervisor');
        })->pluck('user_id');
        $users = User::whereIn('id',$users)->pluck('name','id');
        $rides = Ride::where('park_id',$evaluation->park_id)->pluck('name','id');
        return view('admin.supervisor_evaluation.edit', compact('evaluation','users','rides'));
    }

    public function show($id)
    {
        $list = Evaluation::find($id);
        $items = EvaluationInfo::where('evaluation_id', $id)->get();

        return view('admin.supervisor_evaluation.show', compact('items', 'list', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = ' update supervisor evaluation ';
        $action->save();

        $validated = $request->validate([
            'comment' => 'required|array',
            'question_id' => 'required|array',
            'score' => 'required|array',
            'ride_id' => 'required|exists:rides,id',
            'supervisor_id' => 'required|exists:users,id',
            'remarks'=>'nullable'
        ]);
        $list = Evaluation::find($id);
        $totalScore = array_sum($validated['score']);
        $list->update(array_merge($validated, ['total_score' => $totalScore]));
        
        $list->update($validated);
        $list->lists->each->delete();
        foreach ($validated['question_id'] as $key => $question) {
            EvaluationInfo::query()->create([
                'evaluation_id' => $id,
                'evaluation_question_id' => $question,
                'comment' => $validated['comment'][$key] ?? null,
                'score' => $validated['score'][$key] ?? 0,
            ]);
        }
        alert()->success('Updated Successfully!');
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete supervisor evaluation report';
        $action->save();

        $evaluation = Evaluation::find($id);
        if ($evaluation) {
            foreach ($evaluation->lists as $list) {
                $list->delete();
            }
            $evaluation->delete();
            alert()->success('supervisor Evaluation  deleted successfully');
            return back();
        }
        alert()->error('Supervisor Evaluation  not found');
        return redirect()->route('admin.supervisor-evaluation.index');
    }


}
