<?php

namespace App\Http\Controllers\Admin;

use App\Events\ReportEvent;
use App\Exports\AvilabilityExport;
use App\Models\AvailabilityStoppage;
use App\Models\RideCapacity;
use App\Models\StopageCategory;
use App\Models\StopageSubCategory;
use App\Models\User;
use App\Notifications\ReportNotifications;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GameTime;
use App\Models\Park;
use App\Models\ParkTime;
use App\Models\Ride;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use Maatwebsite\Excel\Facades\Excel;

class AvailabilityReportController extends Controller
{

    public function index(Request $request)
    {
        $outRang = false;

        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        if ($request->ajax()) {


            $from = $request->input('from');
            $to = $request->input('to');
            $datetimeFrom = Carbon::parse($from);
            $datetimeTo = Carbon::parse($to);
            $interval = $datetimeFrom->diffInDays($datetimeTo);
            if ($interval > 30){
                $outRang = true;
            }
            $park_id = $request->input('park_id');
            $items = GameTime::where('park_id', $park_id);

            if ($from) {
                $items = $items->whereDate('date', '>=', $from);
            }
            if ($to) {
                $items = $items->whereDate('date', '<=', $to);
            }
            if ($request->input('ride_id')) {
                $items = $items->where('ride_id', $request->input('ride_id'));
            }
            $items = $items->get();

            if (auth()->user()->hasRole('Super Admin')) {
                $parks = Park::pluck('name', 'id')->all();
            } else {
                $parks = auth()->user()->parks->pluck('name', 'id')->all();
            }

            return view('admin.availability_reports.ajax', compact('items', 'parks', 'request','outRang'));
        }
        return view('admin.availability_reports.index', compact('parks'));
    }

    public function all()
    {
        $parks = auth()->user()->parks->pluck('id');

        $items = GameTime::where('date', Carbon::now()->format('Y-m-d'))
            ->wherein('park_id', $parks)
            ->groupBy('park_id')->get();
        return view('admin.availability_reports.all_reports', compact('items'));
    }

    public function addAvailabilityReport($id)
    {
        $rides = Ride::where('park_id', $id)->get();
        $stopage_category = StopageCategory::pluck('name', 'id')->toArray();
        $Supcategory = StopageSubCategory::pluck('name', 'id')->toArray();
        return view('admin.availability_reports.add', compact('rides', 'stopage_category', 'Supcategory'));

    }
    public function addOLdAvailabilityReport(Request $request)
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        $rides = Ride::where('park_id',$request['park_id'])->get();
        return view('admin.availability_reports.add_old_report', compact('rides','parks'));
    }

    public function create()
    {

    }


    public function store(Request $request)
    {
        

//      dd($request->all());
        $dateExists = GameTime::where('park_id', $request['park_id'])
            ->where('date', $request['date'])
            ->first();
        if ($dateExists) {
            alert()->error('Ride Availability Report Already Exist !');
            return redirect()->back();
        }

        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add availabilty report';
        $action->save(); 

        $users = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Super Admin');
        })->get();
        $time_slot = ParkTime::where('park_id', $request['park_id'])->where('date', $request['date'])->first();
        if (!$time_slot) {
            foreach ($request->ride_id as $key => $value) {
                $list = new GameTime();
                $list->ride_id = $request->ride_id[$key];
                $ride = Ride::find($request->ride_id[$key]);
                $list->zone_id = $ride->zone_id;
                $list->first_status = $request->first_status[$key];
                $list->no_of_gondolas = $request->no_of_gondolas[$key];
                $list->no_of_seats = $request->no_of_seats[$key];
                $list->park_id = $request['park_id'];
                $list->comment = $request->comment[$key];
                $list->date = $request['date'];
                $list->user_id = auth()->user()->id;
                $list->save();
                if (
                    isset($request->stopage_category_id[$key]) &&
                    isset($request->stopage_sub_category_id[$key]) &&
                    $request->stopage_category_id[$key] != null &&
                    $request->stopage_sub_category_id[$key] != null
                ) {
                    AvailabilityStoppage::query()->create([
                        'ride_id' => $request->ride_id[$key],
                        'stopage_category_id' => $request->stopage_category_id[$key],
                        'stopage_sub_category_id' => $request->stopage_sub_category_id[$key],
                        'comment' => $request->comment[$key],
                        'date' => $request['date'],
                        'user_id' => auth()->user()->id,
                        'game_time_id' => $list->id,
                        'park_id' => $request['park_id'],
                    ]);
                }
                if (isset($request->choose[$value]) && $request->choose[$value] == 'resolve') {
//                    $lastStoppage = $ride->rideStoppages()->where('ride_status', 'stopped')->latest()->first();
//                    $timeslot = $lastStoppage?->parkTime?->duration_time;
//                    $time = preg_replace('/\D/', '', $timeslot);

                    $ride->rideStoppages()?->where('ride_status', 'stopped')?->update([
//                        'down_minutes' => $time,
                        'ride_status' => 'active',
                        'stoppage_status' => 'done'
                    ]);
                }

                $old = RideCapacity::where('date', $request['date'])->where('ride_id', $value)->first();

                if ($old == null) {
                    RideCapacity::query()->create([
                        'ride_id' => $value,
                        'ride_availablity_capacity' => $request->no_of_seats[$key],
                        'date' => $request['date'],
                        'park_id' => $request->park_id,
                    ]);
                } else {

                    $rideCa = RideCapacity::where('date',$request['date'])->where('ride_id', $value)->first();
                    if ($rideCa) {
                        $rideCa->ride_availablity_capacity = $request->no_of_seats[$key];
                        $rideCa->save();
                    }
                }
            }
        } else {

            foreach ($request->ride_id as $key => $value) {
                $list = new GameTime();
                $list->close_date = $time_slot->close_date;
                $list->date = $time_slot->date;
                $list->park_time_id = $time_slot->id;
                $list->start = $time_slot->start;
                $list->end = $time_slot->end;
                $list->ride_id = $request->ride_id[$key];
                $list->first_status = $request->first_status[$key];
                $list->no_of_gondolas = $request->no_of_gondolas[$key];
                $list->no_of_seats = $request->no_of_seats[$key];
                $list->park_id = $request['park_id'];
                $list->comment = $request->comment[$key];
                $list->date = $request['date'];
                $list->user_id = auth()->user()->id;
                $list->save();
                if ($request->first_status[$key] == 'no' && isset($request->stopage_category_id[$key]) && isset($request->stopage_sub_category_id[$key])) {

                    AvailabilityStoppage::query()->create([
                        'ride_id' => $request->ride_id[$key],
                        'stopage_category_id' => $request->stopage_category_id[$key],
                        'stopage_sub_category_id' => $request->stopage_sub_category_id[$key],
                        'comment' => $request->comment[$key],
                        'date' => $request['date'],
                        'user_id' => auth()->user()->id,
                        'game_time_id' => $list->id
                    ]);
                }
                $old = RideCapacity::where('date', $request['date'])->where('ride_id', $value)->first();

                if ($old == null) {
                    RideCapacity::query()->create([
                        'ride_id' => $value,
                        'ride_availablity_capacity' => $request->no_of_seats[$key],
                        'date' => $request['date'],
                        'park_id' => $request->park_id,
                    ]);
                } else {

                    $rideCa = RideCapacity::where('date',$request['date'])->where('ride_id', $value)->first();
                    if ($rideCa) {
                        $rideCa->ride_availablity_capacity = $request->no_of_seats[$key];
                        $rideCa->save();
                    }
                }
            }
        }
        $data = [
            'title' => 'Availability Report Added successfully !',
            'user_id' => Auth::user()->id,

        ];
        if ($users) {
            foreach ($users as $user) {
                if ($user) {
                    Notification::send($user, new ReportNotifications($data));
                    event(new ReportEvent($user->id, $data['title'], Carbon::now()));
                }

            }
        }
        alert()->success('Availability Report Added successfully !');
        return redirect()->route('admin.parks.index');

    }

    public function edit($id)
    {
        $items = GameTime::where('park_id', $id)->where('date', Carbon::now()->format('Y-m-d'))->get();
        // return($items);
        $stopage_category = StopageCategory::pluck('name', 'id')->toArray();
        $Supcategory = StopageSubCategory::pluck('name', 'id')->toArray();
        return view('admin.availability_reports.edit', compact('items', 'id', 'stopage_category', 'Supcategory'));
    }


    public function show($id)
    {
        $items = GameTime::where('park_id', $id)->where('date', Carbon::now()->format('Y-m-d'))->get();
        $item = GameTime::where('park_id', $id)->where('date', Carbon::now()->format('Y-m-d'))->first();
        return view('admin.availability_reports.show', compact('items', 'item'));
    }

    public function update(Request $request)
    {
        
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update availabilty report';
        $action->save(); 

        $list = GameTime::where('park_id', $request->park_id)->where('date', Carbon::now()->format('Y-m-d'))->get();
        $users = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Super Admin');
        })->get();

        foreach ($request->ride_id as $key => $value) {
            $gameTime = $list[$key];
//            $gameTime->first_status = $request->first_status[$key];
            $gameTime->second_status = $request->second_status[$key];
            $gameTime->no_of_gondolas = $request->no_of_gondolas[$key];
            $gameTime->no_of_seats = $request->no_of_seats[$key];
            $gameTime->comment = $request->comment[$key];
            $gameTime->update();
            $old = RideCapacity::where('date', Carbon::now()->toDateString())->where('ride_id', $value)->first();

            if ($old != null) {
                $old->ride_availablity_capacity = $request->no_of_seats[$key];
                $old->save();
            }
            if ($request->second_status[$key] == 'no' && $request->stopage_category_id[$key] != null &&  $request->stopage_sub_category_id[$key] != null) {
                $stoppage = AvailabilityStoppage::where('game_time_id', $gameTime->id)->first();
                if ($stoppage) {
                    $stoppage->update([
                        'stopage_category_id' => $request->stopage_category_id[$key],
                        'stopage_sub_category_id' => $request->stopage_sub_category_id[$key],
                        'comment' => $request->comment[$key],
                    ]);
                } else {
                    AvailabilityStoppage::query()->create([
                        'ride_id' => $request->ride_id[$key],
                        'stopage_category_id' => $request->stopage_category_id[$key],
                        'stopage_sub_category_id' => $request->stopage_sub_category_id[$key],
                        'comment' => $request->comment[$key],
                        'date' => Carbon::now()->format('Y-m-d'),
                        'user_id' => auth()->user()->id,
                        'game_time_id' => $gameTime->id,
                        'park_id' => $request->park_id,
                    ]);
                }
            }
        }

        $users = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Super Admin');
        })->get();
        $data = [
            'title' => 'Availability Report Added successfully !',
            'user_id' => Auth::user()->id
        ];
        if ($users) {
            foreach ($users as $user) {
                Notification::send($user, new ReportNotifications($data));
                event(new ReportEvent($user->id, $data['title'], Carbon::now()));
            }
        }

        alert()->success('Second Status Added successfully To Report !');
        return redirect()->route('admin.parks.index');

    }

    public function showAvailabilityReport(Request $request)
    {
        // return $request;
        $from = $request->input('from');
        $to = $request->input('to');
        $park_id = $request->input('park_id');
        $items = GameTime::whereBetween('date', [$from, $to])
            ->where('park_id', $park_id)
            ->get();
        //  return $items;
        if ($request->input('ride_id')) {
            $items = $items->where('ride_id', $request->input('ride_id'));
        }
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }

        return view('admin.availability_reports.index', compact('items', 'parks', 'request'));
    }

    public function destroy($id)
    {
        
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete availabilty report';
        $action->save(); 
        $items = GameTime::where('park_id', $id)->where('date', Carbon::now()->format('Y-m-d'))->get();
        if ($items) {
            foreach ($items as $item) {
                $item->delete();
            }
            alert()->success('This Availability Report Deleted Successfully');
            return back();
        }
        alert()->error('This Availability Report not found');
        return redirect()->route('admin.park.index');
    }

    public function approve($id, $date)
    {
        
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'approve availabilty report';
        $action->save();  

        $park_name = Park::where('id', $id)->pluck('name')->first();

        $reports = GameTime::where('park_id', $id)->where('date', $date)->get();

        foreach ($reports as $report) {
            $report->status = 'approved';
            $report->verified_by_id = auth()->user()->id;
            $report->save();
        }
        $users = User::whereHas('roles', function ($query) {
            return $query->where('name', 'Super Admin');
        })->get();

        $data = [
            'user_id' => Auth::user()->id,
            'title' => 'Ride Availability Report to park ' . $park_name . 'verified successfully!',
        ];
        if ($users) {
            foreach ($users as $user) {
                Notification::send($user, new ReportNotifications($data));
                event(new ReportEvent($user->id, $data['title'], Carbon::now()));
            }
        }
        alert()->success('Availabilty Report Approved successfully !');
        return redirect()->route('admin.availability_reports.all');
    }

    protected function export($items)
    {
        
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'export availabilty report';
        $action->save(); 

        $items = GameTime::whereIn('id', json_decode($items))->get();
        return Excel::download(new AvilabilityExport($items), 'availability.csv');

    }

}
