<?php

namespace App\Http\Controllers\Admin;

use App\Events\timeSlotNotification;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\ParkTime\ParkTimeRequest;
use App\Http\Requests\Dashboard\ParkTime\EntranceCountRequest;
use App\Models\AvailabilityStoppage;
use App\Models\GameTime;
use App\Models\HealthAndSafetyReport;
use App\Models\MaintenanceReport;
use App\Models\Park;
use App\Models\ParkTime;
use App\Models\ParkWeather;
use App\Models\Ride;
use App\Models\StopageCategory;
use App\Models\StopageSubCategory;
use App\Models\RideOpsReport;
use App\Models\SkillGameReport;
use App\Models\TechReport;
use App\Models\RideStoppages;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;
use RakibDevs\Weather\Weather;


class ParkTimeController extends Controller
{

    public function index()
    {
        $times = [];
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('id');
        } else {
            $parks = auth()->user()->parks->pluck('id');
        }
//        $parks = auth()->user()->parks->pluck('id');
        $currentDate = Carbon::now()->toDateString();
        $currentTime = Carbon::now()->format('H:i');
        if (auth()->user()->hasRole('Super Admin')) {
            $items = ParkTime::where('date', $currentDate)
                ->orWhere(function ($subquery) use ($currentDate, $currentTime) {
                    $subquery->where('close_date', $currentDate)
                        ->where('end', '>=', $currentTime);
                })->get();

            $items_check = ParkTime::where('date', $currentDate)
                ->orWhere(function ($subquery) use ($currentDate, $currentTime) {
                    $subquery->where('close_date', $currentDate)
                        ->where('end', '>=', $currentTime);
                })->pluck('id');
        } else {
            $items = ParkTime::where('date', $currentDate)
                ->orWhere(function ($subquery) use ($currentDate, $currentTime) {
                    $subquery->where('close_date', $currentDate)
                        ->where('end', '>=', $currentTime);
                })->whereIn('park_id', $parks)
                ->get();
            $items_check = ParkTime::where('date', $currentDate)
                ->orWhere(function ($subquery) use ($currentDate, $currentTime) {
                    $subquery->where('close_date', $currentDate)
                        ->where('end', '>=', $currentTime);
                })->whereIn('park_id', $parks)->pluck('id');
        }
//         dd($items);

        $tech_data_exist = TechReport::wherein('park_time_id', $items_check)->distinct()->pluck('park_time_id')->toArray();
        $ops_data_exist = RideOpsReport::wherein('park_time_id', $items_check)->distinct()->pluck('park_time_id')->toArray();
        $maintenance_data_exist = MaintenanceReport::wherein('park_time_id', $items_check)->distinct()->pluck('park_time_id')->toArray();
        $skill_data_exist = SkillGameReport::wherein('park_time_id', $items_check)->distinct()->pluck('park_time_id')->toArray();
        $health_data_exist = HealthAndSafetyReport::wherein('park_time_id', $items_check)->distinct()->pluck('park_time_id')->toArray();

        return view('admin.park_times.index', compact('parks', 'items', 'tech_data_exist', 'ops_data_exist', 'maintenance_data_exist', 'health_data_exist', 'skill_data_exist'));
    }


    public function create()
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->toArray();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->toArray();
        }
        return view('admin.park_times.add', compact('parks'));
    }


    public function store(ParkTimeRequest $request)
    {
        
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add time slot ';
        $action->save();  

        $startTime = \Illuminate\Support\Carbon::parse("$request->date $request->start");
        $endTime = \Illuminate\Support\Carbon::parse("$request->close_date $request->end");

        if (!$endTime->greaterThan($startTime)){
            alert()->error(' End datetime must be bigger than start time');
            return redirect()->back();
        }
        $hasArcad = Ride::where('park_id', $request['park_id'])
                   ->where('ride_type_id', 12)
                   ->exists();
    
    if (!$hasArcad) {
        $checkAvailabilty = GameTime::where([
            ['date', $request['date']],
            ['park_id', $request['park_id']]
        ])->first();
        if (!$checkAvailabilty) {
            alert()->error(' Please Add Availability Report First !');
            return redirect()->back();
        }
        }        
       
        $dateExists = ParkTime::where([
            ['date', $request['date']],
            ['park_id', $request['park_id']]
        ])->first();
        if ($dateExists) {
            alert()->error(' Time Slot Already Exist !');
            return redirect()->back();
        }


        $park = Park::find($request['park_id']);
        $city = $park->branches;
        $wt = new Weather();
//        $info = $wt->getCurrentByCord(22.902743,2.175341);
        $info = $wt->getCurrentByCity($city['name']);

        $data = $request->validated();
        $start_date = $data['date'];
        $end_date = $data['close_date'];

        $to_time = $data['start'];
        $from_time = $data['end'];
        $start_timestamp = strtotime("$start_date $to_time");
        $end_timestamp = strtotime("$end_date $from_time");
        $data['duration_time'] = round(abs($start_timestamp - $end_timestamp) / 60, 2) . " minute";
        if($request['date'] === Carbon::now()->toDateString()){
        $data['general_weather'] = $info->weather[0]->main;
        $data['description'] = $info->weather[0]->description;
        $data['temp'] = $info->main->temp;
        $data['windspeed_avg'] = $info->wind->speed;
        }
        $parkTime = ParkTime::create($data);
        $lastInsertedId = $parkTime->id;

    /*    //add weatheer
        $start_date = $request['date'];
        $end_date = $request['close_date'];
        $from_time = $request['start'];
        $to_time = $request['end'];

        // Iterate over each hour within the time slot
        $currentDateTime = Carbon::createFromFormat('Y-m-d H:i:s', "$start_date 00:00:00")->setTimeFromTimeString($from_time);

        $endDateTime = Carbon::createFromFormat('Y-m-d H:i:s', "$end_date 00:00:00")->setTimeFromTimeString($to_time);

        while ($currentDateTime <= $endDateTime) {
            // Get weather data for the current hour
            $info = $wt->getCurrentByCity($city['name'], $currentDateTime);
            // Prepare data for storage
            $weatherdata['time'] = $currentDateTime->toTimeString(); // Increment time by 1 hour for the next iteration
            $weatherdata['temperature'] = $info->main->temp;
            $weatherdata['windspeed_avg'] = $info->wind->speed;
            $weatherdata['park_time_id'] = $lastInsertedId;
     //   dd($data['temp'] );
            ParkWeather::create($weatherdata);
    // Move to the next hour
            $currentDateTime->addHours(2);
        }
        // end weather

*/
        $availablitystoppage = AvailabilityStoppage::where('park_id', $request['park_id'])->where('date', Carbon::now()->toDateString())->get();
        foreach ($availablitystoppage as $item) {
            $ride = Ride::where('id',$item->ride_id)->first();
            $lastStoppage = $ride->rideStoppages()?->where('ride_status', 'stopped')?->latest()?->first();
            if($lastStoppage?->parent_id != null){
                $parent = $lastStoppage?->parent_id;
            }else{
                $parent = $lastStoppage?->id;
            }
            RideStoppages::query()->create([
                'ride_id' => $item->ride_id,
                'ride_status' => 'stopped',
                'stopage_sub_category_id' => $item->stopage_sub_category_id,
                'stopage_category_id' => $item->stopage_category_id,
                'description' => $item->comment,
                'date' => Carbon::now()->toDateString(),
                'time' => Carbon::now()->toTimeString(),
                'opened_date' => $request->input('date'),
                'type' => 'all_day',
                'end_date' => $data['close_date'],
                'time_slot_end' => $data['end'],
                'time_slot_start' => $request->start,
                'down_minutes' => $parkTime->duration_time,
                'user_id' => $item->user_id,
                'park_id' => $parkTime->park_id,
                'zone_id' => $ride->zone_id,
                'park_time_id' => $parkTime->id,
                'stoppage_status' => 'working',
                'parent_id' => $parent
            ]);

        }

        $lists = GameTime::where('park_id', $request->park_id)
            ->where('date', $request['date'])
            ->get();
        foreach ($lists as $list) {
            $list->start = $request->input('start');
            $list->end = $request->input('end');
            $list->close_date = $data['close_date'];
            $list->park_time_id = $lastInsertedId;
            $list->save();
        }
        $date = [
            'user_name' => auth()->user()->name,
            'start' => $request->input('start'),
            'end' => $request->input('end'),
            'date' => $request->input('date'),
            'close_date' => $request->input('close_date'),
        ];

        event(new timeSlotNotification($date));

     /*    $latestStoppages = RideStoppages::where('park_id', $request['park_id'])
            ->where('stoppage_status', '!=', 'done')
            ->whereDate('created_at', '!=', now()) // Exclude stoppages for the current date
            ->select('ride_id', \DB::raw('MAX(created_at) as latest_created_at'))
            ->groupBy('ride_id');
        $stoppages = RideStoppages::select('ride_stoppages.*')
            ->joinSub($latestStoppages, 'latest_stoppages', function ($join) {
                $join->on('ride_stoppages.ride_id', '=', 'latest_stoppages.ride_id')
                    ->on('ride_stoppages.created_at', '=', 'latest_stoppages.latest_created_at');
            })
            ->get();
        $stopage_category = StopageCategory::pluck('name', 'id')->toArray();
        $stopage_sub_category = StopageSubCategory::pluck('name', 'id')->toArray();
        $extended_stoppages = RideStoppages::where('date', '=', Carbon::now()->format('Y-m-d'))
            ->where('stoppage_status', '!=', 'done')->pluck('ride_id');
        if ($stoppages->isNotEmpty()) {
            alert()->success('Time Slot And Weather Added Successfully, But There Were Stoppages Not solved, Pleae Solve Or Extend It');
            return view('admin.not_solved_stoppages.index', compact('extended_stoppages', 'stoppages', 'stopage_category', 'stopage_sub_category'));
        } else {
            alert()->success('Time Slot And Weather Status Added successfully to the park !');
            return redirect()->route('admin.park_times.index');
        } */
        alert()->success('Time Slot And Weather Status Added successfully to the park !');
        return redirect()->route('admin.park_times.index');
    }

    public function search(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'search on time slot ';
        $action->save(); 

        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('id');
        } else {
            $parks = auth()->user()->parks->pluck('id');
        }
        $date = $request->input('date');
        $items = ParkTime::query()
            ->Where('date', $date)
            ->get();
        if (auth()->user()->hasRole('Super Admin')) {
            $items_check = ParkTime::where('date', '>=', $date)->pluck('id');;
        } else {
            $items_check = ParkTime::where('date', '>=', $date)
                ->wherein('park_id', $parks)->pluck('id');

        }
        $tech_data_exist = TechReport::wherein('park_time_id', $items_check)->distinct()->pluck('park_time_id')->toArray();
        $ops_data_exist = RideOpsReport::wherein('park_time_id', $items_check)->distinct()->pluck('park_time_id')->toArray();
        $maintenance_data_exist = MaintenanceReport::wherein('park_time_id', $items_check)->distinct()->pluck('park_time_id')->toArray();
        $skill_data_exist = SkillGameReport::wherein('park_time_id', $items_check)->distinct()->pluck('park_time_id')->toArray();
        $health_data_exist = HealthAndSafetyReport::wherein('park_time_id', $items_check)->distinct()->pluck('park_time_id')->toArray();

        return view('admin.park_times.index', compact('parks', 'items', 'tech_data_exist', 'ops_data_exist', 'maintenance_data_exist', 'health_data_exist', 'skill_data_exist'));
    }

    public function show(ParkTime $parkTime)
    {
        //
    }


    public function edit($id)
    {
        $time = ParkTime::find($id);
        $parks = Park::pluck('name', 'id')->all();
        return view('admin.park_times.edit', compact('parks', 'time'));
    }


    public function update(ParkTimeRequest $request, ParkTime $parkTime)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update time slot  ';
        $action->save(); 

        $data = $request->validated();
        $start_date = $data['date'];
        $end_date = $data['close_date'];
        $to_time = $data['start'];
        $from_time = $data['end'];
        $start_timestamp = strtotime("$start_date $to_time");
        $end_timestamp = strtotime("$end_date $from_time");
        $data['duration_time'] = round(abs($start_timestamp - $end_timestamp) / 60, 2) . " minute";
        $parkTime->update($data);

        $lists = GameTime::where('park_time_id', $parkTime->id)
            ->where('date', $request['date'])
            ->get();
        foreach ($lists as $list) {
            $list->start = $data['start'];
            $list->end = $data['end'];
            $list->close_date = $data['close_date'];
            $list->save();
        }

        $stoppages = RideStoppages::where('park_time_id', $parkTime->id)
            ->where('type', 'all_day')
            ->get();
        foreach ($stoppages as $stoppage) {
            $stoppage->update([
                'time_slot_start' => $parkTime->start,
                'time_slot_end' => $parkTime->end,
                'date' => $parkTime->date,
                'end_date' => $parkTime->close_date,
                'down_minutes' => $parkTime->duration_time,
            ]);
        }

        alert()->success('Park Time Slot Updated successfully !');
        return redirect()->route('admin.park_times.index');
    }


    public function destroya($id)
    {

        $parkTime = ParkTime::find($id);
        if ($parkTime) {

            if ($parkTime->preopeningLists()->exists()) {
                alert()->error('Cannot delete Park Time Slot because it is associated with preopening List records.');
                return back();
            }
            if ($parkTime->healthAndSafetyReports()->exists()) {
                alert()->error('Cannot delete Park Time Slot because it is associated with health  AndSafety Reports records.');
                return back();
            }
            if ($parkTime->maintenanceReports()->exists()) {
                alert()->error('Cannot delete Park Time Slot because it is associated with maintenance Reports records.');
                return back();
            }
            if ($parkTime->gameTimes()->exists()) {
                alert()->error('Cannot delete Park Time Slot because it is associated with Ride Availabilty Report  records.');
                return back();
            }
            if ($parkTime->skillGameReports()->exists()) {
                alert()->error('Cannot delete Park Time Slot because it is associated with skill GameReports  records.');
                return back();
            }
            if ($parkTime->techReports()->exists()) {
                alert()->error('Cannot delete Park Time Slot because it is associated with tech Reports records.');
                return back();
            }

            if ($parkTime->rideStoppages()->exists()) {
                alert()->error('Cannot delete Park Time Slot because it is associated with ride Stoppages  records.');
                return back();
            }
            if ($parkTime->rideOpsReport()->exists()) {
                alert()->error('Cannot delete Park Time Slot because it is associated with RideOps Report  records.');
                return back();
            }
            if ($parkTime->attraction()->exists()) {
                alert()->error('Cannot delete Park Time Slot because it is associated with attraction Audit Check Report  records.');
                return back();
            }
            $parkTime->delete();
            alert()->success('Park Time Slot deleted successfully');
            return back();
        }

        alert()->error('Park Time Slot not found');
        return redirect()->route('admin.park_times.index');
    }



    public function destroy($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete time slot  ';
        $action->save(); 

        $parkTime = ParkTime::find($id);

        if (!$parkTime) {
            alert()->error('Park Time Slot not found');
            return redirect()->route('admin.park_times.index');
        }

        $this->deleteRelatedRecords($parkTime);

        if ($parkTime->attraction()->exists()) {
            $attraction = $parkTime->attraction;
            $attraction->lists()->delete();
            $attraction->delete();
            $parkTime->attraction()->delete();
        }
        if ($parkTime->gameTimes()->exists()) {
            $gameTimes = $parkTime->gameTimes;

            foreach ($gameTimes as $gameTime) {
                $gameTime->stoppage_reason()?->delete();
                $gameTime->delete();
            }

            $parkTime->gameTimes()->delete();
        }

        $parkTime->delete();
        alert()->success('Park Time Slot deleted successfully');
        return back();


    }

    private function deleteRelatedRecords($parkTime)
    {
        $relationshipsToDelete = [
            'rideStoppages',
            'rideOpsReport',
            'preopeningLists',
            'techReports',
            'maintenanceReports',
            'skillGameReports',
            'maintenanceReports',
            'healthAndSafetyReports',
            'queues',
            'cycles',
            'evacuations'

        ];

        foreach ($relationshipsToDelete as $relationship) {
            if ($parkTime->$relationship()->exists()) {
                $parkTime->$relationship()->delete();
                }
            }
        }

    public function add_daily_entrance_count(EntranceCountRequest $request, ParkTime $parkTime)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add daily entrance count  ';
        $action->save(); 

        $toUpdateColumns = ['daily_entrance_count' => $request['daily_entrance_count'],
            'general_comment' => $request['general_comment']
        ];
        $res = ParkTime::findOrFail($request->park_id);
        $res->fill($toUpdateColumns);
        $res->save();
        alert()->success('Daily entrance count added successfully !')->autoclose(50000);
        return redirect()->route('admin.park_times.index');
    }
}

