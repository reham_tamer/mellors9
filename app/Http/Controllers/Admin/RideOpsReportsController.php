<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CustomerFeedbacks;
use App\Models\Park;
use App\Models\ParkTime;
use App\Models\RedFlag;
use App\Models\Ride;
use App\Models\RideOpsReport;
use App\Models\RideStoppages;
use App\Models\TechReport;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RideOpsReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        return view('admin.reports.duty_report', compact('parks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ride_ops_reports.add');
    }

    public function add_ride_ops_report($park_id, $park_time_id)
    {
        $park_time = ParkTime::find($park_time_id);
        $open_date = $park_time->date;
        $data = [];

        $data['How many unavailable rides?'] = Ride::whereHas('rideStoppages', function ($query) use ($park_time_id) {
            $query->where('park_time_id', $park_time_id)->where('type', 'all_day');
        })->count();

        $data['Any medical assistance required?'] = RideStoppages::where('park_time_id', $park_time_id)
            ->whereHas('stopageSubCategory', function ($subQuery) {
                $subQuery->where('name', 'Medics Required');
            })->count();

        $data['Number of complaints received?'] = CustomerFeedbacks::where('date', $open_date)->where('type', 'Complaint')->count();

        $data['How many rides Broke down?'] = Ride::whereHas('rideStoppages', function ($query) use ($park_time_id) {
            $query->where('park_time_id', $park_time_id)->where('type', 'time_slot')
                ->whereHas('stopageSubCategory', function ($subQuery) {
                    $subQuery->whereIn('name', ['Extended Maintenance', 'Ride Fault', 'Ongoing Maintenance']);
                });
        })->count();

        $data['Total Breakdowns'] = RideStoppages::where('park_time_id', $park_time_id)->where('type', 'time_slot')
            ->whereHas('stopageSubCategory', function ($subQuery) {
                $subQuery->whereIn('name', ['Extended Maintenance', 'Ride Fault', 'Ongoing Maintenance']);
            })->count();

        $data['How many Evacuations?'] = RideStoppages::where('park_time_id', $park_time_id)
            ->whereHas('stopageSubCategory', function ($subQuery) {
                $subQuery->whereIn('name', ['Ride Evacuation Abnormal', 'Ride Evacuation Normal']);
            })->count();

        $data['How many stoppages?'] = RideStoppages::where('park_time_id', $park_time_id)->where('type', 'time_slot')
            ->whereHas('stopageSubCategory', function ($subQuery) {
                $subQuery->whereNotIn('name', ['Swipper', 'Extended Maintenance', 'Ride Fault', 'Ongoing Maintenance', 'Ride Evacuation Abnormal', 'Ride Evacuation Normal']);
            })->count();

        $data['How many swipper Issues?'] = RideStoppages::where('park_time_id', $park_time_id)
            ->whereHas('stopageSubCategory', function ($subQuery) {
                $subQuery->where('name', 'Swipper');
            })->count();
//dd($data);
        return view('admin.ride_ops_reports.add', compact('park_id', 'park_time_id', 'data'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add Ride ops report ';
        $action->save();

        $dateExists = RideOpsReport::where('park_time_id', $request['park_time_id'])->first();
        if ($dateExists) {
            return response()->json(['error' => 'ride ops report Report Already Exist !']);
        }
        // dd($request->all());
        $timeSlot = ParkTime::find($request['park_time_id']);
        $rideOpsData = [];
        $redFlagData = [];
        
        foreach ($request->question as $key => $value) {
            $rideOpsData[] = [
                'question' => $request->question[$key],
                'answer' => $request->answer[$key],
                'comment' => $request->comment[$key],
                'park_id' => $request->park_id,
                'park_time_id' => $request->park_time_id,
                'date' => $timeSlot->date,
                'user_id' => auth()->user()->id,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }
        
        foreach ($request->ride as $key => $value) {
            if ($request->ride[$key] != null) {
                $redFlagData[] = [
                    'ride' => $request->ride[$key],
                    'issue' => $request->issue[$key],
                    'park_time_id' => $request->park_time_id,
                    'type' => 'ride_ops',
                    'date' => $timeSlot->date,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
        }
        
        RideOpsReport::insert($rideOpsData);
        RedFlag::insert($redFlagData);
        return response()->json(['success' => 'Ride Ops Report Added successfully']);

//        alert()->success('Preopening List Added successfully !');
//        return redirect()->back();
    }

    public function search(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'search on Ride ops report ';
        $action->save();

        $park_id = $request->input('park_id');
        $date = $request->input('date');
        $rideops = [];
        $parkTime = ParkTime::query()
            ->where('park_id', $request->input('park_id'))
            ->Where('date', $request->input('date'))
            ->first();
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        if ($parkTime) {

            $rideOpsReports = RideOpsReport::where('park_time_id', $parkTime->id)->get()->keyBy('question');
            $rideops = [
                'a' => $rideOpsReports->get('Is weather monitoring equipment working correctly?'),
                'b' => $rideOpsReports->get('Number of complaints received?'),
                'c' => $rideOpsReports->get('Any medical assistance required?'),
                'd' => $rideOpsReports->get('Any issues with ride scanners?'),
                'e' => $rideOpsReports->filter(function ($report) {
                    return in_array($report->question, ['HOE Staff Late?', 'Manpower Staff Late?']);
                })->first(),
                'f' => $rideOpsReports->filter(function ($report) {
                    return in_array($report->question, ['HOE Staff Unavailable?', 'Manpower Staff Unavailable?']);
                })->first(),
                'g' => $rideOpsReports->get('How many unavailable rides?'),
                'h' => $rideOpsReports->get('How many rides Broke down?'),
                'i' => $rideOpsReports->get('Total Breakdowns'),
                'j' => $rideOpsReports->get('How many Evacuations?'),
                'k' => $rideOpsReports->get('How many stoppages?'),
                'l' => $rideOpsReports->get('How many swipper Issues?'),
            ];
    
            $redFlags = RedFlag::query()->where('park_time_id', $parkTime->id)->where('type', 'ride_ops')->get();

            $stoppages = RideStoppages::where('park_time_id', $parkTime->id)->get()->groupBy('stopage_sub_category_id');
            return view('admin.reports.duty_report', compact('rideops', 'parks', 'redFlags', 'park_id', 'date', 'parkTime', 'stoppages'));
        } else
            return view('admin.reports.duty_report', compact('parks', 'park_id', 'date'));
    }


    public function show($id)
    {

        return view('admin.tech_reports.show');

    }

    public function edit_ride_ops_report($park_time_id)
    {
        $items = RideOpsReport::where('park_time_id', $park_time_id)->get();
        $redFlags = RedFlag::query()->where('park_time_id', $park_time_id)->where('type', 'ride_ops')->get();
        return view('admin.ride_ops_reports.edit', compact('items', 'park_time_id', 'redFlags'));
    }

    public function update_request(Request $request)
    {
        /*         dd($request->all());
         */
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update Ride ops report ';
        $action->save();

        $items = RideOpsReport::where('park_time_id', $request->park_time_id)
            ->delete();
        $items = RedFlag::where('park_time_id', $request->park_time_id)->where('type', 'ride_ops')
            ->delete();
        $timeSlot = ParkTime::find($request['park_time_id']);
        $rideOpsData = [];
        foreach ($request->question as $key => $value) {
            $rideOpsData[] = [
                'question' => $value,
                'answer' => $request->answer[$key],
                'comment' => $request->comment[$key],
                'park_time_id' => $request->park_time_id,
                'date' => $timeSlot->date,
                'user_id' => auth()->User()->id,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }

        $redFlagData = [];
        foreach ($request->ride as $key => $value) {
            if ($value != null) {
                $redFlagData[] = [
                    'ride' => $value,
                    'issue' => $request->issue[$key],
                    'park_time_id' => $request->park_time_id,
                    'type' => 'ride_ops',
                    'date' => $timeSlot->date,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
        }
        if (!empty($rideOpsData)) {
            RideOpsReport::insert($rideOpsData);
        }
        if (!empty($redFlagData)) {
            RedFlag::insert($redFlagData);
        }
        alert()->success('Ride Ops Report Updated  successfully !');
        return redirect()->route('admin.park_times.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = RideOpsReport::find($id);
        return view('admin.ride_ops_reports.edit', compact('item'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RideOpsReport $rideOpsReport)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update Ride ops report ';
        $action->save();

        $rideOpsReport->update($request->all());
        $rideOpsReport->user_id = auth()->user()->id;
        $rideOpsReport->save();
        alert()->success('Ride Ops Report updated successfully !');
        return redirect()->route('admin.park_times.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete Ride ops report ';
        $action->save();

        $items = RideOpsReport::where('park_time_id', $id)->get();
        if ($items) {
            foreach ($items as $item) {
                $item->delete();
            }
            alert()->success('This Ride Ops Report Report Deleted Successfully');
            return back();
        }
        alert()->error('This Ride Ops Report  not found');
        return redirect()->route('admin.park_times.index');
    }


    public function cheackRideOps(Request $request)
    {
        $item = RideOpsReport::where('park_time_id', $request->park_time_id)->first();
        /*         dd($item);
         */
        return response()->json(['item' => $item]);
    }
}
