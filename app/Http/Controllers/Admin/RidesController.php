<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Game\GameRequest;
use App\Imports\RidesImport;
use App\Models\Park;
use App\Models\RideStoppages;
use App\Models\RidePark;
use App\Models\RideType;
use App\Models\Ride;
use App\Models\UserPark;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class RidesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if (auth()->user()->hasRole('Super Admin')) {
                $items = Ride::with(['park', 'zone', 'ride_type'])->get();
            } else {
                $parks = auth()->user()->parks->pluck('id');
                $items = Ride::with(['park', 'zone', 'ride_type'])->whereIn('park_id', $parks)->get();
            }
            return Datatables::of($items)
                ->addIndexColumn()
                ->addColumn('park', function ($row) {

                    return $row->park?->name;
                })
                ->addColumn('zone', function ($row) {

                    return $row->zone?->name;
                })
                ->addColumn('category', function ($row) {

                    return $row->ride_cat;
                })
                ->addColumn('country', function ($row) {

                    return $row->country?->name;
                })
                ->addColumn('assignPark', function ($row) {
                    $btn ='';
                    if (auth()->user()->can('addRidePark')  && $row->park_id != null) {
                        $btn = '<a href="' . route('admin.addRidePark', $row->id) . '"
                                        class=" btn btn-success">
                                        <i class="fa  fa-edit"></i>
                    Edit
                                        </a>';
                    }else{
                        $btn = '<a href="' . route('admin.addRidePark', $row->id) . '"
                                        class=" btn btn-success">
                                        <i class="fa-solid fa-xmark"></i>
                    Assign
                                        </a>';
                    }


                    if ($row->park_id != null) {
                        $btn .= ' <a class="btn btn-danger" data-name="Assigned Park and Zone "
                                            data-url="' . route('admin.destroy_park', $row) . '"
                                            onclick="delete_form(this)">
                                            <i class="fa fa-close"></i>
                                            </a>';
                                        }else{
                        $btn .= '<a href="' . route('admin.addRideCountry', $row->id) . '"
                                        class=" btn btn-primary mt-2">
                                        <i class="fa  fa-location"></i>
                    Location
                                        </a>';
                    }
                    return $btn;


                })
                ->addColumn('assignZone', function ($row) {
                    $btn ='';
                     if(auth()->user()->can('addRideZone')){

                         if ($row->zone_id != null){
                             $btn ='  <a href="' . route('admin.addRideZone', $row->id) . '"
                                        class=" btn btn-success">
                                        <i class="fa fa-edit"></i>

                                      Edit
                                        </a>';
                         }else{
                             $btn ='  <a href="' . route('admin.addRideZone', $row->id) . '"
                                        class=" btn btn-success">

                                        <i class="fa-solid fa-xmark"></i>

                                      Assign
                                        </a>';
                         }
                     }


                    return $btn;
                })
                ->addColumn('action', function ($row) {
                    $btn ='';
                    if (auth()->user()->can('rides-edit')) {
                        $btn = ' <a href="' . route('admin.rides.edit', $row->id) . '" class="btn btn-info">Edit</a>';
                    }


                    if (auth()->user()->can('rides-delete')) {
                        $btn .= ' <a class="btn btn-danger" data-name="' . $row->name . '"
                                           data-url="' . route('admin.rides.destroy', $row->id) . '"
                                           onclick="delete_form(this)">
                                               Delete
                                        </a>';
                    }


                    return $btn;
                })
                ->rawColumns(['name', 'status', 'assignPark','park','zone','category', 'assignZone', 'action','country'])
                ->make(true);
        }
        return view('admin.rides.index_datatable');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::get()->pluck('name', 'id');
        } else {
            $parks = Park::whereIn('id', auth()->user()->parks->pluck('id'))->get()->pluck('name', 'id');
        }
        $ride_types = RideType::pluck('name', 'id')->all();
        return view('admin.rides.add', compact('ride_types', 'parks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(GameRequest $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add Ride ';
        $action->save(); 

        $ride = Ride::create($request->validated());
        $request->park_id ? RidePark::create(['ride_id' => $ride->id, 'park_id' => $request->park_id]) : '';

        alert()->success('Ride Added successfully !');
        return redirect()->route('admin.rides.index');
    }

    public function uploadRides(Request $request)
    {
        return view('admin.rides.exce_upload');
    }
    public function uploadExcleFile(Request $request)
    {
        $this->validate($request, ['file' => 'required']);
        Excel::import(new RidesImport(), $request->file('file'));
        alert()->success('Ride Added successfully !');
        return redirect()->route('admin.rides.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ride = Ride::find($id);
        $ride_types = RideType::pluck('name', 'id')->all();
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::get()->pluck('name', 'id');
        } else {
            $parks = Park::whereIn('id', auth()->user()->parks->pluck('id'))->get()->pluck('name', 'id');
        }
        return view('admin.rides.edit', compact('ride', 'ride_types', 'parks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(GameRequest $request, $id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update ride data ';
        $action->save(); 

        $ride = Ride::find($id);
        $ride->update($request->validated());
        $ride->save();
        $request->park_id ? RidePark::where('ride_id', $ride->id)->update(['park_id' => $request->park_id]) : '';

        alert()->success('Ride updated successfully !');
        return redirect()->route('admin.rides.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete ride  ';
        $action->save();

        $ride = Ride::find($id);
        if ($ride) {
            $ride->delete();
            alert()->success('Ride deleted successfully');
            return back();
        }
        alert()->error('Ride not found');
        return redirect()->route('admin.rides.index');
    }

    public function destroyPark($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'Unassign park to  ride ';
        $action->save();

        $ride = Ride::find($id);
        if ($ride) {
            $ride->update([
                'park_id' => null,
                'zone_id' => null
            ]);
            $stoppages = RideStoppages::where('ride_id', $id)->where('stoppage_status', '!=', 'done')->get();
            if (!$stoppages->isEmpty()) {
                foreach ($stoppages as $stoppage) {
                    $stoppage->update([
                        'stoppage_status' => 'done',
                        'ride_status' => 'active'
                    ]);
                }
            }
            alert()->success('Ride Unassigned to park successfully');
            return back();
        }
        alert()->error('Ride not found');
        return redirect()->route('admin.rides.index');
    }
}
