<?php

namespace App\Http\Controllers\Admin;

use App\Exports\AuditExport;
use App\Exports\CapacityExport;
use App\Exports\EvacuationExport;
use App\Exports\EvaluationOperatorExport;
use App\Exports\EvaluationSupervisorExport;
use App\Exports\ExportStoppage;
use App\Exports\ExportCycles;
use App\Exports\ExportQueues;
use App\Exports\HealthExport;
use App\Exports\ObservationExport;
use App\Exports\AttendanceExport;
use App\Exports\OperatorExport;
use App\Exports\PreopeningListExport;
use App\Http\Controllers\Controller;
use App\Models\Evacuation;
use App\Models\Evaluation;
use App\Models\Observation;
use App\Models\ParkTime;
use App\Models\Attraction;
use App\Models\Park;
use App\Models\Queue;
use App\Models\RideCycles;
use App\Models\PreopeningList;
use App\Models\Ride;
use App\Models\RideCapacity;
use App\Models\RideStoppages;
use App\Models\PreopenInfo;
use App\Models\User;
use App\Models\Attendance;
use App\Models\AttendanceLog;
use App\Models\GeneralIncident;
use App\Models\UserLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends Controller
{


    public function rideStatus()
    {

        $rides = DB::table('rides')
            ->groupBy('rides.id')
            ->join('parks', 'parks.id', '=', 'rides.park_id')
            ->leftJoin('park_times', function ($join) {
                $join->on('park_times.park_id', '=', 'parks.id');
            })
            ->leftJoin('ride_stoppages', function ($join) {
                $join->on('ride_stoppages.ride_id', '=', 'rides.id');
            })
            ->select([
                DB::raw('rides.*'),
                DB::raw('parks.name as parkName'),
                DB::raw('park_times.start,park_times.end,park_times.date,park_times.close_date'),
                DB::raw('ride_stoppages.ride_status as stoppageRideStatus,ride_stoppages.ride_notes,ride_stoppages.description as rideSroppageDescription'),

            ])->get();
        foreach ($rides as $ride) {
            $now = Carbon::now();
            $startDateTime = Carbon::parse("$ride->date $ride->start");
            $endDateTime = Carbon::parse("$ride->close_date $ride->end");

            if ($now->between($startDateTime, $endDateTime)) {

                if ($ride->stoppageRideStatus != null) {
                    $ride->available = $ride->stoppageRideStatus;
                } else {
                    $ride->available = 'active';
                    $ride->ride_notes = '';
                    $ride->rideSroppageDescription = '';
                }

            } else {

                $ride->available = 'closed';
                $ride->ride_notes = 'out of park time slot ';
                $ride->rideSroppageDescription = '';
            }
        }
        return view('admin.reports.ride_status', compact('rides'));
    }

    public function cyclesReport(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'show cycles Report ';
        $action->save();


        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        if ($request->ajax()) {
            $from = $request->input('date_from');
//            $to = $request->input('date_to');
            $park_id = $request->input('park_id');
            $items = RideCycles::where('park_id', $park_id);
            if ($from) {
                $items = $items->whereDate('opened_date', $from);
            }
//            if ($to) {
//                $items = $items->whereDate('opened_date', '<=', $to);
//            }
            if ($request->input('ride_id')) {
                $items = $items->where('ride_id', $request->input('ride_id'));
            } elseif ($request->input('zone_id')) {
                $items = $items->where('zone_id', $request->input('zone_id'));
            }
            if (auth()->user()->hasRole('Super Admin')) {
                $parks = Park::pluck('name', 'id')->all();
            } else {
                $parks = auth()->user()->parks->pluck('name', 'id')->all();
            }
            $items = $items->get();
            return view('admin.reports.show_cycles_report', compact('items', 'parks', 'request'));
        }
        return view('admin.reports.cycles_report', compact('parks'));

    }

    public function queuesReport(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'show queues Report ';
        $action->save();

        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        if ($request->ajax()) {
            $from = $request->input('date_from');
//            $to = $request->input('date_to');
            $park_id = $request->input('park_id');
            $time = ParkTime::where('date', $from)->where('park_id', $park_id)->first();
            $park_time_id = $time?->id;
            $items = Queue::where('park_time_id', $park_time_id);
            if ($from) {
                $items = $items->whereDate('opened_date', $from);
            }
//            if ($to) {
//                $items = $items->whereDate('opened_date', '<=', $to);
//            }
            if ($request->input('ride_id')) {
                $items = $items->where('ride_id', $request->input('ride_id'));
            } elseif ($request->input('zone_id')) {
                $items = $items->where('zone_id', $request->input('zone_id'));
            }
            if (auth()->user()->hasRole('Super Admin')) {
                $parks = Park::pluck('name', 'id')->all();
            } else {
                $parks = auth()->user()->parks->pluck('name', 'id')->all();
            }
            $items = $items->get();
            return view('admin.reports.show_queue_report', compact('items', 'parks', 'request'));
        }
        return view('admin.reports.queue_report', compact('parks'));

    }

    public function stoppagesReport(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'show stoppages Report ';
        $action->save();

        $outRang = false;
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        if ($request->ajax()) {
            $from = $request->input('date_from');
            $to = $request->input('date_to');
            $datetimeFrom = Carbon::parse($from);
            $datetimeTo = Carbon::parse($to);
            $interval = $datetimeFrom->diffInDays($datetimeTo);
            if ($interval > 30) {
                $outRang = true;
            }
            $park_id = $request->input('park_id');
            $items = RideStoppages::where('park_id', $park_id);
            if ($from) {
                $items = $items->whereDate('opened_date', '>=', $from);
            }
            if ($to) {
                $items = $items->whereDate('opened_date', '<=', $to);
            }
            if ($request->input('ride_id')) {
                $items = $items->where('ride_id', $request->input('ride_id'));
            } elseif ($request->input('zone_id')) {
                $items = $items->where('zone_id', $request->input('zone_id'));
            }
            if (auth()->user()->hasRole('Super Admin')) {
                $parks = Park::pluck('name', 'id')->all();
            } else {
                $parks = auth()->user()->parks->pluck('name', 'id')->all();
            }
            $items = $items->get();
            return view('admin.reports.show_stoppage_report', compact('items', 'parks', 'request', 'outRang'));
        }
        return view('admin.reports.stoppage_report', compact('parks'));

    }


    public function inspectionListReport(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'show inspection List Report ';
        $action->save();

        $outRang = false;
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        if ($request->ajax()) {
            $from = $request->input('date_from');
            $to = $request->input('date_to');
            $park_id = $request->input('park_id');

            $datetimeFrom = Carbon::parse($from);
            $datetimeTo = Carbon::parse($to);
            $interval = $datetimeFrom->diffInDays($datetimeTo);
            if ($interval > 30) {
                $outRang = true;
            }
            $items = PreopenInfo::where('park_id', $park_id)->where('type', 'inspection_list')->with('lists');
            if ($from) {
                $items = $items->whereDate('opened_date', '>=', $from);
            }
            if ($to) {
                $items = $items->whereDate('opened_date', '<=', $to);
            }
            if ($request->input('ride_id')) {
                $rideId = $request->input('ride_id');
                $items = $items->where('ride_id', $rideId);
            } elseif ($request->input('zone_id')) {
                $items = $items->where('zone_id', $request->input('zone_id'));
            }
            $items = $items->get();

            return view('admin.reports.show_inspection_list_report', compact('items', 'parks', 'request', 'outRang'));

        }
        return view('admin.reports.inspection_list_report', compact('parks'));

    }


    public function auditReport(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'show audit Report ';
        $action->save();

        $outRang = false;
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }

        if ($request->ajax()) {
            $from = $request->input('date_from');
            $to = $request->input('date_to');
            $park_id = $request->input('park_id');
            $datetimeFrom = Carbon::parse($from);
            $datetimeTo = Carbon::parse($to);
            $interval = $datetimeFrom->diffInDays($datetimeTo);
            if ($interval > 30) {
                $outRang = true;
            }
            $items = Attraction::where('park_id', $park_id)->with('lists');

            if ($from) {
                $items = $items->whereDate('date', '>=', $from);
            }
            if ($to) {
                $items = $items->whereDate('date', '<=', $to);
            }
            if ($request->input('ride_id')) {
                $rideId = $request->input('ride_id');
                $items = $items->where('ride_id', $rideId);
            } elseif ($request->input('zone_id')) {
                $items = $items->where('zone_id', $request->input('zone_id'));
            }
            $items = $items->get();

            return view('admin.reports.show_audit_report', compact('items', 'parks', 'outRang'));

        }
        return view('admin.reports.audit_report', compact('parks'));

    }


    public function showInspections($id)
    {
        $list = PreopenInfo::findOrFail($id);
        $items = PreopeningList::where('preopen_info_id', $id)->get();

        return view('admin.reports.show_inspection_list_report', compact('items', 'list', 'id'));

    }

    public function showInspectionDetails($id)
    {
        $list = PreopenInfo::findOrFail($id);
        $items = PreopeningList::where('preopen_info_id', $id)->get();

        return view('admin.reports.show_inspection_details', compact('items', 'list', 'id'));

    }


    public function attendanceTimeReport(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'show attendants Report ';
        $action->save();

        $outRang = false;
        $users = Attendance::pluck('name', 'id');
        if ($request->ajax()) {
            $from = $request->input('from');
            $to = $request->input('to');
            $datetimeFrom = Carbon::parse($from);
            $datetimeTo = Carbon::parse($to);
            $interval = $datetimeFrom->diffInDays($datetimeTo);
            if ($interval > 30) {
                $outRang = true;
            }
            $user_id = $request->input('user_id');
            $items = AttendanceLog::query();

            if ($from) {
                $items = $items->where('open_date', '>=', $from);
            }
            if ($to) {
                $items = $items->where('open_date', '<=', $to);
            }
            if ($user_id) {
                $items = $items->where('attendance_id', $user_id);
            }
            if ($request->park_id) {
                $items = $items->where('park_id', $request->park_id);
            }
            if ($request->ride_id) {
                $items = $items->where('ride_id', $request->ride_id);
            }
            $items = $items?->where('type', 'logout')->get();
            return view('admin.reports.ajax.attendance_time_report', compact('items','outRang'));
        }
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        return view('admin.reports.attendance_time_report', compact('users', 'parks'));

    }


    public function operatorTimeReport(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'show operator time Report ';
        $action->save();

        $outRang = false;
        $users = User::whereHas('roles', function ($query) {
            $query->whereIn('name', ['Operation', 'Operations/Operator', 'Breaker']);
        })->pluck('name', 'id');
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        if ($request->ajax()) {
            $from = $request->input('from');
            $to = $request->input('to');
            $datetimeFrom = Carbon::parse($from);
            $datetimeTo = Carbon::parse($to);
            $interval = $datetimeFrom->diffInDays($datetimeTo);
            if ($interval > 30) {
                $outRang = true;
            }
            $user_id = $request->input('user_id');
            $items = UserLog::query();

            if ($from) {
                $items = $items->whereDate('open_date', '>=', $from);
            }
            if ($to) {
                $items = $items->whereDate('open_date', '<=', $to);
            }
            if ($request->input('user_id')) {
                $items = $items->where('user_id', $user_id);
            }
            if ($request->input('ride_id')) {
                $items = $items->where('ride_id', $request->input('ride_id'));
            }
            if ($request->input('park_id')) {
                $items = $items->where('park_id', $request->input('park_id'));
            }
            $users = User::whereHas('roles', function ($query) {
                $query->whereIn('name', ['Operation', 'Operations/Operator']);
            })->pluck('name', 'id');
            $currentUser = User::selectRaw('name')->where('id', $user_id)->first();
            $items = $items?->whereIn('type', ['logout', 'off'])->get();

            return view('admin.reports.ajax.operator_time_report', compact('items', 'currentUser', 'users', 'outRang'));
        }
        return view('admin.reports.operator_time_report', compact('users', 'parks'));

    }


    public function observationReport(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'show observation Report ';
        $action->save();

        $outRang = false;

        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        if ($request->ajax()) {

            $from = $request->input('date_from');
            $to = $request->input('date_to');
            $park_id = $request->input('park_id');
            $datetimeFrom = Carbon::parse($from);
            $datetimeTo = Carbon::parse($to);
            $interval = $datetimeFrom->diffInDays($datetimeTo);
            if ($interval > 30){
                $outRang = true;
            }
            $items = Observation::where('park_id', $park_id);

            if ($from) {
                if (auth()->user()->hasRole('Maintenance/Worker') || auth()->user()->hasRole('Maintenance/Supervisor') ||
                    auth()->user()->hasRole('Maintenance/Director') || auth()->user()->hasRole('Maintenance/Manager') ||
                    auth()->user()->hasRole('Maintenance/Chief')) {

                    $items->where('department_id', 1)->where('date_reported', '>=', $from);

                } elseif (auth()->user()->hasRole('Health & safety/Supervisor') || auth()->user()->hasRole('Health & safety/Manager') ||
                    auth()->user()->hasRole('Health & safety/Director')) {
                    $items->where('department_id', 3)->where('date_reported', '>=', $from);

                } elseif (auth()->user()->hasRole('Operations/Zones Manager') || auth()->user()->hasRole('Operations/Zone supervisor') ||
                    auth()->user()->hasRole('Operations/Operator')) {
                    $items->where('department_id', 2)->where('date_reported', '>=', $from);

                } elseif (auth()->user()->hasRole('Technical Services/Engineer') || auth()->user()->hasRole('Technical Services/Manager') ||
                    auth()->user()->hasRole('Technical Services/Director')) {
                    $items->where('department_id', 4)->where('date_reported', '>=', $from);

                } elseif (auth()->user()->hasRole('Client')) {
                    $items->where('department_id', 5)->where('date_reported', '>=', $from);

                } else {
                    $items->where('date_reported', '>=', $from);

                }
                // dd ($items);
                if ($to) {
                    $items = $items->where('date_reported', '<=', $to);
                }

                if ($request->input('ride_id')) {
                    $rideId = $request->input('ride_id');
                    $items = $items->where('ride_id', $rideId);
                } elseif ($request->input('zone_id')) {
                    $items = $items->where('zone_id', $request->input('zone_id'));
                }
                // return ($items);

                $items = $items->get();
            }
            return view('admin.reports.ajax.observation_report', compact('items', 'request', 'parks','outRang'));
        }
        return view('admin.reports.observation_report', compact('parks'));

    }


    public function incidentReport(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'show incident Report ';
        $action->save();

        $outRang = false;

        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        if ($request->ajax()) {
            $from = $request->input('from');
            $to = $request->input('to');
            $datetimeFrom = Carbon::parse($from);
            $datetimeTo = Carbon::parse($to);
            $interval = $datetimeFrom->diffInDays($datetimeTo);
            if ($interval > 30){
                $outRang = true;
            }
            $park_id = $request->input('park_id');

            $items = GeneralIncident::where('park_id', $park_id);
            if ($request->input('type')) {
                $items = $items->where('type', $request->input('type'));
            }
            if ($from) {
                $items = $items->whereDate('date', '>=', $from);
            }
            if ($to) {
                $items = $items->whereDate('date', '<=', $to);
            }
            $items = $items->get();

            if (auth()->user()->hasRole('Super Admin')) {
                $parks = Park::pluck('name', 'id')->all();
            } else {
                $parks = auth()->user()->parks->pluck('name', 'id')->all();
            }

            return view('admin.reports.ajax.incident_report', compact('items', 'parks','outRang'));
        }
        return view('admin.reports.incident_report', compact('parks'));

    }

    protected function exportCycles($items)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'export cycles Report ';
        $action->save();

        $items = RideCycles::whereIn('id', json_decode($items))->get();

        return Excel::download(new ExportCycles($items), 'cycles.csv');

    }

    protected function exportQueues($items)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'export queue Report ';
        $action->save();

        $items = Queue::whereIn('id', json_decode($items))->get();

        return Excel::download(new ExportQueues($items), 'queues.csv');

    }

    protected function exportStoppage($items)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'export stoppages Report ';
        $action->save();

        $items = RideStoppages::whereIn('id', json_decode($items))->get();

        return Excel::download(new ExportStoppage($items), 'stoppage.csv');

    }

    protected function exportInspection($items)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'export  preopening  Report ';
        $action->save();

        $items = PreopenInfo::whereIn('id', json_decode($items))->with('lists')->get();
        return Excel::download(new PreopeningListExport($items), 'inspection.csv');

    }

    protected function exportAudit($items)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'sxport audit Report ';
        $action->save();

        $items = Attraction::whereIn('id', json_decode($items))->get();

        return Excel::download(new AuditExport($items), 'audit.csv');

    }

    protected function exportAttendance($items)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'export attendants time Report ';
        $action->save();

        $items = AttendanceLog::whereIn('id', json_decode($items))->get();

        return Excel::download(new AttendanceExport($items), 'attendance.csv');

    }

    protected function exportOperator($items)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'export operators time Report ';
        $action->save();

        $items = UserLog::whereIn('id', json_decode($items))->get();

        return Excel::download(new OperatorExport($items), 'operator.csv');

    }

    protected function exportObservation($items)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'export observation  Report ';
        $action->save();

        $items = Observation::whereIn('id', json_decode($items))->get();

        return Excel::download(new ObservationExport($items), 'observation.csv');

    }

    protected function exportHealth($items)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'export health & safety  Report ';
        $action->save();

        $items = GeneralIncident::whereIn('id', json_decode($items))->get();

        return Excel::download(new HealthExport($items), 'incident.csv');

    }

    protected function exportCapacity($items)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'export ride capacity  Report ';
        $action->save();

        $items = RideCapacity::whereIn('id', json_decode($items))->get();

        return Excel::download(new CapacityExport($items), 'capacity.csv');

    }

    protected function exportEvacuation($items)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'export evacuation  Report ';
        $action->save();

        $items = Evacuation::whereIn('id', json_decode($items))->get();

        return Excel::download(new EvacuationExport($items), 'evacuation.csv');

    }

    public function evaluationOperator(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'show operators  evaluation Report ';
        $action->save();

        $outRang = false;

        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        if ($request->ajax()) {
            $from = $request->input('from');
            $to = $request->input('to');
            $park_id = $request->input('park_id');

            $items = Evaluation::query()->whereNotNull('operator_id')->where('park_id', $park_id);

            if ($from) {
                $items = $items->whereDate('date', '>=', $from);
            }
            if ($to) {
                $items = $items->whereDate('date', '<=', $to);
            }
            if ($request->input('ride_id')) {
                $items = $items->where('ride_id', $request->input('ride_id'));
            }
            $datetimeFrom = Carbon::parse($from);
            $datetimeTo = Carbon::parse($to);
            $interval = $datetimeFrom->diffInDays($datetimeTo);
            if ($interval > 30){
                $outRang = true;
            }
            $items = $items->get();

            if (auth()->user()->hasRole('Super Admin')) {
                $parks = Park::pluck('name', 'id')->all();
            } else {
                $parks = auth()->user()->parks->pluck('name', 'id')->all();
            }

            return view('admin.reports.ajax.evaluation_operator_report', compact('items', 'parks','outRang'));
        }
        return view('admin.reports.Evaluation.evaluation_operator_report', compact('parks'));

    }

    protected function exportEvaluationOperator($items)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'export operators  evaluation Report ';
        $action->save();

        $items = Evaluation::query()->whereIn('id', json_decode($items))->get();

        return Excel::download(new EvaluationOperatorExport($items), 'evaluation_operator.csv');

    }

    public function evaluationSupervisor(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'show supervisor  evaluation Report ';
        $action->save();

        $outRang = false;


        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        if ($request->ajax()) {
            $from = $request->input('from');
            $to = $request->input('to');
            $park_id = $request->input('park_id');

            $items = Evaluation::query()->whereNotNull('supervisor_id')->where('park_id', $park_id);

            if ($from) {
                $items = $items->whereDate('date', '>=', $from);
            }
            if ($to) {
                $items = $items->whereDate('date', '<=', $to);
            }
            $datetimeFrom = Carbon::parse($from);
            $datetimeTo = Carbon::parse($to);
            $interval = $datetimeFrom->diffInDays($datetimeTo);
            if ($interval > 30){
                $outRang = true;
            }
            if ($request->input('supervisor_id')) {
                $items = $items->where('supervisor_id', $request->input('supervisor_id'));
            }
            $items = $items->get();

            if (auth()->user()->hasRole('Super Admin')) {
                $parks = Park::pluck('name', 'id')->all();
            } else {
                $parks = auth()->user()->parks->pluck('name', 'id')->all();
            }

            return view('admin.reports.ajax.evaluation_supervisor_report', compact('items', 'parks','outRang'));
        }
        return view('admin.reports.Evaluation.evaluation_supervisor_report', compact('parks'));

    }

    protected function exportEvaluationSupervisor($items)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'export supervisor  evaluation Report ';
        $action->save();

        $items = Evaluation::query()->whereIn('id', json_decode($items))->get();

        return Excel::download(new EvaluationSupervisorExport($items), 'evaluation_supervisor.csv');

    }

    public function operator_show_time_update(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update  operators  time  ';
        $action->save();

        $log = UserLog::find($request->id);
        $log->update([
            'time' => Carbon::parse($request->start)->toTimeString(),
            'date' => Carbon::parse($request->start)->toDateString(),
            'end_time' => $request->end,
            'shift_minutes' => $request->minutes,
        ]);
        return response()->json();
    }

    public function delete_operator_time(UserLog $userLog)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete operators ';
        $action->save();

        $userLog->delete();
        alert()->success('Deleted Attendances Time successfully !');
        return redirect()->back();
    }


}
