<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\InspectionList\ZoneInspectionListRequest;
use App\Models\InspectionList;
use App\Models\Zone;
use App\Models\ZoneInspectionInfo;
use App\Models\ZoneInspection;
use Illuminate\Http\Request;
use App\Models\ZoneInspectionList;
use Carbon\Carbon;

class ZonePreclosingController extends Controller
{
   
    public function index()
    {
    }

   
    public function create()
    {
    }
  
       public function add_zone_preclose_elements($zone_id)
    {
        $inspection_list=InspectionList::where('type','preclosing')->get();
        return view('admin.zone_preclosing.add',compact('inspection_list','zone_id'));
    }
    
    public function edit_zone_preclose_elements( $zone_id)
    {
        $zone=Zone::find($zone_id);
        $list=ZoneInspectionList::where('zone_id',$zone_id)
        ->where('lists_type','preclosing')->pluck('inspection_list_id')->toArray();
        $inspection_list=InspectionList::where('type','preclosing')->get();
        $zones=Zone::pluck('name','id')->toArray();    
        return view('admin.zone_preclosing.edit',compact('inspection_list','zone','list','zone_id'));

    }

    public function store(ZoneInspectionListRequest $request)
    {
        $zone = Zone::find($request['zone_id']);
        $inspectionListIds = $request['inspection_list_id'];
        $zone->inspection_list()->attach($inspectionListIds, ['lists_type' => 'preclosing']);
         alert()->success('zone Preclosing Elements Added Successfully !');
        return redirect()->route('admin.zone_inspection_lists.index');
    }

    public function update_zone_preclose_elements(Request $request,  $zone_id)
    {
        ZoneInspectionList::where('zone_id',$request->zone_id) ->where('lists_type','preclosing')->delete();
        $zone = Zone::find($request['zone_id']);
        $inspectionListIds = $request['inspection_list_id'];
        $zone->inspection_list()->attach($inspectionListIds, ['lists_type' => 'preclosing']);
        alert()->success('Zone Preclosing Elements Updated Successfully !');
        return redirect()->route('admin.zone_inspection_lists.index');
    }
    public function show_zone_preclose_list($zone_id,$date)
    {
        $list = ZoneInspectionInfo::where('opened_date',$date )
        ->where('zone_id', $zone_id)->where('type','preclosing')
        ->first();
        if(! $list){
            alert()->error('PreClosing not added yet for this Zone !');
            return redirect()->back();
        }
    $preopenInfo_id=$list->id;
    $items =ZoneInspection::where('zone_inspection_infos_id',$preopenInfo_id)->get()->unique('inspection_list_id');

    return view('admin.zone_preclosing.show', compact('items', 'list'));
    }

    public function approve($id,$date)
    {
       $item= ZoneInspectionInfo::where('opened_date', $date)
                ->where('zone_id', $id)->where('type','preclosing')
                ->first();
        $item->status = 'approved';
        $item->approve_by_id  = \auth()->user()->id;
        $item->save();
        alert()->success('Preclosing List Approved successfully !');
        return redirect()->back();
    }
    public function edit( $id)
    {
       

    }

    public function update(zoneInspectionListRequest $request, zoneInspectionList $zoneInspectionList)
    {
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }


}
