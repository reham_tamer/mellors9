<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\User\StoreRequest;
use App\Http\Requests\Dashboard\User\UpdateRequest;
use App\Models\Branch;
use App\Models\Department;
use App\Models\Park;
use App\Models\Ride;
use App\Models\RideUser;
use App\Models\User;
use App\Models\UserPark;
use App\Models\UserZone;
use App\Models\Zone;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;
use function Symfony\Component\String\length;

//use DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if (auth()->user()->hasRole('Super Admin')) {
                $items = User::where('id', '!=', 1);
            } else {
                $adminParkRoles = ['Maintenance/Worker', 'Maintenance/Supervisor', 'Maintenance/Manager', 'Maintenance/Director', 'Maintenance/Chief', 'Technical Services/Engineer',
                    'Technical Services/Manager', 'Technical Services/Director', 'Health & safety/Supervisor', 'Health & safety/Manager', 'Health & safety/Director',
                    'Operations/Zones Manager', 'Operations/Zone supervisor', 'Operations/Operator', 'Breaker'
                ];
                $items = User::where('id', '!=', 1)
                    ->doesntHave('parks')
                    ->orWhereHas('parks', function ($query) {
                        return $query->whereIn('park_id', auth()->user()->parks?->pluck('id')->toArray());
                    })
                    ->whereHas('roles', function ($query) use ($adminParkRoles) {
                        $query->whereIn('name', $adminParkRoles);
                    })
                    ->where('id', '!=', auth()->id());

            }

            return Datatables::of($items)
                ->addIndexColumn()
                ->addColumn('name', function ($row) {

                    return $row->name;
                })
                ->addColumn('accessibility', function ($row) {
                    if (auth()->user()->can('userRoles') || auth()->user()->hasRole('Park Admin')) {
                        $user_parks_exist = UserPark::where('user_id', $row->id)->first();
                        if ($user_parks_exist) {
                            $btn = '<a href="' . route('admin.userRoles', $row->id) . '" class="edit btn btn-primary btn-sm"> Edit Accessibility</a>';//                    }else{

                        } else {
                            $btn = '<a href="' . route('admin.userRoles', $row->id) . '" class="edit btn btn-primary btn-sm"> Add Accessibility</a>';//                    }else{

                        }
                    } else {
                        $btn = 'Not Allow';
                    }

                    return $btn;

                })
                ->addColumn('role', function ($row) {

                    return $row->roles()?->first()?->name;
                })
                ->addColumn('status', function ($row) {

                    return $row->status == 1 ? '<sapn class="bg-success text-white" style="padding: 5px">Active</sapn>' : ' <sapn class="bg-danger text-white" style="padding: 5px">Not active</sapn>';
                })
                ->addColumn('action', function ($row) {
                    $btn = '';

                    if (auth()->user()->can('users-edit') || auth()->user()->hasRole('Park Admin')) {
                        $btn = ' <a href="' . route('admin.users.edit', $row->id) . '" class="btn btn-info">Edit</a>';
                    }


                    if (auth()->user()->can('users-delete')) {
                        $btn .= ' <a class="btn btn-danger" data-name="' . $row->name . '"
                                           data-url="' . route('admin.users.destroy', $row->id) . '"
                                           onclick="delete_form(this)">
                                               Delete
                                        </a>';
                    }


                    return $btn;
                })
                ->rawColumns(['name', 'accessibility', 'role', 'status', 'action'])
                ->make(true);
        }

        return view('admin.users.index_new');
    }

    public
    function userRoles($id)
    {
        $user = User::findOrFail($id);
        $user_id = $id;

        $userRole = $user->roles()->first()->name;

        $list = UserPark::where('user_id', $id)->pluck('park_id')->toArray();
        $branch = $user->branchs?->pluck('id')->toArray();

        $listZone = UserZone::where('user_id', $user_id)->pluck('zone_id')->toArray();
        $zones = Zone::whereIn('branch_id', $branch)->get();

        $listRide = RideUser::where('user_id', $user_id)->pluck('ride_id')->toArray();
        $listRideZone = Ride::whereIn('id', $listRide)->pluck('zone_id')->toArray();
        $rides = Ride::all();

        if (auth()->user()->hasRole('Super Admin')) {
            $branches = Branch::all();
            $parks = Park::has('rides_park')->get();

        } else {
            $branches = Branch::whereIn('id', auth()->user()->branchs->pluck('id'))->get();
            $parks = Park::whereIn('id', auth()->user()->parks->pluck('id'))->get();
        }


        return view('admin.users.roles', compact('user', 'parks', 'list', 'user_id', 'listZone', 'zones', 'listRide', 'rides', 'branches', 'userRole', 'listRideZone'));

    }

    public
    function getZones(Request $request)
    {


        $listZone = UserZone::where('user_id', $request->user_id)->pluck('zone_id')->toArray();
        if ($request->arr != null) {
            $zones = Zone::where('park_id', $request->arr)->get();
        } else {
            $zones = collect([]);
        }
        $roleUser = $request->roleUser;
        $branche = $zones->first()?->branches;


        return view('admin.users.zone_ajax', compact('listZone', 'zones', 'roleUser', 'branche'));

    }

    public
    function rolesUpdate(Request $request, $id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update user role ';
        $action->save();

        $user = User::find($id);
        if (isset($request['branch_id'])) {
            $user->branchs()->sync($request['branch_id']);
        }

        UserPark::where('user_id', $id)->delete();
        $parkIds = $request['park_id'];
        $user->parks()->attach($parkIds);

        UserZone::where('user_id', $user->id)->delete();
        $zoneIds = $request['zone_id'];
        $user->zones()->sync($zoneIds);

        RideUser::where('user_id', $user->id)->delete();
//        if ($user->roles->first()?->name == 'Operations/Operator') {
//            RideUser::where('ride_id', $request['ride_id'])->delete();
//        }

        $rides = $request['ride_id'];
        $user->rides()->sync($rides);


        alert()->success('update successfully !');
        return redirect()->route('admin.users.index');


    }

    public
    function getRiders(Request $request)
    {

        $listRide = RideUser::where('user_id', $request->user_id)->pluck('ride_id')->toArray();
        $user = User::findOrFail($request->user_id);

        if ($request->arr != null) {
            $rides = Ride::where('zone_id', $request->arr)->get();

        } else {
            $rides = collect([]);
        }
        $roleUser = $request->roleUser;
//        dd($rides);


        return view('admin.users.ride_ajax', compact('listRide', 'rides', 'roleUser'));

    }

    // protected function getPark(Request $request)
    // {
    //     $parks = Park::whereIn('branch_id', $request->arr)->get();
    //     $list = UserPark::where('user_id', $request->user_id)->pluck('park_id')->toArray();
    //     $roleUser = $request->roleUser;

    //     return view('admin.users.park_ajax', compact('parks', 'list','roleUser'));

    // }
    protected
    function getPark(Request $request)
    {
//        dd($request->arr);
        if (isset($request->arr)) {
            $parks = Park::whereIn('branch_id', $request->arr)->get();
            $branches = Branch::whereIn('id', $request->arr)->get();

        } else {
            $parks = collect();
            $branches = collect();

        }
        $list = UserPark::where('user_id', $request->user_id)->pluck('park_id')->toArray();
        $userRole = $request->roleUser;
        $listZone = UserZone::where('user_id', $request->user_id)->pluck('zone_id')->toArray();
        $zones = Zone::get();
        $user = User::find($request->user_id);
        $listRide = RideUser::where('user_id', $request->user_id)->pluck('ride_id')->toArray();
        $rides = Ride::all();
//        dd($parks);

        return view('admin.users.all_items', compact('parks', 'list', 'userRole', 'listZone', 'branches', 'listRide', 'user', 'zones', 'rides'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function create()
    {
        $adminParkRoles = ['Maintenance/Worker', 'Maintenance/Supervisor', 'Maintenance/Manager', 'Maintenance/Director', 'Maintenance/Chief', 'Technical Services/Engineer',
            'Technical Services/Manager', 'Technical Services/Director', 'Health & safety/Supervisor', 'Health & safety/Manager', 'Health & safety/Director',
            'Operations/Zones Manager', 'Operations/Zone supervisor', 'Operations/Operator', 'Breaker'
        ];

        if (auth()->user()->hasRole('Super Admin')) {
            $roles = Role::where('name', '!=', 'Super Admin')->orderBy('id', 'desc')->pluck('name', 'name')->all();
        } else {
            $roles = $adminParkRoles;
        }
        $departments = Department::pluck('name', 'id')->all();
        $branches = Branch::pluck('name', 'id')->all();
        return view('admin.users.add', compact('roles', 'departments', 'branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public
    function store(StoreRequest $request)
    {

        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add new user ';
        $action->save();

        $adminRoles = [
            'Park Admin',
            'Client',
            'Management/General Manager',
            'Management/CEO',
            'Management/Operations Manager',
            'Management/Director',
            'Management/Chief'
        ];
        $current_role = $request->input('roles');

        if (auth()->user()->hasRole('Park Admin')) {

            if (in_array($current_role, $adminRoles)) {
                alert()->danger('You don\'t have permission to add a user with this role!' . $current_role);
                return redirect()->back();
            }
        }

        $user = User::create($request->validated());
        $user->assignRole($current_role);

        alert()->success('User added successfully!');
        return redirect()->route('admin.users.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit(User $user)
    {
        $adminParkRoles = ['Maintenance/Worker', 'Maintenance/Supervisor', 'Maintenance/Manager', 'Maintenance/Director', 'Maintenance/Chief', 'Technical Services/Engineer',
            'Technical Services/Manager', 'Technical Services/Director', 'Health & safety/Supervisor', 'Health & safety/Manager', 'Health & safety/Director',
            'Operations/Zones Manager', 'Operations/Zone supervisor', 'Operations/Operator', 'Breaker'
        ];
        if (auth()->user()->hasRole('Super Admin')) {
            $roles = Role::where('name', '!=', 'Super Admin')->orderBy('id', 'desc')->pluck('name', 'name')->all();
        } else {
            $roles = Role::whereIn('name', $adminParkRoles)->pluck('name', 'name')->toArray();
        }
        if ($user->hasRole('Super Admin')) {
            $roles = Role::orderBy('id', 'desc')->pluck('name', 'name')->all();
        }

        $branches = Branch::pluck('name', 'id')->all();
        $userRole = $user->roles()->first()->name ?? null;
        $departments = Department::pluck('name', 'id')->all();

        return view('admin.users.edit')->with([
            'user' => $user,
            'roles' => $roles,
            'userRole' => $userRole,
            'departments' => $departments,
            'branches' => $branches
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(UpdateRequest $request, User $user)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update user data ';
        $action->save();

        $validate = $request->validated();
        if ($validate['password'] == null) {
            unset($validate['password']);
        }
        $user->update($validate);

        \DB::table('model_has_roles')->where('model_id', $user->id)->delete();
        $user->assignRole($request->input('roles'));

        if (isset($request['park_id'])) {
            UserPark::where('user_id', $user->id)->delete();
            $parkIds = $request['park_id'];
            $user->parks()->attach($parkIds);
        }
        if (isset($request['zone_id'])) {
            UserZone::where('user_id', $user->id)->delete();
            $zoneIds = $request['zone_id'];
            $user->zones()->sync($zoneIds);
        }

        alert()->success('user edited successfully !');
        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(User $user)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete user  ';
        $action->save();
        if ($user->logs->isNotEmpty() || $user->parks->isNotEmpty()) {
            alert()->success('can not delete user related with data !');
        } else {
            $user->delete();
            alert()->success('user deleted successfully !');
        }

        return back();
    }
}
