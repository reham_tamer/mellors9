<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Accident\AccidentRequest;
use App\Http\Requests\Dashboard\Accident\InvestigationRequest;
use App\Models\Accident;
use App\Models\Department;
use App\Models\GeneralIncident;
use App\Models\GameTime;
use App\Models\Park;
use App\Models\Zone;
use App\Models\ParkTime;
use App\Models\Ride;
use Carbon\Carbon;

class InvestigationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $userparks = Park::pluck('name', 'id');
            $parks = Park::pluck('id');
        } else {
            $userparks = auth()->user()->parks->pluck('name', 'id');
            $parks = auth()->user()->parks->pluck('id');
        }

        $startDate = dateTime()?->date;
        $endDate = dateTime()?->close_date;
        $startTime = dateTime()?->start;
        $endTime = dateTime()?->end;
//dd( $endTime);

        if ($startDate && $endDate && $startTime && $endTime) {
            $startDateTime = "{$startDate} {$startTime}";
            $endDateTime = "{$endDate} {$endTime}";
            //   dd($startDateTime);

            $items = GeneralIncident::where('type', 'investigation')->whereIn('park_id', $parks)
                ->where(function ($query) use ($startDateTime, $endDateTime) {
                    $query->whereBetween('date', [$startDateTime, $endDateTime]);
                })
                ->get();
//dd( $items);
            return view('admin.investigation.index', compact('items', 'userparks'));
        } else {
            $items = GeneralIncident::where('type', 'investigation')->whereIn('park_id', $parks)
                ->whereDate('date', Carbon::now()->toDateString())->get();
//dd($items);
            return view('admin.investigation.index', compact('items', 'userparks'));
        }
    }
      

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
            $zones = Zone::pluck('name', 'id')->all();
            $rides = Ride::pluck('name', 'id')->all();

        }else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all(); 
            $zones = auth()->user()->zones->pluck('name', 'id')->all(); 
            $rides = auth()->user()->rides->pluck('name', 'id')->all(); 
                }   

        $departments = Department::pluck('name', 'id')->all();
        return view('admin.investigation.add', compact('departments', 'parks', 'zones', 'rides'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InvestigationRequest $request)
    {       
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'add investigation ';
        $action->save(); 

         $data = $request->validated();
            $incident = GeneralIncident::create([
                'type' => 'investigation',
                'status' => 'pending',
                'date' => $data['date'],
                'created_by_id' => auth()->user()->id,
                'value' => $data
            ]);
            if ($request['choose'] == 'ride') {
                $incident->ride_id = $data['ride_id'];
                $incident->park_id = $data['park_id'];
                $gameTime = GameTime::where('ride_id', $data['ride_id'])->where('date',$data['date'])->first();
                $incident->game_time_id = $gameTime?->id;
            }
            if ($request['choose'] == 'park') {
                $incident->park_id = $data['park_id'];
                $incident->ride_id = null;
                $incident->text = $data['text'];
            }
            $parkTime = ParkTime::where('park_id', $data['park_id'])->where('date',$data['date'])->first();
            $incident->park_time_id = $parkTime?->id;
            $incident->save();
        alert()->success('Incident Investigation Report Added successfully !');
        return redirect()->route('admin.investigation.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();
            $zones = Zone::pluck('name', 'id')->all();
            $rides = Ride::pluck('name', 'id')->all();

        }else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all(); 
            $zones = auth()->user()->zones->pluck('name', 'id')->all(); 
            $rides = auth()->user()->rides->pluck('name', 'id')->all(); 
                } 
        $departments = Department::pluck('name', 'id')->all();
        $accident = GeneralIncident::find($id);
        return view('admin.investigation.edit', compact('accident', 'departments','parks','zones','rides'));

    }
    public function show($id)
    {
        $departments = Department::pluck('name', 'id')->all();
        $accident = GeneralIncident::find($id);
        return view('admin.investigation.show', compact('accident', 'departments'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InvestigationRequest $request, $id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update investigation no.'.$id;
        $action->save();

        $data = $request->validated();
        $incident = GeneralIncident::findOrFail($id);
        $incident->update([
            'value' => $request->validated()
        ]);
        if ($request['choose'] == 'ride') {
            $incident->ride_id = $data['ride_id'];
            $incident->park_id = $data['park_id'];
        }
        if ($request['choose'] == 'park') {
            $incident->park_id = $data['park_id'];
            $incident->ride_id = null;
            $incident->text = $data['text'];
        }
        $incident->save();

        alert()->success('Investigation form updated successfully !');
        return redirect()->route('admin.investigation.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'delete investigation ';
        $action->save();  

        $accident = GeneralIncident::find($id);
        if ($accident) {
            $accident->delete();
            alert()->success('Investigation Report deleted successfully');
            return back();
        }
        alert()->error('Investigation Report not found');
        return redirect()->route('admin.investigation.index');
    }

    public function approve($id)
    {
        $rsr = GeneralIncident::find($id);
        $rsr->status = 'approved';
        $rsr->approve_by_id  = \auth()->user()->id;
        $rsr->save();
        alert()->success('Investigation form Approved successfully !');
        return redirect()->back();
    }
    public function approve_director($id)
    {
        $rsr = GeneralIncident::find($id);
        $rsr->director_approve = 'approved';
        $rsr->approve_by_id  = \auth()->user()->id;
        $rsr->save();
        alert()->success('Investigation form Approved successfully !');
        return redirect()->back();
    }
    public function search(Request $request){
        // dd($request);
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'search investigation ';
        $action->save(); 

         $date = $request->input('date');
         $park_id = $request->input('park_id');
         $userparks = auth()->user()->parks->pluck('name','id');
 
         $timeSlot=ParkTime::where('date',$date)->where('park_id',$park_id)->first();
         $parks = auth()->user()->parks->pluck('id');
         if($timeSlot !=null){
         $startDate = $timeSlot->date;
         $endDate = $timeSlot->close_date;
         $startTime = $timeSlot->start;
         $endTime = $timeSlot->end;
         
         $startDateTime = "{$startDate} {$startTime}";
         $endDateTime = "{$endDate} {$endTime}";
         
             $items = GeneralIncident::where('type', 'investigation')->whereIn('park_id', $parks)
                 ->where(function ($query) use ($startDateTime, $endDateTime) {
                     $query->whereBetween('date', [$startDateTime, $endDateTime]);
                 })
                 ->get();
         
             return view('admin.investigation.index', compact('items','userparks'));
         } else {
             $items=[];
         return view('admin.investigation.index', compact('items','userparks'));
         }
     }
}
