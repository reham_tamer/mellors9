<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Branch\BranchRequest;
use App\Models\Branch;
use App\Models\Country;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items=Branch::all();
       return view('admin.branches.index',compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::pluck('name', 'id')->all();
        return view('admin.branches.add',compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchRequest $request)
    {
        Branch::create($request->validated());
        alert()->success('Branch Added successfully !');
        return redirect()->route('admin.branches.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countries = Country::pluck('name', 'id')->all();

//        return view('admin.branches.edit')->with('branch',Branch::find($id));
        return view('admin.branches.edit',['branch'=>Branch::find($id) ,'countries' => $countries]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BranchRequest $request, Branch $branch)
    {
        $branch->update($request->validated());
        $branch->save();

        alert()->success('Branch updated successfully !');
        return redirect()->route('admin.branches.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branch=Branch::find($id);
        if ($branch){
            $branch->delete();
            alert()->success('Branch deleted successfully');
            return back();
        }
        alert()->error('Branch not found');
        return redirect()->route('admin.branches.index');
    }
}
