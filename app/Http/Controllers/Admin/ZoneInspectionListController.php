<?php

namespace App\Http\Controllers\Admin;

use App\Models\UserPark;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\InspectionList\zoneInspectionListRequest;
use App\Models\InspectionList;
use App\Models\Zone;
use Illuminate\Http\Request;
use App\Models\ZoneInspectionList;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;

class ZoneInspectionListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            if (auth()->user()->hasRole('Super Admin')) {
                $zones = Zone::where('type','without_rides')->get();
                $zone_inspect = Zone::where('type','without_rides')->pluck('id');
            } else {
                $parks = auth()->user()->parks->pluck('id');
                // return $zones;
                $zones = Zone::wherein('park_id', $parks)->where('type','without_rides')->get();
                $zone_inspect = Zone::wherein('park_id', $parks)->where('type','without_rides')->pluck('id');
            }
            $inspection_data_exist = ZoneInspectionList::where('lists_type', 'inspection')
                ->wherein('zone_id', $zone_inspect)->distinct()->pluck('zone_id')->toArray();
            $daily_data_exist = ZoneInspectionList::where('lists_type', 'daily')
                ->wherein('zone_id', $zone_inspect)->distinct()->pluck('zone_id')->toArray();
            $preopening_data_exist = ZoneInspectionList::where('lists_type', 'preopening')
                ->wherein('zone_id', $zone_inspect)->distinct()->pluck('zone_id')->toArray();

            $preclosing_data_exist = zoneInspectionList::where('lists_type', 'preclosing')
                ->wherein('zone_id', $zone_inspect)->distinct()->pluck('zone_id')->toArray();
            return Datatables::of($zones)
                ->addIndexColumn()
                ->addColumn('name', function ($row) {
                    return $row->name;
                })
                 ->addColumn('park_name', function ($row) {
                    return $row->parks ? $row->parks->name : ''; 
                })
                ->addColumn('daily_lists', function ($row) use ($daily_data_exist) {
                    if (in_array($row->id, $daily_data_exist)) {
                        $btn = '<a href="' . url('edit_zone_daily_elements/' . $row->id) . '">
                                        <button type="button" class="edit btn btn-success">
                                        <i class="fa fa-edit"></i> Edit
                                        </button>
                                    </a>';

                    } else {
                        $btn = ' <a href="' . url('add_zone_daily_elements/' . $row->id) . '">
                                    <button type="button" class="add btn btn-info">
                                    <i class="fa fa-plus"></i> Add
                                    </button>
                                    </a>';//                    }else{

                    }

                    return $btn;
                })
                ->addColumn('inspection_lists', function ($row) use ($inspection_data_exist) {
                    if (in_array($row->id, $inspection_data_exist)) {
                        $btn = '<a href="' . url('edit_zone_weekly_elements/' . $row->id) . '">
                                        <button type="button" class="edit btn btn-success">
                                        <i class="fa fa-edit"></i> Edit
                                        </button>
                                    </a>';

                    } else {
                        $btn = ' <a href="' . url('add_zone_weekly_elements/' . $row->id) . '">
                                    <button type="button" class="add btn btn-info">
                                    <i class="fa fa-plus"></i> Add
                                    </button>
                                    </a>';//                    }else{

                    }

                    return $btn;
                })
                ->addColumn('preopening_lists', function ($row) use ($preopening_data_exist) {
                    if (in_array($row->id, $preopening_data_exist)) {
                        $btn = '<a href="' . url('edit_zone_preopen_elements/' . $row->id) . '">
                                        <button type="button" class="edit btn btn-success">
                                        <i class="fa fa-edit"></i> Edit
                                        </button>
                                    </a>';

                    } else {
                        $btn = ' <a href="' . url('add_zone_preopen_elements/' . $row->id) . '">
                                    <button type="button" class="add btn btn-info">
                                    <i class="fa fa-plus"></i> Add
                                    </button>
                                    </a>';//                    }else{

                    }

                    return $btn;
                })
                ->addColumn('preclosing_lists', function ($row) use ($preclosing_data_exist) {
                    if (in_array($row->id, $preclosing_data_exist)) {
                        $btn = '<a href="' . url('edit_zone_preclose_elements/' . $row->id) . '">
                                        <button type="button" class="edit btn btn-success">
                                        <i class="fa fa-edit"></i> Edit
                                        </button>
                                    </a>';

                    } else {
                        $btn = ' <a href="' . url('add_zone_preclose_elements/' . $row->id) . '">
                                    <button type="button" class="add btn btn-info">
                                    <i class="fa fa-plus"></i> Add
                                    </button>
                                    </a>';//                    }else{

                    }

                    return $btn;
                })

                ->rawColumns(['name','park_name', 'daily_lists','inspection_lists', 'preopening_lists', 'preclosing_lists'])
                ->make(true);
        }

        return view('admin.zone_inspection_lists.index_datatable');
    }

    public function create()
    {

    }

    public function add_zone_inspection_lists($zone_id)
    {
        $inspection_list = InspectionList::where('type', 'inspection')->get();
        return view('admin.zone_inspection_lists.add', compact('inspection_list', 'zone_id'));
    }

    public function edit_zone_inspection_lists($zone_id)
    {
        $zone = Zone::find($zone_id);
        $list = ZoneInspectionList::where('zone_id', $zone_id)
            ->where('lists_type', 'inspection_list')->pluck('inspection_list_id')->toArray();
        $inspection_list = InspectionList::where('type', 'inspection')->get();
        $zones = Zone::pluck('name', 'id')->toArray();
        return view('admin.zone_inspection_lists.edit', compact('inspection_list', 'zone', 'list', 'zone_id'));

    }


    public function store(ZoneInspectionListRequest $request)
    {
        $zone = Zone::find($request['zone_id']);
        $inspectionListIds = $request['inspection_list_id'];
        $zone->inspection_list()->attach($inspectionListIds, ['lists_type' => 'inspection_list']);
        alert()->success('zone Inspection Elements Added Successfully !');
        return redirect()->route('admin.zone_inspection_lists.index');
    }


    public function edit($id)
    {


    }


    public function update_zone_inspection_lists(Request $request, $zone_id)
    {
        ZoneInspectionList::where('zone_id', $request->zone_id)->where('lists_type', 'inspection_list')->delete();
        $zone = Zone::find($request['zone_id']);
        $inspectionListIds = $request['inspection_list_id'];
        $zone->inspection_list()->attach($inspectionListIds, ['lists_type' => 'inspection_list']);
        alert()->success('zone Inspection List Updated Successfully !');
        return redirect()->route('admin.zone_inspection_lists.index');
    }

    public function update(ZoneInspectionListRequest $request, ZoneInspectionList $ZoneInspectionList)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inspection_list = InspectionList::find($id);
        if ($inspection_list) {

            $inspection_list->delete();
            alert()->success('Inspection element deleted successfully');
            return back();
        }
        alert()->error('department not found');
        return redirect()->route('admin.inspection_lists.index');
    }

    public function cheackzoneInspectionList(Request $request)
    {
        $item = ZoneInspectionList::where('zone_id', $request->zone_id)->first();
        /*         dd($item);
         */
        return response()->json(['item' => $item]);
    }
}
