<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\GameTime\GameTimeRequest;
use App\Models\GameTime;
use App\Models\Park;
use App\Models\Game;
use App\Models\ParkTime;
use App\Models\Ride;
use App\Models\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GameTimeController extends Controller
{
    
    public function index()
    {
        $items=Ride::all();
        return view('admin.game_times.index',compact('items'));
    }

    public function all_times($id)
    {
        $items=GameTime::where('park_time_id',$id)->get();
        return view('admin.game_times.all_games_times',compact('items'));
    }

   
    public function create()
    {

    }

  
    public function store(GameTimeRequest $request)
    {  
        $dateExists = GameTime::where([
            ['park_time_id',$request['park_time_id']],
            ['ride_id',$request['ride_id']]
        ])->first();
        if ($dateExists){
            $gametime = GameTime::where([
                ['park_time_id',$request['park_time_id']],
                ['ride_id',$request['ride_id']]
            ])->first();
            $gametime->update($request->validated());
            alert()->success('Ride Time Slot Updated Successfully !');
                }else{
                    GameTime::create($request->validated());
                    alert()->success('Time Slot Added Successfully to the Ride !');
                     }
        return redirect()->route('admin.park_times.index');
    }

    public function show($id)
    {
        $items=Ride::where('park_id',$id)->get();
        return view('admin.game_times.index',compact('items'));
    }
    public function all_rides($park_id,$park_time_id)
    {
        if(auth()->user()->can('park_times-list'))
        {
            $items=GameTime::where('park_time_id',$park_time_id)->get();
       // dd($items);
        if (auth()->user()->hasRole('Super Admin') || (auth()->user()->hasRole('Park Admin')) ||
            auth()->user()->hasRole('Management/CEO') || (auth()->user()->hasRole('Operations/Zones Manager')) ||
            auth()->user()->hasRole('Management/Operations Manager') || (auth()->user()->hasRole('Management/Director'))||
            auth()->user()->hasRole('Management/Chief') || (auth()->user()->hasRole('Operations/Zones Manager')) ||
            auth()->user()->hasRole('Technical Services/Engineer') || (auth()->user()->hasRole('Technical Services/Manager')) ||
            auth()->user()->hasRole('Technical Services/Director') || (auth()->user()->hasRole('Technical Services/Manager')) ||
            auth()->user()->hasRole('Health & safety/Director') || (auth()->user()->hasRole('Health & safety/Manager')) ||
            auth()->user()->hasRole('Maintenance/Chief') || (auth()->user()->hasRole('Maintenance/Director')) ||
            auth()->user()->hasRole('Maintenance/Manager') || (auth()->user()->hasRole('Client')) 
        ) {
            $zones=Zone::pluck('id');
        }else {
            $zones = auth()->user()->zones->pluck('id'); 
             }       
         //    dd($zones);
        $zone_rides=Ride::wherein('zone_id', $zones)->pluck('id')->toArray();
        //dd($zone_rides);

        return view('admin.game_times.index',compact('items','park_time_id','zone_rides'));
        }else
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GameTime  $GameTime
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $park_id=Ride::where('id',$id)->pluck('park_id')->first();
        $time=ParkTime::where('park_id',$park_id)->first();
        return view('admin.game_times.edit',compact('time','id','park_id'));

    }
    
    public function edit_ride_time($ride_id,$park_time_id)
    {
        $dateExists = GameTime::where([
            ['park_time_id',$park_time_id],
            ['ride_id',$ride_id]
        ])->first();
        if ($dateExists){
            $time=GameTime::where('park_time_id',$park_time_id)->where('ride_id',$ride_id)->first();
        }else{
        $time=ParkTime::findOrFail($park_time_id);
        }
        return view('admin.game_times.edit',compact('time','park_time_id','ride_id'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GameTime  $GameTime
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GameTime $gameTime)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GameTime  $GameTime
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gameTime=GameTime::find($id);
        if ($gameTime){

            $gameTime->delete();
            alert()->success('Time Slot deleted successfully');
            return back();
        }
        alert()->error('GameTime not found');
        return redirect()->route('admin.game_times.index');
    }
}
