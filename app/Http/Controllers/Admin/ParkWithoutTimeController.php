<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Zone;
use App\Models\Park;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;
use App\Models\ZoneInspectionInfo;



class ParkWithoutTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if (auth()->user()->hasRole('Super Admin')) {
            $zones = Zone::where('type','without_rides')->get();
            $allZones = Zone::where('type','without_rides')->pluck('name','id');
        } else {
            $parks = auth()->user()->parks->pluck('id');
            $zones = Zone::wherein('park_id', $parks)->where('type','without_rides')->get();
            $allZones = Zone::wherein('park_id', $parks)->where('type','without_rides')->pluck('name','id');
        }
        $date=Carbon::now()->toDateString();
        $open_data_exist = ZoneInspectionInfo::where('type', 'preopening')->whereDate('opened_date', $date) ->pluck('zone_id')->toArray();
        $close_data_exist = ZoneInspectionInfo::where('type', 'preclosing')->whereDate('opened_date', $date) ->pluck('zone_id')->toArray();
        $weekly_data_exist = ZoneInspectionInfo::where('type', 'inspection_list')->whereDate('opened_date', $date) ->pluck('zone_id')->toArray();
        $daily_data_exist = ZoneInspectionInfo::where('type', 'daily')->whereDate('opened_date', $date) ->pluck('zone_id')->toArray();

        return view('admin.park_without_time.index',compact('zones','allZones','daily_data_exist','close_data_exist','weekly_data_exist','open_data_exist','date'));
    }
    public function search(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'search on park without times';
        $action->save();

        $zone_id = $request->input('zone_id');
        $date = $request->input('date');
    
        $query = Zone::query();
    
        if (auth()->user()->hasRole('Super Admin')) {
            $parks=Park::all()->pluck('id');
            $query->where('type', 'without_rides');
        } else {
            $parks = auth()->user()->parks->pluck('id');
            $query->whereIn('park_id', $parks)->where('type', 'without_rides');
        }
    
        if ($zone_id) {
            $query->where('id', $zone_id);
        }
    
        if ($date) {
            $zone_ids_with_date = ZoneInspectionInfo::whereDate('opened_date', $date)
                ->pluck('zone_id')
                ->toArray();
            $query->whereIn('id', $zone_ids_with_date);
        }
    
        $zones = $query->get();
    
        $allZones = Zone::wherein('park_id', $parks)->where('type','without_rides')->pluck('name','id');
    
        // Inspection data variables
        $open_data_exist = ZoneInspectionInfo::where('type', 'preopening')
            ->whereDate('opened_date', $date)
            ->where('zone_id', $zone_id)
            ->pluck('zone_id')
            ->toArray();
        $close_data_exist = ZoneInspectionInfo::where('type', 'preclosing')
            ->whereDate('opened_date', $date)
            ->where('zone_id', $zone_id)
            ->pluck('zone_id')
            ->toArray();
        $weekly_data_exist = ZoneInspectionInfo::where('type', 'inspection_list')
            ->whereDate('opened_date', $date)
            ->where('zone_id', $zone_id)
            ->pluck('zone_id')
            ->toArray();
        $daily_data_exist = ZoneInspectionInfo::where('type', 'daily')
            ->whereDate('opened_date', $date)
            ->where('zone_id', $zone_id)
            ->pluck('zone_id')
            ->toArray();
    
        return view('admin.park_without_time.index', compact(
            'zones', 
            'allZones', 
            'close_data_exist', 
            'weekly_data_exist', 
            'open_data_exist', 
            'daily_data_exist', 
            'date'
        ));
    }
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.departments.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentRequest $request)
    {
        Department::create(['name' => $request->input('name')]);
        alert()->success('Department Added successfully !');
        return redirect()->route('admin.departments.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.departments.edit')->with('department',Department::find($id));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentRequest $request, Department $department)
    {
        $department->update($request->validated());
        $department->save();

        alert()->success('department updated successfully !');
        return redirect()->route('admin.departments.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department=Department::find($id);
        if ($department){

            $department->delete();
            alert()->success('department deleted successfully');
            return back();
        }
        alert()->error('department not found');
        return redirect()->route('admin.departments.index');
    }
}
