<?php

namespace App\Http\Controllers\Admin;

use App\Models\CycleRideNumber;
use App\Models\ParkTime;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller {
    public function test() {
        $array = [];

        $currentDate = Carbon::now()->toDateString();
        $currentTime = Carbon::now('Asia/Riyadh')->format('H:i');
         $w = ParkTime::where('date', $currentDate)
             ->orWhere(function ($subquery) use ($currentDate, $currentTime) {
                 $subquery->where('close_date', $currentDate)
                     ->where('end', '>=', $currentTime);
             })->get()
             ->each(function ($parkTime) {
                 foreach ($parkTime->parks->rides as $ride) {
                     dd($ride);
                     $cycles = $ride->cycle?->where('park_time_id', $parkTime->id)->where('start_time', '<=', Carbon::now('Asia/Riyadh'));
                     $totalRiders = $cycles?->sum('riders_count') + $cycles?->sum('number_of_disabled') + $cycles?->sum('number_of_vip') + $cycles?->sum('number_of_ft');
//                    if ($totalRiders > 0) {

                     $array[] = array('ride_id' => $ride->name, 'rider_count' => $totalRiders, 'date' => Carbon::now('Asia/Riyadh'));
//                    }
                     return $array;
                 }
             });
         dd($w);

    }

    public function getLogin() {
        if (Auth::Check()) {
            return redirect('/');
        }
        return view('auth.login');
    }

    public function postLogin() {
        $inputs = Request()->all();

        if (Auth::attempt(['email' => $inputs['email'], 'password' => $inputs['password'], 'deleted_at' => null])) {
            $status = Auth::user()->status;
            if ($status == 1) {
                return redirect('/');
            } else {
                Auth::logout();
                return back()->withInput()->withErrors(['error' => 'Not Allowed ,Not active user']);
            }
        } else {
            return back()->withInput()->withErrors(['error' => 'Wrong email or password']);
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('login');
    }


}
