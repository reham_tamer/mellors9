<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\MobileVersionRequest;
use App\Models\MobileVersion;
use App\Models\Ride;


class MobileVersionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = MobileVersion::query()->get();

        return view('admin.versions.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.versions.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(MobileVersionRequest $request)
    {
        MobileVersion::create($request->validated());
        alert()->success('Version Added successfully !');
        return redirect()->route('admin.versions.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $version = MobileVersion::find($id);
        return view('admin.versions.edit', compact('version'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(MobileVersionRequest $request, MobileVersion $version)
    {
        $version->update($request->validated());

        alert()->success('Version updated successfully !');
        return redirect()->route('admin.versions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MobileVersion::find($id)->delete();

        alert()->success('Version deleted successfully');
        return redirect()->route('admin.versions.index');
    }


}
