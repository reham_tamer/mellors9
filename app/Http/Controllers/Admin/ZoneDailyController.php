<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\InspectionList\ZoneInspectionListRequest;
use App\Models\InspectionList;
use App\Models\ZoneInspectionInfo;
use App\Models\ZoneInspection;
use App\Models\Zone;
use Illuminate\Http\Request;
use App\Models\ZoneInspectionList;
use Carbon\Carbon;

class ZoneDailyController extends Controller
{

    public function index()
    {
    }


    public function create()
    {
    }

       public function add_zone_daily_elements($zone_id)
    {
        $inspection_list=InspectionList::where('type','daily')->get();
        return view('admin.zone_daily_inspection.add',compact('inspection_list','zone_id'));
    }

    public function edit_zone_daily_elements( $zone_id)
    {
        $zone=Zone::find($zone_id);
        $list=ZoneInspectionList::where('zone_id',$zone_id)
        ->where('lists_type','daily')->pluck('inspection_list_id')->toArray();

        $inspection_list=InspectionList::where('type','daily')->get();
        $zones=Zone::pluck('name','id')->toArray();
        return view('admin.zone_daily_inspection.edit',compact('inspection_list','zone','list','zone_id'));

    }

    public function store(ZoneInspectionListRequest $request)
    {
        $zone = Zone::find($request['zone_id']);
        $inspectionListIds = $request['inspection_list_id'];
        $zone->inspection_list()->attach($inspectionListIds, ['lists_type' => 'daily']);
         alert()->success('Zone Hourly Elements Added Successfully !');
        return redirect()->route('admin.zone_inspection_lists.index');
    }

    public function update_zone_daily_elements(Request $request,  $zone_id)
    { 
        ZoneInspectionList::where('zone_id',$request->zone_id)->where('lists_type','daily')->delete();
        $zone = Zone::find($request['zone_id']);
        $inspectionListIds = $request['inspection_list_id'];
        $zone->inspection_list()->attach($inspectionListIds, ['lists_type' => 'daily']);
        alert()->success('Zone Hourly Elements Updated Successfully !');
        return redirect()->route('admin.zone_inspection_lists.index');
    }
    public function show_zone_daily_list($zone_id,$date)
    {
        $items = ZoneInspectionInfo::where('opened_date', $date)
        ->where('zone_id', $zone_id)->where('type','daily')
        ->get();
        if(! $items){
            alert()->error('Hourly Inspection List not added yet for this Zone !');
            return redirect()->back();
        }

    return view('admin.zone_daily_inspection.all_lists', compact('items'));
    }

    public function show($id)
    {
        $list = ZoneInspectionInfo::find($id);
        if(! $list){
            alert()->error('daily Inspection List not added yet for this Zone !');
            return redirect()->back();
        }

    $preopenInfo_id=$list->id;

    $items =ZoneInspection::where('zone_inspection_infos_id',$preopenInfo_id)->get()->unique('inspection_list_id');

    return view('admin.zone_daily_inspection.show', compact('items', 'list'));
    }

    public function approve($id)
    {
       $item= ZoneInspectionInfo::find($id);
        $item->status = 'approved';
        $item->approve_by_id  = \auth()->user()->id;
        $item->save();
        alert()->success('Hourly Inspection List Approved Successfully !');
        return redirect()->back();
    }

    public function edit( $id)
    {


    }

    public function update(ZoneInspectionListRequest $request, ZoneInspectionList $zoneInspectionList)
    {
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }


}
