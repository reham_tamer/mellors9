<?php

namespace App\Http\Controllers\Admin;


use App\Models\GameTime;
use App\Models\Park;
use App\Models\ParkTime;
use App\Models\Ride;
use App\Models\RideCapacity;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;


class RideCapacityController extends Controller
{
    public function index(Request $request)
    {
        $from = $request->input('from');
        $to = $request->input('to');

        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('id');
            $park = Park::pluck('name','id');

            $rides = Ride::pluck('name', 'id')->all();

        } else {
            $parks = auth()->user()->parks->pluck('id');
            $park = auth()->user()->parks->pluck('name','id');
            $rides = auth()->user()->rides->pluck('name', 'id');
        }

        if (request('ride_id') && $from != null) {
            $items = RideCapacity::whereIn('park_id', $parks)
                ->where('ride_id', request('ride_id'))->where(function ($query) use ($from, $to) {
                    $query->whereBetween('date', [$from, $to]);
                })
                ->get();

        } else {
            if (dateTime()) {
                $items = RideCapacity::whereIn('park_id', $parks)->where(function ($query) {
                    $query->whereBetween('date', [dateTime()->date, dateTime()->close_date]);
                })->get();
            } else {
                $items = collect();
            }

        }

        return view('admin.ride_capacity.index', compact('items', 'rides','park'));
    }

    protected function report(Request $request)
    {
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'show ride capacity Report ';
        $action->save();

        $outRang = false;
        if (auth()->user()->hasRole('Super Admin')) {
            $parks = Park::pluck('name', 'id')->all();

        } else {
            $parks = auth()->user()->parks->pluck('name', 'id')->all();
        }
        if ($request->ajax()){
            $park_id = $request->input('park_id');
            $items = RideCapacity::where('park_id', $park_id);

            if ($request->input('ride_id')) {
                $items = $items->where('ride_id', $request->input('ride_id'));
            }
            if ($request->input('from')) {
                $items = $items->where('date', '>=', $request->input('from'));
            }

            if ($request->input('to')) {
                $items = $items->where('date', '<=', $request->input('to'));
            }
            $datetimeFrom = \Carbon\Carbon::parse($request->input('from'));
            $datetimeTo = Carbon::parse($request->input('to'));
            $interval = $datetimeFrom->diffInDays($datetimeTo);
            if ($interval > 30){
                $outRang = true;
            }
            $items = $items->get();
            return view('admin.ride_capacity.ajax', compact('items', 'parks','outRang'));

        }



        return view('admin.ride_capacity.report', compact( 'parks'));

    }




    public function update(Request $request)
    {

        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'update ride capacity ';
        $action->save();

        $capacity = RideCapacity::find($request->id);
        $capacity->update(['final_capacity' => $request->final_capacity]);
        alert()->success('Final Capacity Updated successfully !');
        return redirect()->back();

    }

    public function changeCapacity(Request $request)
    {
        
        $action = new \App\Models\Action();
        $action->user_id = auth()->User()->id;
        $action->action = 'change ride capacity ';
        $action->save();

        $request->validate([
            'ride_id' => 'required|exists:rides,id',
            'park_id' => 'required|exists:parks,id',
            'final_capacity' => 'required',
        ]);

        if (!dateTime()) {
            alert()->error('Please Add Park Time First !');
            return redirect()->back();
        }
        $lastParkTime = ParkTime::where('park_id',$request->park_id)->orderBy('id', 'desc')->first();
        RideCapacity::query()->create([
            'final_capacity' => $request->final_capacity,
            'park_id' => $request->park_id,
            'ride_id' => $request->ride_id,
            'date' => $lastParkTime?->date,
            'time' => Carbon::now()->toTimeString()
        ]);
        alert()->success('Final Capacity Updated successfully !');
        return redirect()->back();

    }




}
