<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class CloseDbConnection
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        
        DB::disconnect();
        
        return $response;
    }
}
