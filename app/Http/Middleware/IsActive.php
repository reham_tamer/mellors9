<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsActive {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (!Auth::Check() || Auth::User()->status != 1) {
            if (Auth::Check()) {
                Auth::logout();
            }
            return redirect('login');
        }
        return $next($request);
    }

}
