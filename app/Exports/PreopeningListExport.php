<?php

namespace App\Exports;

use App\Models\PreopeningList;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class PreopeningListExport implements FromView
{

    public function __construct(public  $items)
    {
    }

    public function view(): View
    {
        return view('admin.reports.exports.inspection', [
            'items' => $this->items
        ]);
    }
}
