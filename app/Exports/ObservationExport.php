<?php

namespace App\Exports;

use App\Models\PreopeningList;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class ObservationExport implements FromView
{

    public function __construct(public  $items)
    {
    }

    public function view(): View
    {
        return view('admin.reports.exports.abservation', [
            'items' => $this->items
        ]);
    }
}
