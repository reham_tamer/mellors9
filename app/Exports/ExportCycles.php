<?php

namespace App\Exports;
use Illuminate\Contracts\View\View;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;

class ExportCycles implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct(public  $items)
    {
    }

    public function view(): View
    {
        return view('admin.reports.exports.cycles', [
            'items' => $this->items
        ]);
    }





}
