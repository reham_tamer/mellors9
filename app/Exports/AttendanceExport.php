<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class AttendanceExport implements FromView
{

    public function __construct(public  $items)
    {
    }

    public function view(): View
    {
        return view('admin.reports.exports.attendance', [
            'items' => $this->items
        ]);
    }
}
