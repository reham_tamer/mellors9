<?php

use App\Http\Controllers\Api\AttendanceController;
use App\Http\Controllers\Api\Auth\AttendanceAuthController;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\DailyCheckListQuestionController;
use App\Http\Controllers\Api\EvaluationController;
use App\Http\Controllers\Api\HealthSaftyController;
use App\Http\Controllers\Api\HomeController;
use App\Http\Controllers\Api\RideController;
use App\Http\Controllers\Api\RideStoppageController;
use App\Http\Controllers\Api\TrainingController;
use App\Http\Controllers\Api\TrainingEvacuationController;
use App\Http\Controllers\Api\ZoneCheckListController;
use App\Http\Controllers\Api\SupervisorController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Symfony\Component\Console\Input\Input;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::middleware('settimezone')->group(function () {

Route::post('login', [AuthController::class, 'login']);
Route::post('attendance-login', [AttendanceAuthController::class, 'login']);
//});
Route::middleware('throttle:300,1')->group(function () {

    Route::get('get_user_log', [RideController::class, 'userLogDay']);
    Route::get('get_park_users_log', [RideController::class, 'allParkUsersLog']);
    Route::get('get_attendant_log', [RideController::class, 'attendanceLogDay']);
    Route::get('get_park_attendants_log', [RideController::class, 'allParkAttendancesLog']);
    Route::get('parks', [RideController::class, 'parks']);
    Route::get('operators', [RideController::class, 'operators']);
    Route::get('attendants', [RideController::class, 'attendants']);
    Route::get('parks/{park_id}/rides', [RideController::class, 'rides_by_park']);

});
Route::post('versions', [RideController::class, 'versions']);

Route::middleware(['auth:sanctum', 'settimezone', 'throttle:60,1','close-db'])->group(function () {
    // operators
    Route::get('home', [RideController::class, 'home']);
    Route::get('settings', [RideController::class, 'timeSlot']);

    Route::get('ride/{id}', [RideController::class, 'ride']);
    Route::get('ride_preopening/{id}', [RideController::class, 'ridePreopening']);
    Route::get('ride_preclosing/{id}', [RideController::class, 'ridePreclosing']);
    Route::get('ride_inspection_list/{id}', [RideController::class, 'rideInspectionList']);
    Route::post('store_inspection', [RideController::class, 'storeInspection']);

    Route::get('get_ride_status', [RideController::class, 'rideStatus']);
    Route::post('add_ride_stoppage', [RideStoppageController::class, 'addRideStoppage']);
    Route::get('get_ride_stoppages/{id}', [RideStoppageController::class, 'getRideStoppages']);
    Route::post('update_stoppage', [RideStoppageController::class, 'UpdateRideStoppages']);
    Route::post('update_stoppage_category', [RideStoppageController::class, 'UpdateStoppageCategory']);
    Route::post('reopen', [RideStoppageController::class, 'reopen']);
    Route::post('add_another_stoppage', [RideStoppageController::class, 'addAnotherStoppage']);

    Route::post('add_cycle', [RideController::class, 'addCycle']);
    Route::post('update_cycle_count', [RideController::class, 'updateCycleCount']);
    Route::post('update_cycle_duration', [RideController::class, 'updateCycleDuration']);

    Route::post('add_queues', [RideController::class, 'addQueues']);
    Route::get('get_queues/{id}', [RideController::class, 'getQueues']);
    Route::post('stop_queues', [RideController::class, 'stopQueues']);
    Route::get('get_last_queue', [RideController::class, 'getLastQueue']);

    Route::get('get_cycle', [RideController::class, 'getCycle']);

    //zone supervisor
    Route::get('home_zone_supervisor', [SupervisorController::class, 'home']);
    Route::get('preopening_list/{id}', [SupervisorController::class, 'preopeningList']);
    Route::get('preclosing_list/{id}', [SupervisorController::class, 'preclosingList']);
    Route::post('add_feedback', [SupervisorController::class, 'storeCustomerFeedback']);
    Route::get('get_complaints', [SupervisorController::class, 'getComplaint']);

    Route::post('update_inspection', [SupervisorController::class, 'updateInspectionList']);
    Route::post('add_observation', [SupervisorController::class, 'observation']);
    Route::get('category_observation', [SupervisorController::class, 'observationCategory']);
    Route::get('get_questions', [SupervisorController::class, 'questions']);
    Route::post('add_attraction', [SupervisorController::class, 'addAttraction']);

    Route::get('get_capacity/{id}', [SupervisorController::class, 'getCapacity']);
    Route::post('update_ride_capacity', [SupervisorController::class, 'updateRideCapacity']);
    Route::post('change_capacity_ride_supervisor', [SupervisorController::class, 'changeCapacityBySupervisor']);


    Route::post('logout', [AuthController::class, 'logout']);
    Route::get('notifications', [AuthController::class, 'notifications']);

    Route::get('main_page', [HomeController::class, 'home']);
    Route::get('zone_inspection_info', [HomeController::class, 'zoneInspectionInfo']);
    Route::get('test_event', [HomeController::class, 'event']);
    Route::get('test_event2', [HomeController::class, 'event2']);

    Route::post('incident', [HealthSaftyController::class, 'incident']);
    Route::post('investigation', [HealthSaftyController::class, 'investigation']);
    Route::post('add_evacuation', [HealthSaftyController::class, 'evacuation']);
    Route::get('departments', [HealthSaftyController::class, 'departments']);
    Route::get('/pusher/beams-auth', function (Request $request) {
        $beamsClient = new \Pusher\PushNotifications\PushNotifications(array(
            "instanceId" => "5ff5290c-319d-46b8-b077-84ad0350650c",
            "secretKey" => "4080E12EBBEC3A3914D4E1939E52C5E6BBD6DA8BBC3F72481B7F3AD8209A9A61",
        ));
        $userID = $request->user()->id; // If you use a different auth system, do your checks here
        $userIDInQueryParam = Auth::user()->id;

        if ($userID != $userIDInQueryParam) {
            return response('Inconsistent request', 401);
        } else {
            $beamsToken = $beamsClient->generateToken($userID);
            return response()->json($beamsToken);
        }
    });

    //attendance
    Route::group(['prefix' => 'attendance', 'as' => 'attendance.'], function () {
        Route::post('read-qr', [AttendanceController::class, 'read']);
        Route::get('get-current-attendance', [AttendanceController::class, 'get_attendance']);

        Route::post('choose-ride/{id}', [AttendanceController::class, 'chooseRide']);
        Route::post('logout', [AttendanceController::class, 'logout']);

    });

    Route::get('get_daily_question', [DailyCheckListQuestionController::class, 'questions']);
    Route::post('store_daily_checklist', [DailyCheckListQuestionController::class, 'storeDailyCheckList']);


    Route::get('home_zones_supervisor', [ZoneCheckListController::class, 'home']);
    Route::get('zone_preopening/{id}', [ZoneCheckListController::class, 'zonePreopening']);
    Route::get('zone_preclosing/{id}', [ZoneCheckListController::class, 'zonePreclosing']);
    Route::get('zone_inspection_list/{id}', [ZoneCheckListController::class, 'zoneInspectionList']);
    Route::get('zone_daily_list/{id}', [ZoneCheckListController::class, 'zoneDailyList']);
    Route::post('store_zone_inspection', [ZoneCheckListController::class, 'storeZoneInspection']);


    Route::group(['prefix' => 'evaluation', 'as' => 'evaluation.'], function () {
        Route::get('question', [EvaluationController::class, 'questions']);
        Route::post('store', [EvaluationController::class, 'storeEvaluation']);
        Route::get('get_supervisors_by_park', [EvaluationController::class, 'getSupervisorByPark']);
        Route::get('question_supervisors', [EvaluationController::class, 'questionsSupervisors']);
        Route::get('get_breakers_by_zone', [EvaluationController::class, 'getbreakerByZone']);

    });
    Route::group(['prefix' => 'training', 'as' => 'training.'], function () {
        Route::group(['prefix' => 'operator', 'as' => 'operator.'], function () {
            Route::get('', [TrainingController::class, 'operatorChecklist']);
            Route::get('show/{training}', [TrainingController::class, 'operatorChecklistShow']);
            Route::post('store', [TrainingController::class, 'operatorChecklistStore']);
        });

        Route::group(['prefix' => 'attendance', 'as' => 'attendance.'], function () {
            Route::get('', [TrainingController::class, 'attendanceChecklist']);
            Route::get('show/{training}', [TrainingController::class, 'attendanceChecklistShow']);
            Route::post('store', [TrainingController::class, 'attendanceChecklistStore']);
        });
        Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
            Route::get('', [TrainingController::class, 'trainingAdmin']);
            Route::get('show/{training}', [TrainingController::class, 'trainingAdminShow']);
            Route::get('user/{training}', [TrainingController::class, 'trainingAdminShowUser']);
            Route::post('store/signature', [TrainingController::class, 'trainingAdminStoreSignature']);
            Route::post('add/comments', [TrainingController::class, 'addComment']);
            Route::post('add/comments', [TrainingController::class, 'addComment']);
            Route::post('add/additions', [TrainingController::class, 'addAdditions']);

        });
    });
    Route::group(['prefix' => 'evacuation/training', 'as' => 'evacuation-training.'], function () {
        Route::get('', [TrainingEvacuationController::class, 'trainingAdmin']);
        Route::get('show/{training}', [TrainingEvacuationController::class, 'trainingAdminShow']);
        Route::get('user/{training}', [TrainingEvacuationController::class, 'trainingAdminShowUser']);
        Route::post('store', [TrainingEvacuationController::class, 'adminChecklistUsersStore']);
        Route::post('admin/store/signature', [TrainingEvacuationController::class, 'trainingAdminStoreSignature']);
    });
    Route::post('assign-ride', [RideController::class, 'assignRide']);
    Route::post('time-login', [AuthController::class, 'timeLogin']);
    Route::post('time-logout', [AuthController::class, 'TimeLogout']);


});
