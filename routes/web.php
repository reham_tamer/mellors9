<?php

use Illuminate\Http\Request;
use App\Events\RsrReportEvent;
use App\Events\PrivetChatEvent;
use App\Events\RideStatusEvent;
use App\Events\timeSlotNotification;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();
Route::get('login', [AdminController::class, 'getLogin'])->name('login');
Route::get('test', [AdminController::class, 'test'])->name('test');
Route::post('login', [AdminController::class, 'postLogin'])->name('postlogin');
Route::post('logout', [AdminController::class, 'logout'])->name('logout');

Route::group(['middleware' => ['active', 'settimezone', 'close-db'], 'as' => 'admin.'], function () {

    Route::get('/', 'Admin\IndexController')->name('index');

    Route::get('/riders', 'Admin\IndexController@riders')->name('riders');
    Route::get('/riders_park/{id}', 'Admin\IndexController@singlePage')->name('singlePage');

    Route::get('/rider_cycles', 'Admin\IndexController@ridersCycles')->name('riders-cycles');
    Route::get('/rider_cycle_park/{id}', 'Admin\IndexController@singlePageRiderCycle')->name('singlePage-cycles');

    Route::get('queues_time_slot', 'Admin\QueueController@queuesTimeSlot')->name('queue.timeSlot');
    Route::get('queues/time/{id}', 'Admin\QueueController@queuesTime')->name('queue.time');
    Route::get('queues/ride/{id}/{time}', 'Admin\QueueController@queuesRide')->name('queue.ride');


    Route::get('statistics', 'Admin\IndexController@statistics')->name('statistics');
    Route::get('statistics/park/{id}', 'Admin\IndexController@statisticPark')->name('statisticPark');
    Route::get('make_all_read', 'Admin\IndexController@makeAllRead')->name('makeAllRead');
    Route::get('all_notifications', 'Admin\IndexController@allNotifications')->name('allNotifications');
    Route::resource('roles', 'Admin\RoleController'); // done
    Route::resource('users', 'Admin\UserController'); // done
    Route::resource('attendances', 'Admin\AttendanceController'); // done
    Route::get('upload-attendance', 'Admin\AttendanceController@uploadAttendance')->name('uploadAttendance');
    Route::post('upload-attendance-excle', 'Admin\AttendanceController@uploadAttendancesExcleFile')->name('uploadAttendancesExcleFile');
    Route::get('upload-attendance-time', 'Admin\AttendanceController@uploadAttendanceTime')->name('uploadAttendanceTime');
    Route::post('upload-attendance-time-excle', 'Admin\AttendanceController@uploadAttendancesTimeExcleFile')->name('uploadAttendancesTimeExcleFile');

    Route::get('attendance_show_time/{attendanceLog}', 'Admin\AttendanceController@show_time')->name('show_time');
    Route::get('attendance_update-time', 'Admin\AttendanceController@show_time_update')->name('show_time_update');
    Route::get('attendance_time_delete/{attendanceLog}', 'Admin\AttendanceController@delete')->name('attendance-time-delete');
    Route::get('attendance_add_time/{id}', 'Admin\AttendanceController@addTime')->name('addTime');
    Route::Post('store_time', 'Admin\AttendanceController@StoreTime')->name('attendances.store_time');

    Route::resource('operators', 'Admin\OperatorController');
    Route::get('operator_add_time/{id}', 'Admin\OperatorController@addTime')->name('operatorAddTime');
    Route::get('upload-operator-time', 'Admin\OperatorController@uploadOperatorTime')->name('uploadOperatorTime');
    Route::post('upload-operator-time-excle', 'Admin\OperatorController@uploadOperatorTimeExcleFile')->name('uploadOperatorTimeExcleFile');


    Route::get('operator_update-time', 'Admin\ReportsController@operator_show_time_update')->name('operator-show-time-update');
    Route::get('operator_time_delete/{userLog}', 'Admin\ReportsController@delete_operator_time')->name('operator-time-delete');

    Route::resource('departments', 'Admin\DepartmentController');

    Route::resource('branches', 'Admin\BranchController');//done
    Route::resource('parks', 'Admin\ParkController');//done
    Route::get('get_by_branch', 'Admin\ParkController@get_by_branch')->name('parks.get_by_branch');
    Route::resource('zones', 'Admin\ZoneController');//done
    Route::resource('versions', 'Admin\MobileVersionsController');//done
    Route::get('get_by_branch_id', 'Admin\ZoneController@get_by_branch')->name('zones.get_by_branch');


    Route::resource('park_without_times', 'Admin\ParkWithoutTimeController');//done
    Route::get('/search_park_without_times/', 'Admin\ParkWithoutTimeController@search')->name('search_park_without_times');

    Route::resource('park_times', 'Admin\ParkTimeController');//done
    Route::resource('schedule_park_times', 'Admin\ScheduleParkTimeController');
    Route::get('/search_park_times/', 'Admin\ParkTimeController@search');

    Route::resource('game_times', 'Admin\GameTimeController');//done
    Route::get('/edit_ride_time/{ride_id}/{park_time_id}', 'Admin\GameTimeController@edit_ride_time')->name('editRideTime');

    Route::PATCH('daily_entrance_count', 'Admin\ParkTimeController@add_daily_entrance_count')->name('park_times.daily_entrance_count');
    Route::get('/all-rides/{park_id}/{time_slot_id}', 'Admin\GameTimeController@all_rides')->name('allRides');

    Route::get('/game-all-times/{id}', 'Admin\GameTimeController@all_times');

    Route::resource('ride_types', 'Admin\RideTypeController');
    Route::resource('games', 'Admin\GameController');//done

    //operation
    Route::resource('stoppage-category', 'Admin\StoppageCategoryController');//done
    Route::resource('stoppage-sub-category', 'Admin\StoppageSubCategoryController');//done
    Route::resource('rides', 'Admin\RidesController');//done
    Route::delete('destroy_park/{id}', 'Admin\RidesController@destroyPark')->name('destroy_park');


    Route::get('upload-rides', 'Admin\RidesController@uploadRides')->name('uploadRides');
    Route::Post('upload-rides-with-excel', 'Admin\RidesController@uploadExcleFile')->name('uploadExcleFile');

    Route::resource('rides-stoppages', 'Admin\RideStoppageController');//done
    Route::get('/search_stoppages/', 'Admin\RideStoppageController@search')->name('searchStoppage');
    Route::Post('upload-stoppages-with-excel', 'Admin\RideStoppageController@uploadStoppagesExcleFile')->name('uploadStoppagesExcleFile');
    Route::post('get-images', 'Admin\RideStoppageController@getImage')->name('getImage');
    Route::get('/show_stoppages/{ride_id}/{park_time_id}', 'Admin\RideStoppageController@show_stoppages')->name('showStoppages');
    Route::get('/add_stoppage/{ride_id}/{park_time_id}', 'Admin\RideStoppageController@add_stoppage')->name('addStoppage');
    Route::PATCH('update_stoppage_status', 'Admin\RideStoppageController@extend')->name('rides-stoppages.updateStoppageStatus');
    Route::get('solve_stoppage/{id}', 'Admin\RideStoppageController@solveStoppage')->name('solveStoppage');

    Route::resource('rides-cycles', 'Admin\RideCyclesController');//done
    Route::Post('upload-cycles-with-excel', 'Admin\RideCyclesController@uploadCycleExcleFile')->name('uploadCycleExcleFile');
    Route::get('/search_ride_cycle/', 'Admin\RideCyclesController@search')->name('searchRideCycle');
    Route::get('/show_cycles/{ride_id}/{park_time_id}', 'Admin\RideCyclesController@show_cycles')->name('showCycles');
    Route::get('/add_cycle/{ride_id}/{park_time_id}', 'Admin\RideCyclesController@add_cycle')->name('addCycle');

    Route::resource('queues', 'Admin\QueueController');//done
    Route::Post('upload-queues-with-excel', 'Admin\QueueController@uploadQueueExcleFile')->name('uploadQueueExcleFile');
    Route::get('/search_queues/', 'Admin\QueueController@search')->name('search');
    Route::get('/show_queues/{ride_id}/{park_time_id}', 'Admin\QueueController@show_queues')->name('showQueues');
    Route::get('/add_queue/{ride_id}/{park_time_id}', 'Admin\QueueController@add_queue')->name('addQueue');

    Route::resource('inspection_lists', 'Admin\InspectionListController');//done

    Route::resource('preopening_lists', 'Admin\PreopeningListController');//done
    Route::get('/add_preopening_list_to_ride/{ride_id}/{park_time_id}', 'Admin\PreopeningListController@add_preopening_list_to_ride');
    Route::post('/update_preopening_list/{preopen_info_id}/', 'Admin\PreopeningListController@update_ride_preopening_list')->name('updatePreopeningList');
    Route::get('/cheack_preopening_list/', 'Admin\PreopeningListController@cheackPreopeningList')->name('cheackPreopeningList');
    Route::get('/show_preopening_list/{ride_id}/{park_time_id}', 'Admin\PreopeningListController@show_ride_preopening_list')->name('showPreopeningList');
    Route::get('/delete_preopening/{id}/{park_id}/{park_time_id}', 'Admin\PreopeningListController@deletePreopenetng')->name('deletePreopeningList');

    Route::resource('general_questions', 'Admin\GeneralQuestionsController');//done

    Route::get('/show_general_questions/{ride_id}/{park_time_id}', 'Admin\GeneralQuestionsController@show_questions')->name('show_questions');
    Route::get('/add_general_questions/{ride_id}/{park_time_id}', 'Admin\GeneralQuestionsController@add_general_questions');
    Route::get('/edit_questions/{id}', 'Admin\GeneralQuestionsController@edit')->name('edit_questions');
    Route::get('/show_questions_list/{id}', 'Admin\GeneralQuestionsController@show_questions_list')->name('show_questions_list');
    Route::post('/update_questions/{id}', 'Admin\GeneralQuestionsController@update')->name('update_questions');
    Route::post('/store_questions', 'Admin\GeneralQuestionsController@store')->name('store_questions');
    Route::get('questions/{id}/approve', 'Admin\GeneralQuestionsController@approve');


    Route::get('/zone_rides/{zone_id}', 'Admin\PreopeningListController@zone_rides')->name('zoneRides');
    Route::resource('ride_inspection_lists', 'Admin\RideInspectionListController');
    Route::get('/add_ride_inspection_lists/{ride_id}', 'Admin\RideInspectionListController@add_ride_inspection_lists')->name('addRideInspectionLists');
    Route::get('/cheack_ride_inspection_lists/', 'Admin\RideInspectionListController@cheackRideInspectionList')->name('cheackRideInspectionList');
    Route::get('/edit_ride_inspection_lists/{ride_id}', 'Admin\RideInspectionListController@edit_ride_inspection_lists')->name('editRideInspectionLists');
    Route::post('/update_ride_inspection_lists/{ride_id}', 'Admin\RideInspectionListController@update_ride_inspection_lists')->name('updatRideInspectionLists');

    Route::resource('zone_inspection_lists', 'Admin\ZoneInspectionListController');
    Route::get('/add_zone_inspection_lists/{zone_id}', 'Admin\ZoneInspectionListController@add_zone_inspection_lists')->name('addZoneInspectionLists');
    Route::get('/cheack_zone_inspection_lists/', 'Admin\ZoneInspectionListController@cheackzoneInspectionList')->name('cheackZoneInspectionList');
    Route::get('/edit_zone_inspection_lists/{zone_id}', 'Admin\ZoneInspectionListController@edit_zone_inspection_lists')->name('editZoneInspectionLists');
    Route::post('/update_zone_inspection_lists/{zone_id}', 'Admin\ZoneInspectionListController@update_zone_inspection_lists')->name('updatZoneInspectionLists');

    Route::resource('complaints', 'Admin\CustomerComplaintController');//done
    Route::resource('customer_feedbacks', 'Admin\CustomerFeedbackController');//done
    Route::resource('feedback_sub_category', 'Admin\FeedbackSubCategoryController');
    Route::get('/search_customer_feedbacks/', 'Admin\CustomerFeedbackController@search')->name('searchCustomerFeedBack');
    Route::post('get-feedback-sub-categories', 'Admin\GeneralController@getFeedbackSubCategories')->name('getFeedbackSubCategories');

    Route::get('get-park-zones', 'Admin\GeneralController@getParkZones')->name('getParkZones');
    Route::post('get-sub-stoppages-categories', 'Admin\GeneralController@getSubStoppageCategories')->name('getSubStoppageCategories');
    Route::get('get_park_rides', 'Admin\GeneralController@getParkRides')->name('getParkRides');
    Route::get('get-zone-rides', 'Admin\GeneralController@getZonerides')->name('getZonerides');
    Route::get('getHasSkillgame', 'Admin\GeneralController@getHasSkillgame')->name('getHasSkillgame');
    Route::get('getSupervisorByPark', 'Admin\GeneralController@getSupervisorByPark')->name('getSupervisorByPark');
    Route::get('getOperatorByPark', 'Admin\GeneralController@getOperatorByPark')->name('getOperatorByPark');
    Route::get('get-training-checklists', 'Admin\GeneralController@getTrainingChecklists')->name('getTrainingChecklists');
    Route::get('get-users-by-type', 'Admin\GeneralController@getUsersByType')->name('getUsersByType');
    Route::get('get-list-type', 'Admin\GeneralController@getListType')->name('getListType');
    Route::get('get-checklist-type', 'Admin\GeneralController@getChecklistType')->name('getChecklistType');


    Route::resource('rsr_reports', 'Admin\RsrReportController');//done
    Route::get('rsr_reports/{id}/approve', 'Admin\RsrReportController@approve');
    Route::get('add_rsr_stoppage_report/{id}', 'Admin\RsrReportController@addRsrStoppageReport');
    Route::post('get-rsr-images', 'Admin\RsrReportController@getImage')->name('getRsrImage');
    Route::get('search', 'Admin\RsrReportController@search')->name('rsrReport.search');

    Route::resource('availability_reports', 'Admin\AvailabilityReportController');
    Route::get('add-availability-report/{park_id}', 'Admin\AvailabilityReportController@addAvailabilityReport')->name('addAvailabilityReport');
    Route::get('show-availability-report', 'Admin\AvailabilityReportController@showAvailabilityReport')->name('reports.showAvailabilityReport');
    Route::get('all-reports', 'Admin\AvailabilityReportController@all')->name('availability_reports.all');
    Route::get('availability_report/{id}/{date}/approve', 'Admin\AvailabilityReportController@approve')->name('approveAvailabilityReport');
    Route::get('addOLdAvailabilityReport', 'Admin\AvailabilityReportController@addOLdAvailabilityReport')->name('addOLdAvailabilityReport');

    Route::get('rides-status', 'Admin\ReportsController@rideStatus')->name('reports.rideStatus');
    Route::get('operator-time-report', 'Admin\ReportsController@operatorTimeReport')->name('reports.operatorTimeReport');
    Route::get('attendance-time-report', 'Admin\ReportsController@attendanceTimeReport')->name('reports.attendanceTimeReport');
    Route::get('show-operator-time-report', 'Admin\ReportsController@showOperatorTimeReport')->name('reports.showOperatorTimeReport');
    Route::get('stoppages-report', 'Admin\ReportsController@stoppagesReport')->name('reports.stoppagesReport');
    Route::get('cycles-report', 'Admin\ReportsController@cyclesReport')->name('reports.cyclesReport');
    Route::get('queues-report', 'Admin\ReportsController@queuesReport')->name('reports.queuesReport');
    Route::get('show-stoppages-report', 'Admin\ReportsController@showstoppagesReport')->name('reports.showStoppagesReport');

    Route::resource('incidents', 'Admin\IncidentController');//done
    Route::get('/add_incident_report/{ride_id}/{park_time_id}', 'Admin\IncidentController@add_incident_report')->name('addIncidentReport');
    //Route::resource('questions','Admin\QuestionController');

    Route::resource('accidents', 'Admin\AccidentController');//done
    Route::get('/add_accident_report/{ride_id}/{park_time_id}', 'Admin\AccidentController@add_accident_report')->name('addAccidentReport');

    Route::resource('health_and_safety_reports', 'Admin\HealthAndSafetyReportController');//done
    Route::get('/add_health_and_safety_report/{park_id}/{time_slot_id}', 'Admin\HealthAndSafetyReportController@add_health_and_safety_report')->name('addHealthAndSafetyReport');
    Route::get('/edit_health_and_safety_report/{time_slot_id}', 'Admin\HealthAndSafetyReportController@edit_health_and_safety_report')->name('editHealthAndSafetyReport');
    Route::post('/updateRequest/', 'Admin\HealthAndSafetyReportController@update_request')->name('updateRequest');
    Route::get('/search_health_and_safety/', 'Admin\HealthAndSafetyReportController@search')->name('searchHealthAndSafetyReport');
    Route::get('/cheack_health/', 'Admin\HealthAndSafetyReportController@cheackHealth')->name('cheackHealth');
    /* Route::post('/updateRequest', function (Request $request) {
         dd('aaaaaaaaa');
     })->name('updateRequest');*/
    Route::resource('skill_game_reports', 'Admin\SkillGameReportController');//done
    Route::get('/add_skill_game_report/{park_id}/{time_slot_id}', 'Admin\SkillGameReportController@add_skill_game_report')->name('addSkillGameReport');
    Route::get('/edit_skill_game_report/{time_slot_id}', 'Admin\SkillGameReportController@edit_skill_game_report')->name('editSkillGameReport');
    Route::post('/updateskillGame/', 'Admin\SkillGameReportController@update_request')->name('updateskillGame');
    Route::get('/search_skill_game_reports/', 'Admin\SkillGameReportController@search')->name('searchSkillGameReport');
    Route::get('/cheack_skill_game/', 'Admin\SkillGameReportController@cheackSkillGame')->name('cheackSkillGame');

    Route::resource('maintenance_reports', 'Admin\MaintenanceReportController');//done
    Route::get('/add_maintenance_report/{park_id}/{time_slot_id}', 'Admin\MaintenanceReportController@add_maintenace_report')->name('addMaintenanceReport');
    Route::get('/edit_maintenance_report/{time_slot_id}', 'Admin\MaintenanceReportController@edit_maintenance_report')->name('editMaintenanceReport');
    Route::post('/updateMaintenance/', 'Admin\MaintenanceReportController@update_request')->name('updateMaintenance');
    Route::get('/search_maintenance_reports/', 'Admin\MaintenanceReportController@search')->name('searchMaintenanceReport');
    Route::get('/cheack_maintenance/', 'Admin\MaintenanceReportController@cheackMaintenance')->name('cheackMaintenance');

    Route::resource('tech-reports', 'Admin\TechReportsController');//done
    Route::get('/add-tech-report/{park_id}/{time_slot_id}', 'Admin\TechReportsController@add_tech_report')->name('addTechReport');
    Route::get('/edit_tech_report/{time_slot_id}', 'Admin\TechReportsController@edit_tech_report')->name('editTechReport');
    Route::post('/updateTechnical/', 'Admin\TechReportsController@update_request')->name('updateTechnical');
    Route::get('/search_tech_reports/', 'Admin\TechReportsController@search')->name('searchTechReport');
    Route::get('/cheack_tech/', 'Admin\TechReportsController@cheackTech')->name('cheackTech');

    Route::resource('ride-ops-reports', 'Admin\RideOpsReportsController');//done
    Route::get('/add-ride-ops-report/{park_id}/{time_slot_id}', 'Admin\RideOpsReportsController@add_ride_ops_report')->name('addOpsReport');
    Route::get('/edit_ride_ops_report/{time_slot_id}', 'Admin\RideOpsReportsController@edit_ride_ops_report')->name('editOpsReport');
    Route::post('/updateRideOps/', 'Admin\RideOpsReportsController@update_request')->name('updateRideOps');
    Route::get('/search_ride_ops_reports/', 'Admin\RideOpsReportsController@search')->name('searchOpsReport');
    Route::get('/cheack_ride_ops/', 'Admin\RideOpsReportsController@cheackRideOps')->name('cheackRideOps');

    Route::get('/search_duty_summary_reports/', 'Admin\DutySummaryController@search')->name('searchDutySummaryReport');

    Route::resource('duty-report', 'Admin\RideOpsReportsController');
    Route::get('inspection-list-report', 'Admin\ReportsController@inspectionListReport')->name('reports.inspectionListReport');
    Route::get('show-inspection-list-report', 'Admin\ReportsController@showInspectionListReport')->name('reports.showInspectionListReport');
    Route::get('show-inspections/{id}', 'Admin\ReportsController@showInspections')->name('show-inspections');
    Route::get('show-inspection-details/{id}', 'Admin\ReportsController@showInspectionDetails')->name('show-inspection-details');

    Route::get('audit-report', 'Admin\ReportsController@auditReport')->name('reports.auditReport');
    Route::get('show-audit-report', 'Admin\ReportsController@showAuditReport')->name('reports.showAuditReport');


    Route::resource('ride_parks', 'Admin\RideParkController');
    Route::get('/add_ride_park/{ride_id}', 'Admin\RideParkController@addRidePark')->name('addRidePark');
    Route::get('/add_ride_country/{ride_id}', 'Admin\RideParkController@addRideCountry')->name('addRideCountry');
    Route::get('/add_ride_country/{ride_id}', 'Admin\RideParkController@addRideCountry')->name('addRideCountry');
    Route::post('/update_ride_country', 'Admin\RideParkController@updateRideCountry')->name('updateRideCountry');
    Route::resource('ride_zones', 'Admin\RideZoneController');
    Route::get('/add_ride_zone/{ride_id}', 'Admin\RideZoneController@addRideZone')->name('addRideZone');

    Route::resource('user_parks', 'Admin\UserParkController');
    Route::get('/add_user_park/{user_id}', 'Admin\UserParkController@addUserPark')->name('addUserPark');
    Route::get('/user_roles/{user_id}', 'Admin\UserController@userRoles')->name('userRoles');
    Route::get('/get_zone', 'Admin\UserController@getZones')->name('getZone');
    Route::get('/get_parks_by_branch', 'Admin\UserController@getPark')->name('getParkBranch');

    Route::get('/get_riders', 'Admin\UserController@getRiders')->name('getRiders');
    Route::post('/role_update/{id}', 'Admin\UserController@rolesUpdate')->name('rolesUpdate');
    Route::resource('user_zones', 'Admin\UserZoneController');
    Route::get('/add_user_zone/{user_id}', 'Admin\UserZoneController@addUserZone')->name('addUserZone');

    Route::resource('ride_users', 'Admin\RideUserController');
    Route::get('/add_ride_user/{ride_id}', 'Admin\RideUserController@addRideUser')->name('addRideUser');

    Route::resource('ride_preopen', 'Admin\RidePreopeningController');
    Route::get('/add_ride_preopen_elements/{ride_id}', 'Admin\RidePreopeningController@add_ride_preopen_elements');
    Route::get('/edit_ride_preopen_elements/{ride_id}', 'Admin\RidePreopeningController@edit_ride_preopen_elements');
    Route::post('/update_ride_preopen_elements/{ride_id}', 'Admin\RidePreopeningController@update_ride_preopen_elements')->name('updatRidePreopenElements');

    Route::resource('ride_preclose', 'Admin\RidePreclosingController');
    Route::get('/add_ride_preclose_elements/{ride_id}', 'Admin\RidePreclosingController@add_ride_preclose_elements');
    Route::get('/edit_ride_preclose_elements/{ride_id}', 'Admin\RidePreclosingController@edit_ride_preclose_elements');
    Route::post('/update_ride_preclose_elements/{ride_id}', 'Admin\RidePreclosingController@update_ride_preclose_elements')->name('updatRidePrecloseElements');

    // zone inspection
    Route::resource('zone_preopen', 'Admin\ZonePreopeningController');
    Route::get('/add_zone_preopen_elements/{ride_id}', 'Admin\ZonePreopeningController@add_zone_preopen_elements');
    Route::get('/edit_zone_preopen_elements/{ride_id}', 'Admin\ZonePreopeningController@edit_zone_preopen_elements');
    Route::post('/update_zone_preopen_elements/{ride_id}', 'Admin\ZonePreopeningController@update_zone_preopen_elements')->name('updatZonePreopenElements');

    Route::resource('zone_preclose', 'Admin\ZonePreclosingController');
    Route::get('/add_zone_preclose_elements/{zone_id}', 'Admin\ZonePreclosingController@add_zone_preclose_elements');
    Route::get('/edit_zone_preclose_elements/{zone_id}', 'Admin\ZonePreclosingController@edit_zone_preclose_elements');
    Route::post('/update_zone_preclose_elements/{zone_id}', 'Admin\ZonePreclosingController@update_zone_preclose_elements')->name('updatZonePrecloseElements');

    Route::resource('zone_weekly', 'Admin\ZoneWeeklyController');
    Route::get('/add_zone_weekly_elements/{zone_id}', 'Admin\ZoneWeeklyController@add_zone_weekly_elements');
    Route::get('/edit_zone_weekly_elements/{zone_id}', 'Admin\ZoneWeeklyController@edit_zone_weekly_elements');
    Route::post('/update_zone_weekly_elements/{zone_id}', 'Admin\ZoneWeeklyController@update_zone_weekly_elements')->name('updatZoneWeeklyElements');

    Route::resource('zone_daily', 'Admin\ZoneDailyController');
    Route::get('/add_zone_daily_elements/{zone_id}', 'Admin\ZoneDailyController@add_zone_daily_elements');
    Route::get('/edit_zone_daily_elements/{zone_id}', 'Admin\ZoneDailyController@edit_zone_daily_elements');
    Route::post('/update_zone_daily_elements/{zone_id}', 'Admin\ZoneDailyController@update_zone_daily_elements')->name('updatZoneDailyElements');

    Route::get('show_zone_preclose_list/{zone_id}/{date}', 'Admin\ZonePreclosingController@show_zone_preclose_list')->name('show_zone_preclose_list');
    Route::get('show_zone_preopen_list/{zone_id}/{date}', 'Admin\ZonePreopeningController@show_zone_preopen_list')->name('show_zone_preopen_list');
    Route::get('show_zone_weekly_list/{zone_id}/{date}', 'Admin\ZoneWeeklyController@show_zone_weekly_list')->name('show_zone_weekly_list');
    Route::get('show_zone_daily_list/{zone_id}/{date}', 'Admin\ZoneDailyController@show_zone_daily_list')->name('show_zone_daily_list');

    Route::get('preclosApprove/{id}/{date}', 'Admin\ZonePreclosingController@approve')->name('preclosApprove');
    Route::get('preopenApprove/{id}/{date}', 'Admin\ZonePreopeningController@approve')->name('preopenApprove');
    Route::get('weeklyApprove/{id}/{date}', 'Admin\ZoneWeeklyController@approve')->name('weeklyApprove');
    Route::get('dailyApprove/{id}/', 'Admin\ZoneDailyController@approve')->name('dailyApprove');

    Route::resource('observations', 'Admin\ObservationController');
    Route::get('/search_observation/', 'Admin\ObservationController@search');
    Route::get('/add_observation/{ride_id}', 'Admin\ObservationController@add_observation')->name('add_observation');

    Route::get('observation-report', 'Admin\ReportsController@observationReport')->name('reports.observationReport');
    Route::get('show_observation-report', 'Admin\ReportsController@showObservationReport')->name('reports.showObservationReport');


    Route::get('/auto-refresh-page', 'Admin\AutoRefreshController')->name('auto-refresh-page');
    Route::resource('incident', 'Admin\GeneralIncidentController');
    Route::get('incident/{id}/approve', 'Admin\GeneralIncidentController@approve')->name('incident-approve-manager');
    Route::get('incident/{id}/approve_director', 'Admin\GeneralIncidentController@approve_director')->name('incident-approve-director');
    Route::get('search_incident', 'Admin\GeneralIncidentController@search');
    Route::Post('get-incident-images', 'Admin\GeneralIncidentController@getImage')->name('getIncidentImage');


    Route::resource('investigation', 'Admin\InvestigationController');
    Route::get('investigation/{id}/approve', 'Admin\InvestigationController@approve')->name('investigation-approve-manager');
    Route::get('investigation/{id}/approve_director', 'Admin\InvestigationController@approve_director')->name('investigation-approve-director');
    Route::get('/search_investigation/', 'Admin\InvestigationController@search');

    Route::resource('evacuations', 'Admin\EvacuationsController');
    Route::get('/search_evacuation/', 'Admin\EvacuationsController@search');
    Route::get('/evacuation_stoppages', 'Admin\EvacuationsController@evacuationStoppages')->name('evacuationStoppages');
    Route::get('/add_ride_evacuation/{id}', 'Admin\EvacuationsController@addEvacuationRide')->name('addEvacuationRide');
    Route::get('/add_park_evacuation/{id}', 'Admin\EvacuationsController@addEvacuationPark')->name('addEvacuationPark');
    Route::get('evacuation_report', 'Admin\EvacuationsController@reportEvacuation')->name('evacuation.report');

    Route::get('evaluation_operator_report', 'Admin\ReportsController@evaluationOperator')->name('evaluation.operator.report');
    Route::get('evaluation_supervisor_report', 'Admin\ReportsController@evaluationSupervisor')->name('evaluation.supervisor.report');

    Route::get('/get_zones', 'Admin\GeneralIncidentController@getZones')->name('getZones');
    Route::get('/get_rides', 'Admin\GeneralIncidentController@getRides')->name('getRides');

    Route::resource('statement', 'Admin\StatementController');
    Route::get('statement/{id}/approve', 'Admin\StatementController@approve');
    Route::get('add_statment/{id}', 'Admin\StatementController@addStatment')->name('addStatment');
    Route::get('show_statment/{id}', 'Admin\StatementController@showStatment')->name('showStatment');

    Route::get('incident-report', 'Admin\ReportsController@incidentReport')->name('reports.incidentReport');
    Route::get('show-incident-report', 'Admin\ReportsController@showIncidentReport')->name('reports.showIncidentReport');

    Route::get('ride_capacity', 'Admin\RideCapacityController@index')->name('rideCapacity');
    Route::post('ride_capacity_update', 'Admin\RideCapacityController@update')->name('rideCapacity.update');
    Route::post('change_capacity', 'Admin\RideCapacityController@changeCapacity')->name('changeCapacity');

    Route::get('ride_capacity_report', 'Admin\RideCapacityController@report')->name('rideCapacity.report');

    Route::resource('preopen_list', 'Admin\PreOpeneController');
    Route::get('show_preopen_list/{ride_id}/{park_time_id}', 'Admin\PreOpeneController@show_preopen_list')->name('show_preopen_list');

    Route::resource('preopen_list', 'Admin\PreCloseController');
    Route::get('show_preclose_list/{ride_id}/{park_time_id}', 'Admin\PreCloseController@show_preclose_list')->name('show_preclose_list');

    Route::resource('units', 'Admin\UnitController');
    Route::resource('units_properties', 'Admin\UnitPropertyController');

    Route::get('add_move_in/{id}', 'Admin\UnitPropertyController@addMovein')->name('addMovein');
    Route::get('add_intial_check/{id}', 'Admin\UnitPropertyController@addIntialCheck')->name('addIntialCheck');
    Route::get('add_move_out/{id}', 'Admin\UnitPropertyController@addMoveout')->name('addMoveout');

    Route::get('export-stoppage/{items}', 'Admin\ReportsController@exportStoppage')->name('export.stoppage');
    Route::get('export-cycles/{items}', 'Admin\ReportsController@exportCycles')->name('export.cycles');
    Route::get('export-queues/{items}', 'Admin\ReportsController@exportQueues')->name('export.queues');
    Route::get('inspecttio-list/{items}', 'Admin\ReportsController@exportInspection')->name('export.inspection');
    Route::get('audit/{items}', 'Admin\ReportsController@exportAudit')->name('export.audit');
    Route::get('operator/{items}', 'Admin\ReportsController@exportOperator')->name('export.operator');
    Route::get('attendance/{items}', 'Admin\ReportsController@exportAttendance')->name('export.attendance');
    Route::get('availability-report/{items}', 'Admin\AvailabilityReportController@export')->name('export.availability');
    Route::get('observation/report/{items}', 'Admin\ReportsController@exportObservation')->name('export.observation');
    Route::get('Health/report/{items}', 'Admin\ReportsController@exportHealth')->name('export.health');
    Route::get('capacity/report/{items}', 'Admin\ReportsController@exportCapacity')->name('export.capacity');
    Route::get('evacuation/report/{items}', 'Admin\ReportsController@exportEvacuation')->name('export.evacuation');
    Route::get('evaluation/operator/report/{items}', 'Admin\ReportsController@exportEvaluationOperator')->name('export.evaluation.operator');
    Route::get('evaluation/supervisor/report/{items}', 'Admin\ReportsController@exportEvaluationSupervisor')->name('export.evaluation.supervisor');

    Route::get('clear-flash-message', 'GeneralController@clearMessage')->name('clear.flash.message');

    Route::resource('daily-check', 'Admin\DailyCheckListQuestionController');
    Route::get('add_am_check', 'Admin\DailyCheckListQuestionController@addAmCheck')->name('addAmCheck');
    Route::get('edit_am_list', 'Admin\DailyCheckListQuestionController@editaAmList')->name('editAmList');
    Route::get('show_am_list/{id}', 'Admin\DailyCheckListQuestionController@showAmList')->name('showAmList');
    Route::get('am-search', 'Admin\DailyCheckListQuestionController@amSearch')->name('amSearch');

    Route::get('index_pm', 'Admin\DailyCheckListQuestionController@indexPm')->name('indexPm');
    Route::get('add_pm_check', 'Admin\DailyCheckListQuestionController@addPmCheck')->name('addPmCheck');
    Route::get('edit_pm_list', 'Admin\DailyCheckListQuestionController@editPmList')->name('editPmList');
    Route::get('show_pm_list/{id}', 'Admin\DailyCheckListQuestionController@showPmList')->name('showPmList');
    Route::get('pm-search', 'Admin\DailyCheckListQuestionController@pmSearch')->name('pmSearch');
    Route::Post('get-daily-images', 'Admin\DailyCheckListQuestionController@getImage')->name('getDailyImage');


    Route::resource('operator-evaluation', 'Admin\OperatorEvaluationController');
    Route::resource('supervisor-evaluation', 'Admin\SupervisorEvaluationController');
    Route::get('evaluations/{id}/approve', 'Admin\OperatorEvaluationController@approve');
    Route::get('search_evaluation/', 'Admin\SupervisorEvaluationController@search');
    Route::get('search_operator_evaluation/', 'Admin\OperatorEvaluationController@search');

    Route::resource('country', 'Admin\CountryController');

    Route::resource('training', 'Admin\TrainingController');
    Route::get('training_search/', 'Admin\TrainingController@search');
    Route::get('show-users-lists/{id}', 'Admin\TrainingController@showUsersLists')->name('showUsersLists');
    Route::get('show-user-checklist/{user_id}/{training_id}', 'Admin\TrainingController@showUserChecklist')->name('showUserChecklist');

    Route::get('show-user-actions/', 'Admin\GeneralController@showActions')->name('showActions');
    Route::get('actions_search/', 'Admin\GeneralController@actionSearch')->name('actionSearch');



});
