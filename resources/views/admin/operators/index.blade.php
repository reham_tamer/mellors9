@extends('admin.layout.app')

@section('title')
    All Operators & Breakers
@endsection
@section('content')

    <div class="card-box">

        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <table id="users-datatable" class="table table-striped table-bordered dt-responsive nowrap users-datatable" >
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Name
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Code
                            </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Park
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                ride
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Role
                        </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Status
                        </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Add Time
                            </th>

                        </tr>
                        </thead>


                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')

    @include('admin.datatable.scripts')

    <script type="text/javascript">
        $(function () {

            var table = $('#users-datatable').DataTable({
                processing: false,
                serverSide: false,
                ajax: "{{ route('admin.operators.index') }}",
                columns: [
                        {data: 'id', name: 'id'},
                        {data: 'name', name: 'name'},
                        {data: 'code', name: 'code'},
                        {data: 'park', name: 'park'},
                        {data: 'ride', name: 'ride'},
                        {data: 'role', name: 'role'},
                        {data: 'status', name: 'status'},
                        {data: 'add_time', name: 'add_time', orderable: false, searchable: false},
                    ],
            });

        });
    </script>
@endsection
