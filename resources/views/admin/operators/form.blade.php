<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="name">Name</label>
            {!! Form::text('name',null,['class'=>'form-control','id'=>'name','placeholder'=>'name']) !!}
            @error('name')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="first_name">Code</label>
            {!! Form::text('code',null,['class'=>'form-control','id'=>'code','placeholder'=>'code']) !!}
            @error('code')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="name">Last Name</label>
            {!! Form::text('last_name',null,['class'=>'form-control','id'=>'last_name','placeholder'=>'last name']) !!}
            @error('name')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label>Park :</label>
            {!! Form::select('park_id', $parks,@$userPark?$userPark:null, array('class' => 'form-control select2')) !!}
            @error('park_id')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>


    <div class="col-xs-12">
        <div class="form-group">
            <button type="submit" class="btn waves-effect waves-light btn-primary">Save</button>
        </div>
    </div>
</div>


@push('scripts')



@endpush
