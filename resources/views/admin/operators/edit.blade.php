@extends('admin.layout.app')

@section('title')
    Edit Attendance -
    {{ $user->name }}
@stop
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Edit Attendance - {{ $user->name }} </h4>
                {!!Form::model($user , ['route' => ['admin.attendances.update' , $user->id] , 'method' => 'PATCH','enctype'=>"multipart/form-data",'class'=>'form-horizontal','files' => true,'id'=>'form']) !!}
                @include('admin.attendances.form')
                {!!Form::close() !!}
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->
@endsection

@push('scripts')
    {!! JsValidator::formRequest(\App\Http\Requests\Dashboard\Attendance\AttendanceRequest::class, '#form'); !!}
@endpush
