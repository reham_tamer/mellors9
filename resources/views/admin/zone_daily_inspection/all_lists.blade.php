@extends('admin.layout.app')

@section('title')
Hourly Check Lists
@endsection

@section('content')
    <div class="card-box">
       
        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <div class='responsive-wrapper'>
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1" aria-sort="ascending">ID
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    List
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Created At
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Process
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Status
                                </th>
                            </tr>
                        </thead>

                        <tbody>

                            @foreach ($items as $item)
                                <tr role="row" class="odd" id="row-{{ $item->id }}">
                                    <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                    <td>Hourly Check Lists {{ $loop->iteration }}</td>
                                    <td>{{ $item->created_at }}</td>
                                    <td>            
                                        <a href="{{ route('admin.zone_daily.show', $item->id) }}" class="btn btn-success">
                                             <i class="fa fa-info"></i>  Show </a>
                                          
                                    </td>
                                    <td>
                                        @if($item->status=='pending')
                                        <a href="{{url('dailyApprove/'.$item->id )}}"
                                        class="btn btn-xs btn-danger"><i class="fa fa-check"></i> Approve</a>
                                        @endif
                                        @if($item->status=='approved')
                                        <span class="btn btn-xs btn-info">Approved</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>


                </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('footer')
    @include('admin.datatable.scripts')
@endsection
