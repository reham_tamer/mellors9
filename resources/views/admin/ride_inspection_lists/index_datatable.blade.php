@extends('admin.layout.app')

@section('title')
Rides Inspection List
@endsection

@section('content')

<div class="card-box">
<h3>Add Elements To Rides Inspection lists </h3>
<br>
    <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer dt">
        <div class="row">
            <div class="col-sm-12">
                <table id="datatable-buttons-inspection" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Ride
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Park
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Inspection List
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Preopening Check List
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Preclosing Check List
                            </th>
                        </tr>
                    </thead>


                </table>


            </div>
        </div>

    </div>
</div>


@endsection

@section('footer')
@include('admin.datatable.scripts')
<script type="text/javascript">
    $(function () {

        var table = $('#datatable-buttons-inspection').DataTable({
            processing: false,
            serverSide: false,
            ajax: "{{ route('admin.ride_inspection_lists.index') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                { data: 'park_name', name: 'park_name' },
                {data: 'inspection_lists', name: 'inspection_lists'},
                {data: 'preopening_lists', name: 'preopening_lists'},
                {data: 'preclosing_lists', name: 'preclosing_lists'},

            ]
        });

    });
</script>
@endsection
