@extends('admin.layout.app')

@section('title')
    Observations
@endsection

@section('content')

    <div class="card-box">
        <form class="formSection" action="{{url('/search_observation')}}" method="GET">

            @csrf
            <div class="row">
                <div class='col-md-8'>
                    <div class="form-group">
                        <label for="middle_name">Time Slot Date </label>
                        {!! Form::date('date',\Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date_to']) !!}
                    </div>
                </div>
                <div class='col-md-2 mtButton'>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary save_btn waves-effect">Show</button>
                    </div>
                </div>
            </div>
        </form>
        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <table id="datatable-buttons-observation"
                           class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Ride
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Date Reported
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Snag
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Department
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Category
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Created by
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Feedback
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Reported on tech sheet
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Rf number
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Picture
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Process
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Operation
                            </th>

                        </tr>
                        </thead>

                        <tbody>


                        </tbody>
                    </table>


                </div>
            </div>

        </div>
    </div>

@endsection
@section('footer')
    @include('admin.datatable.scripts')
    <script type="text/javascript">
        $(function () {

            var table = $('#datatable-buttons-observation').DataTable({
                processing: false,
                serverSide: false,
                ajax: "{{ route('admin.observations.index') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'ride', name: 'ride'},
                    {data: 'date_reported', name: 'date_reported'},
                    {data: 'snag', name: 'snag'},
                    {data: 'department', name: 'department', orderable: false, searchable: false},
                    {data: 'observationCategory', name: 'observationCategory'},
                    {data: 'created_by', name: 'created_by'},
                    {data: 'maintenance_feedback', name: 'maintenance_feedback'},
                    {data: 'reported_on_tech_sheet', name: 'reported_on_tech_sheet'},
                    {data: 'rf_number', name: 'rf_number'},
                    {data: 'image', name: 'image'},
                    {data: 'solve', name: 'solve', orderable: false, searchable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
@endsection



