@extends('admin.layout.app')

@section('title')
    Observations
@endsection

@section('content')

    <div class="card-box">
        <form class="formSection" action="{{url('/search_observation')}}" method="GET">

            @csrf
            <div class="row">
                <div class='col-md-8'>
                    <div class="form-group">
                        <label for="middle_name">Time Slot Date </label>
                        {!! Form::date('date',\Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date_to']) !!}
                    </div>
                </div>
                <div class='col-md-2 mtButton'>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary save_btn waves-effect">Show</button>
                    </div>
                </div>
            </div>
        </form>
        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Ride
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Date Reported
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Snag
                            </th>
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Department
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Category
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Created by
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Feedback
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Reported on tech sheet
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Rf number
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Picture
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Solve
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Delete
                            </th>

                        </tr>
                        </thead>

                        <tbody>

                        @foreach ($items as $item)
                            <tr role="row" class="odd" id="row-{{ $item->id }}">
                                <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                <td>{{ $item->ride?->name }}</td>
                                <td>{{ $item->date_reported }}</td>
                                <td>{{ $item->snag }}</td>
                                <td>{{ $item->department?->name }}</td>
                                <td>{{ $item->observationCategory?->name }}</td>
                                <td>{{ $item->created_by?->name }}</td>
                                <td>{{ $item->maintenance_feedback }}</td>
                                <td>{{ $item->reported_on_tech_sheet }}</td>
                                <td>{{ $item->rf_number }}</td>
                                <td>
                                    @if ($item->image != null)
                                        <a>
                                            <img class="img-preview"
                                                 src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($item->image),now()->addMinutes(30)) }}"
                                                 style="height: 50px; width: 50px">
                                        </a>
                                    @endif

                                </td>

                                <td>
                                    @if(auth()->user()->can('observations-edit'))
                                        @if($item->date_resolved === null)
                                            <a href="{{ route('admin.observations.edit', $item->id) }}"
                                               class="btn btn-info">Resolve</a>
                                        @else
                                            <span style="background-color: rgb(110, 237, 110)">Solved</span>
                                        @endif
                                    @endif
                                </td>
                                {!!Form::open( ['route' => ['admin.observations.destroy',$item->id] ,'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                                {!!Form::close() !!}
                                <td>
                                    @if(auth()->user()->can('observations-edit'))
                                        @if($item->date_resolved != null)
                                            <a href="{{ route('admin.observations.edit', $item->id) }}"
                                               class="btn btn-info">Edit</a>
                                        @endif
                                        <a class="btn btn-danger"
                                           data-name="observation for {{ $item->ride?->name }}"
                                           data-url="{{ route('admin.observations.destroy', $item) }}"
                                           onclick="delete_form(this)">
                                            Delete
                                        </a>
                                    @endif

                                </td>

                            </tr>

                        @endforeach

                        </tbody>
                    </table>


                </div>
            </div>

        </div>
    </div>

@endsection
@section('footer')
    @include('admin.datatable.scripts')
@endsection



