@extends('admin.layout.app')

@section('title')
    Statistics
@stop

@section('content')

    <div class="row home-flex">

        <div class="col col-xs-6 col-md-4 col-lg-3">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Departments Number</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb"
                               value=" {{ App\Models\Department::count() }}" data-skin="tron" data-angleOffset="180"
                               data-readOnly=true data-thickness=".15" style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{ App\Models\Department::count() }} </h2>
                        <p class="text-muted">Department</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col col-xs-6 col-md-4 col-lg-3">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-0">Number of Users</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value="{{ App\Models\User::count() }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{ App\Models\User::count() }} </h2>
                        <p class="text-muted">User</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->


        <div class="col col-xs-6 col-md-4 col-lg-3">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Branches Number</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ App\Models\Branch::count() }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{ App\Models\Branch::count() }} </h2>
                        <p class="text-muted">Branch</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->
        <div class="col col-xs-6 col-md-4 col-lg-3">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Park Number</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ App\Models\Park::count() }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{ App\Models\Park::count() }} </h2>
                        <p class="text-muted">Park</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->
        <div class="col col-xs-6 col-md-4 col-lg-3">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Zones Number</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ App\Models\Zone::count() }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{ App\Models\Zone::count() }} </h2>
                        <p class="text-muted">Zone</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->
        <div class="col col-xs-6 col-md-4 col-lg-3">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Rides Number</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ App\Models\Ride::count() }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{ App\Models\Ride::count() }} </h2>
                        <p class="text-muted">Ride</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->


        <div class="col col-xs-6 col-md-4 col-lg-3">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-0">Number of Ride Types</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value="{{ App\Models\RideType::count() }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{ App\Models\RideType::count() }} </h2>
                        <p class="text-muted">Ride Types</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col col-xs-6 col-md-4 col-lg-3">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-0">Number of Stoppages Categories</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value="{{ App\Models\StopageCategory::count() }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{ App\Models\StopageCategory::count() }} </h2>
                        <p class="text-muted">StopageCategory</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col col-xs-6 col-md-4 col-lg-3">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-0">Number of Stoppages Sub Categories</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value="{{ App\Models\StopageSubCategory::count() }}"
                               data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0"> {{ App\Models\StopageSubCategory::count() }} </h2>
                        <p class="text-muted">StopageSubCategory</p>
                    </div>
                </div>
            </div>
        </div><!-- end col -->


    </div>
    <h3>Reports with time slot</h3>

    @forelse ($park_times as $time)

        <h3 class="text-center">{{ $time->parks?->name }}</h3>
        {{--        <br>--}}
        <a href="{{route('admin.statisticPark',$time->park_id)}}"><h4 class="text-center"><i class="fa fa-search"></i>Filter Days</h4></a>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Rides Count</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = App\Models\Ride::where('park_id', $time->park_id)->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">Rides Count</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3  ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Stoppages</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb"
                               value=" {{ $time->rideStoppages?->count() }}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $time->rideStoppages?->count() }}
                        </h2>
                        <p class="text-muted"> Stoppages</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3  ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Cycles</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb"
                               value=" {{ $time->cycles?->count() }}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $time->cycles?->count()}}
                        </h2>
                        <p class="text-muted"> Cycles</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3  ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Queues</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb"
                               value=" {{ $time->queues?->count() }}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $time->queues?->count()}}
                        </h2>
                        <p class="text-muted"> Queues</p>
                    </div>
                </div>

            </div>
        </div>


        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">RSR Report</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = App\Models\RsrReport::where('park_id', $time->park_id)
                                ?->where('date', Illuminate\Support\Carbon::now()->toDateString())
                                ->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">RSR Report</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Availabilty Report</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = App\Models\GameTime::where('park_id', $time->park_id)
                                ?->where('date', Illuminate\Support\Carbon::now()->toDateString())
                                ->first() ?'1':0;
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback?'1':0 }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">Availabilty Report</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Preopening List</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = $time->preopeningLists?->where('type','preopening')->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">Preopening List</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Preclosing List</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = $time->preopeningLists?->where('type','preclosing')->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">Preclosing List</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Inspection List</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback2 =$time->preopeningLists?->where('type','inspection_list')->count();

                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback2 }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback2 }}
                        </h2>
                        <p class="text-muted">Inspection List</p>
                    </div>
                </div>

            </div>
        </div>


        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Positive Customer Feedback </h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback3 = App\Models\CustomerFeedbacks::where('park_id', $time->park_id)
                            ->where(function ($query) use ($time) {
                                $query->where('date', $time->date);
                                })->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback3 }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback3 }}
                        </h2>
                        <p class="text-muted">Positive Customer Feedback</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Negative Customer Feedback </h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = App\Models\CustomerFeedbacks::where('park_id', $time->park_id)
                                ?->where('type','Complaint')->where(function ($query) use ($time) {
                                $query->where('date', $time->date);
                                })->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">Negative Customer Feedback</p>
                    </div>
                </div>

            </div>
        </div>

        {{-- <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Observation</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $Observation = App\Models\Observation::where('park_id', $time->park_id)
                                ?->where('date_reported', Illuminate\Support\Carbon::now()->toDateString())
                                ->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                            data-bgColor="#fff6eb" value=" {{ $Observation }}" data-skin="tron"
                            data-angleOffset="180" data-readOnly=true data-thickness=".15"
                            style="margin-left: -62px;" />
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $Observation }}
                        </h2>
                        <p class="text-muted">Observations</p>
                    </div>
                </div>

            </div>
        </div> --}}
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Investigation</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $Investigation = App\Models\GeneralIncident::where('type', 'investigation')
                                ->where('park_id', $time->park_id)
                                ?->where(function ($query) use ($time) {
                                $query->where('date', $time->date);
                                })
                                ->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $Investigation }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $Investigation }}
                        </h2>
                        <p class="text-muted">Investigation</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Incident</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $Incident = App\Models\GeneralIncident::where('type', 'incident')
                                ->where('park_id', $time->park_id)
                                ?->where(function ($query) use ($time) {
                                $query->where('date', $time->date);
                                })
                                ->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $Incident }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $Incident }}
                        </h2>
                        <p class="text-muted">Incident</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Witness Statement</h4>
                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $Incident = App\Models\IncidentStatement::where('park_id', $time->park_id)
                                ?->where(function ($query) use ($time) {
                                $query->where('date', $time->date);
                                })
                                ->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $Incident }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $Incident }}
                        </h2>
                        <p class="text-muted">Witness Statement</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Attraction Audit Check Report Approved</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = App\Models\Attraction::where('park_id', $time->park_id)
                                ?->where('approve',1)->where('date', $time->date)
                                ->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">Attraction Audit Check Report Approved</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Attraction Audit Check Report Pennding</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = App\Models\Attraction::where('park_id', $time->park_id)
                                ?->where('approve',0)->where('date', $time->date)
                                ->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">Attraction Audit Check Report Pennding</p>
                    </div>
                </div>

            </div>
        </div>
    @empty
        <div>not found yet</div>
    @endforelse

@endsection
