@extends('admin.layout.app')

@section('title', 'Main Page')
@section('content')
    <div id="riders">
        <div class="row mt-4">
            <div class="col-lg-12 col-xs-12">
                <div class='contentRide'>
                    <div class="row ">

{{--                        <h4 class="contentRide_title">Total Avg: {{$cycleRecords->sum('avg')}}</h4>--}}
                        @if($flattenedLastRecords->isNotEmpty())
                            <table class="table table-bordered table-striped mt-4">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    @foreach($times as $time)
                                        @php
                                            $date = new DateTime($time->date);
                                            $date2 = new DateTime($time->date);

                                        @endphp
                                        <th scope="col">({{ $date->modify('-1 hour')->format('g a') }} - {{ $date2->format('g a') }} ) </th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($flattenedLastRecords as $key => $ride)

                                    <tr>
                                        <!-- Ride Name and Theoretical Number -->
                                        <th scope="row">
                                            {{ $ride[0]->ride?->name }}
                                            ({{ $ride[0]->ride?->theoretical_number ?? 'N/A' }})
                                        </th>
                                        <!-- Rider Counts for Each Time -->
                                        @foreach($times as $time)

                                            @php
                                                // Find the matching record for this ride and time
                                                $matchingRecord = $ride->firstWhere(function ($record) use ($key, $time) {
                                                    return $record->ride_id == $key && $record->date == $time->date;
                                                });
                                            @endphp
                                            <td>{{ $matchingRecord->avg ?? 0 }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p>No data available for the selected records.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            const timeId = "{{ $id }}";
            let url = "{{ route('admin.singlePage-cycles', ':id') }}".replace(':id', timeId);

            function autoRefresh() {
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (response) {
                        $('#riders').html(response);
                        console.log('sucess');
                    },
                    error: function (xhr, status, error) {
                        console.error('Auto-refresh failed:', error);
                    }
                });
            }

            setTimeout(function () {
                setInterval(autoRefresh, 40000); // Runs every 40 seconds
            }, 1800000); // Delay for haf hour (3600000 ms)
        });
    </script>
@endpush
