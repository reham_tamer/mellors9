@extends('admin.layout.app')

@section('title')
    Main Page
@stop

@section('content')
    <!-- <div id="auto-refresh-content">              -->
    <div id="times-content">
        @foreach ($times as $time)
            @if($parks->contains($time->park_id))
                <div class="row">
                    <div class="col-lg-12 col-xs-12">
                        <div class='contentRide'>
                            <a href="{{ url('/all-rides/' . $time->park_id . '/' . $time->id) }}">
                                <div class="contentRide_header card-box " style="  background-color: #f5d2a9;">
                                    <h3 class="contentRide_title" class="bold">{{ $time->parks->name }}</h3>
                                    @php

                                        $rides_times = \App\Models\GameTime::where('park_time_id', $time->id)->get();
                                          $rides_duration = 0;

                                          foreach ($rides_times as $ride) {
                                              $start_date = $ride['date'];
                                              $end_date = $ride['close_date'];
                                              $to_time = $ride['start'];
                                              $from_time = $ride['end'];
                                              $start_timestamp = strtotime("$start_date $to_time");
                                              $end_timestamp = strtotime("$end_date $from_time");
                                              $duration_time = round(abs($start_timestamp - $end_timestamp) / 60, 2);
                                              $rides_duration += $duration_time;
                                          }
                                          $total_stoppages = \App\Models\RideStoppages::where('park_time_id', $time->id)->sum('down_minutes');
                                          $a = $rides_duration != 0 ? $total_stoppages / $rides_duration : 0;
                                          $b = is_numeric($a) ? 1 - $a : 1;
                                          $time_slot_availabilty = is_numeric($b) ? $b * 100 : 0;
                                    @endphp

                                    <h3>TIME SLOT AVAILABILITY : {{number_format($time_slot_availabilty, 2, '.') }}</h3>
                                    <div class="contentRide_mins">
                                        <h3 class="contentRide_title">Park Hours
                                            : {{ $time->duration_time ?? 0 . ' minute' }} </h3>
                                        <p> {{ $time->date }} : ( {{ $time->start }} - {{ $time->end }} )</p>
                                    </div>
                                </div>
                            </a>


                            <div class="home-flex card-box" id="rides">
                                @foreach ($rides as $ride)
                                    @if ($ride->park_id === $time->parks->id)
                                        @php $rideTimee = \App\Models\Ride::find($ride->id);
                                            $rideTime = $rideTimee->rideStoppages?->where('park_time_id',$time->id);
                                             $capacity = \App\Models\RideCapacity::where('ride_id', $ride->id)->where('date',$time->date)->first();
                                             $seatAvailability = $rideTimee->number_of_seats > 0 ? round($capacity?->final_capacity / $rideTimee->number_of_seats,1) * 100 . '%' : 0 . '%';
/*                                              $preclosing = $rideTimee->preopening_lists?->where('type', 'preclosing')?->where('park_time_id',$time->id)->first();
 */                                             $preopening = $rideTimee->preopening_lists?->where('type', 'preopening')?->where('park_time_id',$time->id)->first();
                                        @endphp

                                        @if ($rideTime->isNotEmpty()  && $rideTime?->where('stopage_category_id',5)->first() != null && $ride->available == 'active')
                                            <div class="playBox yellow cardGame " id="rideQueue{{ $ride->id }}">
                                                <!-- Start Tooltip -->
                                                <div class="tooltip-outer">
                                                    <div class="tooltip-icon" id="tooltip{{ $ride->id }}"
                                                         data-toggle="tooltip"
                                                         title="{{ $ride?->stoppageSubCategoryName }}!"><i
                                                            class="fa fa-info-circle">
                                                        </i></div>
                                                </div>
                                                <!-- !!End Tooltip -->

                                                <a href="{{ url('/show_stoppages/' . $ride->id . '/' . $time->id) }}">

                                                    <div class="card-box" id="rideStatus{{ $ride->id }}">

                                                        <h4 class="header-title m-t-0 m-b-0">{{ $ride->name }}
                                                          {{--   <i
                                                                class="fa fa-edit"   @if($preclosing) title="preclosing added" style="color:green" @else title="preclosing not added yet" style="color:red" @endif >
                                                            </i> --}}
                                                        </h4>
                                                        <h6 class="m-b-0">SA :{{$seatAvailability}}</h6>

                                                    </div>
                                                </a>

                                            </div>
                                        @elseif ($ride->available == 'active')
                                            @php
                                                $model = App\Models\Ride::find($ride->id);

                                                $queue = $model
                                                    ->queue()
                                                    ?->where(function ($query) use ($time) {
                                                        $query->whereBetween('start_time', [$time?->date, $time?->close_date])->orWhereDate('start_time', $time->date);
                                                    })
                                                    ->latest()
                                                    ->first();
                                            @endphp
                                                <!-- NOTE : kindly add class : (playHasQue) to (playBox) div if the play has Que --->
                                                @if($ride->ride_type_id == 12)
                                                <div class="playBox @if($rideTime != null) yes @else gray @endif cardGame @if ($queue && $queue->queue_seconds == 0) playHasQue @endif"
                                                    id="rideQueue{{ $ride->id }}">
                                            @else
                                                <div class="playBox @if($rideTime != null && $preopening != null) yes @else gray @endif cardGame @if ($queue && $queue->queue_seconds == 0) playHasQue @endif"
                                                    id="rideQueue{{ $ride->id }}">
                                            @endif
                                                <div class="playBox 
                                                @if($ride->ride_type_id == 12)
                                                    @if($rideTime != null) yes @else gray @endif 
                                                @else
                                                    @if($rideTime != null && $preopening != null) yes @else gray @endif 
                                                @endif
                                                cardGame 
                                                @if ($queue && $queue->queue_seconds == 0) playHasQue @endif"
                                                id="rideQueue{{ $ride->id }}">
                                            </div>
                                                <a href="{{ url('/show_stoppages/' . $ride->id . '/' . $time->id) }}">
                                                    <div class="card-box" id="rideStatus{{ $ride->id }}">
                                                        <h4 class="header-title m-t-0 m-b-0">{{ $ride->name }}
                                                            {{--   <i
                                                                class="fa fa-edit"   @if($preclosing) title="preclosing added" style="color:green" @else title="preclosing not added yet" style="color:red" @endif >
                                                            </i> --}}
                                                        </h4>
                                                        <div class="contentRide_header row">
                                                            <h6 class=" mr-3">SA :{{$seatAvailability}}</h6>

                                                        </div>

                                                    </div>
                                                </a>
                                            </div>
                                        @elseif($ride->type == 'time_slot' && $ride->available == 'stopped' || $ride->available =='closed' && $ride->type == 'time_slot')
                                            <div class="playBox no cardGame " id="rideQueue{{ $ride->id }}">
                                                <!-- Start Tooltip -->
                                                <div class="tooltip-outer">
                                                    <div class="tooltip-icon" id="tooltip{{ $ride->id }}"
                                                         data-toggle="tooltip"
                                                         title="{{ $ride?->stoppageSubCategoryName }}!"><i
                                                            class="fa fa-info-circle">
                                                        </i></div>
                                                </div>
                                                <!-- !!End Tooltip -->

                                                <a href="{{ url('/show_stoppages/' . $ride->id . '/' . $time->id) }}">

                                                    <div class="card-box" id="rideStatus{{ $ride->id }}">

                                                        <h4 class="header-title m-t-0 m-b-0">{{ $ride->name }}
                                                           {{--   <i
                                                                class="fa fa-edit"   @if($preclosing) title="preclosing added" style="color:green" @else title="preclosing not added yet" style="color:red" @endif >
                                                            </i> --}}
                                                        </h4>
                                                        <h6 class="m-b-0">SA :{{$seatAvailability}}</h6>


                                                    </div>
                                                </a>

                                            </div>
                                        @elseif($ride->available == 'stopped' && $ride->type == 'all_day' || $ride->available =='closed' && $ride->type == 'all_day' )

                                            <div class="playBox orang cardGame " id="rideQueue{{ $ride->id }}">
                                                <!-- Start Tooltip -->
                                                <div class="tooltip-outer">
                                                    <div class="tooltip-icon" id="tooltip{{ $ride->id }}"
                                                         data-toggle="tooltip"
                                                         title="stoppage"><i
                                                            class="fa fa-info-circle">
                                                        </i></div>
                                                </div>
                                                <!-- !!End Tooltip -->

                                                <a href="{{ url('/show_stoppages/' . $ride->id . '/' . $time->id) }}">

                                                    <div class="card-box" id="rideStatus{{ $ride->id }}">

                                                        <h4 class="header-title m-t-0 m-b-0">{{ $ride->name }}
                                                           {{--  <i
                                                                class="fa fa-edit"   @if($preclosing) title="preclosing added" style="color:green" @else title="preclosing not added yet" style="color:red" @endif >
                                                            </i> --}}
                                                        </h4>
                                                        <h6 class="m-b-0">SA :{{$seatAvailability}}</h6>

                                                    </div>
                                                </a>
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                            <div class="row">
                                <div class="col-xs-12">


                                    <!-- Include JustGage JavaScript -->
                                    <script type="text/javascript"
                                            src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
                                    <script type="text/javascript"
                                            src="https://cdnjs.cloudflare.com/ajax/libs/justgage/1.2.9/justgage.min.js"></script>

                                    <div class="catdescription card-box row"
                                         style="  background-color: #fff6eb; padding: 10px;">
                                         @if (!empty($total_riders))
                                            <div class=" card-box" style="  background-color: #f8e9d8; padding: 10px;">
                                                <h3 style="text-align: center;"> Total Riders :
                                                    @forelse ($total_riders as $total_rider_id => $total_rider_rides)
                                                        @if ($total_rider_id === $time->id)
                                                            @foreach ($total_rider_rides as $ride)
                                                                <span
                                                                    id="park-{{ $time->park_id }}">{{ $ride->total_rider }}</span>
                                                            @endforeach
                                                        @endif
                                                    @empty
                                                        <span id="park-{{ $time->park_id }}">0</span>
                                                    @endforelse
                                                </h3>
                                            </div>
                                        @endif
                                        <div class="row">
                                            @foreach ($cycles as $index => $cycle)
                                                @if ($cycle->park_time_id === $time->id)
                                                    <div class="col-lg-4">
                                                        <h4 class="cat">{{ ucfirst($cycle->ride_cat) }} Rides</h4>
                                                        <div style="text-align: center;"
                                                             id="cycle_{{ $index }}_{{ $cycle->ride_cat }}"></div>
                                                        <script>
                                                            var cycleGauge{{ $index }}_{{ $cycle->ride_cat }} = new JustGage({
                                                                id: "cycle_{{ $index }}_{{ $cycle->ride_cat }}",
                                                                value: {{ $cycle->avg_duration }},
                                                                min: 0,
                                                                max: 180,
                                                                title: "{{ ucfirst($cycle->ride_cat) }} Cycle Duration",
                                                                label: "{{ ucfirst($cycle->ride_cat) }} Cycle Duration / Min",
                                                                pointer: true
                                                            });
                                                        </script>
                                                        @foreach ($queues as $queue)
                                                            @if ($queue->ride_cat === $cycle->ride_cat && $queue->park_time_id === $cycle->park_time_id)
                                                                <div style="text-align: center;"
                                                                     id="queue_{{ $index }}_{{ $queue->ride_cat }}_{{ $loop->index }}"></div>
                                                                <script>
                                                                    var queueGauge{{ $index }}_{{ $queue->ride_cat }}_{{ $loop->index }} = new JustGage({
                                                                        id: "queue_{{ $index }}_{{ $queue->ride_cat }}_{{ $loop->index }}",
                                                                        value: {{ $queue->avg_queue_minutes }},
                                                                        min: 0,
                                                                        max: 180,
                                                                        title: "{{ ucfirst($queue->ride_cat) }} Queue Duration",
                                                                        label: "{{ ucfirst($queue->ride_cat) }} Queue Duration / Min",
                                                                        pointer: true
                                                                    });
                                                                </script>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>


                                    </div>


                                </div>
                            </div>

                            <div class="contentDescription card-box" style="background-color: #eee; padding: 10px;">
                                @php
                                    $timeStoppages = $stoppages
                                        ->where('park_time_id', $time->id)
                                        ->whereIn('park_id', $parks)
                                        ->groupBy('stopage_category_id');
                                @endphp

                                @if($timeStoppages)
                                    @foreach ($timeStoppages as $categoryId => $categoryStoppages)

                                        @php
                                            $firstStoppage = $categoryStoppages->first();
                                        @endphp

                                        @if ($firstStoppage)
                                            <h4 class="bold">{{ $firstStoppage->stopageCategory?->name }}</h4>
                                        @endif

                                        @foreach ($categoryStoppages as $stoppage)

                                            @if($stoppage->children->isNotEmpty())

                                                <div class="row col-md-12">
                                                    @if ($stoppage->stoppage_status != 'done' && $stoppage->type != 'all_day')

                                                        <span
                                                            style="color: red;">{{ $stoppage->time_slot_start }}</span>
                                                        /
                                                        <span
                                                            style="color: rgb(53 74 185);">{{ $stoppage->ride->name }}</span>
                                                        {{ $stoppage->stopageSubCategory?->name }} {!! $stoppage->description ?? '' !!}
                                                    @else
                                                        @if ($stoppage->down_minutes == 0)
                                                            1
                                                        @else
                                                            <span
                                                                style="color: #b96105">{{ $stoppage->down_minutes }}</span>
                                                            mins
                                                        @endif
                                                        <span
                                                            style="color: rgb(53 74 185);">{{ $stoppage->ride?->name }}</span>
                                                        {{ $stoppage->stopageSubCategory?->name }}
                                                        <span
                                                            style="color: red;">{!! $stoppage->description ?? '' !!}</span>
                                                    @endif

                                                    =>
                                                    @foreach($stoppage->children as $key => $child)
                                                        @if ($child->stoppage_status != 'done' && $child->type != 'all_day')

                                                            <span
                                                                style="color: red;">{{ $child->time_slot_start }}</span>
                                                            /
                                                            <span
                                                                style="color: rgb(53 74 185);">{{ $child->ride->name }}</span>
                                                            {{ $child->stopageSubCategory?->name }} {!! $child->description ?? '' !!}
                                                        @else
                                                            @if ($child->down_minutes == 0)
                                                                1
                                                            @else
                                                                <span
                                                                    style="color: #b96105">{{ $child->down_minutes }}</span>
                                                                mins
                                                            @endif
                                                            <span
                                                                style="color: rgb(53 74 185);">{{ $child->ride?->name }}</span>
                                                            {{ $child->stopageSubCategory?->name }}
                                                            <span
                                                                style="color: red;">{!! $child->description ?? '' !!}</span>
                                                        @endif
                                                        @if($key+1  < $stoppage->children->count())
                                                            =>
                                                        @endif

                                                    @endforeach
                                                </div>
                                            @elseif($stoppage->parent_id == null || isset($stoppage->parent) && $stoppage->parent?->park_time_id != $time->id)

                                                @if ($stoppage->stoppage_status != 'done' && $stoppage->type != 'all_day')

                                                    <p>
                                                        <span
                                                            style="color: red;">{{ $stoppage->time_slot_start }}</span>
                                                        /
                                                        <span
                                                            style="color: rgb(53 74 185);">{{ $stoppage->ride->name }}</span>
                                                        {{ $stoppage->stopageSubCategory?->name }} {!! $stoppage->description ?? '' !!}
                                                    </p>
                                                @else
                                                    <p>
                                                        @if ($stoppage->down_minutes == 0)
                                                            1
                                                        @else
                                                            <span
                                                                style="color: #b96105">{{ $stoppage->down_minutes }}</span>
                                                            mins
                                                        @endif
                                                        <span
                                                            style="color: rgb(53 74 185);">{{ $stoppage->ride?->name }}</span>
                                                        {{ $stoppage->stopageSubCategory?->name }}
                                                        <span
                                                            style="color: red;">{!! $stoppage->description ?? '' !!}</span>
                                                    </p>
                                                @endif

                                            @endif
                                        @endforeach
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
@stop


@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            function autoRefresh() {
                $.ajax({
                    url: "{{ route('admin.auto-refresh-page') }}",
                    type: 'GET',
                    success: function (response) {
                        $('#times-content').html('');
                        $('#times-content').html(response);
                        $('.cardGame').each(function () {
                            if ($(this).hasClass("yes")) {
                                $(this).addClass("yesImportant");

                            } else if ($(this).hasClass("no")) {
                                $(this).addClass("noImportant");
                            }
                            else if ($(this).hasClass("gray")) {
                                $(this).addClass("grayImportant");
                            }
                        });
                    }
                });
            }

            setInterval(autoRefresh, 40000);

            $('.cardGame').each(function () {
                if ($(this).hasClass("yes")) {
                    $(this).addClass("yesImportant");
/*                     console.log("$(this)")
 */
                } else if ($(this).hasClass("no")) {
                    $(this).addClass("noImportant");
                }else if ($(this).hasClass("gray")) {
                                $(this).addClass("grayImportant");
                            }
            });

        });
    </script>
@endpush
