<div id="riders">
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class='contentRide'>
                <div class="row">

                    <h4 class="contentRide_title">Total Riders: {{$cycleRecords->sum('rider_count')}}</h4>
                    @if($flattenedLastRecords->isNotEmpty())
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                @foreach($times as $time)
                                    @php
                                        $date = new DateTime($time->date);
                                        $date2 = new DateTime($time->date);

                                    @endphp
                                    <th scope="col">({{ $date->modify('-1 hour')->format('g a') }} - {{ $date2->format('g a') }} ) </th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($flattenedLastRecords as $key => $ride)

                                <tr>
                                    <!-- Ride Name and Theoretical Number -->
                                    <th scope="row">
                                        {{ $ride[0]->ride?->name }}
                                        ({{ $ride[0]->ride?->theoretical_number ?? 'N/A' }})
                                    </th>
                                    <!-- Rider Counts for Each Time -->
                                    @foreach($times as $time)

                                        @php
                                            // Find the matching record for this ride and time
                                            $matchingRecord = $ride->firstWhere(function ($record) use ($key, $time) {
                                                return $record->ride_id == $key && $record->date == $time->date;
                                            });
                                        @endphp
                                        <td>{{ $matchingRecord->rider_count ?? 0 }}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p>No data available for the selected records.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>


</div>
