<li class="text-muted menu-title">main Menu</li>
@if (auth()->user()->can('mainpage'))
    <li>
        <a href="{{ route('admin.index') }}" class="waves-effect"><i class="zmdi zmdi-view-dashboard"></i>
            <span>Main Page </span>
        </a>
    </li>
    <li>
        <a href="{{ route('admin.riders') }}" class="waves-effect"><i class="zmdi zmdi-view-dashboard"></i>
            <span>Riders Number</span>
        </a>
    </li>
    <li>
        <a href="{{ route('admin.riders-cycles') }}" class="waves-effect"><i class="zmdi zmdi-flickr"></i>
            <span>Cycles Number</span>
        </a>
    </li>
     <li>
        <a href="{{ route('admin.queue.timeSlot') }}" class="waves-effect"><i class="zmdi zmdi-view-dashboard"></i>
            <span> Ride Queues</span>
        </a>
    </li>
@endif

{{-- @if (!auth()->user()->hasRole('Client'))
 --}} @if (auth()->user()->can('statistics'))
    <li>
        <a href="{{ route('admin.statistics') }}" class="waves-effect"><i class="zmdi zmdi-view-dashboard"></i>
            <span>Statistics </span>
        </a>
    </li>
@endif

@if (auth()->user()->hasRole('Super Admin')  )
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-view-week"></i> <span>Roles
            </span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li><a href="{{ route('admin.roles.index') }}">All Roles</a></li>
            <li><a href="{{ route('admin.roles.create') }}">Add New Role</a></li>
        </ul>
    </li>
@endif

@if (auth()->user()->can('departments-list') ||
        auth()->user()->can('departments-create'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-view-list"></i><span> Departments </span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('departments-list')
                <li><a href="{{ route('admin.departments.index') }}">All Departments</a></li>
            @endcan
            @can('departments-create')
                <li><a href="{{ route('admin.departments.create') }}">Add New Department</a></li>
            @endcan


        </ul>
    </li>
@endif
@if (auth()->user()->can('branches-list') ||
        auth()->user()->can('branches-create'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="fa-solid fa-code-branch"></i><span>Branches </span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li><a href="{{ route('admin.country.index') }}">All Countries</a></li>

            @can('branches-list')
                <li><a href="{{ route('admin.branches.index') }}">All Branches</a></li>
            @endcan
{{--            @can('branches-create')--}}
{{--                <li><a href="{{ route('admin.branches.create') }}">Add New Branch</a></li>--}}
{{--            @endcan--}}

        </ul>
    </li>
@endif



@if (auth()->user()->can('parks-list') ||
        auth()->user()->can('parks-create'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-local-parking"></i><span>Parks </span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('parks-list')
                <li><a href="{{ route('admin.parks.index') }}">All Parks</a></li>
            @endcan
            @can('parks-create')
                <li><a href="{{ route('admin.parks.create') }}">Add New Park</a></li>
            @endcan

        </ul>
    </li>
@endif
@if (auth()->user()->can('inspection_lists-list') ||
        auth()->user()->can('inspection_lists-create'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-traffic"></i><span>Ride/Zone Inspection
                Elements
            </span><span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('inspection_lists-list')
                <li><a href="{{ route('admin.inspection_lists.index') }}">Add Inspection Elements</a></li>
            @endcan
            @can('inspection_lists-create')
                <li><a href="{{ route('admin.ride_inspection_lists.index') }}">Assign Inspection Elements To Ride</a>
                </li>
            @endcan
            @can('inspection_lists-create')
                <li><a href="{{ route('admin.zone_inspection_lists.index') }}">Assign Inspection Elements To Zone</a>
                </li>
            @endcan
        </ul>
    </li>
@endif

@if (auth()->user()->can('zones-list') ||
        auth()->user()->can('zones-create'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="fa-solid fa-list"></i><span>Zones
            </span><span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('zones-list')
                <li><a href="{{ route('admin.zones.index') }}">All Zones</a></li>
            @endcan
            @can('zones-create')
                <li><a href="{{ route('admin.zones.create') }}">Add New Zone</a></li>
            @endcan

        </ul>
    </li>
@endif

@if (auth()->user()->can('users-list') ||
        auth()->user()->can('users-create') || auth()->user()->hasRole('Park Admin') )
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="fa-solid fa-users"></i><span> Users </span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li><a href="{{ route('admin.users.index') }}">All Users</a></li>

            <li><a href="{{ route('admin.users.create') }}">Add New User</a></li>

        </ul>
    </li>
    @endif
    @if (auth()->user()->can('users-list') ||
        auth()->user()->can('users-create') || auth()->user()->hasRole('Park Admin') || auth()->user()->hasRole('Training Admin'))

    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-circle-o"></i><span> Operators times </span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li><a href="{{ route('admin.operators.index') }}">All Operators & Breakers</a></li>
            <li><a href="{{ route('admin.uploadOperatorTime') }}">Upload Operators & Breakers Time</a></li>


        </ul>
    </li>

    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-circle-o"></i><span> Attendants times </span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li><a href="{{ route('admin.attendances.index') }}">All Attendants</a></li>
            <li><a href="{{ route('admin.attendances.create') }}">Add New Attendant</a></li>
            <li><a href="{{ route('admin.uploadAttendance') }}">Upload Attendants</a></li>
            <li><a href="{{ route('admin.uploadAttendanceTime') }}">Upload Attendants Time</a></li>
        </ul>
    </li>
@endif

@if (auth()->user()->can('park_times-list') ||
        auth()->user()->can('park_times-create'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="fa-regular fa-clock"></i><span>Parks Time Slot
            </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('park_times-list')
                <li><a href="{{ route('admin.park_times.index') }}">All Parks Time Slot</a></li>
            @endcan
            @can('park_times-create')
                <li><a href="{{ route('admin.park_times.create') }}">Add Time Slot</a></li>
            @endcan

        </ul>
    </li>
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="fa-regular fa-calendar"></i><span>Schedules Parks Time
            </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('park_times-list')
                <li><a href="{{ route('admin.schedule_park_times.index') }}">Schedule Parks Time</a></li>
            @endcan
            @can('park_times-create')
                <li><a href="{{ route('admin.schedule_park_times.create') }}">Add Schedule Time</a></li>
            @endcan

        </ul>
    </li>
@endif
@if (auth()->user()->can('park_times-list') ||
        auth()->user()->can('park_times-create'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-view-week"></i><span>Parks Without Time Slot
            </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('park_times-list')
                <li><a href="{{ route('admin.park_without_times.index') }}">All Parks</a></li>
            @endcan
        </ul>
    </li>
@endif

@if (auth()->user()->can('ride_types-list') ||
        auth()->user()->can('ride_types-create'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-view-list"></i><span>Ride Types </span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('ride_types-list')
                <li><a href="{{ route('admin.ride_types.index') }}">All Ride Types</a></li>
            @endcan
            @can('ride_types-create')
                <li><a href="{{ route('admin.ride_types.create') }}">Add Ride Type</a></li>
            @endcan

        </ul>
    </li>
@endif


@if (auth()->user()->can('stoppage-category-list') ||
        auth()->user()->can('stoppage-category-create'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-collection-text"></i><span>Stoppages
                Categories </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('stoppage-category-list')
                <li><a href="{{ route('admin.stoppage-category.index') }}"> Stoppage Categories</a></li>
            @endcan
            @can('stoppage-category-create')
                <li><a href="{{ route('admin.stoppage-sub-category.index') }}"> Stoppage Sub Categories</a></li>
            @endcan

        </ul>
    </li>
@endif

@if (auth()->user()->can('rides-list') ||
        auth()->user()->can('rides-create'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="fa-solid fa-database"></i><span>Rides </span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('rides-list')
                <li><a href="{{ route('admin.rides.index') }}">All Rides</a></li>
            @endcan
            @can('rides-create')
                <li><a href="{{ route('admin.rides.create') }}">Add New Rides</a></li>
           
                <li><a href="{{ route('admin.uploadRides') }}">Upload Arcade games</a></li>
            @endcan 

        </ul>
    </li>
@endif

@if (auth()->user()->can('uploadStoppagesExcleFile') ||
        auth()->user()->can('uploadQueueExcleFile') ||
        auth()->user()->can('uploadCycleExcleFile'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-collection-text"></i><span>Upload Rides
                Operations
            </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <!--   <li><a href="{{ route('admin.rides-stoppages.index') }}"> Rides Stoppages</a></li>
        <li><a href="{{ route('admin.rides-cycles.index') }}"> Rides Cycles</a></li>
        <li><a href="{{ route('admin.queues.index') }}">Rides Queues</a></li> -->
            @can('uploadStoppagesExcleFile')
                <li><a href="{{ route('admin.rides-stoppages.create') }}"> Upload Stoppages</a></li>
            @endcan
            @can('uploadCycleExcleFile')
                <li><a href="{{ route('admin.rides-cycles.create') }}"> Upload Cycles</a></li>
            @endcan
            @can('uploadQueueExcleFile')
                <li><a href="{{ route('admin.queues.create') }}">Upload Queues</a></li>
            @endcan
            
        </ul>
    </li>
@endif
@if (auth()->user()->can('rsr_reports-list') ||
        auth()->user()->can('rsr_reports-create'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-collection-text"></i><span>RSR Reports
            </span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('rsr_reports-list')
                <li><a href="{{ route('admin.rsr_reports.index') }}">All RSR Reports</a></li>
            @endcan
            @can('rsr_reports-create')
                <li><a href="{{ route('admin.rsr_reports.create') }}">Add RSR Report Witout Stoppage</a></li>
            @endcan
        </ul>
    </li>
@endif
@if (auth()->user()->can('show-availabilty-report') ||
        auth()->user()->can('add-availabilty-report'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i
                class="zmdi zmdi-collection-text"></i><span>Availability Reports
            </span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('show-availabilty-report')
                <li><a href="{{ route('admin.availability_reports.all') }}">All Ride availability Reports</a></li>
            @endcan
            @can('add-availabilty-report')
                <li><a href="{{ route('admin.parks.index') }}">Add Ride availability Report</a></li>
            @endcan
        </ul>
    </li>
@endif
@if (auth()->user()->can('customer_feedbacks-list') ||
        auth()->user()->can('customer_feedbacks-create'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="fa-solid fa-comment-dots"></i><span>Customer
                Feedback</span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('customer_feedbacks-list')
                <li><a href="{{ route('admin.customer_feedbacks.index') }}"> Show Customer Feedback</a></li>
            @endcan
            @can('customer_feedbacks-create')
                <li><a href="{{ route('admin.customer_feedbacks.create') }}"> Add Customer Feedback</a></li>

                <li><a href="{{ route('admin.complaints.index') }}">Positive & Negative Items</a></li>
                <li><a href="{{ route('admin.feedback_sub_category.index') }}">Add Customer Feedback Subcategory </a>
                </li>

            @endcan

        </ul>
    </li>
@endif

@if (auth()->user()->can('observations-list') ||
        auth()->user()->can('observations-edit'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="fas fa-tools"></i><span>Observations
            </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('observations-edit')
                <li><a href="{{ route('admin.observations.index') }}"> Show Observations</a></li>
            @endcan

        </ul>
    </li>
@endif

     @if (auth()->user()->can('showIncidentReport'))
     <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="glyphicon glyphicon-heart"></i><span>Health &
                Safety
            </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
                <li><a href="{{ route('admin.incident.index') }}">Accident & Incident</a></li>
                <li><a href="{{ route('admin.investigation.index') }}">Investigation Incident</a></li>
                <li><a href="{{ route('admin.evacuations.index') }}">Evacuations</a></li>
                <li><a href="{{ route('admin.daily-check.index') }}">AM Shift H&S Checklist</a></li>
                <li><a href="{{ route('admin.indexPm') }}">PM Operational H&S Checklist </a></li>
        </ul>
    </li>
    @endif
    @if (auth()->user()->can('operator-evaluation-list') ||
        auth()->user()->can('supervisor-evaluation-list'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="glyphicon glyphicon-star"></i><span>Evaluation
            </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('operator-evaluation-list')
                <li><a href="{{ route('admin.operator-evaluation.index') }}">Operator Performance Evaluation</a></li>
            @endcan
            @can('supervisor-evaluation-list')
                <li><a href="{{ route('admin.supervisor-evaluation.index') }}">Supervisor Performance Evaluation</a>
                </li>
            @endcan

        </ul>
    </li>
@endif
@if (auth()->user()->can('traininglist-create'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="fas fa-chalkboard-teacher"></i><span>Training
            </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li><a href="{{ route('admin.training.index') }}"> All Trainings checklists </a></li>

            <li><a href="{{ route('admin.training.create') }}">Add Training CheckList to users</a></li>
        </ul>
    </li>
@endif



@if (auth()->user()->can('duty-report-list') ||
        auth()->user()->can('stoppagesReport') ||
        auth()->user()->can('inspectionListReport') ||
        auth()->user()->can('rideStatus') ||
        auth()->user()->can('showIncidentReport') ||
        auth()->user()->can('showauditReport') ||
        auth()->user()->can('rideCapacity_report')
         || auth()->user()->hasRole('Training Admin')
        )
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="fa-regular fa-folder-open"></i><span>Reports
            </span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @if (auth()->user()->can('duty-report-list')
            // || auth()->user()->hasRole('Client')
             )
                <li><a href="{{ route('admin.duty-report.index') }}">Duty Report</a></li>
            @endif
            @can('stoppagesReport')
                <li><a href="{{ route('admin.reports.stoppagesReport') }}">Stoppages Report</a></li>
            @endcan
            @can('stoppagesReport')
                <li><a href="{{ route('admin.reports.cyclesReport') }}">Cycles Report</a></li>
            @endcan
            @can('stoppagesReport')
                <li><a href="{{ route('admin.reports.queuesReport') }}">Queues Report</a></li>
            @endcan
            @can('inspectionListReport')
                <li><a href="{{ route('admin.reports.inspectionListReport') }}">Inspection Lists Report</a></li>
            @endcan
            @can('showauditReport')
                <li><a href="{{ route('admin.reports.auditReport') }}">Attraction Audit Check Report</a></li>
            @endcan
            @if(auth()->user()->can('duty-report-list') || auth()->user()->hasRole('Training Admin'))

                <li><a href="{{ route('admin.reports.operatorTimeReport') }}">Operator Time Report</a></li>
                <li><a href="{{ route('admin.reports.attendanceTimeReport') }}">Attendants Time Report</a></li>
            @endcan
            @can('rideStatus')
                <li><a href="{{ route('admin.availability_reports.index') }}">Ride Availability Report</a></li>
            @endcan
            @can('showObservationReport')
                <li><a href="{{ route('admin.reports.observationReport') }}">Observation Report</a></li>
            @endcan
            @can('showIncidentReport')
                <li><a href="{{ route('admin.reports.incidentReport') }}">Health & Safety Report</a></li>
                <li><a href="{{ route('admin.evacuation.report') }}">Evacuation Report</a></li>
                @endcan
            @can('rideCapacity_report')
                <li><a href="{{ route('admin.rideCapacity.report') }}">Ride Capacity Report</a></li>
            @endcan
            @can('operator-evaluation-list')
            <li><a href="{{ route('admin.evaluation.operator.report') }}">Evaluation Operator Report</a></li>
            <li><a href="{{ route('admin.evaluation.supervisor.report') }}">Evaluation Supervisor Report</a></li>
            @endcan
        </ul>
    </li>
@endif
@if (auth()->user()->hasRole('Super Admin'))

<li class="has_sub">
    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-mobile"></i><span> Users actions </span>
        <span class="menu-arrow"></span></a>
    <ul class="list-unstyled">
        <li><a href="{{ route('admin.showActions') }}">All Users actions</a></li>

    </ul>
</li>
@endif
@if (auth()->user()->can('rideCapacity_list') )
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-local-parking"></i><span>Ride Capacity
            </span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">

            <li><a href="{{ route('admin.rideCapacity') }}">Show Ride Capacity</a></li>


        </ul>
    </li>
@endif
@if (auth()->user()->hasRole('Super Admin'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-mobile"></i><span> Mobile Versions </span>
            <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            <li><a href="{{ route('admin.versions.index') }}">All Mobile Versions</a></li>
            <li><a href="{{ route('admin.versions.create') }}">Add New Mobile Version</a></li>

        </ul>
    </li>


@endif

{{--
@if (auth()->user()->can('units-list') ||
        auth()->user()->can('units-list'))
    <li class="has_sub">
        <a href="javascript:void(0);" class="waves-effect"><i class="glyphicon glyphicon-heart"></i><span> Rental Units
            </span> <span class="menu-arrow"></span></a>
        <ul class="list-unstyled">
            @can('units-list')
                <li><a href="{{ route('admin.units.index') }}"> Rental Units</a></li>
             @endcan
        </ul>
    </li>
@endif --}}
