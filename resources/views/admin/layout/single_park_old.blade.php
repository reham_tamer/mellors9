@extends('admin.layout.app')

@section('title')
    Main Page
@stop

@section('content')
    <div id="riders">
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                <div class='contentRide'>
                    <div class="contentRide_header">
                        <a href="{{ url('/all-rides/' . $time->park_id . '/' . $time->id) }}"><h3
                                class="contentRide_title" class="bold">{{ $time->parks->name }}</h3>
                        </a>
                        <div class="contentRide_mins">
                            <h3 class="contentRide_title">Park Hours
                                : {{ $time->duration_time ?? 0 . ' minute' }} </h3>
                            <p> {{ $time->date }} : ( {{ $time->start }} - {{ $time->end }} )</p>
                        </div>
                    </div>
                    <div class="row">
                        @php
                            $cycles = $time->cycles->groupBy(function($date) {
                                $dt = new DateTime($date->start_time);
                            return $dt->format('H');
      //                                return $dt->modify('- 1 hour')->format('H');

                         });

                             $rides = $time->cycles;
                        @endphp

                        @php $total = 0; @endphp

                        @foreach($time->cycles as $key =>$cycle)
                            @php $totalRider = $cycle->riders_count + $cycle->number_of_disabled + $cycle->number_of_vip + $cycle->number_of_ft;
                                             $total =  $total +$totalRider;
                            @endphp

                        @endforeach

                        <h4 class="contentRide_title"> Total Riders : {{$total}}</h4>
                        @if($cycles->isNotEmpty())
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    @php $keys=[]; $ride=[]; @endphp
                                    @foreach($cycles as $key =>$cycle1 )

                                        @php $keys[] += $key ; $hour = $key @endphp
                                        <th scope="col"> {{date("g:i a", strtotime("$hour:00:00"))}}</th>
                                    @endforeach

                                </tr>
                                </thead>
                                <tbody>

                                @foreach($rides->groupBy('ride_id')  as $key=> $ride)
                                    <tr>
                                        <th scope="row">{{$ride[0]?->ride?->name}}
                                            ({{$ride[0]?->ride?->theoretical_number}})
                                        </th>

                                        @foreach($keys as $key)
                                            @php
                                                $cycles = $ride[0]?->ride?->cycle?->where('park_time_id',$time->id)->groupBy(function($date) {
                                                     $dt = new DateTime($date->start_time);
                                                     return (int)$dt->format('H');
                                                 });

                                            @endphp
                                            @if(isset( $cycles[$key]))
                                                @php
                                                    $totalRiders = $cycles[$key]->sum('riders_count') + $cycles[$key]->sum('number_of_disabled') + $cycles[$key]->sum('number_of_vip') + $cycles[$key]->sum('number_of_ft');
                                                @endphp
                                                <td>{{$totalRiders}}</td>
                                            @else
                                                <td>0</td>

                                            @endif

                                        @endforeach


                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @endif
                    </div>

                </div>
            </div>
        </div>


    </div>
@stop


@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            var time = "{{$time->id}}";
            var url = "{{ route('admin.singlePage', ":id") }}";
            url = url.replace(':id', time);

            function autoRefresh() {
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (response) {
                        console.log(response)
                        $('#riders').html('')
                        $('#riders').html(response)
                    }
                });
            }

            setInterval(autoRefresh, 40000);

        });
    </script>
@endpush
