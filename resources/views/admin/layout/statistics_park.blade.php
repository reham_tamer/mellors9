@extends('admin.layout.app')

@section('title')
    Statistics {{$park_time?->parks?->name}}
@stop

@section('content')
    <div class="card-box">
        <form class="formSection" method="GET">
            <div class="row">
                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="middle_name">Time Slot Date </label>
                        {!! Form::date('date', request()->date, ['class' => 'form-control', 'id' => 'date']) !!}
                    </div>
                </div>

                <div class="col-md-2 mtButton text-center">
                    <div class="input-group-btn mx-auto">
                        <button type="submit" class="btn btn-primary save_btn waves-effect">Show</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    @if ($park_time)
        <div class="col-md-12 p-0">
            @if(!request()->date)
                <h3 class="left col-md-11 text-primary">Recent time slot</h3>
            @endif
            <a href="{{route('admin.statistics')}}"><h3 class="left text-danger col-md-1">Back</h3></a>

        </div>

        {{--        <h4 class="text-center header-title m-t-0 m-b-0">{{ $park_time->parks?->name }}</h4>--}}
        <br>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Rides Count</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = App\Models\Ride::where('park_id', $park_time->park_id)->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">Rides Count</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3  ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Stoppages</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb"
                               value=" {{ $park_time->rideStoppages?->count() }}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $park_time->rideStoppages?->count() }}
                        </h2>
                        <p class="text-muted"> Stoppages</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3  ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Cycles</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb"
                               value=" {{ $park_time->cycles?->count() }}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $park_time->cycles?->count()}}
                        </h2>
                        <p class="text-muted"> Cycles</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3  ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Queues</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb"
                               value=" {{ $park_time->queues?->count() }}"
                               data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $park_time->queues?->count()}}
                        </h2>
                        <p class="text-muted"> Queues</p>
                    </div>
                </div>

            </div>
        </div>


        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">RSR Report</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = App\Models\RsrReport::where('park_id', $park_time->park_id)
                                ?->where('date', Illuminate\Support\Carbon::now()->toDateString())
                                ->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">RSR Report</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Availabilty Report</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = App\Models\GameTime::where('park_id', $park_time->park_id)
                                ?->where('date', Illuminate\Support\Carbon::now()->toDateString())
                                ->first() ?'1':0;
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback?'1':0 }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">Availabilty Report</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Preopening List</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = $park_time->preopeningLists?->where('type','preopening')->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">Preopening List</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Preclosing List</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = $park_time->preopeningLists?->where('type','preclosing')->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">Preclosing List</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Inspection List</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback2 =$park_time->preopeningLists?->where('type','inspection_list')->count();

                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback2 }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback2 }}
                        </h2>
                        <p class="text-muted">Inspection List</p>
                    </div>
                </div>

            </div>
        </div>


        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Positive Customer Feedback </h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback3 = App\Models\CustomerFeedbacks::where('park_id', $park_time->park_id)
                            ->where(function ($query) use ($park_time) {
                                $query->where('date', $park_time->date);
                                })->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback3 }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback3 }}
                        </h2>
                        <p class="text-muted">Positive Customer Feedback</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Negative Customer Feedback </h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = App\Models\CustomerFeedbacks::where('park_id', $park_time->park_id)
                                ?->where('type','Complaint')->where(function ($query) use ($park_time) {
                                $query->where('date', $park_time->date);
                                })->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">Negative Customer Feedback</p>
                    </div>
                </div>

            </div>
        </div>

        {{-- <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Observation</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $Observation = App\Models\Observation::where('park_id', $park_time->park_id)
                                ?->where('date_reported', Illuminate\Support\Carbon::now()->toDateString())
                                ->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                            data-bgColor="#fff6eb" value=" {{ $Observation }}" data-skin="tron"
                            data-angleOffset="180" data-readOnly=true data-thickness=".15"
                            style="margin-left: -62px;" />
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $Observation }}
                        </h2>
                        <p class="text-muted">Observations</p>
                    </div>
                </div>

            </div>
        </div> --}}
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Investigation</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $Investigation = App\Models\GeneralIncident::where('type', 'investigation')
                                ->where('park_id', $park_time->park_id)
                                ?->where(function ($query) use ($park_time) {
                                $query->where('date', $park_time->date);
                                })
                                ->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $Investigation }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $Investigation }}
                        </h2>
                        <p class="text-muted">Investigation</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Incident</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $Incident = App\Models\GeneralIncident::where('type', 'incident')
                                ->where('park_id', $park_time->park_id)
                                ?->where(function ($query) use ($park_time) {
                                $query->where('date', $park_time->date);
                                })
                                ->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $Incident }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $Incident }}
                        </h2>
                        <p class="text-muted">Incident</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Witness Statement</h4>
                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $Incident = App\Models\IncidentStatement::where('park_id', $park_time->park_id)
                                ?->where(function ($query) use ($park_time) {
                                $query->where('date', $park_time->date);
                                })
                                ->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $Incident }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $Incident }}
                        </h2>
                        <p class="text-muted">Witness Statement</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Attraction Audit Check Report Approved</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = App\Models\Attraction::where('park_id', $park_time->park_id)
                                ?->where('approve',1)->where('date', $park_time->date)
                                ->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">Attraction Audit Check Report Approved</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col col-xs-6 col-md-4 col-lg-3 ">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-0">Attraction Audit Check Report Pennding</h4>

                <div class="widget-chart-1">
                    <div class="widget-chart-box-1">
                        @php
                            $feedback = App\Models\Attraction::where('park_id', $park_time->park_id)
                                ?->where('approve',0)->where('date', $park_time->date)
                                ->count();
                        @endphp
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#b59369"
                               data-bgColor="#fff6eb" value=" {{ $feedback }}" data-skin="tron"
                               data-angleOffset="180" data-readOnly=true data-thickness=".15"
                               style="margin-left: -62px;"/>
                    </div>

                    <div class="widget-detail-1">
                        <h2 class="p-t-10 m-b-0">
                            {{ $feedback }}
                        </h2>
                        <p class="text-muted">Attraction Audit Check Report Pennding</p>
                    </div>
                </div>

            </div>
        </div>
    @else
        <h4>Not found time slot </h4>
    @endif

@endsection
