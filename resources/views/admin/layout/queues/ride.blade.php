@extends('admin.layout.app')

@section('title')
    Ride queue by Average minutes
@stop

@section('content')
{{--    @dd($queues)--}}
    <div id="col-md-12 row">
        <h3 class="contentRide_title text-primary" class="bold">{{ $ride->name }}</h3>
        <div style="width: 80%; margin: auto;">
            <canvas id="barChart"></canvas>
        </div>
    </div>
@stop

@push('scripts')
{{--    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/4.2.1/chart.umd.js"></script>
    <script>
        var ctx = document.getElementById('barChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: @json($queues['labels']),
                datasets: [{
                    label: 'Data',
                    data: @json($queues['data']),
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
        window.setTimeout( function() {
            window.location.reload();
        }, 1000 * 60 * 60); //every hour
    </script>

@endpush
