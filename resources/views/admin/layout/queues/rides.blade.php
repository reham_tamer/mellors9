@extends('admin.layout.app')

@section('title')
    Main Page
@stop

@section('content')
    <div id="col-md-12 row">
        <br>
        @forelse ($rides as $ride)
            <div class="col-md-6">
                <div class='contentRide'>
                    <div class="contentRide_header">
                        <a href="{{ route('admin.queue.ride',[$ride->id,$id]) }}"><h3
                                class="contentRide_title text-danger" class="bold">{{ $ride->name }}</h3>
                        </a>
                        <div class="contentRide_mins">
                            <h3 class="contentRide_title">Total Waiting Minutes
                                : {{ (integer)($ride->queues->where('park_time_id',$id)->sum('queue_seconds') / 60 ?? 0) . ' minutes' }} </h3>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class='contentRide'>Not found rides</div>
        @endforelse
    </div>
@stop

@push('scripts')
    <script type="text/javascript">
    </script>
@endpush
