@extends('admin.layout.app')

@section('title')
    Main Page
@stop

@section('content')
    <div id="riders  mt-6">
        <br>
         @forelse ($times as $time)
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class='contentRide'>
                        <div class="contentRide_header">
                            <a href="{{ route('admin.queue.time',$time->id) }}"><h3
                                    class="contentRide_title text-danger" class="bold">{{ $time->parks->name }}</h3>
                            </a>
                            <div class="contentRide_mins">
                                <h3 class="contentRide_title">Park Hours
                                    : {{ $time->duration_time ?? 0 . ' minute' }} </h3>
                                <p> {{ $time->date }} : ( {{ $time->start }} - {{ $time->end }} )</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @empty
            <div class='contentRide'>Not found time slot yet</div>
        @endforelse

    </div>
@stop


@push('scripts')
    <script type="text/javascript">
    </script>
@endpush
