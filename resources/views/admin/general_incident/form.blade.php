@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="row">
    <div class="col-xs-12">
        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
        <thead>
        <th style="border: 2px solid;
        padding: 8px;">Department</th>
        <th style="border: 2px solid;
        padding: 8px;">Location</th>
        <th style="border: 2px solid;
        padding: 8px;">Date/Time of Event</th>
        <th style="border: 2px solid;
        padding: 8px;"> Ref No.</th>
    </thead>
    <tbody>
         <td style="border: 2px solid;
    padding: 8px;"><div class="form-line">
            {!! Form::select('department_id', $departments, null, [
                'class' => 'form-control select2',
                'placeholder' => 'Choose Department...',
            ]) !!}
            @error('department_id')
                <div class="invalid-feedback" style="color: #ef1010">
                    {{ $message }}
                </div>
            @enderror
        </div> </td>
         <td style="border: 2px solid;
    padding: 8px;"> <div class="form-line">
            <input type="radio" name="choose" id="ride" value="ride" checked class="ml-4"> Ride

            <input type="radio" name="choose" id="park" value="park" class="ml-4" > Park
            
        </div>
        <label class="form-label">Parks</label>
        <div class="form-line">
            {!! Form::select('park_id', $parks, null, [
                'class' => 'form-control select2 Parks',
                'placeholder' => 'Choose Park...',
            ]) !!}
            @error('park_id')
                <div class="invalid-feedback" style="color: #ef1010">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div  id="rides"  >

        <label class="form-label">Rides</label>
        <div class="form-line">
            {!! Form::select('ride_id', $rides, null, [
                'class' => 'form-control select2 Rides',
                'placeholder' => 'Choose Ride...',
            ]) !!}
            @error('ride_id')
                <div class="invalid-feedback" style="color: #ef1010">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-xs-12" id="text" style="display: none;">

        <label class="form-label">Location</label>
        <div class="form-line">
            {!! Form::input('text', 'text', null, ['class' => 'form-control  ', 'placeholder' => 'text']) !!}
            @error('text')
                <div class="invalid-feedback" style="color: #ef1010">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</td>
         <td style="border: 2px solid;
    padding: 8px;"> <div class="form-line">
            {!! Form::datetimeLocal('date', null, ['class' => 'form-control', 'placeholder' => 'Start Time']) !!}
            @error('date')
                <div class="invalid-feedback" style="color: #ef1010">
                    {{ $message }}
                </div>
            @enderror
        </div></td>
         <td style="border: 2px solid;
    padding: 8px;">  <div class="form-line">
            {!! Form::input('text', 'report_reference_no', null, [
                'class' => 'form-control  ',
                'placeholder' => 'Report Reference No',
            ]) !!}
            @error('report_reference_no')
                <div class="invalid-feedback" style="color: #ef1010">
                    {{ $message }}
                </div>
            @enderror
        </div></td>
    </tbody>
</table>

<table id="datatable-buttons" style="border: 2px solid;
padding: 8px;" class="table table-striped table-bordered dt-responsive nowrap">
<tbody>
    <td style="border: 2px solid;
    padding: 8px;" class="highlighted-cell">Type Of Event
    </td>
    <td style="border: 2px solid;
    padding: 8px;">
        <input type="radio" name="type_of_event" value="Injury" class="ml-3"> Injury
    </td >
    <td style="border: 2px solid;
    padding: 8px;">
        <input type="radio" name="type_of_event" value="Health" class="ml-3"> Health
    </td>
    <td style="border: 2px solid;
    padding: 8px;">
        <input type="radio" name="type_of_event" value="Near Miss" class="ml-3"> Near Miss
    </td>
     <td style="border: 2px solid;
    padding: 8px;">
    <input type="radio" name="type_of_event" @if (isset($accident->value['type_of_event']) && $accident->value['type_of_event'] == 'Incident') checked @endif
    value="Incident" class="ml-3"> Incident
    </td>
    <td style="border: 2px solid;
    padding: 1px;">
        <input type="radio" name="type_of_event" value="Property Damage" class="ml-3"> Property Damage
    </td>
    <td style="border: 2px solid;
    padding: 8px;">
        <input type="radio" name="type_of_event" value="Unsafe Behaviour" class="ml-3"> Unsafe Behaviour 
    </td>
    <td colspan="8" style="border: 2px solid;
    padding: 8px;">
     <input type="radio" name="type_of_event" value="Other" class="ml-4"> Other
    
      <input type="text" value="" name="other_of_type_of_event" class="form-control" placeholder="Describe Other ">
    </td>
</tbody>
</table>
       
<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
    <tbody>
        <td style="border: 2px solid;
        padding: 8px;" class="highlighted-cell">Harm (or Potential for Harm)
        </td>
         <td style="border: 2px solid;
    padding: 8px;">
            <input type="radio" name="harm" value="Major or Fatal" class="ml-4">Major or Fatal  
        </td>
         <td style="border: 2px solid;
    padding: 8px;">
            <input type="radio" name="harm" value="Serious" class="ml-4"> Serious
        </td>
         <td style="border: 2px solid;
    padding: 8px;">
            <input type="radio" name="harm" value="Minor" class="ml-4"> Minor
        </td>
        
    </tbody>
    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
        <tbody>
            <td style="border: 2px solid;
            padding: 8px;" class="highlighted-cell">Area of Injury
            </td>
             <td style="border: 2px solid;
        padding: 8px;">
                <input type="radio" name="area_of_injury" value="Major or Fatal" class="ml-4">Head  
            </td>
             <td style="border: 2px solid;
        padding: 8px;">
                <input type="radio" name="area_of_injury" value="Arms" class="ml-4"> Arms
            </td>
             <td style="border: 2px solid;
        padding: 8px;">
                <input type="radio" name="area_of_injury" value="Hands" class="ml-4"> Hands
            </td>
            <td style="border: 2px solid;
            padding: 8px;">
                    <input type="radio" name="area_of_injury" value="Torso" class="ml-4"> Torso
                </td>
                <td style="border: 2px solid;
                padding: 8px;">
                        <input type="radio" name="area_of_injury" value="Back" class="ml-4"> Back
                    </td>
            <td style="border: 2px solid;
            padding: 8px;">
            <input type="radio" name="area_of_injury" value="Legs" class="ml-4"> Legs
        </td>
        <td style="border: 2px solid;
        padding: 8px;">
                <input type="radio" name="area_of_injury" value="Back" class="ml-4"> Feet
            </td>
            <td style="border: 2px solid;
            padding: 8px;">
                <input type="radio" name="area_of_injury" value="Fainting/Dizziness" class="ml-4"> Fainting/Dizziness
            </td>
            <tr>
                <td style="border: 2px solid;
                padding: 8px;">
                </td>
            <td colspan="8" style="border: 2px solid;
            padding: 8px;">
             <input type="radio" name="area_of_injury" value="Other" class="ml-4"> Other
            
              <input type="text" value="" name="other_of_area_of_injury" class="form-control" placeholder="Describe Other (ie. Seizure, Anxiety Attack)">
            </td>
            </tr>
        </tbody>
    </table>
    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
        <tbody>
            <tr>
                <td style="border: 2px solid;
                padding: 8px;" class="highlighted-cell">Person Involved
                </td>
                 <td style="border: 2px solid;
    padding: 8px;">
                    Name: 
                        {!! Form::input('text', 'name', null, ['class' => 'form-control']) !!}
                        @error('time')
                            <div class="invalid-feedback" style="color: #ef1010">
                                {{ $message }}
                            </div> 
                            @enderror                     
                 </td>
                 <td style="border: 2px solid;
    padding: 8px;">
                    Position:
                        {!! Form::input('text', 'position', null, ['class' => 'form-control']) !!}
                        @error('position')
                            <div class="invalid-feedback" style="color: #ef1010">
                                {{ $message }}
                            </div>
                        @enderror 
               </td>
              
            </tr>
            <tr>
            <td style="border: 2px solid;
            padding: 8px;"></td>
                        
            <td style="border: 2px solid;
            padding: 8px;">
                            Gender: 
                            <select name="gender" class="form-control">
                                <option value="">Select Gender</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Other">Other</option>


                            </select>
                                @error('gender')
                                    <div class="invalid-feedback" style="color: #ef1010">
                                        {{ $message }}
                                    </div> 
                                    @enderror                     
                         </td>
                         <td style="border: 2px solid;
            padding: 8px;">
                            Age:
                                {!! Form::input('number', 'age', null, ['class' => 'form-control']) !!}
                                @error('age')
                                    <div class="invalid-feedback" style="color: #ef1010">
                                        {{ $message }}
                                    </div>
                                @enderror 
                       </td>
                      
                    </tr>
            <tr>
                 <td style="border: 2px solid;
    padding: 8px;"></td>
                 <td style="border: 2px solid;
    padding: 8px;">
                    Address: {!! Form::input('text', 'address', null, ['class' => 'form-control']) !!}
                        @error('address')
                            <div class="invalid-feedback" style="color: #ef1010">
                                {{ $message }}
                            </div>
                        @enderror  
               </td>
              <td style="border: 2px solid;
    padding: 8px;">
                Contact No: 
                    {!! Form::input('text', 'phone', null, ['class' => 'form-control']) !!}
                    @error('phone')
                        <div class="invalid-feedback" style="color: #ef1010">
                            {{ $message }}
                        </div>
                    @enderror
             </td>
            </tr>
           
        </tbody>
        </table>
        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
            <tbody>
                <tr>
                    <td style="border: 2px solid;
                    padding: 8px;" class="highlighted-cell">Brief Description of Event:<br>
                (Details of what happened, when,<br> where and any emergency action taken)
                    </td>
                     <td style="border: 2px solid;
        padding: 8px;">
                        <div class="form-line">
                            {!! Form::textArea('description', null, ['class' => 'form-control  ', 'placeholder' => 'Description']) !!}
                            @error('description')
                                <div class="invalid-feedback" style="color: #ef1010">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>                     
                     </td>
                     
                </tr>
               
               
            </tbody>
            </table>
            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                <tbody>
                    <tr>
                        <td style="border: 2px solid;
                        padding: 8px;" class="highlighted-cell">Details of witness(es), if any:<br>
                (Name, position, contact number etc.)
                        </td>
                         <td style="border: 2px solid;
            padding: 8px;">
                            <div class="form-line">
                                {!! Form::textArea('details', null, ['class' => 'form-control  ', 'placeholder' => 'Description']) !!}
                                @error('details')
                                    <div class="invalid-feedback" style="color: #ef1010">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>                     
                         </td>
                         
                    </tr>
                   
                   
                </tbody>
                </table>
    
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <tbody>
                        <tr>
                            <td style="border: 2px solid;
                            padding: 8px;" class="highlighted-cell">Investigation Required
                            </td>
                             <td style="border: 2px solid;
                padding: 8px;">
                               <div class="form-line">
                                <input type="radio" name="investigation" value="Yes" class="ml-3"> Yes
                                <input type="radio" name="investigation" value="No" class="ml-3"> No
                
                            </div>                     
                             </td>
                             <td style="border: 2px solid;
                padding: 8px;" class="highlighted-cell">
                               RIDDOR reportable 
                           </td>
                           <td style="border: 2px solid;
                           padding: 8px;">
                                <div class="form-line">
                                <input type="radio" name="reportable" value="Yes" class="ml-3"> Yes
                                <input type="radio" name="reportable" value="No" class="ml-3"> No
                
                            </div>
                                      </td>
                        </tr>
                        <tr>
                             <td style="border: 2px solid;
                padding: 8px;" class="highlighted-cell"> Investigation Level:</td>
                             <td style="border: 2px solid;
                padding: 8px;">
                                <div class="form-line">
                                    <input type="radio" name="investigation_level" value="High" class="ml-3"> High
                                    <input type="radio" name="investigation_level" value="Med" class="ml-3"> Med
                                    <input type="radio" name="investigation_level" value="Low" class="ml-3"> Low
                                    <input type="radio" name="investigation_level" value="Minimal" class="ml-3"> Minimal
                                    <input type="radio" name="investigation_level" value="N/A" class="ml-3"> N/A
                                </div>  
                           </td>
                          <td style="border: 2px solid;
                padding: 8px;" class="highlighted-cell">
                            Entered in Accident Book
                            
                         </td>
                         <td style="border: 2px solid;
                padding: 8px;">
                            <div class="form-line">
                                <input type="radio" name="book" value="Yes" class="ml-3"> Yes
                                <input type="radio" name="book" value="No" class="ml-3"> No
                
                            </div>
                         </td>
                        </tr>
                       
                    </tbody>
                    </table>
                    <div class="form-group">
                        <label for="name"> Upload Images (only images not files) </label>
                    
                        @include('admin.general_incident.images_upload')
                    </div>
        </div>

    <div class="col-xs-12 aligne-center contentbtn">
        <button class="btn btn-primary waves-effect" type="submit">Save</button>
    </div>
</div>
