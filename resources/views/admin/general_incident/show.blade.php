@extends('admin.layout.app')

@section('title')
    Incident /Accident Form QMS-F-13
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <input type="button" value="Print Report" id="printDiv" class="btn btn-primary printBtn">
        </div>
        <div class="col-xs-12 printable_div" id="myDivToPrint">
            <div class="col-xs-12 printOnly">
                <div class="logo">
                    <img src="{{ asset('/_admin/assets/images/logo1.png') }}" alt="Mellors-img" title="Mellors"
                        class="image">
                </div>
                <h3 class="table-title"> Incident /Accident Form QMS-F-13 </h3>
            </div>
            <div class="row">
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                        <th style="border: 2px solid;  padding: 1px;">Department</th>
                        <th style="border: 2px solid;  padding: 1px;">Location</th>
                        <th style="border: 2px solid;  padding: 1px;">Date/Time of Event</th>
                        <th style="border: 2px solid;  padding: 1px;"> Ref No.</th>
                    </thead>
                    <tbody>
                        <td style="border: 2px solid;  padding: 1px;">
                            {!!  $accident->value['department_id'] == 1 ? 'Maintenance' : 
                            ($accident->value['department_id'] == 2 ? 'Operations' : 
                            ($accident->value['department_id'] == 3 ? 'Health & Safety' : 
                            ($accident->value['department_id'] == 4 ? 'Technical' : 'Client')))
                            !!}
                        </td>
                        <td style="border: 2px solid;  padding: 1px;">
                            <span> {{ $accident->park->name }} / {{ $accident->ride->name ?? $accident->text }}</span>

                        <td style="border: 2px solid;  padding: 1px;">
                            {!! $accident->value['date'] !!}
                        </td>
                        <td style="border: 2px solid;  padding: 1px;">
                            {!! $accident->value['report_reference_no'] !!}

                        </td>
                    </tbody>
                </table>

                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <tbody>
                        <td style="border: 2px solid;  padding: 1px;" class="highlighted-cell"><strong>Type Of
                            Event</strong>
                        </td>
                        <td style="border: 2px solid;
                            padding: 1px;">
                            <input type="radio" name="type_of_event" @if (isset($accident->value['type_of_event']) && $accident->value['type_of_event'] == 'Injury') checked @endif
                                value="Injury" class="ml-3"> Injury
                        </td>
                        <td style="border: 2px solid;
                            padding: 1px;">
                            <input type="radio" name="type_of_event" @if (isset($accident->value['type_of_event']) && $accident->value['type_of_event'] == 'Health') checked @endif
                                value="Health" class="ml-3"> Health
                        </td>
                        <td style="border: 2px solid;
                            padding: 1px;">
                            <input type="radio" name="type_of_event" @if (isset($accident->value['type_of_event']) && $accident->value['type_of_event'] == 'Near Miss') checked @endif
                                value="Near Miss" class="ml-3"> Near Miss
                        </td>
                        <td style="border: 2px solid;
                            padding: 1px;">
                            <input type="radio" name="type_of_event" @if (isset($accident->value['type_of_event']) && $accident->value['type_of_event'] == 'Incident') checked @endif
                                value="Incident" class="ml-3"> Incident
                        </td>
                        <td style="border: 2px solid;
                        padding: 1px;">
                        
                        <input type="radio" name="type_of_event" @if (isset($accident->value['type_of_event']) && $accident->value['type_of_event'] == 'Property Damage') checked @endif
                            value="Property Damage" class="ml-3"> Property Damage
                    </td>
                        <td style="border: 2px solid;
                        padding: 1px;">
                        
                        <input type="radio" name="type_of_event" @if (isset($accident->value['type_of_event']) && $accident->value['type_of_event'] == 'Unsafe Behaviour') checked @endif
                            value="Unsafe Behaviour" class="ml-3"> Unsafe Behaviour
                    </td>
                        <td style="border: 2px solid;
                        padding: 1px;">
                                        <input type="radio" name="type_of_event" @if (isset($accident->value['type_of_event']) && $accident->value['type_of_event'] == 'Other') checked @endif
                                            value="Other" class="ml-3"> Other
                                               {!! $accident?->value['other_of_type_of_event'] ?? '' !!}
                
                                    </td>
                    </tbody>
                </table>
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <tbody>
                        <td style="border: 2px solid;  padding: 1px;" class="highlighted-cell"><strong>Area of
                                Injury</strong>
                        </td>
                        <td style="border: 2px solid;
                            padding: 1px;">
                            <input type="radio" name="area_of_injury" @if (isset($accident->value['area_of_injury']) && $accident->value['area_of_injury'] == 'Head') checked @endif
                                value="Accident" class="ml-3"> Head
                        </td>
                        <td style="border: 2px solid;
                            padding: 1px;">
                            <input type="radio" name="area_of_injury" @if (isset($accident->value['area_of_injury']) && $accident->value['area_of_injury'] == 'Arms') checked @endif
                                value="Health" class="ml-3"> Arms
                        </td>
                        <td style="border: 2px solid;
                            padding: 1px;">
                            <input type="radio" name="area_of_injury" @if (isset($accident->value['area_of_injury']) && $accident->value['area_of_injury'] == 'Hands') checked @endif
                                value="Near Miss" class="ml-3"> Hands
                        </td>
                        <td style="border: 2px solid;
                            padding: 1px;">
                            <input type="radio" name="area_of_injury" @if (isset($accident->value['area_of_injury']) && $accident->value['area_of_injury'] == 'Torso') checked @endif
                                value="Incident" class="ml-3">
                                Torso
                        </td>
                        <td style="border: 2px solid;
                        padding: 1px;">
                            <input type="radio" name="area_of_injury" @if (isset($accident->value['area_of_injury']) && $accident->value['area_of_injury'] == 'Back') checked @endif
                                value="Incident" class="ml-3"> Back
                        </td>
                        <td style="border: 2px solid;
                    padding: 1px;">
                            <input type="radio" name="area_of_injury" @if (isset($accident->value['area_of_injury']) && $accident->value['area_of_injury'] == 'Legs') checked @endif
                                value="Incident" class="ml-3"> Legs
                        </td>
                        <td style="border: 2px solid;
                padding: 1px;">
                            <input type="radio" name="area_of_injury" @if (isset($accident->value['area_of_injury']) && $accident->value['area_of_injury'] == 'Feet') checked @endif
                                value="Incident" class="ml-3"> Feet
                        </td>
                        <td style="border: 2px solid;
            padding: 1px;">
                            <input type="radio" name="area_of_injury" @if (isset($accident->value['area_of_injury']) && $accident->value['area_of_injury'] == 'Fainting/Dizziness') checked @endif
                                value="Incident" class="ml-3"> Fainting/Dizziness
                        </td>
                        <td style="border: 2px solid;
                        padding: 1px;">
                                        <input type="radio" name="area_of_injury" @if (isset($accident->value['area_of_injury']) && $accident->value['area_of_injury'] == 'Other') checked @endif
                                            value="Incident" class="ml-3"> Other
                                               {!! $accident?->value['other_of_area_of_injury'] ?? '' !!}
                
                                    </td>
                    </tbody>
                </table>

                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <tbody>
                        <td style="border: 2px solid; padding: 1px;" class="highlighted-cell">

                            <strong> Harm (or Potential for Harm)</strong>
                        </td>


                        <td style="border: 2px solid;  padding: 1px;">
                            <input type="radio" name="harm" @if (isset($accident->value['harm']) &&  $accident->value['harm'] == 'Fatal or Major' ||  $accident->value['harm'] == 'Major or Fatal') checked @endif
                                value="Fatal or Major" class="ml-3"> Fatal or Major
                        </td>
                        <td style="border: 2px solid;  padding: 1px;">
                            <input type="radio" name="harm" @if (isset($accident->value['harm']) && $accident->value['harm'] == 'Serious') checked @endif
                                value="Serious" class="ml-3"> Serious
                        </td>
                        <td style="border: 2px solid;  padding: 1px;">

                            <input type="radio" name="harm" @if (isset($accident->value['harm']) && $accident->value['harm'] == 'Minor') checked @endif
                                value="Minor" class="ml-3"> Minor
                        </td>
                        <td style="border: 2px solid;  padding: 1px;">

                            <input type="radio" name="harm" @if (isset($accident->value['harm']) && $accident->value['harm'] == 'Property Damage') checked @endif
                                value="Property Damage" class="ml-3"> Property Damage
                        </td>
                    </tbody>
                </table>
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <tbody>
                        <tr>
                            <td style="border: 2px solid;  padding: 1px;" class="highlighted-cell">

                                <strong> Person Involved</strong>

                            </td>
                            <td style="border: 2px solid;  padding: 1px;">
                                Name:
                                {!! $accident?->value['name'] ?? ('' - $accident?->value['gender'] ?? '') !!}
                            </td>
                            <td style="border: 2px solid;  padding: 1px;">
                                Position:
                                {!! $accident?->value['position'] ?? '' !!}

                            </td>

                        </tr>
                        <tr>
                            <td style="border: 2px solid;  padding: 1px;"></td>
                            <td style="border: 2px solid;  padding: 1px;">
                                Address: {!! $accident?->value['address'] ?? '' !!}

                            </td>
                            <td style="border: 2px solid;  padding: 1px;">
                                Contact No:
                                {!! $accident?->value['phone'] ?? '' !!}

                            </td>
                        </tr>

                    </tbody>
                </table>
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <tbody>
                        <tr>
                            <td style="border: 2px solid;
                            padding: 1px;"
                                class="highlighted-cell"><strong>Brief Description of Event:<br>
                                    (Details of what happened, when,<br> where and any emergency action taken)
                                </strong></td>
                            <td style="border: 2px solid;  padding: 1px;">
                                <div class="form-line">
                                    <p>
                                        {!! nl2br(e($accident?->value['description'] ?? '')) !!}
                                    </p>
                                </div>
                            </td>

                        </tr>


                    </tbody>
                </table>
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <tbody>
                        <tr>
                            <td style="border: 2px solid;
                                padding: 1px;"
                                class="highlighted-cell"><strong>Details of witness(es), if any:<br>
                                    (Name, position, contact number etc.)</strong>
                            </td>
                            <td style="border: 2px solid;
                              padding: 1px;">
                                {!! nl2br(e($accident?->value['details'] ?? '')) !!}
            </div>
            </td>
            </tr>
            </tbody>
            </table>

            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                <tbody>
                    <tr>
                        <td style="border: 2px solid;
                                    padding: 1px;"
                            class="highlighted-cell"><strong>Investigation Required</strong>
                        </td>
                        <td style="border: 2px solid;  padding: 1px;">
                            <div class="form-line">
                                <input type="radio" name="investigation"
                                    @if (isset($accident->value['investigation']) && $accident->value['investigation'] == 'Yes' ||  $accident->value['investigation'] == 'yes') checked @endif value="Yes" class="ml-3">
                                Yes
                                <input type="radio" name="investigation"
                                    @if (isset($accident->value['investigation']) && $accident->value['investigation'] == 'No' ||  $accident->value['investigation'] == 'no') checked @endif value="No" class="ml-3"> No
                            </div>
                        </td>
                        <td style="border: 2px solid;  padding: 1px;" class="highlighted-cell">
                            <strong> RIDDOR reportable </strong>
                        </td>
                        <td style="border: 2px solid;
                                   padding: 1px;">
                            <div class="form-line">
                                <input type="radio" name="reportable" @if (isset($accident->value['reportable']) && $accident->value['reportable'] == 'Yes' ||$accident->value['reportable'] == 'yes') checked @endif
                                    value="Yes" class="ml-3">
                                Yes
                                <input type="radio" name="reportable" @if (isset($accident->value['reportable']) && $accident->value['reportable'] == 'No' || $accident->value['reportable'] == 'no') checked @endif
                                    value="No" class="ml-3">
                                No

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 2px solid;  padding: 1px;" class="highlighted-cell">
                            <strong> Investigation Level:</strong>
                        </td>
                        <td style="border: 2px solid;  padding: 1px;">
                            <div class="form-line">
                                <input type="radio" name="investigation_level"
                                    @if (isset($accident->value['investigation_level']) && $accident->value['investigation_level'] == 'High') checked @endif value="High" class="ml-3">
                                High
                                <input type="radio" name="investigation_level"
                                    @if (isset($accident->value['investigation_level']) && $accident->value['investigation_level'] == 'Med') checked @endif value="Med" class="ml-3">
                                Med
                                <input type="radio" name="investigation_level"
                                    @if (isset($accident->value['investigation_level']) && $accident->value['investigation_level'] == 'Low') checked @endif value="Low" class="ml-3">
                                Low
                                <input type="radio" name="investigation_level"
                                    @if (isset($accident->value['investigation_level']) && $accident->value['investigation_level'] == 'Minimal') checked @endif value="Minimal" class="ml-3">
                                Minimal
                                <input type="radio" name="investigation_level"
                                    @if (isset($accident->value['investigation_level']) && $accident->value['investigation_level'] == 'N/A') checked @endif value="N/A" class="ml-3">
                                N/A
                            </div>
                        </td>
                        <td style="border: 2px solid;  padding: 1px;" class="highlighted-cell">
                            <strong> Entered in Accident Book</strong>

                        </td>
                        <td style="border: 2px solid;  padding: 1px;">
                            <div class="form-line">
                                <input type="radio" name="book" @if (isset($accident->value['book']) && $accident->value['book'] == 'Yes' || $accident->value['book'] == 'yes') checked @endif
                                    value="Yes" class="ml-3">
                                Yes
                                <input type="radio" name="book" @if (isset($accident->value['book']) && $accident->value['book'] == 'No' || $accident->value['book'] == 'no') checked @endif
                                    value="No" class="ml-3">
                                No

                            </div>
                        </td>
                    </tr>

                </tbody>
            </table>
            <div class="form-group">
                @if (isset($images))
                    <div class="form-group">
                        <label class=" bold title">Images :</label>
                        <div class="form-line row">
                            @foreach ($images as $item)
                                <div class="col-lg-6">
                                    <div class="flex-img">
                                        <a>
                                            <img class="img"
                                                src="{{ Storage::disk('s3')->temporaryUrl('images/' . baseName($item->image), now()->addMinutes(30)) }}"
                                                style="height: 300px; width: 200px"></a>
                                    </div>
                                    <div class="image-comment">
                                        <p>{{ $item->comment }}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>


            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                    <th style="border: 2px solid; border-color: #0b0b0b;  padding: 1px;">Reported By</th>
                    <th style="border: 2px solid;border-color: #0b0b0b;  padding: 1px;">Position</th>
                    <th style="border: 2px solid;border-color: #0b0b0b;  padding: 1px;">Date</th>
                </thead>
                <tbody>
                    <td style="border: 2px solid; border-color: #0b0b0b;  padding: 1px;">
                        {!! $accident->created_by->name !!}
                    </td>
                    <td style="border: 2px solid; border-color: #0b0b0b;  padding: 1px;">
                        {{ $accident->created_by->roles()->first()->name }}
                    <td style="border: 2px solid;  padding: 1px;">
                        {!! $accident->date !!}
                    </td>

                </tbody>
            </table>

        </div>
    </div>
    </div>
@endsection
@push('scripts')
    <script language="javascript">
        $('#printDiv').click(function() {
            $('#myDivToPrint').show();
            window.print();
            return false;
        });
    </script>
@endpush
