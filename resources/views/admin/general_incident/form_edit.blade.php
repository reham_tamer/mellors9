
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
            <div class="row">
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                        <th style="border: 2px solid;  padding: 8px;">Department</th>
                        <th style="border: 2px solid;  padding: 8px;">Location</th>
                        <th style="border: 2px solid;  padding: 8px;">Date/Time of Event</th>
                        <th style="border: 2px solid;  padding: 8px;"> Ref No.</th>
                    </thead>
                    <tbody>
                        <td style="border: 2px solid;  padding: 8px;">
                            <div class="form-line">
                                <div class="form-line">
                                    {!! Form::select('department_id', $departments, $accident->value['department_id'], [
                                        'class' => 'form-control select2',
                                        'placeholder' => 'Choose Department...',
                                    ]) !!}
                                </div>
                        </td>
                        <td style="border: 2px solid;  padding: 8px;">
                            <div class="form-line">
                                <span> {{ $accident->park->name }} / {{ $accident->ride->name ?? $accident->text }}</span>
                            <input type="hidden" name="park_id" value="{{ $accident->park_id }}" >
                        </td>
                        <td style="border: 2px solid;  padding: 8px;">
                            <div class="form-line">
                                {!! Form::datetimeLocal('date', $accident->value['date'], [
                                    'class' => 'form-control' ,  
                                    'placeholder' => 'Start Time',
                                ]) !!} @error('date')
                                    <div class="invalid-feedback" style="color: #ef1010">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </td>
                        <td style="border: 2px solid;  padding: 8px;">
                            <div class="form-line">
                                {!! Form::input('text', 'report_reference_no', $accident->value['report_reference_no'] ?? '', [
                                    'class' => 'form-control  ',
                                    'placeholder' => 'Report Reference No',
                                ]) !!}

                            </div>
                        </td>
                    </tbody>
                </table>

                <table id="datatable-buttons" style="border: 2px solid;
        padding: 8px;"
                    class="table table-striped table-bordered dt-responsive nowrap">
                    <tbody>
                        <td style="border: 2px solid;  padding: 8px;" class="highlighted-cell">Type Of Incident
                        </td>
                        <td style="border: 2px solid;
                            padding: 8px;">
                            <input type="radio" name="type_of_event" @if (isset($accident->value['type_of_event']) && $accident->value['type_of_event'] == 'Injury') checked @endif
                            value="Injury" class="ml-3"> Injury
                        </td>
                        <td style="border: 2px solid;
                            padding: 8px;">
                            <input type="radio" name="type_of_event" @if (isset($accident->value['type_of_event']) && $accident->value['type_of_event'] == 'Health') checked @endif
                                value="Health" class="ml-3"> Health
                        </td>
                        <td style="border: 2px solid;
                            padding: 8px;">
                            <input type="radio" name="type_of_event" @if (isset($accident->value['type_of_event']) && $accident->value['type_of_event'] == 'Near Miss') checked @endif
                                value="Near Miss" class="ml-3"> Near Miss
                        </td>
                        <td style="border: 2px solid;
                        padding: 1px;">
                        <input type="radio" name="type_of_event" @if (isset($accident->value['type_of_event']) && $accident->value['type_of_event'] == 'Incident') checked @endif
                            value="Incident" class="ml-3"> Incident
                    </td>
                    <td style="border: 2px solid;
                    padding: 1px;">
                    
                    <input type="radio" name="type_of_event" @if (isset($accident->value['type_of_event']) && $accident->value['type_of_event'] == 'Property Damage') checked @endif
                        value="Property Damage" class="ml-3"> Property Damage
                   </td>
                        <td style="border: 2px solid;
                        padding: 8px;">
                        <input type="radio" name="type_of_event" @if (isset($accident->value['type_of_event']) && $accident->value['type_of_event'] == 'Unsafe Behaviour') checked @endif
                            value="Unsafe Behaviour" class="ml-3"> Unsafe Behaviour
                    </td>
                    <tr>
                        <td style="border: 2px solid;
                        padding: 8px;">
                        </td>
                    <td colspan="8" style="border: 2px solid;
                    padding: 8px;">
                     <input type="radio" name="type_of_event" value="Other" @if (isset($accident->value['type_of_event']) && $accident->value['type_of_event'] == 'Other') checked @endif class="ml-4"> Other
                    
                     <input type="text" value="{{ $accident?->value['other_of_type_of_event'] ?? '' }}" name="other_of_type_of_event" class="form-control">
                    </td>
                    </tr>
                    </tbody>
                </table>

                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <tbody>
                        <td style="border: 2px solid;  padding: 8px;" class="highlighted-cell">Harm (or
                            Potential for Harm)
                        </td>

                        <td style="border: 2px solid;  padding: 8px;">
                            <input type="radio" name="harm" @if (isset($accident->value['harm']) &&  $accident->value['harm'] == 'Fatal or Major' ||  $accident->value['harm'] == 'Major or Fatal') checked @endif
                                value="Fatal or Major" class="ml-3"> Major or Fatal  
                        </td>
                        <td style="border: 2px solid;  padding: 8px;">
                            <input type="radio" name="harm" @if (isset($accident->value['harm']) && $accident->value['harm'] == 'Serious') checked @endif
                                value="Serious" class="ml-3"> Serious
                        </td>
                        <td style="border: 2px solid;  padding: 8px;">

                            <input type="radio" name="harm" @if (isset($accident->value['harm']) &&  $accident->value['harm'] == 'Minor') checked @endif
                                value="Minor" class="ml-3"> Minor
                        </td>
                        
                    </tbody>
                </table>
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <tbody>
                        <td style="border: 2px solid;
                        padding: 8px;" class="highlighted-cell">Area of Injury
                        </td>
                         <td style="border: 2px solid;
                    padding: 8px;">
                            <input type="radio" name="area_of_injury" @if (isset($accident->value['area_of_injury']) &&  $accident->value['area_of_injury'] == 'Head') checked @endif value="Head" class="ml-4">Head  
                        </td>
                         <td style="border: 2px solid;
                    padding: 8px;">
                            <input type="radio" name="area_of_injury" value="Arms" @if (isset($accident->value['area_of_injury']) && $accident->value['area_of_injury'] == 'Arms') checked @endif class="ml-4"> Arms
                        </td>
                         <td style="border: 2px solid;
                    padding: 8px;">
                            <input type="radio" name="area_of_injury" value="Hands" @if (isset($accident->value['area_of_injury']) &&  $accident->value['area_of_injury'] == 'Hands') checked @endif class="ml-4"> Hands
                        </td>
                        <td style="border: 2px solid;
                        padding: 8px;">
                                <input type="radio" name="area_of_injury" value="Torso" @if (isset($accident->value['area_of_injury']) &&  $accident->value['area_of_injury'] == 'Torso') checked @endif class="ml-4"> Torso
                            </td>
                            <td style="border: 2px solid;
                            padding: 8px;">
                                    <input type="radio" name="area_of_injury" value="Back" @if (isset($accident->value['area_of_injury']) &&  $accident->value['area_of_injury'] == 'Back') checked @endif class="ml-4"> Back
                                </td>
                        <td style="border: 2px solid;
                        padding: 8px;">
                        <input type="radio" name="area_of_injury" value="Legs" @if (isset($accident->value['area_of_injury']) && $accident->value['area_of_injury'] == 'Legs') checked @endif class="ml-4"> Legs
                    </td>
                    <td style="border: 2px solid;
                    padding: 8px;">
                            <input type="radio" name="area_of_injury" value="Back" @if (isset($accident->value['area_of_injury']) &&  $accident->value['area_of_injury'] == 'Feet') checked @endif class="ml-4"> Feet
                        </td>
                        <td style="border: 2px solid;
                        padding: 8px;">
                            <input type="radio" name="area_of_injury" value="Fainting/Dizziness" @if (isset($accident->value['area_of_injury']) &&  $accident->value['area_of_injury'] == 'Fainting/Dizziness') checked @endif class="ml-4"> Fainting/Dizziness
                        </td>
                        <tr>
                            <td style="border: 2px solid;
                            padding: 8px;">
                            </td>
                        <td colspan="8" style="border: 2px solid;
                        padding: 8px;">
                         <input type="radio" name="area_of_injury" value="Other" @if (isset($accident->value['area_of_injury']) && $accident->value['area_of_injury'] == 'Other') checked @endif class="ml-4"> Other
                        
                         <input type="text" value="{{ $accident?->value['other_of_area_of_injury'] ?? '' }}" name="other_of_area_of_injury" class="form-control">
                        </td>
                        </tr>
                    </tbody>
                </table>
    
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <tbody>
                        <tr>
                            <td style="border: 2px solid;  padding: 8px;" class="highlighted-cell">
                                Person Involved
                            </td>
                            <td style="border: 2px solid;  padding: 8px;">
                                Name:
                                {!! Form::input('text', 'name', $accident?->value['name'] ?? '', ['class' => 'form-control' ,  ]) !!}
                                @error('time')
                                    <div class="invalid-feedback" style="color: #ef1010">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </td>
                            <td style="border: 2px solid;  padding: 8px;">
                                Position:
                                {!! Form::input('text', 'position', $accident?->value['position'] ?? '', ['class' => 'form-control' ,  ]) !!}
                                @error('position')
                                    <div class="invalid-feedback" style="color: #ef1010">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </td>

                        </tr>
                        <tr>
                            <td style="border: 2px solid;
                            padding: 8px;"></td>
                                        
                            <td style="border: 2px solid;
                            padding: 8px;">
                                            Gender: 
                                            <select name="gender" class="form-control">
                                                <option value="">Select Gender</option>
                                                <option value="Child" @if (($accident->value['gender'] ?? '') == 'Male') @selected(true) @endif>Male</option>
                                                <option value="Teen" @if (($accident->value['gender'] ?? '') == 'Female') @selected(true) @endif>Female</option>
                                                <option value="Adult" @if (($accident->value['gender'] ?? '') == 'Other') @selected(true) @endif>Other</option>
                                            </select>
                                                @error('gender')
                                                    <div class="invalid-feedback" style="color: #ef1010">
                                                        {{ $message }}
                                                    </div> 
                                                    @enderror                     
                                         </td>
                                         <td style="border: 2px solid;
                            padding: 8px;">
                                            Age:

                                            {!! Form::input('number', 'age', $accident?->value['age'] ?? '', ['class' => 'form-control']) !!}
                                            @error('age')
                                                    <div class="invalid-feedback" style="color: #ef1010">
                                                        {{ $message }}
                                                    </div>
                                                @enderror 
                                       </td>
                                      
                                    </tr>
                        <tr>
                            <td style="border: 2px solid;  padding: 8px;"></td>
                            <td style="border: 2px solid;  padding: 8px;">Address
                                {!! Form::input('text', 'address', $accident?->value['address'] ?? '', ['class' => 'form-control' ,  ]) !!}
                                @error('address')
                                    <div class="invalid-feedback" style="color: #ef1010">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </td>
                            <td style="border: 2px solid;  padding: 8px;">
                                Contact No:
                                {!! Form::input('text', 'phone', $accident?->value['phone'] ?? '', ['class' => 'form-control' ,  ]) !!}
                                @error('phone')
                                    <div class="invalid-feedback" style="color: #ef1010">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </td>
                        </tr>

                    </tbody>
                </table>
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <tbody>
                        <tr>
                            <td style="border: 2px solid;
                            padding: 8px;" class="highlighted-cell">Brief Description of Event:<br>
                                (Details of what happened, when,<br> where and any emergency action taken)
                            </td>
                            <td style="border: 2px solid;  padding: 8px;">
                                <div class="form-line">
                                    {!! Form::textArea('description', $accident?->value['description'] ?? '', [
                                        'class' => 'form-control  ',
                                        'placeholder' => 'Description',
                                    ]) !!} @error('description')
                                        <div class="invalid-feedback" style="color: #ef1010">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </td>

                        </tr>


                    </tbody>
                </table>
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <tbody>
                        <tr>
                            <td style="border: 2px solid;
                                padding: 8px;"
                                class="highlighted-cell">Details of witness(es), if any:<br>
                                (Name, position, contact number etc.)
                            </td>
                            <td style="border: 2px solid;
                              padding: 8px;">
                                <div class="form-line">
                                    {!! Form::textArea('details', $accident?->value['details'] ?? '', [
                                        'class' => 'form-control  ',
                                        'placeholder' => 'Description',
                                    ]) !!} @error('details')
                                        <div class="invalid-feedback" style="color: #ef1010">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <tbody>
                        <tr>
                            <td style="border: 2px solid;
                                    padding: 8px;"
                                class="highlighted-cell">Investigation Required
                            </td>
                            <td style="border: 2px solid;  padding: 8px;">
                                <div class="form-line">
                                    <input type="radio" name="investigation"
                                    @if (isset($accident->value['investigation']) && $accident->value['investigation'] == 'Yes' ||  $accident->value['investigation'] == 'yes') checked @endif value="Yes" class="ml-3">
                                    Yes
                                    <input type="radio" name="investigation"
                                    @if (isset($accident->value['investigation']) && $accident->value['investigation'] == 'No' ||  $accident->value['investigation'] == 'no') checked @endif value="No" class="ml-3"> No
                                </div>
                            </td>
                            <td style="border: 2px solid;  padding: 8px;" class="highlighted-cell">
                                RIDDOR reportable
                            </td>
                            <td style="border: 2px solid;
                                   padding: 8px;">
                                                <div class="form-line">
                                                    <input type="radio" name="reportable" @if (isset($accident->value['reportable']) && $accident->value['reportable'] == 'Yes' ||$accident->value['reportable'] == 'yes') checked @endif
                                                        value="Yes" class="ml-3">
                                                    Yes
                                                    <input type="radio" name="reportable" @if (isset($accident->value['reportable']) && $accident->value['reportable'] == 'No' || $accident->value['reportable'] == 'no') checked @endif
                                                        value="No" class="ml-3">
                                                    No
                    
                                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td style="border: 2px solid;  padding: 8px;" class="highlighted-cell">
                                Investigation Level:</td>
                            <td style="border: 2px solid;  padding: 8px;">
                                <div class="form-line">
                                    <input type="radio" name="investigation_level"
                                        @if ($accident?->value['investigation_level'] == 'High') checked @endif value="High" class="ml-3">
                                    High
                                    <input type="radio" name="investigation_level"
                                        @if ($accident?->value['investigation_level'] == 'Med') checked @endif value="Med" class="ml-3">
                                    Med
                                    <input type="radio" name="investigation_level"
                                        @if ($accident?->value['investigation_level'] == 'Low') checked @endif value="Low" class="ml-3">
                                    Low
                                    <input type="radio" name="investigation_level"
                                        @if ($accident?->value['investigation_level'] == 'Minimal') checked @endif value="Minimal" class="ml-3">
                                    Minimal
                                    <input type="radio" name="investigation_level"
                                        @if ($accident?->value['investigation_level'] == 'N/A') checked @endif value="N/A" class="ml-3">
                                    N/A
                                </div>
                            </td>
                            <td style="border: 2px solid;  padding: 8px;" class="highlighted-cell">
                                Entered in Accident Book

                            </td>
                            <td style="border: 2px solid;  padding: 8px;">
                                  <div class="form-line">
                                <input type="radio" name="book" @if (isset($accident->value['book']) && $accident->value['book'] == 'Yes' || $accident->value['book'] == 'yes') checked @endif
                                    value="Yes" class="ml-3">
                                Yes
                                <input type="radio" name="book" @if (isset($accident->value['book']) && $accident->value['book'] == 'No' || $accident->value['book'] == 'no') checked @endif
                                    value="No" class="ml-3">
                                No

                            </div>
                            </td>
                        </tr>

                    </tbody>
                </table>
                <div class="form-group">
                    @if (isset($album))
                    <label class="form-label">Images :</label>
                    <div class="row">
                        @foreach ($album as $item)
                        <div class="col-lg-6">
                            <div class="flex-img">
                                <a >
                                <img class="img-preview" src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($item->image),now()->addMinutes(30)) }}"
                                style="height: 40px; width: 40px"></a> 
                                <span>{{$item->comment}}</span>

                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="name"> Upload Images (only images not files) </label>
                
                    @include('admin.general_incident.images_upload')
                </div>
                <div class="col-xs-12 aligne-center contentbtn">
                    <button class="btn btn-primary waves-effect" type="submit">Save</button>
                </div>
            </div>
           