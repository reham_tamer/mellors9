@extends('admin.layout.app')

@section('title')
    Accident / Incident
@endsection

@section('content')

    <div class="card-box">
        <form class="formSection" action="{{url('/search_incident/')}}" method="GET">

            @csrf
            <div class="row">
                <div class='col-md-4'>
                    <div class="form-group">
                        <label for="middle_name">Time Slot Date </label>
                        {!! Form::date('date',request()->date ?? \Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date_to']) !!}
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <label for="middle_name">Park </label>
                        {!! Form::select('park_id', $userparks, request()->park_id, [
                            'class' => 'form-control select2 Parks',
                            'placeholder' => 'Choose Park...',
                        ]) !!}
                    </div>
                </div>
                <div class='col-md-2 mtButton'>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary save_btn waves-effect">Show</button>
                    </div>
                </div>
            </div>
            {!!Form::close() !!}
            <br>
        <a class="input-group-btn" href="{{ route('admin.incident.create') }}">
            <button type="button" class="btn waves-effect waves-light btn-primary">Add Accident / Incident Form QMS-F-13</button>
        </a>
        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <div class='responsive-wrapper'>
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1" aria-sort="ascending">ID
                                </th>
                                <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1" aria-sort="ascending">Reference No
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Date
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Type Of Event
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Person Name
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Location
                            </th>
                            @if(auth()->user()->can('addStatment'))
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Witness Statement
                                </th>
                                @endcan
                                @if(auth()->user()->can('incident-approve-manager'))

                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Manager Verfication
                                </th>
                                @endif
                                @if(auth()->user()->can('incident-approve-director'))

                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Director Verfication
                                </th>
                                @endif
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Process
                                </th>
                            </tr>
                        </thead>

                        <tbody>

                            @foreach ($items as $item)
                                @if($item->value)
                                <tr role="row" class="odd" id="row-{{ $item->id }}">
                                    <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                    <td tabindex="0" class="sorting_1">{{  $item->value['report_reference_no'] }}</td>
                                    <td>
                                       <?php
                                        $originalDate = $item->value['date'];
                                        $formattedDate = Carbon\Carbon::parse($originalDate)->format('Y-m-d H:i');$item->value['date']
                                        ?>
                                        {{ $formattedDate }}
                                    </td>
                                    <td>{{ $item->value['type_of_event'] }}</td>
                                    <td>{{ $item->value['name'] }}</td>
                                    <td>{{ $item->park->name }} / {{ $item->ride->name ??  $item->text }} </td>
                                    @if(auth()->user()->can('addStatment'))
                                   <td>
                                     <a href="{{ url('show_statment/' . $item->id) }}"
                                        class="btn btn-primary"><i class="fa fa-plus"></i>  Witness Statment </a>
                                   </td>
                                   @endcan
                                   @if(auth()->user()->can('incident-approve-manager'))
                                   <td>
                                        @if($item->status=='pending')
                                        <a href="{{url('incident/'.$item->id.'/approve')}}"
                                        class="btn btn-xs btn-danger"><i class="fa fa-check"></i> Approve</a>
                                        @endif
                                        @if($item->status=='approved')
                                       <span>Verified</span>
                                          @endif
                                    </td>
                                       @endif
                                       @if(auth()->user()->can('incident-approve-director'))

                                      <td>
                                          @if($item->director_approve=='pending')
                                          <a href="{{url('incident/'.$item->id.'/approve_director')}}"
                                          class="btn btn-xs btn-danger"><i class="fa fa-check"></i> Approve</a>
                                          @endif
                                          @if($item->director_approve=='approved')
                                         <span>Verified</span>
                                            @endif
                                      </td>
                                      @endif

                                    {!! Form::open([
                                        'route' => ['admin.incident.destroy', $item->id],
                                        'id' => 'delete-form' . $item->id,
                                        'method' => 'Delete',
                                    ]) !!}
                                    {!! Form::close() !!}
                                    <td>
                                        @if(auth()->user()->can('incident-edit'))
                                        <a href="{{ route('admin.incident.edit', $item) }}" class="btn btn-info">Edit</a>
                                        @endif

                                        @if(auth()->user()->can('incident-list'))

                                        <a href="{{ route('admin.incident.show', $item->id) }}" class="btn btn-info">show</a>
                                        @endif

                                        @if(auth()->user()->can('incident-delete'))

                                        <a class="btn btn-danger" data-name=""
                                            data-url="{{ route('admin.incident.destroy', $item) }}"
                                            onclick="delete_form(this)">
                                            Delete
                                        </a>
                                        @endif

                                    </td>
                                </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    @include('admin.datatable.scripts')
@endsection
