@extends('admin.layout.app')

@section('title')
   Operator Performance Evaluation
@endsection

@section('content')
    <div class="card-box">
        <form class="formSection" action="{{url('/search_operator_evaluation/')}}" method="GET">
            @csrf
            <div class="row">
                <div class='col-md-3'>
                    <div class="form-group">
                        <label for="last_name">Select Park</label>
                        {!! Form::select('park_id',$userparks,request()->park_id, array('class' => 'form-control park','id'=>'park','placeholder'=>'Choose Park', 'required' => 'required') ) !!}
                    </div>
                </div>
                <div class='col-md-3'>
                    <div class="form-group">
                        <label for="supervisor_id">Select Operator </label>
                        {!! Form::select('operator_id',$users?? [],null, array('class' => 'form-control supervisor','id'=>'supervisor','placeholder'=>'Choose Operator...')) !!}
                    </div>
                </div>

                <div class='col-md-3'>
                    <div class="form-group">
                        <label for="middle_name">Time Slot Date From </label>
                        {!! Form::date('date',request()->date ?? \Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date', 'required' => 'required']) !!}
                    </div>
                </div>
                <div class='col-md-3'>
                    <div class="form-group">
                        <label for="middle_name">Time Slot Date To </label>
                        {!! Form::date('date_to',request()->date_to ?? \Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date_to', 'required' => 'required']) !!}
                    </div>
                </div>

                <div class='col-md-12 mtButton ' style="text-align: center">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary save_btn waves-effect">Show</button>
                    </div>
                </div>
            </div>
            {!!Form::close() !!}
            <br>
            <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1" aria-sort="ascending">ID
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Date
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Operator Name
                               </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Total Score
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Ride
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Park
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                   Status
                                </th>

                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Process
                                </th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach ($items as $item)
                                <tr role="row" class="odd" id="row-{{ $item->id }}">
                                    <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                    <td>{{ $item->date}}</td>
                                    <td>{{ $item->operator?->name }}</td>
                                    <td>{{ $item->total_score }}</td>
                                    <td>{{ $item->ride?->name }}</td>
                                    <td>{{ $item->park?->name }}

                                        <td>
                                            @if ($item->approve == 'pending')
                                                <a href="{{ url('evaluations/' . $item->id . '/approve') }}"
                                                    class="btn btn-xs btn-danger"><i class="fa fa-check"></i> Approve</a>
                                            @else
                                                <span>Verified</span>
                                            @endif

                                    </td>
                                        <td>


                                       @if(auth()->user()->can('operator-evaluation-list'))

                                        <a href="{{ route('admin.operator-evaluation.show', $item->id) }}"
                                              class="btn btn-info">show</a>
                                                <a href="{{ route('admin.operator-evaluation.edit', $item->id) }}"
                                                   class="btn btn-info">edit</a>
                                      @endcan

                                            {!!Form::open( ['route' => ['admin.operator-evaluation.destroy',$item->id] ,'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                                            {!!Form::close() !!}
                                            @if(auth()->user()->can('operator-evaluation-delete'))
                                            <a class="btn btn-danger" data-name=""
                                               data-url="{{ route('admin.operator-evaluation.destroy', $item) }}"
                                               onclick="delete_form(this)">
                                               Delete
                                            </a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
@endsection
@section('footer')
    @include('admin.datatable.scripts')
@endsection
@push('scripts')

    <script type="text/javascript">
        $("#park").change(function () {
            $.ajax({
                url: "{{ route('admin.getOperatorByPark') }}?park_id=" + $(this).val(),
                method: 'GET',
                success: function (data) {
                    $('#supervisor').html(data.html);
                }
            });
        });

    </script>
@endpush
