<div class="font-bold m-3 text-center">
    <span class="font-bold m-3 text-danger" >Total Count</span> :  @if (isset($items))
        {{$items->count()}}
    @else
        0
    @endif
</div>
@if(isset($items) && count($items) >0)
    <div class="form-group left row col-md-12">
        <a class="btn btn-info" href="{{ route('admin.export.evacuation',$items->pluck('id')) }}">Export File Csv</a>

    </div>
    <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
        <div class="row">

            <div class="col-xs-12 ">
                <div class="col-xs-12 ">

                </div>
                <div class="col-xs-12">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Park
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Ride
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Total rider
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                First rider off
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Last rider off
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                First aid
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Evacuation details
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Customer service gesture
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Portal overview
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Time Slot Date
                            </th>

                        </tr>
                        </thead>

                        <tbody>
                        @if (isset($items))
                            @foreach ($items as $item)
                                <tr role="row" class="odd" id="row-{{ $item->id }}">
                                    <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                    <td>{{ $item->park?->name ?? '' }}</td>
                                    <td>{{ $item->ride?->name ?? '' }}</td>
                                    <td>{{ $item->total_rider }}</td>
                                    <td>{{  $item->first_rider_off }} </td>
                                    <td> {{ $item->last_rider_off }}</td>
                                    <td>{!!  $item->first_aid !!}</td>
                                    <td>{!! $item->evacuation_details  !!}</td>
                                    <td> {!! $item->customer_service_gesture   !!}</td>
                                    <td> {!! $item->portal_overview !!}</td>

                                    <td>{{ $item->submit_date }}</td>
                                </tr>
                            @endforeach
                        @endif

                        </tbody>
                    </table>


                </div>
            </div>
        </div>

    </div>
@endif
