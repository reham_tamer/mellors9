{{--@include('admin.common.errors')--}}
<div class="row">
<div class="col-xs-12">
<div class="form-group form-float">
    <label class="form-label">Inspection Element Name</label>
    <div class="form-line">
        {!! Form::text("name",null,['class'=>'form-control','placeholder'=>' Inspection Element'])!!}
        @error('name')
        <div class="invalid-feedback" style="color: #ef1010">
            {{ $message }}
        </div>
        @enderror
    </div>
</div>
<div class="col-xs-3">
    <div class="form-group form-float">
        <label class="form-label">Element Type</label>
        <div class="form-line">
            {!! Form::select('type', ['daily'=>'Daily Inspection','inspection'=>'Inspection','preopening'=>'PreOpening','preclosing'=>'PreClosing'],null, array('class' =>
            'form-control','id'=>'type','placeholder'=>'Choose type... ')) !!}
       
        </div>
    </div>
</div>
<div class="col-xs-12 aligne-center contentbtn">
    <button class="btn btn-primary waves-effect" type="submit">Save</button>
</div>
</div>
</div>