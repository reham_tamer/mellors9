@extends('admin.layout.app')

@section('title')
Users Actions
@endsection

@section('content')

    <div class="card-box">

       
        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <form class="formSection" action="{{url('/actions_search/')}}" method="GET">
                    @csrf
                    <div class="row">
                        <div class='col-md-3'>
                            <div class="form-group">
                                <label for="middle_name">Time Slot Date From </label>
                                {!! Form::date('date',request()->date ?? \Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date', 'required' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12 mtButton ' style="text-align: center">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary save_btn waves-effect">Show</button>
                        </div>
                    </div>
                </form>
                <div class="col-sm-12">
                    <div class='responsive-wrapper'>
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Action
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Action Time
                            </th>

                        </tr>
                        </thead>

                        <tbody>

                        @foreach ($actions as $item)
                            <tr role="row" class="odd" id="row-{{ $item->id }}">
                                <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                <td>{{$item->user->name }} - {{  $item->action }}</td>
                                <td>{{ $item->created_at }}</td>
                            </tr>

                        @endforeach

                        </tbody>
                    </table>


                </div>
                </div>
            </div>

        </div>
    </div>


@endsection
@section('footer')
    @include('admin.datatable.scripts')
@endsection



