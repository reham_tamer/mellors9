@extends('admin.layout.app')

@section('title')
    Assign Country To Ride
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30"> Assign Country To Ride </h4>
                {!!Form::open( ['route' => 'admin.updateRideCountry' ,'class'=>'form phone_validate', 'method' => 'Post', 'enctype'=>"multipart/form-data",'class'=>'form-horizontal','files' => true]) !!}
                @csrf
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group form-float">
                            <label class="form-label">Country</label>
                            <div class="form-line">
                                <select name="country_id" class="form-control" id="park">
                                    <option value=""> Choose Country...</option>
                                    @foreach($countries as $country)
                                        <option
                                            value="{{ $country->id }}" {{ $country->id == $ride->country_id ? 'selected' : '' }} > {{ $country->name}}</option>
                                    @endforeach

                                </select>

                                @error('country_id')
                                <div class="invalid-feedback" style="color: #ef1010">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <input type="hidden" value="{{$ride->id}}" name="ride_id">
                    <div class="col-xs-12 aligne-center contentbtn">
                        <button class="btn btn-primary waves-effect" type="submit">Save</button>
                    </div>
                </div>

                {!!Form::close() !!}
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->
@endsection
@push('scripts')
    {!! JsValidator::formRequest(\App\Http\Requests\Dashboard\Ride\RideParkRequest ::class, '#form'); !!}
@endpush
