@extends('admin.layout.app')

@section('title')
    Parks
@endsection

@section('content')

     <div class="card-box">
        <p class="text-muted font-14 mb-3">
            @if(auth()->user()->can('add-availabilty-report'))

            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#extendModal1">
                <i class="fa fa-edit"></i> Add an Old Availability Report
            </button>
            @endif
            <div class="modal fade" id="extendModal1" tabindex="-1" aria-hidden="true" role="dialog">

                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">
                                choose Park 
                            </div>
                       <div class="modal-body">

                        {!!Form::open( ['route' => 'admin.addOLdAvailabilityReport' ,'class'=>'form phone_validate',
                                      'method' => 'get',
                                      'class'=>'form-horizontal']) !!}
                        <br>
                            <label class="form-label"> Parks </label>
                            <div class="form-line">
                                {!! Form::select('park_id', $items->pluck('name','id'),null,
                                array('class' =>
                                'form-control '))
                                !!}                           
                            </div>
                        </div>
                       </div>
                    <div class="modal-footer">
                            <button class="btn btn-primary waves-effect saveProject" type="submit">Add
                            </button>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>

       
        </p> 
        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Parks
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Branch
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Availability Report
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Process
                            </th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach ($items as $item)

                            <tr role="row" class="odd" id="row-{{ $item->id }}">
                                <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->branches->name }}</td>
                                <td>
                                    {!!Form::open( ['route' => ['admin.availability_reports.destroy',$item->id]
                                    ,'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                                    {!!Form::close() !!}
                                    @if(auth()->user()->can('add-availabilty-report'))
                                        @if(in_array($item->id, $items_check_second))
                                            <a href="{{ route('admin.availability_reports.edit', $item->id) }}"
                                               class="btn btn-success"><i class="fa fa-edit"></i> Update First & Second
                                                Status</a>
                                        @elseif(in_array($item->id, $items_check))

                                            <a href="{{ route('admin.availability_reports.edit', $item->id) }}"
                                               class="btn btn-success"><i class="fa fa-edit"></i> Add Second Status</a>
                                            <a class="btn btn-danger" data-name="availability report"
                                               data-url="{{ route('admin.availability_reports.destroy', $item) }}"
                                               onclick="delete_form(this)">
                                                <i class="fa fa-close"></i>
                                            </a>
                                           
                                        @else
                                            <a href="{{ route('admin.addAvailabilityReport', $item->id) }}"
                                               class="btn btn-info">Add First Status</a>

                                        @endif
                                    @endif
                                </td>

                                {!!Form::open( ['route' => ['admin.parks.destroy',$item->id] ,'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                                {!!Form::close() !!}
                                <td>
                                    @if(auth()->user()->can('parks-edit'))
                                        <a href="{{ route('admin.parks.edit', $item) }}"
                                           class="btn btn-info">Edit</a>
                                    @endif
                                    @if(auth()->user()->can('parks-delete'))

                                        <a class="btn btn-danger" data-name="{{ $item->name }}"
                                           data-url="{{ route('admin.parks.destroy', $item) }}"
                                           onclick="delete_form(this)">
                                            Delete
                                        </a>
                                    @endif

                                </td>

                            </tr>

                        @endforeach

                        </tbody>
                    </table>


                </div>
            </div>

        </div>
    </div>
</div>  

@endsection

@section('footer')
    @include('admin.datatable.scripts')
@endsection


