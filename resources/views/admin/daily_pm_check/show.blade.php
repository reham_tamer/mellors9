@extends('admin.layout.app')

@section('title')
Daily PM Operational H&S Checklist 
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <input type="button" value="Print Report" id="printDiv" class="btn btn-primary printBtn"></input>
        </div>
        <div class="col-xs-12 printable_div " id="myDivToPrint">
            <div class="report-head">
                <div class="report-body">
                    <h3 class="report-title">Daily PM Operational H&S Checklist </h3>
                </div>
                <div class="report-logo">
                    <img src="{{ asset('/_admin/assets/images/logo1.png') }}" alt="Mellors-img" title="Mellors"
                        class="image">
                </div>
            </div>
            <div class="row">
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <tr role="row">
                            <td   class="type-header">Inspector Name </td>
                            <td> {{ $list->user->name }}</td>
                            <td  class="type-header">Date </td>
                            <td>{{ $list->date }}</td>
                        </tr>
                        <tr role="row">
                            <td  class="type-header">Inspector Signature </td>
                            <td>
                               @if ($list->signature != null)
                                <a>
                                    <img class="img-preview"
                                    src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($list->signature),now()->addMinutes(30)) }}"
                                    style="height: 40px; width: 40px">
                                </a> 
                                @endif                           </td>
                            <td   class="type-header"> Project Name </td>
                            <td> {{ $list->park->name }} </td>
                        </tr>
                </table>
            
                    @if (isset($items) && count($items) > 0)
                        <div class="row">
                    <div class='responsive-wrapper'>
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                    <tr role="row" class="type-header">
                                        <th style="text-align: center;">CHECKPOINTS</th>
                                        <th>Status</th>
                                        <th>Comments</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $groupedItems = $items->groupBy('question.question_type');
                                    @endphp

                                    @foreach ($groupedItems as $questionType => $group)
                                       
                                    @if($questionType)

                                    <tr >
                                            <td colspan="4" class="type-header">
                                                <strong> {{ $questionType }}</strong>
                                            </td>
                                    </tr>
                                    @endif
                                        @foreach ($group as $value)
                                            <tr>
                                                <td>{{ $value->question->name ?? '' }}</td>
                                                <td>{{ $value->answer == 0 ?'No' :'Yes'   }} </td>
                                                <td>{{ $value->comment }} </td>
                                            </tr>
                                            @if($value->question->is_rides_question === 1)
                                            @foreach($rides as $ride)
                                            <tr>
                                                <td>
                                                   <b> Ride Name: {{ $ride->ride->name }} </b>
                                                </td>
                                                <td colspan="2">
                                                    {{ $ride->comment_ride }} 
                                                </td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="form-group">
                                @if (isset($images))
                                    <div class="form-group">
                                        <label class=" bold title">Images :</label>
                                        <div class="form-line row">
                                            @foreach ($images as $item)
                                                <div class="col-lg-6">
                                                    <div class="flex-img">
                                                        <a>
                                                            <img class="img"
                                                                src="{{ Storage::disk('s3')->temporaryUrl('images/' . baseName($item->image), now()->addMinutes(30)) }}"
                                                                style="height: 300px; width: 200px"></a>
                                                    </div>
                                                    <div class="image-comment">
                                                        <p>{{ $item->comment }}</p>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        </div>
                   
                    @endif
                <label>Additional Comments</label>
                 <p> {{ $list->additional_comment}}</p>
                
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script language="javascript">
        $('#printDiv').click(function() {
            $('#myDivToPrint').show();
            window.print();
            return false;
        });
    </script>
@endpush
