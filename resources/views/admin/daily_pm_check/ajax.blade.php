<div class="font-bold m-3 text-center">
    <span class="font-bold m-3 text-danger" >Total Count</span> :  @if (isset($items)) {{$items->count()}}@else 0 @endif
</div>
<div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
    <div class="row">
        <div class="col-sm-12">
            <div id="items">

            <div class='responsive-wrapper'>
            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                    <tr role="row">
                        <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1" aria-sort="ascending">ID
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Daily PM Operational H&S Checklist 
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Created At
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Process
                        </th>
                       
                    </tr>
                </thead>

                <tbody>

                     @foreach ($items as $item)
                        <tr role="row" class="odd" id="row-{{ $item->id }}">
                            <td tabindex="0" class="sorting_1">{{ $loop->iteration }}</td>
                            <td> LIST {{ $loop->iteration }}</td>
                            <td>{{ $item->date }}</td>
                            <td>
                                 {{--    <a href="{{ route('admin.editAmList', $item->id) }}">
                                        <button type="button" id="add" class="add btn btn-success">
                                            <i class="fa fa-edit"></i>Edit List
                                        </button>
                                    </a> --}}
                                    <a href="{{ route('admin.showPmList', $item->id) }}">
                                        <button type="button" id="add" class="add btn btn-primary">
                                            <i class="fa fa-info"></i>  Show / print List
                                        </button>
                                    </a>
                            </td>
                          
                        </tr>
                    @endforeach 

                </tbody>
            </table>
        </div>
        </div>
    </div>
    </div>
</div>


