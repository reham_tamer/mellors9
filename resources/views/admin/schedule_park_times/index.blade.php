@extends('admin.layout.app')

@section('title')
    Schedule Parks Time Slot
@endsection

@section('content')

    <div class="card-box">

        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Park
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Open Date
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Open Time
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Close Date
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Close Time
                            </th>


                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Process
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($items as $item)

                                <tr role="row" class="odd" id="row-{{ $item->id }}">

                                    <td tabindex="0" class="sorting_1">{{ $item->id }}
                                        <input type="hidden" name="park_time_id" id="park-time-id" class="park-time-id"
                                               value="{{ $item->id }}">
                                    </td>
                                    <td>{{ $item->park->name }}</td>
                                    <td>{{ $item->start_date }}</td>
                                    <td>{{ $item->start }}</td>
                                    <td>{{ $item->end_date }}</td>
                                    <td>{{ $item->end }}</td>

                                   <td>

                                       <a href="{{ route('admin.schedule_park_times.edit', $item) }}" class="btn btn-info">Edit</a>
                                       <a class="btn btn-danger" data-name="{{ $item->name }}"
                                          data-url="{{ route('admin.schedule_park_times.destroy', $item) }}"
                                          onclick="delete_form(this)">
                                           Delete
                                       </a>
                                   </td>


                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script type="text/javascript">
        $("#ClientStore").popover({
            title: '<h4>Add Daily Entrance Count</h4>',
            container: 'body',
            placement: 'bottom',
            html: true,
            content: function () {
                return $('#popover-form').html();
            }
        });
    </script>
@endpush

@section('footer')
    @include('admin.datatable.scripts')
@endsection
