@extends('admin.layout.app')

@section('title')
All Rides Stoppages
@endsection

@section('content')

<div class="card-box">


    <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
        <div class="row">
            <div class="col-sm-12">
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Ride Name
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Ride Number
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Added By
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Time Slot Date
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Time
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Ride_Status
                            </th>
                            <!--    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Change Stoppage Status
                            </th> -->
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Stoppage Status
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Stoppage Category
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Stoppage Sub Category
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Description
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Down Times
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Process
                            </th>
                        </tr>
                    </thead>

                    <tbody>

                     @if(isset($stoppages) )
                     @foreach ($stoppages as $all_day_stoppages )

                        <tr role="row" class="odd" id="row-{{ $all_day_stoppages->id }}">
                            <td tabindex="0" class="sorting_1">{{ $all_day_stoppages->id }}</td>
                            <td>{{ $all_day_stoppages->ride->name }}</td>
                            <td>{{ $all_day_stoppages->ride->id }}</td>
                            <td>{{ $all_day_stoppages->user->name }}</td>
                            <td>{{ $all_day_stoppages->opened_date }}</td>
                            <td>{{ $all_day_stoppages->time_slot_start }}</td>
                            <td>
                                @if($all_day_stoppages->ride_status=='stopped')
                                <span class=" btn-xs btn-danger">Stopped</span>
                                @else
                                <span class=" btn-xs btn-success">Active</span>
                                @endif
                            </td>

                            <td>
                                @if($all_day_stoppages->stoppage_status=='pending')
                                <span class=" btn-xs btn-primary">Pending
                                    @elseif($all_day_stoppages->stoppage_status=='working')
                                    <span class=" btn-xs btn-danger">Working On
                                        @else
                                        <span class=" btn-xs btn-success">Solved
                                            @endif
                            </td>
                            <td>{{ $all_day_stoppages->stopageCategory->name ?? "name" }}</td>
                            <td>{{ $all_day_stoppages->stopageSubCategory->name ?? "name" }}</td>
                            <td>{{ $all_day_stoppages->description }}</td>
                            <td>{{ $all_day_stoppages->down_minutes?? "Stop All Day" }}</td>
                            {!!Form::open( ['route' => ['admin.rides-stoppages.destroy',$all_day_stoppages->id] ,'id'=>'delete-form'.$all_day_stoppages->id, 'method' => 'Delete']) !!}
                            {!!Form::close() !!}
                            <td>
                            @if(auth()->user()->can('rides-stoppages-edit'))
                    
                           <!-- Button to trigger the Solve Modal -->
                            <button type="button" class="btn btn-success open-solve-modal" data-toggle="modal" data-target="#solveModal" data-stoppages="{{ json_encode($all_day_stoppages) }}">
                                <i class="fa fa-edit"></i> Solve
                            </button>

                            <!-- Button to trigger the Extend Modal -->
                            <button type="button" class="btn btn-danger open-extend-modal" data-toggle="modal" data-target="#extendModal" data-stoppages="{{ json_encode($all_day_stoppages) }}">
                                <i class="fa fa-edit"></i> Extend
                            </button>
                             @endif

                            @if(auth()->user()->can('rides-stoppages-delete'))
                            <a class="btn btn-danger" data-name="{{ $all_day_stoppages->name }}" data-url="{{ route('admin.rides-stoppages.destroy', $all_day_stoppages) }}" onclick="delete_form(this)">
                                Delete
                            </a>
                            @endif
                         </td>
                        </tr>
                    @endforeach
                 @endif

            </tbody>
            </table>
        </div>
    </div>
   </div>
</div>

            <!-- Solve Modal Content -->

            <div class="modal fade" id="solveModal" tabindex="-1" aria-hidden="true" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Solve Stoppage</h4>
                        </div>
                        <div class="modal-body">
                            <!-- Input fields in the Solve Modal -->
                            <form action="{{ route('admin.solveStoppage') }}" method="post">
                                @csrf
                                <input type="hidden" name="stoppage_id" id="solve-stoppage-id">
                                <label for="stoppage_status">Stoppage Status</label>
                                <select name="stoppage_status" id="stoppage_status" class="form-control">
                                    <option value="done">Solved</option>
                                </select>
                                <label for="description">Stoppage Description</label>
                                <textarea name="description" id="description" class="form-control summernote"></textarea>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Extend Modal Content -->
            <div class="modal fade" id="extendModal" tabindex="-1" aria-hidden="true" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Extend Stoppage</h4>
                        </div>
                        <div class="modal-body">
                            <!-- Input fields in the Extend Modal -->
                            <form action="{{ route('admin.rides-stoppages.updateStoppageStatus') }}" method="post">
                                @csrf
                                @method('PATCH')
                                <input type="hidden" name="stoppage_id" id="extend-stoppage-id">
                                <label for="stoppage_status">Stoppage Status</label>
                                <select name="stoppage_status" id="stoppage_status" class="form-control">
                                    <option value="working">Working On</option>
                                </select>
                                <label for="description">Stoppage Description</label>
                                <textarea name="description" id="description" class="form-control summernote"></textarea>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
 @endsection

 @push('scripts')
 <script type="text/javascript">

 $(document).ready(function () {
    // When Solve Modal is shown
    $('#solveModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var stoppagesData = button.data('stoppages'); // Get the data attribute value

        // Populate Solve Modal fields
        $('#solveModal .modal-title').text('Solve Stoppage');
        $('#solve-stoppage-id').val(stoppagesData.id);
        // Add more fields as needed
    });

    // When Extend Modal is shown
    $('#extendModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var stoppagesData = button.data('stoppages'); // Get the data attribute value

        // Populate Extend Modal fields
        $('#extendModal .modal-title').text('Extend Stoppage');
        $('#extend-stoppage-id').val(stoppagesData.id);
        // Add more fields as needed
    });
});
</script>


 
<script type="text/javascript">
    $("#ClientStore").popover({
        title: '<h4>Update Stoppage Status</h4>',
        container: 'body',
        placement: 'bottom',
        html: true,
        content: function() {
            return $('#popover-form').html();
        }
    });
</script>
<script type="text/javascript">
    $('.mai_category').change(function() {
        var val = $(this).val();
        $.ajax({
            type: "post",
            url: "{{ route('admin.getSubStoppageCategories') }}",
            data: {
                'stopage_category_id': val,
                '_token': "{{ @csrf_token() }}"
            },
            success: function(data) {
                var options = '<option disabled>Choose Main Category</option>';
                $.each(data.subCategory, function(key, value) {
                    options += '<option value="' + value.id + '">' + value.name +
                        '</option>';

                });
                $("#subCategory").empty().append(options);
            }
        });
    });




    $("#ClientStores").popover({
        title: '<h4>Solve Stoppage</h4>',
        container: 'body',
        placement: 'bottom',
        html: true,
        content: function() {
            return $('#popover-forms').html();
        }
    });
</script>

@endpush

@section('footer')
@include('admin.datatable.scripts')
@endsection