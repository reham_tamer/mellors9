<div class="font-bold m-3 text-center">
    <span class="font-bold m-3 text-danger">Total Count</span> : @if (isset($items))
        {{$items->count()}}
    @else
        0
    @endif
</div>
@if(isset($items) && count($items) >0)
    <div class="form-group left row col-md-12">
        <a class="btn btn-info" href="{{ route('admin.export.queues',$items->pluck('id')) }}">Export File Csv</a>

    </div>

    <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
        <div class="row">
            <div class="col-sm-12">
                
            <div class='responsive-wrapper'>
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">

                    <thead>
                    <tr role="row">

                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Branch Name
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Park Name
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Zone Name
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Ride Name
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Ride Number
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Operator Number
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Operator Name
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Time Slot Date
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Start Time
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                        colspan="1">
                            Queue seconds
                        </th>
                       
                    </tr>
                    </thead>

                    <tbody>
                    <div id="items">
                        @if (isset($items))
                            @forelse ($items as $item)
                                <tr role="row" class="odd" id="row-{{ $item->id }}">
                                    <td>{{ $item->parkTime->parks->branches->name }}</td>
                                    <td>{{ $item->parkTime->parks->name }}</td>
                                    <td>{{ $item->ride->zone->name }}</td>
                                    <td>{{ $item->ride->name }}</td>
                                    <td>{{ $item->ride->id }}</td>
                                    <td>{{ $item->user->user_number ?? '' }}</td>
                                    <td>{{ $item->user->name }}</td>
                                    <td>{{ $item->opened_date }}</td>
                                    <td>{{ \Carbon\Carbon::parse($item->start_time)->format('H:i:s') }}</td>
                                    </td>
                                    <td>{{ $item->queue_seconds }}</td>
                                
                                </tr>
                            @empty
                                <div class="text-center">Not Found Result</div>
                    @endforelse
                    @endif

                    </tbody>
                </table>

            </div>
            </div>
        </div>
    </div>
@endif
