@extends('admin.layout.app')

@section('title')
    Attendance Time Report
@endsection

@section('content')

    <div class="card-box">
        <form class="formSection" method="GET">

            <div class="row">
                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="last_name">Select Park</label>
                        {!! Form::select('park_id', $parks, null, [
                            'class' => 'form-control park',
                            'id' => 'park',
                            'placeholder' => 'Choose Park',
                        ]) !!}
                    </div>
                </div>
                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="last_name">Select Ride</label>
                        {!! Form::select('ride_id', [], null, [
                            'class' => 'form-control ride',
                            'id' => 'ride',
                            'placeholder' => 'Choose Ride',
                        ]) !!}
                    </div>
                </div>
                <div class='col-md-3'>
                    <div class="form-group">
                        <label for="last_name">Select Attendance</label>
                        {!! Form::select('user_id',$users,null, array('class' => 'form-control select2','id'=>'user','placeholder'=>'Choose Attendance') ) !!}
                    </div>
                </div>

                <div class='col-md-3'>
                    <div class="form-group">
                        <label for="middle_name">Open Date From </label>
                        {!! Form::date('from',\Carbon\Carbon::now()->toDate(),['class'=>'form-control ','id'=>'date']) !!}
                    </div>
                </div>
                <div class='col-md-3'>
                    <div class="form-group">
                        <label for="middle_name">Open Date to </label>
                        {!! Form::date('to',\Carbon\Carbon::now()->toDate(),['class'=>'form-control ','id'=>'date_to']) !!}
                    </div>
                </div>
{{--                <div class='col-md-3'>--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="middle_name">Time Slot Date To </label>--}}
{{--                        {!! Form::date('to',\Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date_to']) !!}--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class='col-md-2 mtButton'>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-primary save_btn waves-effect">Show</button>
                    </div>
                </div>
            </div>
        {!!Form::close() !!}
    </div>
    <div id="items">

    </div>

@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).on("click", ".save_btn", function () {

            var user = $('#user').val();
            var date_from = $('#date').val();
            var date_to = $('#date_to').val();
            var park_id = $('#park').val();
            var ride_id = $('#ride').val();
            $.ajax({
                url: "{{ route('admin.reports.attendanceTimeReport') }}",
                method: 'GET',
                data: {
                    user_id: user,
                    from: date_from,
                    to: date_to,
                    park_id: park_id,
                    ride_id: ride_id,
                },
                success: function (data) {
                    console.log(data)
                    $('#items').html(' ');
                    $('#items').html(data);

                }
            });
        });
    </script>

@endpush

@section('footer')
    @include('admin.datatable.scripts')
    <script type="text/javascript">
        $("#park").change(function () {
            $.ajax({
                url: "{{ route('admin.getParkRides') }}?park_id=" + $(this).val(),
                method: 'GET',
                success: function (data) {
                    $('#ride').html(data.html);
                }
            });
        });
    </script>
@endsection


