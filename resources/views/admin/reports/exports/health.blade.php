
@if(isset($items) && count($items) >0)

    <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
        <div class="row">

            <div class="col-xs-12">
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1" aria-sort="ascending">ID
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Location
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Date
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Report Type
                        </th>

                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Status
                        </th>

                    </tr>
                    </thead>

                    <tbody>

                    @foreach ($items as $item)
                        <tr role="row" class="odd" id="row-{{ $item->id }}">
                            <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                            <td>{{ $item->park?->name }} / {{ $item->ride?->name ?? $item->text }} </td>
                            <td>{{ $item->date }}</td>
                            <td>{{ $item->type =='incident' ? 'Incident /Accident QMS-F-13' :'Incident Investigation QMS-F-14' }}</td>

                            @if(auth()->user()->hasRole('Health & Safety Manager') || auth()->user()->hasRole('Super Admin')||
                             auth()->user()->hasRole('Park Admin') || auth()->user()->hasRole('Branch Admin'))
                                <td>
                                    @if($item->status=='pending')
                                          Approve
                                    @else
                                        <span>Verified</span>

                                    @endif

                                </td>
                            @endif

                        </tr>
                    @endforeach

                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endif
