<table>

    <thead>
    <tr role="row">
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Branch Name
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Park Name
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Ride Name
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Ride Number
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Operator Number
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Operator Name
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Time Slot Date
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
        colspan="1">
            Start Time
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
        colspan="1">
            Re-open Time
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Ride Status
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Stoppage Status
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Stoppage Category
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Stoppage Sub Category
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Ride Notes
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Stoppage Description
    </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Down Times
        </th>

    </tr>
    </thead>

    <tbody>
    <div id="items">
        @if (isset($items))
            @forelse ($items as $item)
                <tr role="row" class="odd" id="row-{{ $item->id }}">
                    <td>{{ $item->park->branches->name }}</td>
                    <td>{{ $item->park->name }}</td>
                    <td>{{ $item->ride->name }}</td>
                    <td>{{ $item->ride->id }}</td>
                    <td>{{ $item->user?->user_number ?? '' }}</td>
                    <td>{{ $item->user?->name }}</td>
                    <td>{{ $item->opened_date }}</td>
                    <td>{{ $item->time_slot_start }}</td>
                    <td>{{ $item->time_slot_end ?: $item->parkTime->end }}</td>
                    <td>
                        @if ($item->ride_status == 'stopped')
                            <span class=" btn-xs btn-danger">Stopped</span>
                        @else
                            <span class=" btn-xs btn-success">Active</span>
                        @endif
                    </td>
                    <td>
                        @if ($item->stoppage_status == 'pending')
                            <span class=" btn-xs btn-primary">Pending
                                            @elseif($item->stoppage_status == 'working')
                                    <span class=" btn-xs btn-danger">Working On
                                                @else
                                            <span class=" btn-xs btn-success">Solved
                        @endif
                    </td>
                    <td>{{ $item->stopageCategory?->name ?? 'name' }}</td>
                    <td>{{ $item->stopageSubCategory?->name ?? 'name' }}</td>
                    <td>{{ $item->ride_notes }}</td>
                    <td>{{ $item->description }}</td>
                    <td>{{ $item->down_minutes ?? 'Stop All Day' }}</td>

                </tr>
            @empty
                <div class="text-center">Not Found Result</div>
    @endforelse
    @endif
    </div>
    </tbody>
</table>
