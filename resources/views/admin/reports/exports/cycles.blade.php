<table>

    <thead>
        <tr role="row">

            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                colspan="1">
                Branch Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                colspan="1">
                Park Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                colspan="1">
                Zone Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                colspan="1">
                Ride Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                colspan="1">
                Ride Number
            </th>
            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                colspan="1">
                Operator Number
            </th>
            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                colspan="1">
                Operator Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                colspan="1">
                Time Slot Date
            </th>
            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                colspan="1">
                Start Time
            </th>
            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                colspan="1">
                Duration/sec
            </th>
            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                colspan="1">
                 Riders Count
            </th>
            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                colspan="1">
                Riders VIP
            </th>
            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                colspan="1">
               Riders Ft
            </th>
            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                colspan="1">
                Riders Dissable
            </th>
            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
              Total Riders/cycle
        </th>
        </tr>
        </thead>

        <tbody>
        <div id="items">
            @if (isset($items))
                @forelse ($items as $item)
                    <tr role="row" class="odd" id="row-{{ $item->id }}">
                        <td>{{ $item->park->branches->name }}</td>
                        <td>{{ $item->park->name }}</td>
                        <td>{{ $item->zone->name }}</td>
                        <td>{{ $item->ride->name }}</td>
                        <td>{{ $item->ride->id }}</td>
                        <td>{{ $item->user->user_number ?? '' }}</td>
                        <td>{{ $item->user->name }}</td>
                        <td>{{ $item->opened_date }}</td>
                        <td>{{ $item->start_time }}</td>
                        <td>{{ $item->duration_seconds }}</td>
                        <td>{{ $item->riders_count }}</td>
                        <td>{{ $item->number_of_vip}}</td>
                        <td>{{ $item->number_of_ft}}</td>
                        <td>{{ $item->number_of_disabled }}</td>
                        <td>{{ $item->riders_count + $item->number_of_vip + $item->number_of_ft + $item->number_of_disabled }}</td>

                    </tr>
            @empty
                <div class="text-center">Not Found Result</div>
    @endforelse
    @endif

    </tbody>
</table>
