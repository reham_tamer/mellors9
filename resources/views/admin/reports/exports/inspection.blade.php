<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
    <thead>
    <tr role="row">
        <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1" aria-sort="ascending">ID
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
            Park
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
            Zone
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
            Ride
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
            Time Slot Date
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
            Inspection Element 
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
        Any Assue ?             
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
            Comment
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
            Created At
        </th>

    </tr>
    </thead>

    <tbody>
        @php
        $outerLoopIteration = 1; 
        @endphp
    @forelse ($items as $item)
    @foreach ($item->lists as $list)
        @if($list->status == 'yes')
        <tr role="row" class="odd" id="row-{{ $item->id }}">
            <td tabindex="0" class="sorting_1">{{ $outerLoopIteration }}</td>
            <td> LIST {{ $outerLoopIteration }}</td>
            <td>{{$item->park?->name}}</td>
            <td>{{$item->zone?->name}}</td>
            <td>{{ $item->ride?->name }}</td>
            <td>{{ $item->opened_date }}</td>
            <td>{{ $list->inspection_list->name }}</td>
            <td>{{ $list->status }}</td>
            <td>{{ $list->comment }}</td>
            <td> {{$item->created_at }} </td>
           
        </tr>
        @endif
        @endforeach
        @php
$outerLoopIteration++; 
@endphp
    @empty
        not found
    @endforelse
    </tbody>
</table>
