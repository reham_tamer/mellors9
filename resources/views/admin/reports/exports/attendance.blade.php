<div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
    <div class="row">
        <div class="col-sm-12">
            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                <tr role="row">
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Attendance Name
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Code
                        </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Ride Name
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Park Name
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Time Slot Date
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Login Time
                    </th>
{{--                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">--}}
{{--                        Logout Time--}}
{{--                    </th>--}}
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Work Minutes
                    </th>
                </tr>
                </thead>

                <tbody>
                @if(isset($items))
                    @foreach($items as $item)
                        <tr role="row" class="odd">
                            <td>{{ $item->attendance?->name}}</td>
                            <td>{{ $item->attendance?->code}}</td>
                            <td>{{ $item->ride?->name }}</td>
                            <td>{{ $item->park?->name }}</td>
                            <td>{{ $item->open_date}}</td>
                            <td>{{ \Carbon\Carbon::parse($item->date_time_start)->toTimeString()}}</td>
{{--                            <td>{{ \Carbon\Carbon::parse($item->date_time_end)->toTimeString()}}</td>--}}
                            <td>{{ $item->shift_minutes}} M</td>
                        </tr>
                    @endforeach

                @endif
                </tbody>
            </table>

        </div>
    </div>

</div>
