<div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
    <div class="row">
        <div class="col-sm-12">
            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                <tr role="row">
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Ride Name
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Count of Cycles
                        </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                       Date
                    </th>

                </tr>
                </thead>

                <tbody>
                @if(isset($items))
                    @foreach($items as $item)
                        <tr role="row" class="odd">
                            <td>{{ $item['ride_id']}}</td>
                            <td>{{ $item['rider_count']}}</td>
                            <td>{{ $item['date']}}</td>
                        </tr>
                    @endforeach

                @endif
                </tbody>
            </table>

        </div>
    </div>

</div>
