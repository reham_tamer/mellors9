<div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
    <div class="row">
        <div class="col-sm-12">
            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                <tr role="row">
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Operator Name
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                    Code
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Ride Name
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Park Name
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Time Slot Date
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Login Time
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Work Minutes
                    </th>
                </tr>
                </thead>

                <tbody>

                @if(isset($items))
                    @foreach($items as $item)
                        <tr role="row" class="odd">
                            <td>{{ $item->user?->name}}</td>
                            <td>{{ $item->user?->code}}</td>
                            <td>{{ $item->ride?->name }}</td>
                            <td>{{ $item->park?->name }}</td>
                            <td>{{ $item->open_date}}</td>
                            <td>{{ $item->time}}</td>
                            <td>{{ $item->shift_minutes}}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>

        </div>
    </div>

</div>
