<div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
    <div class="row">

        <div class="col-xs-12 ">

            <div class="col-xs-12">
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1" aria-sort="ascending">ID
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Location
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Date
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Operator
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Total Score
                        </th>

                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Added By Supervisor
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Brackets Based on Total Score
                        </th>
                    </tr>
                    </thead>

                    <tbody>

                    @foreach ($items as $item)
                        <tr role="row" class="odd" id="row-{{ $item->id }}">
                            <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                            <td>{{ $item->park->name }} / {{ $item->ride->name ?? $item->text }} </td>
                            <td>{{ $item->date }}</td>
                            <td>{{ $item->operator?->name }}</td>
                            <td>{{$item->total_score}}</td>
                            <td>{{$item->supervisor?->name}}</td>
                            <td>{{$item->brackets}}</td>


                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
