
@if(isset($items) && count($items) >0)

    <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
        <div class="row">

            <div class="col-xs-12 printable_div" id="myDivToPrint">
               
                <div class="col-xs-12">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Park
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Zone
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Ride
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Time Slot Date
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                First Status
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Second Status
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                No Of Gondolas
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                No Of Seats
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Comment
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        @if (isset($items))
                            @foreach ($items as $item)
                                <tr role="row" class="odd" id="row-{{ $item->id }}">
                                    <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                    <td>{{ $item->parks?->name }}</td>
                                    <td>{{ $item->zone?->name }}</td>
                                    <td>{{ $item->rides?->name }}</td>
                                    <td>{{ $item->date }}</td>
                                    <td>{{ $item->first_status }}</td>
                                    <td>{{ $item->second_status }}</td>
                                    <td>{{ $item->no_of_gondolas }}</td>
                                    <td>{{ $item->no_of_seats }}</td>
                                    <td>{!! $item->comment !!} </td>
                                </tr>
                            @endforeach
                        @endif

                        </tbody>
                    </table>


                </div>
            </div>
        </div>

    </div>
@endif
