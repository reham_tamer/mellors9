<div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
    <div class="row">

        <div class="col-xs-12 ">

            <div class="col-xs-12">
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                    <tr role="row">

                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Location
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Date
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Supervisor
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Total Score
                        </th>


                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Added By
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Status
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Brackets Based on Total Score
                        </th>
                    </tr>
                    </thead>

                    <tbody>

                    @foreach ($items as $item)
                        <tr role="row" class="odd" id="row-{{ $item->id }}">
                             <td>{{ $item->park->name }} </td>
                            <td>{{ $item->date }}</td>
                            <td>{{ $item->supervisor?->name }}</td>
                            <td>{{$item->total_score}}</td>
                            <td>{{$item->addedBy?->name}}</td>
                            <td>{{$item->approve}}</td>
                            <td>{{$item->brackets}}</td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
