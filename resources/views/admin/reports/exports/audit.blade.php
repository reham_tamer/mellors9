<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
    <thead>
    <tr role="row">
       
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            List
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Park
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Zone
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Ride
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Created By
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Approve By
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Created At
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Any issue ?
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Status
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Note
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Corrective Action
        </th>
      
    </tr>
    </thead>

    <tbody>
    @php
        $outerLoopIteration = 1;
    @endphp
    @forelse ($items as $item)
        @foreach ($item->lists as $list)

        <tr>
            <td> LIST {{ $outerLoopIteration }}</td>
            <td>{{$item->park?->name}}</td>
            <td>{{$item->zone?->name}}</td>
            <td>{{ $item->ride?->name }}</td>
            <td>{{ $item->created_by?->name }}</td>
            <td>{{ $item->approve_by?->name }}</td>
            <td>{{ isset($item->datetime) ? $item->datetime : $item->date }}</td>
            <td>{{ $list->question?->name }}</td>
            <td>{{ $list->status ?? 'N/A' }}</td>
            <td>{{ $list->note }}</td>
            <td>{{ $list->corrective_action }}</td>
           
        </tr>

        @endforeach
        @php
            $outerLoopIteration++;
        @endphp
    @empty
        not found
    @endforelse
    </tbody>
</table>
