
@if(isset($items) && count($items) >0)

    <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
        <div class="row">
            <div class="form-group left row col-md-12">
                <a class="btn btn-info" href="{{ route('admin.export.operator',$items->pluck('id')) }}">Export File Csv</a>

            </div>
            <div class="col-xs-12 printable_div" id="myDivToPrint">

                <div class="col-xs-12">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            {{-- <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Park
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Zone
                            </th> --}}
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Ride
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Time Slot Date
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Date Resolved
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Snag
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                 Feedback
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Reported on tech sheet
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Rf number
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Department
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Category
                           </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1">
                                Created by
                            </th> 
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Image
                            </th>

                        </tr>
                        </thead>

                        <tbody>
                        @if (isset($items))
                            @foreach ($items as $item)
                                <tr role="row" class="odd" id="row-{{ $item->id }}">
                                    <td tabindex="0" class="sorting_1">{{ $loop->iteration }}</td>
                                    <td>{{ $item->ride->name }}</td>
                                    <td>{{ $item->date_reported }}</td>
                                    <td>{{ $item->date_resolved ?? 'not resolved yet' }}</td>
                                    <td>{{ $item->snag }}</td>
                                    <td>{{ $item->maintenance_feedback }}</td>
                                    <td>{{ $item->reported_on_tech_sheet }}</td>
                                    <td>{{ $item->rf_number }}</td>
                                    <td> {{ $item->department?->name }} </td>
                                    <td>{{ $item->observationCategory?->name }}</td>
                                    <td>{{ $item->created_by?->name }}</td>


                                    <td>
                                        @if ($item->image != null || $item->image !=0)

                                            <a>
                                                <img class="img-preview"
                                                     src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($item->image),now()->addMinutes(30)) }}"
                                                     style="height: 40px; width: 40px"></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endif
