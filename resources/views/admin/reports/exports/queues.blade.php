<table>

    <thead>
    <tr role="row">

        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Branch Name
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Park Name
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Zone Name
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Ride Name
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Ride Number
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Operator Number
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Operator Name
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Time Slot Date
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Start Time
        </th>
        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
            colspan="1">
            Queue seconds
        </th>

    </tr>
    </thead>

    <tbody>
    <div id="items">
        @if (isset($items))
            @forelse ($items as $item)
                <tr role="row" class="odd" id="row-{{ $item->id }}">
                    <td>{{ $item->parkTime->parks->branches->name }}</td>
                    <td>{{ $item->parkTime->parks->name }}</td>
                    <td>{{ $item->ride->zone->name }}</td>
                    <td>{{ $item->ride->name }}</td>
                    <td>{{ $item->ride->id }}</td>
                    <td>{{ $item->user->user_number ?? '' }}</td>
                    <td>{{ $item->user->name }}</td>
                    <td>{{ $item->opened_date }}</td>
                    <td>{{ \Carbon\Carbon::parse($item->start_time)->format('H:i:s') }}</td>
                    <td>{{ $item->queue_seconds }}</td>

                </tr>
            @empty
                <div class="text-center">Not Found Result</div>
            @endforelse
        @endif
    </div>

    </tbody>
</table>
