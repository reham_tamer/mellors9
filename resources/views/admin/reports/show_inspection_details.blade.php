@extends('admin.layout.app')

@section('title')
Inspection Lists Report
@endsection

@section('content')

        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-xs-12">
                    <input type="button" value="Print Report" id="printDiv" class="btn btn-primary printBtn"></input>
                </div>
                <div class="col-xs-12 printable_div" id="myDivToPrint">
                    <div class="col-xs-12 printOnly">
                        <div class="logo">
                            <img src="{{asset('/_admin/assets/images/logo1.png')}}" alt="Mellors-img" title="Mellors" class="image">
                        </div>
                        <h3 class="table-title">Inspection Lists Report</h3>
                    </div>
                    <div class="col-xs-12">
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1" aria-sort="ascending">ID
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                    Park
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                    Zone
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                    Ride 
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                    Time Slot Date 
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                    Inspection Element 
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Status              
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                    Comment
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                    Created At
                                </th>
                            
                            </tr>
                            </thead>

                            <tbody>
                                @if(isset($items))

                            @foreach ($items as $item)
                                <tr role="row" class="odd" id="row-{{ $item->id }}">
                                    <td tabindex="0" class="sorting_1">{{ $loop->iteration }}</td>
                                    <td>{{$list->park->name}}</td>
                                    <td>{{$list->zone->name}}</td>
                                    <td>{{ $list->ride->name }}</td>
                                    <td>{{ $list->opened_date }}</td>
                                    <td>{{ $item->inspection_list->name }}</td>
                                    <td>{{ $item->status }}</td>
                                    <td>{{ $item->comment }}</td>
                                    <td> {{ $item->created_at }} </td>
                                </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>


                </div>
            </div>
        </div>

    </div>
</div>

@endsection
@push('scripts')

<script type="text/javascript">
$("#park").change(function() {
    $.ajax({
        url: "{{ route('admin.getParkRides') }}?park_id=" + $(this).val(),
        method: 'GET',
        success: function(data) {
            $('#ride').html(data.html);
        }
    });
});
</script>

<script language="javascript">
  $('#printDiv').click(function(){
      $('#myDivToPrint').show();
         window.print();
         return false;
});
</script>
@endpush
@section('footer')
@include('admin.datatable.scripts')
@endsection


