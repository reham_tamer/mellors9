@extends('admin.layout.app')
@section('title')
    Duty Report
@endsection
@section('content')
    <form id="myForm" class="formSection" action="{{ url('/search_ride_ops_reports') }}" method="GET">
        @csrf
        <div class="row">
            <div class='col-md-5'>
                <div class="form-group">
                    <label for="last_name">Select Park</label>
                    {!! Form::select('park_id', $parks, isset($park_id) ? $park_id : null, array('class' => 'form-control select2 park','id'=>'park_id')) !!}
                </div>
            </div>
            <div class='col-md-5'>
                <div class="form-group">
                    <label for="middle_name">Date</label>
                    <input type="date" name="date" class="form-control" id="date"
                           value="{{ isset($date) ? $date : '' }}">
                </div>
            </div>

        </div>
    </form>

    <ul class="nav nav-tabs tabsLinkDuty">
        <li class="{{ (request()->is('search_ride_ops_reports*')) ? 'active' : '' }}"><a data-toggle="tab" href="#Ride">Ride
                Ops</a></li>
        <li class="{{ (request()->is('search_health_and_safety*')) ? 'active' : '' }}"><a data-toggle="tab"
                                                                                          href="#Health">Health &
                safety</a></li>
        <li class="{{ (request()->is('search_maintenance_reports*')) ? 'active' : '' }}"><a data-toggle="tab"
                                                                                            href="#Maintenance">Maintenance</a>
        </li>
        <li class="{{ (request()->is('search_tech_reports*')) ? 'active' : '' }}"><a data-toggle="tab"
                                                                                     href="#Teachnical">Technical
                Services</a></li>
        <li id="skill_game" class="skill_game {{ (request()->is('search_skill_game_reports*')) ? 'active' : '' }}"><a
                data-toggle="tab" href="#Skill">Skill Games</a></li>
        <li class="{{ (request()->is('search_duty_summary_reports*')) ? 'active' : '' }}"><a data-toggle="tab"
                                                                                             href="#Duty">Duty
                Summary</a></li>
    </ul>
    <div class="col-xs-12 mt-3">
        <br>
        <button type="button" value="Print Report" onClick="printdiv('printReportt');"
                class="btn btn-primary printBtn mt-3">Print Report
        </button>
    </div>

    <div class="tab-content tabsContentDuty" id="printReportt">
        <div id="Ride" class="tab-pane fade in active">
            <!-- !! NOTE !! Kindly add class: (Rides) to <table> tag here in Rides condition -->
            @include('admin.reports.show')
        </div>

        <div id="Health" class="tab-pane fade">
            <!-- !! NOTE !! Kindly add class: (TechServices Health health1) to <table> tag here in Rides condition -->
            @include('admin.reports.show')
        </div>

        <div id="Maintenance" class="tab-pane fade">
            <!-- !! NOTE !! Kindly add class: (Maintenance) to <table> tag here in Rides condition -->
            @include('admin.reports.show')
        </div>

        <div id="Teachnical" class="tab-pane fade">
            <!-- !! NOTE !! Kindly add class: (TechServices) to <table> tag here in Rides condition -->
            @include('admin.reports.show')
        </div>

        <div id="Skill" class="tab-pane fade">
            <!-- !! NOTE !! Kindly add class: (SkillGames) to <table> tag here in Rides condition -->
            @include('admin.reports.show')

        </div>

        <div id="Duty" class="tab-pane fade">
            <!-- !! NOTE !! Kindly add class: (Rides) to <table> tag here in Rides condition -->
            @include('admin.reports.summery')

        </div>
    </div>

@endsection

@push('scripts')
    <script>
        function printdiv(printpage) {
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.all.item(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            return false;
        }

        $(document).ready(function () {

            var formData = {}; // Object to store form data

            // When a tab is clicked
            $('.nav-tabs a').on('click', function () {
                var tabId = $(this).attr('href'); // Get the href value of the clicked tab
                var formAction = ''; // Variable to store the updated form action

                // Update the form action based on the selected tab
                if (tabId === '#Health') {
                    formAction = "{{ url('/search_health_and_safety') }}";
                } else if (tabId === '#Maintenance') {
                    formAction = "{{ url('/search_maintenance_reports') }}";
                } else if (tabId === '#Teachnical') {
                    formAction = "{{ url('/search_tech_reports') }}";
                } else if (tabId === '#Skill') {
                    formAction = "{{ url('/search_skill_game_reports') }}";
                } else if (tabId === '#Duty') {
                    formAction = "{{ url('/search_duty_summary_reports') }}";
                } else {
                    formAction = "{{ url('/search_ride_ops_reports') }}"; // Default form action
                }

                // Set the updated form action
                $('#myForm').attr('action', formAction);

                // Submit the form
                $('#myForm').submit();
            });

        });

        $('.park').change(function () {
            var val = $(this).val();
            $.ajax({
                type: "get",
                url: "{{ route('admin.getHasSkillgame') }}",
                data: {
                    'park_id': val,
                    '_token': "{{ @csrf_token() }}"
                },
                success: function (response) {
                    var rides = response.rides;

                    if (rides.length === 0) {
                        console.log('No rides found');
                        $('#skill_game').addClass('hidden'); // Hide the element by ID
                    } else {
                        console.log('Rides found');
                        $('#skill_game').removeClass('hidden'); // Show the element by ID
                    }
                }
            });
        });
    </script>

@endpush
@section('footer')
    @include('admin.datatable.scripts')
@endsection
