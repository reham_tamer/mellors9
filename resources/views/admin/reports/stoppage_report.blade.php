@extends('admin.layout.app')

@section('title')
    Stoppages Report
@endsection

@section('content')

    <div class="card-box">
        <form class="formSection" method="GET">

            <div class="row">
                <div class='col-md-4'>
                    <div class="form-group">
                        <label for="last_name">Select Park</label>
                        {!! Form::select('park_id', $parks, null, [
                            'class' => 'form-control park',
                            'id' => 'park',
                            'placeholder' => 'Choose Park',
                        ]) !!}
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <label for="last_name">Select Zone</label>
                        {!! Form::select('zone_id', [], null, [
                            'class' => 'form-control ride',
                            'id' => 'zone',
                            'placeholder' => 'Choose Zone',
                        ]) !!}
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <label for="last_name">Select Ride</label>
                        {!! Form::select('ride_id', [], null, [
                            'class' => 'form-control ride',
                            'id' => 'ride',
                            'placeholder' => 'Choose Ride',
                        ]) !!}
                    </div>
                </div>

                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="middle_name">Time Slot Date From </label>
                        {!! Form::date('from', \Carbon\Carbon::now()->toDate(), ['class' => 'form-control', 'id' => 'date']) !!}
                    </div>
                </div>
                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="middle_name">Time Slot Date To </label>
                        {!! Form::date('to', \Carbon\Carbon::now()->toDate(), ['class' => 'form-control', 'id' => 'date_to']) !!}
                    </div>
                </div>
                <div class="col-md-2 mtButton text-center">
                    <div class="input-group-btn mx-auto">
                        <button type="button" class="btn btn-primary save_btn waves-effect">Show</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="items">

    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $("#park").change(function() {
            $.ajax({
                url: "{{ route('admin.getParkZones') }}?park_id=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#zone').html(data.html);
                }
            });
        });
        $("#zone").change(function() {
            $.ajax({
                url: "{{ route('admin.getZonerides') }}?zone_id=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#ride').html(data.html);
                }
            });
        });
        $(document).on("click", ".save_btn", function() {

            var park = $('#park').val();
            var zone = $('#zone').val();
            var ride = $('#ride').val();
            var date_from = $('#date').val();
            var date_to = $('#date_to').val();
            $.ajax({
                url: "{{ route('admin.reports.stoppagesReport') }}",
                method: 'GET',
                data: {
                    park_id: park,
                    zone_id: zone,
                    ride_id: ride,
                    date_from: date_from,
                    date_to: date_to,
                },
                success: function(data) {
                    console.log(data)
                    $('#items').html(' ');
                    $('#items').html(data);

                }
            });
        });
        $(document).on("click", "#printDiv", function() {

            $('#datatable-buttons_wrapper').show();
            window.print();
            return false;
        });

    </script>
@endpush
@section('footer')
    @include('admin.datatable.scripts')
@endsection
