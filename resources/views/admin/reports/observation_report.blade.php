@extends('admin.layout.app')

@section('title')
    Observation Report
@endsection

@section('content')

<div class="card-box">
    <form class="formSection"   method="GET">

        <div class="row">
    <div class='col-md-4'>
        <div class="form-group">
            <label for="last_name">Select Park</label>
            {!! Form::select('park_id',$parks,null, array('class' => 'form-control park','id'=>'park','placeholder'=>'Choose Park') ) !!}
        </div>
    </div>
    <div class='col-md-4'>
        <div class="form-group">
            <label for="last_name">Select Zone</label>
            {!! Form::select('zone_id', [], null, [
                'class' => 'form-control ride',
                'id' => 'zone',
                'placeholder' => 'Choose Zone',
            ]) !!}
        </div>
    </div>
    <div class='col-md-4'>
        <div class="form-group">
            <label for="last_name">Select Ride</label>
            {!! Form::select('ride_id', [],null, array('class' => 'form-control ride','id'=>'ride','placeholder'=>'Choose Ride')) !!}
        </div>
    </div>

    <div class='col-md-5'>
        <div class="form-group">
            <label for="middle_name">Time Slot Date From </label>
            {!! Form::date('from',\Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date']) !!}
        </div>
    </div>
    <div class='col-md-5'>
        <div class="form-group">
            <label for="middle_name">Time Slot Date To </label>
            {!! Form::date('to',\Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date_to']) !!}
        </div>
    </div>
    <div class='col-md-2 mtButton'>
        <div class="input-group-btn">
            <button type="button" class="btn btn-primary save_btn waves-effect">Show</button>
        </div>
    </div>
</div>
            {!!Form::close() !!}
    </div>
        <br><br>
        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">

                <div class="col-xs-12">

                    <div id="items">
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection
@push('scripts')

<script type="text/javascript">
     $("#park").change(function() {
            $.ajax({
                url: "{{ route('admin.getParkZones') }}?park_id=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#zone').html(data.html);
                }
            });
        });
        $("#zone").change(function() {
            $.ajax({
                url: "{{ route('admin.getZonerides') }}?zone_id=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#ride').html(data.html);
                }
            });
        });
    $(document).on("click", ".save_btn", function() {

        var park = $('#park').val();
        var ride = $('#ride').val();
        var zone = $('#zone').val();
        var date_from = $('#date').val();
        var date_to = $('#date_to').val();
        $.ajax({
            url: "{{ route('admin.reports.observationReport') }}",
            method: 'GET',
            data: {
                park_id: park,
                ride_id: ride,
                zone_id: zone,
                date_from: date_from,
                date_to: date_to,
            },
            success: function(data) {
                console.log(data)
                $('#items').html(' ');
                $('#items').html(data);

            }
        });
    });
</script>


@endpush
@section('footer')
    @include('admin.datatable.scripts')
@endsection
