<div class="font-bold m-3 text-center">
    {{--    <span class="font-bold m-3 text-danger">Total Count</span> : @if (isset($items))--}}
    {{--        {{$items->count()}}--}}
    {{--    @else--}}
    {{--        0--}}
    {{--    @endif--}}
    @if($outRang)
        <span class="font-bold m-3 text-danger">Filter out of range</span>
    @else
        <span class="font-bold m-3 text-danger">Total Count</span> : @if (isset($items))
            {{$items->count()}}
        @else
            0
        @endif
    @endif
</div>
@if(isset($items) && count($items) >0)
    <div class="form-group left row col-md-12">
        <a class="btn btn-info" href="{{ route('admin.export.attendance',$items->pluck('id')) }}">Export File Csv</a>

    </div>
    <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
        <div class="row">
            <div class="col-sm-12">
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                    <tr role="row">

                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                            Name
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                            Ride Name
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                            Park Name
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                            Time Slot Date
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                            Login Time
                        </th>
                        {{--                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">--}}
                        {{--                            Logout Time--}}
                        {{--                        </th>--}}

                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                            Work Minutes
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                            Operation
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    @if(isset($items))
                        @foreach($items as $item)
                            <tr role="row" class="odd" id="row-{{$item->id}}">
                                <td>{{ $item->attendance?->name}}</td>
                                <td>{{ $item->ride?->name }}</td>
                                <td>{{ $item->park?->name }}</td>
                                <td>{{ $item->open_date}}</td>
                                <td>{{ \Carbon\Carbon::parse($item->date_time_start)->toTimeString()}}</td>
                                {{--                                <td>{{ \Carbon\Carbon::parse($item->date_time_end)->toTimeString()}}</td>--}}
                                <td id="{{$item->id}}">{{ $item->shift_minutes}} M</td>
                                {{--                                <td><a type="button" href="{{route('admin.show_time',$item->id)}}">show</a></td>--}}
                                <!-- Button trigger modal -->
                                <td>
                                    @php
                                        $start = $item->date_time_start ;
                                        $end = $item->date_time_end;
                                    @endphp
                                    <button type="button" class="btn btn-primary show-details" data-toggle="modal"
                                            onclick="myFunction('{{$start}}','{{$end}}',{{$item->id}},'{{ $item->shift_minutes}}')"
                                            data-target="#exampleModal">

                                        Edit Time
                                    </button>
                                    <a class="btn btn-danger" data-name="{{ $item->attendance->name }}"
                                       data-url="{{ route('admin.attendance-time-delete', $item) }}"
                                       onclick="delete_form_time(this)"
                                       data-id="{{$item->id}}"
                                       data-mintues="{{$item->shift_minutes}}"
                                    >
                                        Delete
                                    </a>
                                </td>

                                <!-- Modal -->
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    @if(isset($items))
                        <input type="hidden" id="min" value="{{$items->sum('shift_minutes')}}">
                        <table class="table table-striped table-bordered dt-responsive nowrap">
                            <tr>
                                <th>
                                    Total Work Minutes
                                </th>
                                <td id="allMinutes">
                                    {{$items->sum('shift_minutes')  .' Minutes'}}
                                </td>
                            </tr>
                            @endif
                        </table>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Time</h5>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="">
                        <div class="form-group">
                            <label for="name">Login time</label>
                            <input type="datetime-local" name="start" class="form-control" id="start"
                                   onchange="timeUpdate()">

                        </div>
                        <div class="form-group">
                            <label for="name">Logout time</label>
                            <input type="datetime-local" name="end" class="form-control" id="end"
                                   onchange="timeUpdate()">

                        </div>
                        <div class="form-group">
                            <label for="name">Minutes</label>
                            <input readonly type="number" name="minutes" class="form-control" id="minutes">
                        </div>
                        <input type="hidden" name="id" id="id-row">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary save" data-dismiss="modal">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@else
{{--    <div class="text-center"> Not Found</div>--}}
@endif
<script>
    function myFunction(start, end, id, minute) {
        $('#start').val(start)
        $('#end').val(end)
        $('#id-row').val(id)
        $('#minutes').val(minute)
    }

    function timeUpdate() {
        var start = $('#start').val()
        var end = $('#end').val()

        let date1 = new Date(start);
        let date2 = new Date(end);

        var difference = date1.getTime() - date2.getTime(); // This will give difference in milliseconds
        var resultInMinutes = Math.round(difference / 60000);
        $('#minutes').val(Math.abs(resultInMinutes))

    }

    $(document).on("click", ".save", function () {

        var minutes = $('#minutes').val();
        var start = $('#start').val();
        var end = $('#end').val();
        var id = $('#id-row').val();
        $.ajax({
            url: "{{ route('admin.show_time_update') }}",
            method: 'GET',
            data: {
                minutes: minutes,
                start: start,
                end: end,
                id: id,
            },
            success: function (data) {
                alert('Update Time successfully!, please click show again')

            }
        });
    });

    function delete_form_time(element) {
        var name = $(element).data('name');
        var swalTitle = 'Delete: ' + name;
        var swalText = 'You are going to delete ' + name + 'Do you want to continue?';
        swal({
            title: swalTitle,
            text: swalText,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-warning",
            confirmButtonText: "continue",
            cancelButtonText: "cancel"
        }).then(function (isConfirm) {
            if (isConfirm.value) {
                $.ajax({
                    url: $(element).data('url'),
                    method: 'GET',
                    data: {},
                    success: function (data) {
                        $('#row-' + $(element).data('id')).hide();
                        const all = $('#min').val();
                        const thisMin = $(element).data('mintues');
                        $('#allMinutes').html(all - thisMin +' Minutes')
                    }
                });
            }
        });
    }
</script>
