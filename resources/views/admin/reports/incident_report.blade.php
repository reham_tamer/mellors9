@extends('admin.layout.app')

@section('title')
    Health & Safety Report
@endsection

@section('content')

    <div class="card-box">
        <form class="formSection" method="GET">
            <div class="row">
                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="last_name">Select Park</label>
                        {!! Form::select('park_id',$parks,null, array('class' => 'form-control park','id'=>'park','placeholder'=>'Choose Park', 'required' => 'required') ) !!}
                    </div>
                </div>
                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="last_name">Select Report Type (Optional)</label>
                        <select name="type" class="form-control " id="type">
                            <option value=""> Choose...</option>
                            <option value="incident">Incident /Accident QMS-F-13</option>
                            <option value="investigation">Incident Investigation QMS-F-14</option>
                        </select>
                    </div>
                </div>

                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="middle_name">Time Slot Date From </label>
                        {!! Form::date('from',\Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date', 'required' => 'required']) !!}
                    </div>
                </div>
                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="middle_name">Time Slot Date To </label>
                        {!! Form::date('to',\Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date_to', 'required' => 'required']) !!}
                    </div>
                </div>
                <div class='col-md-2 mtButton'>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-primary save_btn waves-effect">Show</button>
                    </div>
                </div>
            </div>
        {!!Form::close() !!}
    </div>

    <div id="items">

    </div>
 @endsection
@push('scripts')

    <script type="text/javascript">
        $("#park").change(function () {
            $.ajax({
                url: "{{ route('admin.getParkRides') }}?park_id=" + $(this).val(),
                method: 'GET',
                success: function (data) {
                    $('#ride').html(data.html);
                }
            });
        });
        $(document).on("click", ".save_btn", function () {

            var park = $('#park').val();
            var date_from = $('#date').val();
            var date_to = $('#date_to').val();
            var type = $('#type').val();
            $.ajax({
                url: "{{ route('admin.reports.incidentReport') }}",
                method: 'GET',
                data: {
                    park_id: park,
                    from: date_from,
                    to: date_to,
                    type: type,
                },
                success: function (data) {
                    console.log(data)
                    $('#items').html(' ');
                    $('#items').html(data);

                }
            });
        });
    </script>
@endpush
@section('footer')
    @include('admin.datatable.scripts')
@endsection




