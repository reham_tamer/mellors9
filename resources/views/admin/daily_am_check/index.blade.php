@extends('admin.layout.app')

@section('title')
Daily AM Shift H&S Checklist 
@endsection

@section('content')
    <div class="card-box">
       
        <form class="formSection"  method="GET">
            <div class="row">
            <div class='col-md-4'>
                <div class="form-group">
                    <label for="last_name">Select Park</label>
                    {!! Form::select('park_id',$parks,null, array('class' => 'form-control park','id'=>'park','placeholder'=>'Choose Park') ) !!}
                </div>
            </div>
        
            <div class='col-md-4'>
                <div class="form-group">
                    <label for="middle_name">Time Slot Date </label>
                    {!! Form::date('from',\Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date']) !!}
                </div>
            </div>
           
            <div class='col-md-2 mtButton'>
                <div class="input-group-btn">
                    <button type="button" class="btn btn-primary save_btn waves-effect">Show</button>
                </div>
            </div>
        </div>
        {!!Form::close() !!}
        <br><br>

        <a href=" {{ url('add_am_check/') }} ">
            <button type="button" class="btn btn-info">Create New Daily AM Shift H&S Checklist Form</button>
        </a>
        <br><br>
        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <div id="items">

                    <div class='responsive-wrapper'>
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1" aria-sort="ascending">ID
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Daily AM Shift H&S Checklist 
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Created At
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Process
                                </th>
                               
                            </tr>
                        </thead>

                        <tbody>

                             @foreach ($items as $item)
                                <tr role="row" class="odd" id="row-{{ $item->id }}">
                                    <td tabindex="0" class="sorting_1">{{ $loop->iteration }}</td>
                                    <td> LIST {{ $loop->iteration }}</td>
                                    <td>{{ $item->date }}</td>
                                    <td>
                                         {{--    <a href="{{ route('admin.editAmList', $item->id) }}">
                                                <button type="button" id="add" class="add btn btn-success">
                                                    <i class="fa fa-edit"></i>Edit List
                                                </button>
                                            </a> --}}
                                            <a href="{{ route('admin.showAmList', $item->id) }}">
                                                <button type="button" id="add" class="add btn btn-primary">
                                                    <i class="fa fa-info"></i>  Show / print List
                                                </button>
                                            </a>
                                    </td>
                                  
                                </tr>
                            @endforeach 

                        </tbody>
                    </table>


                </div>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

<script type="text/javascript">
       
    $(document).on("click", ".save_btn", function() {

        var park = $('#park').val();
        var date = $('#date').val();
        $.ajax({
            url: "{{ route('admin.amSearch') }}",
            method: 'GET',
            data: {
                park_id: park,
                date: date,
            },
            success: function(data) {
                console.log(data)
                $('#items').html(' ');
                $('#items').html(data);

            }
        });
    });
</script>


@endpush

@section('footer')
    @include('admin.datatable.scripts')
@endsection
