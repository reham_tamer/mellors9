<div class="row">
    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
            <tr role="row">
                <td   class="type-header">Inspector Name </td>
                <td>{!! Form::text('Inspector_name', \Auth::user()->name, ['class' => 'form-control', 'readonly']) !!}</td>
            </td>
                <td  class="type-header">Date </td>
                <td>{!! Form::dateTime('date', \Carbon\Carbon::now()->toDateTimeString(), ['class' => 'form-control date', 'id' => 'date']) !!}</td>
            </tr>
            <tr role="row">
                <td  class="type-header">Inspector Signature </td>
                <td>
                    <input type="file" name="signature" class="form-control">
                </td>
                <td   class="type-header">Project Name   </td>
                <td>{!! Form::select('park_id', $parks, null, ['class' => 'form-control park-id', 'id' => 'park','placeholder' => 'Choose Park...']) !!}
                </td>
            </tr>
    </table>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            @if (isset($questions))
                @php
                    $groupedQuestions = $questions->groupBy('question_type');
                @endphp
                <div class="row">
                    <div class='responsive-wrapper'>
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                            <tr role="row" class="type-header">
                                <th style="text-align: center;">CHECKPOINTS </th>
                                <th>Status</th>
                                <th>Comments</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($groupedQuestions as $type => $typeQuestions)
                            @if($type)

                            <tr >
                                    <td colspan="4" class="type-header">
                                        <strong> {{ $type }}</strong>
                                    </td>
                            </tr>
                            @endif
                                @foreach ($typeQuestions as $question)
                                    <tr>
                                        
                                        <td>{{ $question->name ?? '' }}</td>
                                        <td>
                                            <label>
                                                <select name="status[]" id="element_id" class="form-control element-id">
                                                    <option value="0">No</option>
                                                    <option value="1">Yes</option>
                                                </select>
                                                <input type="hidden" name="question_id[]" class="ele_id"
                                                    value="{{ $question->id }}">
                                            </label>
                                        </td>
                                        <td>
                                            {!! Form::textArea('comment[]', null, ['class' => 'form-control comment', 'rows' => '1']) !!}
                                        </td>
                                    </tr>
                                    @if($question->is_rides_question === 1)
                                    @for ($i = 0; $i < 5; $i++)
                                    <tr>
                                        <td>
                                            Ride Name: {!! Form::select('ride_id[]', [], null, ['class' => 'form-control ride-id', 'data-index' => $i, 'placeholder' => 'Select Park First...']) !!}
                                        </td>
                                        <td colspan="2">
                                            {!! Form::textarea('comment_rides[]', null, ['class' => 'form-control comment_rides', 'rows' => '1']) !!}
                                        </td>
                                    </tr>
                                    @endfor
                                    @endif
                                @endforeach
                            @endforeach
                            <input name="form_type" type="hidden" class="form-type" value="am">
                            
                        </tbody>
                    </table>
                    <label>Additional Comments</label>
                    {!! Form::textArea('additional_comment', null, ['class' => 'form-control additional_comment', 'rows' => '1']) !!}
                    
                    </div>
                    <div class="form-group">
                        <label for="name"> Upload Images (only images not files) </label>
                    
                        @include('admin.daily_am_check.images_upload')
                    </div>
                    <div class="col-xs-12 aligne-center contentbtn">
                        <button class="btn btn-primary save_btn waves-effect" type="submit">Save</button>
                    </div>
                </div>
            @else
                <label>No questions</label>
            @endif
        </div>
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#park").change(function() {
                var parkId = $(this).val();
                $.ajax({
                    url: "{{ route('admin.getRides') }}",
                    method: 'GET',
                    data: { park_id: parkId },
                    success: function(data) {
                        $('.ride-id').each(function() {
                            $(this).html(data.html);
                        });
                    },
                    error: function(xhr, status, error) {
                    }
                });
            });
        });
    </script>

<script>
    $(document).ready(function () {
        $('.save_btn').on('click', function (e) {
            e.preventDefault();

            // Collect form data
            const formData = new FormData();
            const csrfToken = $('meta[name="csrf-token"]').attr('content');
            // Append CSRF token
            formData.append('_token', csrfToken);
            const form_type = $('.form-type').val();
            const park_id = $('.park-id').val();
            const additional_comment = $('.additional_comment').val();
            const date = $('#date').val();

            // Append basic fields to formData
            formData.append('form_type', form_type);
            formData.append('park_id', park_id);
            formData.append('additional_comment', additional_comment);
            formData.append('date', date);

            // Append signature file
            const signatureFile = $('input[name="signature"]')[0].files[0];
            if (signatureFile) {
                formData.append('signature', signatureFile);
            }

            // Append questions data
            $('.element-id').each(function () {
                formData.append('status[]', $(this).val());
            });

            $('.ele_id').each(function () {
                formData.append('element_ids[]', $(this).val());
            });

            $('.comment').each(function () {
                formData.append('comment[]', $(this).val());
            });

            $('.ride-id').each(function () {
                formData.append('ride_id[]', $(this).val());
            });

            $('.comment_rides').each(function () {
                formData.append('ride_comment[]', $(this).val());
            });

            // Append images array
            $('input[name^="images"]').each(function () {
                if ($(this).attr('type') === 'file') {
                    const file = $(this)[0].files[0];
                    const name = $(this).attr('name'); // e.g., images[0][file]
                    if (file) {
                        formData.append(name, file);
                    }
                } else if ($(this).attr('type') === 'text') {
                    const value = $(this).val();
                    const name = $(this).attr('name'); // e.g., images[0][comment]
                    formData.append(name, value);
                }
            });
            

            // AJAX request
            $.ajax({
                url: "{{ route('admin.daily-check.store') }}",
                type: 'POST',
                data: formData,
                processData: false, // Prevent jQuery from processing data
                contentType: false, // Set content type to false for FormData
                success: function (response) {
                    if (response.success) {
                        swal({
                            title: "Daily AM Shift H&S Checklist Added Successfully",
                            icon: "success",
                            buttons: ["Ok"]
                        });
                        window.location.href = "{{ route('admin.daily-check.index') }}";
                    } else {
                        swal({
                            title: "Error!",
                            icon: "error",
                            buttons: ["Ok"]
                        });
                    }
                },
                error: function (xhr, status, error) {
                    swal({
                        title: "Error!",
                        text: xhr.responseText,
                        icon: "error",
                        buttons: ["Ok"]
                    });
                }
            });
        });
    });
</script>
@endpush
