@extends('admin.layout.app')

@section('title')
Preopening list
@endsection

@section('content')

    <div class="card-box">

        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Preopening List Items
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Status
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Is Checked By Supervisor ?
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                             Comment
                            </th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach ($items as $item)
                            <tr role="row" class="odd" id="row-{{ $item->id }}">
                                <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                <td>{{ $item->inspection_list->name }}</td>
                               
                                <td>  
                                    {{ $item->status }}
                                </td>
                                <td>  
                                    {{ $item->is_checked }}
                                </td>
                                <td>  
                                    {{ $item->comment }}
                                </td>
                            </tr>
                           
                        @endforeach

                        </tbody>
                    </table>
                   
               {{--  <div>
                
                {!!Form::open( ['route' => ['admin.preopen_list.destroy',$preopening->id] ,'id'=>'delete-form'.$preopening->id, 'method' => 'Delete']) !!}
                {!!Form::close() !!}

                        <a class="btn btn-danger" data-name="Preopen List"
                           data-url="{{ route('admin.preopen_list.destroy', $preopening) }}"
                           onclick="delete_form(this)">
                            Delete
                        </a>
                </div> --}}
                </div>
            </div>

        </div>
    </div>


@endsection


@section('footer')
    @include('admin.datatable.scripts')
@endsection

