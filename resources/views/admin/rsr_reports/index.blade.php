@extends('admin.layout.app')

@section('title')
    RSR Reports 
@endsection

@section('content')

    <div class="card-box">
        <form class="formSection"  method="GET">
        <div class="row">
        <div class='col-md-3'>
            <div class="form-group">
                <label for="last_name">Select Park</label>
                {!! Form::select('park_id',$parks,null, array('class' => 'form-control park','id'=>'park','placeholder'=>'Choose Park') ) !!}
            </div>
        </div>
        <div class='col-md-3'>
            <div class="form-group">
                <label for="last_name">Select Ride</label>
                {!! Form::select('ride_id', [],null, array('class' => 'form-control ride','id'=>'ride','placeholder'=>'Choose Ride')) !!}
            </div>
        </div>
    
        <div class='col-md-3'>
            <div class="form-group">
                <label for="middle_name">Time Slot Date </label>
                {!! Form::date('from',\Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date']) !!}
            </div>
        </div>
       
        <div class='col-md-2 mtButton'>
            <div class="input-group-btn">
                <button type="button" class="btn btn-primary save_btn waves-effect">Show</button>
            </div>
        </div>
    </div>
                {!!Form::close() !!}
        </div>
            <br><br>
        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <div id="items">

                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Ride
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Type
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Status
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Reported_by
                            </th> <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Date
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Process
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        @if(isset($items))

                        @foreach ($items as $item)
                            <tr role="row" class="odd" id="row-{{ $item->id }}">
                                <td tabindex="0" class="sorting_1">{{ $loop->iteration}}</td>
                                <td>{{ $item->rides->name??'' }}</td>
                                <td>{{ $item->type=='with_stoppages'?'With Stoppages':'Without Stoppages' }}</td>
                               
                                 <td>
                                 @if($item->status=='pending')
                                  <a href="{{url('rsr_reports/'.$item->id.'/approve')}}"
                                  class="btn btn-xs btn-danger"><i class="fa fa-check"></i> Approve</a>
                                  @endif
                                  @if($item->status=='approved')
                                <span>Verified</span>
                                    @endif
                                </td>
                                <td>{{ $item->created_by->name ??''}}</td>
                                <td>{{ $item->date }}</td>
                                {!!Form::open( ['route' => ['admin.rsr_reports.destroy',$item->id] ,'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                                {!!Form::close() !!}
                                <td>
                                    @if(auth()->user()->can('rsr_reports-list'))
                                        <a href="{{ route('admin.rsr_reports.show', $item) }}"
                                           class="btn btn-primary">Show/Print</a>
                                    @endif
                                    @if(auth()->user()->can('rsr_reports-edit'))
                                    <a href="{{ route('admin.rsr_reports.edit', $item->id) }}"
                                    class="btn btn-success">Edit</a>
                                    @endif
                                    @if(auth()->user()->can('rsr_reports-delete'))
                                        <a class="btn btn-danger" data-name="{{ $item->name }}"
                                           data-url="{{ route('admin.rsr_reports.destroy', $item) }}"
                                           onclick="delete_form(this)">
                                            Delete
                                        </a>
                                    @endif

                                </td>

                            </tr>

                        @endforeach
@endif
                        </tbody>
                    </table>

                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection
@push('scripts')

<script type="text/javascript">
     $("#park").change(function() {
            $.ajax({
                url: "{{ route('admin.getParkRides') }}?park_id=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#ride').html(data.html);
                }
            });
        });
       
    $(document).on("click", ".save_btn", function() {

        var park = $('#park').val();
        var ride = $('#ride').val();
        var date = $('#date').val();
        $.ajax({
            url: "{{ route('admin.rsrReport.search') }}",
            method: 'GET',
            data: {
                park_id: park,
                ride_id: ride,
                date: date,
            },
            success: function(data) {
                console.log(data)
                $('#items').html(' ');
                $('#items').html(data);

            }
        });
    });
</script>


@endpush

@section('footer')
    @include('admin.datatable.scripts')
@endsection

