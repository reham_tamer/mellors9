@extends('admin.layout.app')

@section('title')
    Mobile versions
@endsection

@section('content')

    <div class="card-box">

        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Type

                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Version

                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Status
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Process
                            </th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach ($items as $item)
                            <tr role="row" class="odd" id="row-{{ $item->id }}">
                                <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                <td>{{ $item->type }}</td>
                                <td>{{ $item->version }}</td>
                                <td>@if($item->status == 1)
                                        <sapn class="bg-success text-white" style="padding: 5px">Active</sapn>
                                    @else
                                        <sapn class="bg-danger text-white" style="padding: 5px">Not active</sapn>
                                    @endif</td>



                                <td>
                                    <a href="{{ route('admin.versions.edit', $item) }}"
                                       class="btn btn-info">Edit</a>


                                    <a class="btn btn-danger" data-name="{{ $item->type }}"
                                       data-url="{{ route('admin.versions.destroy', $item) }}"
                                       onclick="delete_form(this)">
                                        Delete
                                    </a>

                                </td>

                            </tr>

                        @endforeach

                        </tbody>
                    </table>


                </div>
            </div>

        </div>
    </div>

@endsection


@section('footer')
    @include('admin.datatable.scripts')
@endsection

