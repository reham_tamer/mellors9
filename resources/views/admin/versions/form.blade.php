 <div class="row">
     <div class="col-xs-12 col-sm-12 col-md-6">
         <div class="form-group">
             <label>Type :</label>
             <select name="type" class="select2">

                 <option value="ios" @if(isset($version) && $version?->type =='ios' )selected @endif>Ios</option>
                 <option value="android" @if(isset($version) && $version?->type =='android' )selected @endif>Android</option>

             </select>
             @error('type')
             <div class="invalid-feedback" style="color: #ef1010">
                 {{ $message }}
             </div>
             @enderror
         </div>
     </div>
     <div class="col-xs-12 col-sm-12 col-md-6" >
         <div class="form-group">
             <label for="phone">Version Number</label>
             {!! Form::number('version',null,['class'=>'form-control','id'=>'code','placeholder'=>'enter version']) !!}
             @error('version')
             <div class="invalid-feedback" style="color: #ef1010">
                 {{ $message }}
             </div>
             @enderror
         </div>
     </div>
     <div class="col-xs-12 col-sm-12 col-md-6">
         <div class="form-group">
             <label>Status :</label>
             <select name="status" class="select2">

                 <option value="1" @if(isset($version) && $version?->status ==1 )selected @endif>Active</option>
                 <option value="0" @if(isset($version) && $version?->status ==0 )selected @endif>Not active</option>


             </select>
             @error('status')
             <div class="invalid-feedback" style="color: #ef1010">
                 {{ $message }}
             </div>
             @enderror
         </div>
     </div>

    <div class="col-xs-12 aligne-center contentbtn">
        <button class="btn btn-primary waves-effect" type="submit">Save</button>
    </div>
</div>

