{{--@include('admin.common.errors')--}}
<div class="row">
<div class="col-xs-12">
<div class="form-group form-float">
    <label class="form-label">Tenant</label>
    <div class="form-line">
        {!! Form::select("tenant_id",$users,null,['class'=>'form-control select2','placeholder'=>'Choose Tenant...'])!!}
        @error('tenant_id')
        <div class="invalid-feedback" style="color: #ef1010">
            {{ $message }}
        </div>
        @enderror
    </div>
</div>
</div>
<div class="col-xs-12">
    <div class="form-group form-float">
        <label class="form-label">Move-In Date.</label>
        <div class="form-line">
            {!! Form::date("move_in_date",null,['class'=>'form-control','placeholder'=>' Unit NO'])!!}
            @error('unit_no')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
 </div> 
 <div class="mt-3">
    <h4>
        <strong>HEAT/PLUMBING </strong>
    </h4>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label">Heating  </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Heating" value="yes" class="ml-3"> OK
                    <input type="radio" name="Heating" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Heating_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
  
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label">Air Conditioning   </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Air_Conditioning_comment" value="yes" class="ml-3"> OK
                    <input type="radio" name="Air_Conditioning_comment" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Air_Conditioning_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Water Heater </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Water_Heater" value="yes" class="ml-3"> OK
                    <input type="radio" name="Water_Heater" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Water_Heater_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label">  Plumbing </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Plumbing " value="yes" class="ml-3"> OK
                    <input type="radio" name="Plumbing " value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Plumbing_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Washer/Dryer </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Washer/Dryer" value="yes" class="ml-3"> OK
                    <input type="radio" name="Washer/Dryer" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Washer/Dryer_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <br>
    <h4>
        <strong>SAFETY </strong>
    </h4> 
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Door Locks    </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Door_Locks" value="yes" class="ml-3"> OK
                    <input type="radio" name="Door_Locks" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Door_Locks_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label">Smoke Detector   </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Smoke_Detector" value="yes" class="ml-3"> OK
                    <input type="radio" name="Smoke_Detector" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Smoke_Detector_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Fire Extinguisher  </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Fire_Extinguisher" value="yes" class="ml-3"> OK
                    <input type="radio" name="Fire_Extinguisher" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Fire_Extinguisher_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <br>
    <h4>
        <strong>EXTERIOR  </strong>
    </h4> 
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Garage/Car Port   </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Garage/Car_Port" value="yes" class="ml-3"> OK
                    <input type="radio" name="Garage/Car_Port" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Garage/Car_Port_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Landscaping  </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Landscaping" value="yes" class="ml-3"> OK
                    <input type="radio" name="Landscaping" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Landscaping_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Pool/Spa  </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Pool/Spa" value="yes" class="ml-3"> OK
                    <input type="radio" name="Pool/Spa" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Pool/Spa_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <br>
    <h4>
        <strong>ENTRY/LIVING/DINING AREAS  </strong>
    </h4> 
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Walls/Ceiling  </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Walls/Ceiling" value="yes" class="ml-3"> OK
                    <input type="radio" name="Walls/Ceiling" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Walls/Ceiling_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Floor Coverings  </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Floor_Coverings" value="yes" class="ml-3"> OK
                    <input type="radio" name="Floor_Coverings" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Floor_Coverings_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Doors/Screens    </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Doors/Screens" value="yes" class="ml-3"> OK
                    <input type="radio" name="Doors/Screens" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Doors/Screens_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Outlets/Switches   </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Outlets/Switches" value="yes" class="ml-3"> OK
                    <input type="radio" name="Outlets/Switches" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Outlets/Switches_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Light Fixtures  </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Light_Fixtures" value="yes" class="ml-3"> OK
                    <input type="radio" name="Light_Fixtures" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Light_Fixtures_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Windows/Latches  </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Windows/Latches" value="yes" class="ml-3"> OK
                    <input type="radio" name="Windows/Latches" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Windows/Latches_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Windows/Screens </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Windows/Screens" value="yes" class="ml-3"> OK
                    <input type="radio" name="Windows/Screens" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Windows/Screens_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label">Windows Coverings  </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Windows_Coverings" value="yes" class="ml-3"> OK
                    <input type="radio" name="Windows_Coverings" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Windows_Coverings_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Closets   </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Closets" value="yes" class="ml-3"> OK
                    <input type="radio" name="Closets" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Closets_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Fireplace/Other </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Fireplace/Other" value="yes" class="ml-3"> OK
                    <input type="radio" name="Fireplace/Other" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Fireplace/Other_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <br>
    <h4>
        <strong>KITCHEN   </strong>
    </h4> 
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Walls/Ceiling  </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Walls/Ceiling" value="yes" class="ml-3"> OK
                    <input type="radio" name="Walls/Ceiling" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Walls/Ceiling_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label">Floor </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Floor" value="yes" class="ml-3"> OK
                    <input type="radio" name="Floor" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Floor_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Doors/Screens    </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Doors/Screens" value="yes" class="ml-3"> OK
                    <input type="radio" name="Doors/Screens" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Doors/Screens_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Outlets/Switches  </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Outlets/Switches" value="yes" class="ml-3"> OK
                    <input type="radio" name="Outlets/Switches" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Outlets/Switches_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Light Fixtures </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Light_Fixtures" value="yes" class="ml-3"> OK
                    <input type="radio" name="Light_Fixtures" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Light_Fixtures_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label">Windows/Latches </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Windows/Latches_kitchen" value="yes" class="ml-3"> OK
                    <input type="radio" name="Windows/Latches_kitchen" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Windows/Latches_kitchen_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label">Screens/Shades  </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Screens/Shades" value="yes" class="ml-3"> OK
                    <input type="radio" name="Screens/Shades" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Screens/Shades_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label">Window Coverings   </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Window_Coverings_kitchen" value="yes" class="ml-3"> OK
                    <input type="radio" name="Window_Coverings_kitchen" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Window_Coverings_kitchen_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Oven/Range  </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Oven/Range" value="yes" class="ml-3"> OK
                    <input type="radio" name="Oven/Range" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Oven/Range_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label">Fan/Light/Controls     </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Fan/Light/Controls" value="yes" class="ml-3"> OK
                    <input type="radio" name="Fan/Light/Controls" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Fan/Light/Controls_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Refrigerator  </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Refrigerator" value="yes" class="ml-3"> OK
                    <input type="radio" name="Refrigerator" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Refrigerator_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label">Dishwasher  </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Dishwasher" value="yes" class="ml-3"> OK
                    <input type="radio" name="Dishwasher" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Dishwasher_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Sink/Faucets </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Sink/Faucets" value="yes" class="ml-3"> OK
                    <input type="radio" name="Sink/Faucets" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Sink/Faucets_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label">Disposal </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Disposal" value="yes" class="ml-3"> OK
                    <input type="radio" name="Disposal" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Disposal_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Counter_Tops" value="yes" class="ml-3"> OK
                    <input type="radio" name="Counter_Tops" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Counter_Tops_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label">Cabinets/Other </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Cabinets/Other" value="yes" class="ml-3"> OK
                    <input type="radio" name="Cabinets/Other" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Cabinets/Other_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <br>
    <h4>
        <strong>BATHROOM 1   </strong>
    </h4> 
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Walls/Ceiling </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Walls/Ceiling" value="yes" class="ml-3"> OK
                    <input type="radio" name="Walls/Ceiling" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Walls/Ceiling_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Floor</label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Floor" value="yes" class="ml-3"> OK
                    <input type="radio" name="Floor" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Floor_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label">Outlets/Switches </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Outlets/Switches" value="yes" class="ml-3"> OK
                    <input type="radio" name="Outlets/Switches" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Outlets/Switches_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> Light Fixtures</label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="Light_Fixtures" value="yes" class="ml-3"> OK
                    <input type="radio" name="Light_Fixtures" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("Light_Fixtures_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <div class="col-xs-4">
                <label class="form-label"> </label>
            </div>
            <div class="form-line">
                <div class="col-xs-2" style="background-color:#f1cf92;">
                    <input type="radio" name="" value="yes" class="ml-3"> OK
                    <input type="radio" name="" value="no" class="ml-3"> NOT
                </div>
                <div class="col-xs-6" >
                    {!! Form::text("_comment", null, ['class' => 'form-control','style'=>'background-color:#f1cf92', 'placeholder' => 'Comment']) !!}
                </div>
            </div>
        </div>
    </div>
<div class="col-xs-12 aligne-center contentbtn">
    <button class="btn btn-primary waves-effect" type="submit">Save</button>
</div>
</div>
