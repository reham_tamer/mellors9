<div class="font-bold m-3 text-center">
    @if($outRang)
        <span class="font-bold m-3 text-danger">Filter out of range</span>
    @else
        <span class="font-bold m-3 text-danger">Total Count</span> : @if (isset($items))
            {{$items->count()}}
        @else
            0
        @endif
    @endif
</div>
@if(isset($items) && count($items) >0)
    <div class="form-group left row col-md-12">
        <a class="btn btn-info" href="{{ route('admin.export.availability',$items->pluck('id')) }}">Export File Csv</a>

    </div>
    <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
        <div class="row">
            {{--        <div class="col-xs-12">--}}
            {{--            <input type="button" value="Print Report" id="printDiv" class="btn btn-primary printBtn"></input>--}}
            {{--        </div>--}}
            <div class="col-xs-12 printable_div" id="myDivToPrint">
                <div class="col-xs-12 printOnly">
                    <div class="logo">
                        <img src="{{ asset('/_admin/assets/images/logo1.png') }}" alt="Mellors-img" title="Mellors"
                             class="image">
                    </div>
                    <h3 class="table-title">Ride Availability Report</h3>
                </div>
                <div class="col-xs-12">
                    <div class='responsive-wrapper'>
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1" aria-sort="ascending">ID
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Park
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Zone
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Ride
                                </th>

                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Time Slot Date
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    First Status
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Second Status
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    No Of Gondolas
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    No Of Seats
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Comment
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            @if (isset($items))
                                @foreach ($items as $item)
                                    <tr role="row" class="odd" id="row-{{ $item->id }}">
                                        <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                        <td>{{ $item->parks?->name }}</td>
                                        <td>{{ $item->zone?->name }}</td>
                                        <td>{{ $item->rides?->name }}</td>
                                        <td>{{ $item->date }}</td>
                                        <td>{{ $item->first_status }}</td>
                                        <td>{{ $item->second_status }}</td>
                                        <td>{{ $item->no_of_gondolas }}</td>
                                        <td>{{ $item->no_of_seats }}</td>
                                        <td>{!! $item->comment !!} </td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>

    </div>
@endif
