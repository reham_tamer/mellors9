{{--@include('admin.common.errors')--}}
<div class="row">
    <div class="col-sm-12">
        <div class='responsive-wrapper'>
            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                        colspan="1" aria-sort="ascending">ID
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Ride
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        First Status
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Second Status
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        No Of Gondolas
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        No Of Seats
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Comment
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach ($items as $item)

                    <tr role="row" class="odd" id="row-{{ $item->id }}">
                        <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                        <td> {{$item->rides->name}}
                            <input type="hidden" name="ride_id[]" value=" {{$item->rides->id}}">
                        </td>
                        <td>
                            <label>

                                <select name="first_status[]" id="element_id"
                                        class="form-control  " disabled>
                                    @if($item->first_status === "yes")
                                        <option value="yes" selected>Yes</option>
                                        <option value="no">No</option>
                                        <option value="">None</option>

                                    @elseif($item->first_status === "no")
                                        <option value="no" selected>No</option>
                                        <option value="yes">Yes</option>
                                        <option default value="">None</option>

                                    @else
                                        <option default value=""></option>
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    @endif
                                </select>
                            </label>
                        </td>
                        <td>
                            <label>
                                <select name="second_status[]" id="element_id" data-id="{{$item->id}}"
                                        class="form-control element-id"
                                        @if($item->second_status === "no")  readonly="" @endif>
                                    <option value="none"></option>
                                    <option value="yes"
                                            @if($item->second_status == 'yes' || $item->second_status == null) selected @endif>
                                        Yes
                                    </option>
                                    <option value="no" @if($item->second_status == 'no') selected @endif>No</option>
                                </select>
                            </label>
                        </td>
                        <td> {!! Form::number("no_of_gondolas[]",$item->no_of_gondolas,['class'=>'form-control','placeholder'=>' number_of_gondolas'])!!}</td>
                        <td> {!! Form::number("no_of_seats[]",$item->no_of_seats,['class'=>'form-control','placeholder'=>' number_of_seats'])!!}</td>
                        <td id="model{{$item->id}}"
                            @if($item->first_status == "yes"   &&   $item->second_status == 'yes' || $item->second_status == null && $item->first_status == "yes")    hidden @endif >
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#exampleModal{{ $item->id }}">
                                The reason
                            </button>
                        </td>

                        <div class="modal fade" id="exampleModal{{ $item->id }}" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="ml-2">
                                        <h5 class="modal-title" id="exampleModalLabel">{{$item->name}}</h5>
                                        {{--                                     {{$item->stoppage_reason}}--}}
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="">
                                            <div class="form-group ">
                                                <label class="col-lg-12">Stoppage Reasons Main Category
                                                    :</label>
                                                <div class="">
                                                    {!! Form::select('stopage_category_id[]',@$stopage_category?$stopage_category:[],$item->stoppage_reason?->stopage_category_id, array('class' => ' form-control
                                                    mai_category','placeholder'=>'Stoppage main Category','data-id'=>$item->id)) !!}
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="col-lg-12">Stoppage Sub Category :
                                                </label>
                                                <div class="">
                                                    {!! Form::select('stopage_sub_category_id[]',@$Supcategory?$Supcategory:[],$item->stoppage_reason?->stopage_sub_category_id, array('class' => ' form-control
                                                    subCategory','placeholder'=>'Stoppage main Category','id'=>'subCategory'.$item->id)) !!}
                                                </div>
                                            </div>
                                            {!! Form::textArea("comment[]",$item->stoppage_reason?->comment,['class'=>'form-control','placeholder'=>'Comment'])!!}
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <td id="comment{{$item->id}}"
                            @if($item->first_status === "no" ||  $item->second_status == 'no') hidden @endif > {!! Form::textArea("comment[]",$item->comment,['class'=>'form-control','placeholder'=>'Comment'])!!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <input type="hidden" name="park_id" value=" {{$id}}">

    <div class="col-xs-12 aligne-center contentbtn">
        <button class="btn btn-primary waves-effect" type="submit">Save</button>
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        $('.mai_category').change(function () {
            var val = $(this).val();
            var id = $(this).data('id');
            $.ajax({
                type: "post",
                url: "{{ route('admin.getSubStoppageCategories') }}",
                data: {
                    'stopage_category_id': val,
                    '_token': "{{ @csrf_token() }}"
                },
                success: function (data) {
                    var options = '<option disabled>Choose Main Category</option>';
                    $.each(data.subCategory, function (key, value) {
                        options += '<option value="' + value.id + '">' + value.name +
                            '</option>';

                    });
                    $("#subCategory"+id).empty().append(options);
                }
            });
        });

        $('.element-id').change(function () {
            // alert($(this).data('id'))
            // alert($(this).val())
            if ($(this).val() == 'no') {
                $('#comment' + $(this).data('id')).hide();
                $('#model' + $(this).data('id')).show();
            } else {
                $('#comment' + $(this).data('id')).show();
                $('#model' + $(this).data('id')).hide();
            }
        });
    </script>
@endpush
