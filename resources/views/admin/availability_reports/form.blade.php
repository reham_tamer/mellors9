{{--@include('admin.common.errors')--}}
<div class="row">
    <div class="col-sm-12">
            {!! Form::date('date', \Carbon\Carbon::now()->toDate(), ['class' => 'form-control', 'id' => 'date', 'style' => 'display: none;']) !!}
        <div class='responsive-wrapper'>
            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                        colspan="1" aria-sort="ascending">ID
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Ride
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        First Status
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Second Status
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        No Of Gondolas
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        No Of Seats
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                        Comment
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach ($rides as $item)

                    <tr role="row" class="odd" id="row-{{ $item->id }}">
                        <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                        <td> {{$item->name}}
                            <input type="hidden" name="ride_id[]" value=" {{$item->id}}">
                            <input type="hidden" name="park_id" value=" {{$item->park_id}}">
                        </td>
                        <td>
                            <label>
                                <select name="first_status[]" id="element_id" data-id="{{$item->id}}"
                                        class="form-control element-id">
                                    <option default value=""></option>
                                    <option value="yes" selected>Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </label>
                        </td>
                        <td><label></label></td>


                        <td> {!! Form::number("no_of_gondolas[]",$item->no_of_gondolas,['class'=>'form-control','placeholder'=>' number_of_gondolas'])!!}</td>
                        <td> {!! Form::number("no_of_seats[]",$item->capacity_one_cycle,['class'=>'form-control','placeholder'=>' number_of_seats'])!!}</td>

                        <td id="model{{$item->id}}"   @if($item->rideStoppages?->last() && $item->rideStoppages?->last()?->ride_status == 'stoppage')  @else hidden

                        @endif>
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#exampleModal{{ $item->id }}">
                                Add the reason
                            </button>
                        </td>


                        @if($item->rideStoppages?->last() && $item->rideStoppages?->last()?->ride_status != 'active')
                            <td id="stoppage{{$item->id}}">
                                <span class="bold row col-md-12" style="color: #9c3328">The ride has old stoppage</span>
                                <div class="row col-md-6">
                                    <input type="radio" name="choose[{{$item->id}}]" value="another_stoppage" data-toggle="modal"
                                           data-target="#exampleModal{{ $item->id }}">
                                    Add another stoppage
                                </div>
                                <div class=" col-md-6">
                                    <input type="radio" name="choose[{{$item->id}}]" value="resolve" checked>
                                    Resolve it
                                </div>

                            </td>
                        @else
                            <td id="comment{{$item->id}}"> {!! Form::textArea("comment[]",null,['class'=>'form-control','placeholder'=>'Comment'])!!}
                            </td>
                        @endif
                        <div class="modal fade" id="exampleModal{{ $item->id }}" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="ml-2">
                                        <h5 class="modal-title" id="exampleModalLabel">{{$item->name}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="">
                                            <div class="form-group ">
                                                <label class="col-lg-12">Stoppage Reasons Main Category
                                                    :</label>
                                                <div class="" data-id="">
                                                    {!! Form::select('stopage_category_id[]',@$stopage_category?$stopage_category:[],null, array('class' => ' form-control
                                                    mai_category','placeholder'=>'Stoppage main Category' ,'data-id'=>$item->id)) !!}
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="col-lg-12">Stoppage Sub Category :
                                                </label>
                                                <div class="">
                                                    {!! Form::select('stopage_sub_category_id[]',@$Supcategory?$Supcategory:[],null, array('class' => ' form-control
                                                    subCategory','placeholder'=>'Stoppage main Category','id'=>'subCategory'.+$item->id)) !!}
                                                </div>
                                            </div>
                                            {!! Form::textArea("comment[]",null,['class'=>'form-control','placeholder'=>'Comment'])!!}
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-xs-12 aligne-center contentbtn">
        <button class="btn btn-primary waves-effect" type="submit">Save</button>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        $('.mai_category').change(function () {
            var val = $(this).val();
            var id = $(this).data('id');
            $.ajax({
                type: "post",
                url: "{{ route('admin.getSubStoppageCategories') }}",
                data: {
                    'stopage_category_id': val,
                    '_token': "{{ @csrf_token() }}"
                },
                success: function (data) {
                    var options = '<option disabled>Choose Main Category</option>';
                    $.each(data.subCategory, function (key, value) {
                        options += '<option value="' + value.id + '">' + value.name +
                            '</option>';

                    });
                    $("#subCategory" + id).empty().append(options);
                }
            });
        });

        $('.element-id').change(function () {
            // alert($(this).data('id'))
            // alert($(this).val())
            if ($(this).val() == 'no') {
                $('#comment' + $(this).data('id')).hide();
                $('#model' + $(this).data('id')).show();
            } else {
                $('#comment' + $(this).data('id')).show();
                $('#model' + $(this).data('id')).hide();
            }
        });
    </script>
@endpush
