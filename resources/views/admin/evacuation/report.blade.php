@extends('admin.layout.app')

@section('title')
    Evacuation Report
@endsection

@section('content')

    <div class="card-box">
        <form class="formSection" action="{{ url('/evacuation_report/') }}" method="GET">

            <div class="row">
                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="last_name">Select Park</label>
                        {!! Form::select('park_id', $parks, null, [
                            'class' => 'form-control park',
                            'id' => 'park',
                            'placeholder' => 'Choose Park',
                        ]) !!}
                    </div>
                </div>
                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="last_name">Select Ride</label>
                        {!! Form::select('ride_id', [], null, [
                            'class' => 'form-control ride',
                            'id' => 'ride',
                            'placeholder' => 'Choose Ride',
                        ]) !!}
                    </div>
                </div>

                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="middle_name">Time Slot Date From </label>
                        {!! Form::date('from', \Carbon\Carbon::now()->toDate(), ['class' => 'form-control', 'id' => 'date']) !!}
                    </div>
                </div>
                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="middle_name">Time Slot Date To </label>
                        {!! Form::date('to', \Carbon\Carbon::now()->toDate(), ['class' => 'form-control', 'id' => 'date_to']) !!}
                    </div>
                </div>
                <div class='col-md-2 mtButton'>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-primary save_btn waves-effect">Show</button>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
    <br><br>
    <div id="items">

    </div>

@endsection
@push('scripts')
    <script type="text/javascript">
        $("#park").change(function () {
            $.ajax({
                url: "{{ route('admin.getParkRides') }}?park_id=" + $(this).val(),
                method: 'GET',
                success: function (data) {
                    $('#ride').html(data.html);
                }
            });
        });
    </script>
    <script language="javascript">
        $('#printDiv').click(function () {
            $('#myDivToPrint').show();
            window.print();
            return false;
        });
        $(document).on("click", ".save_btn", function () {

            var park = $('#park').val();
            var date_from = $('#date').val();
            var date_to = $('#date_to').val();
            var ride = $('#ride').val();
            $.ajax({
                url: "{{ route('admin.evacuation.report') }}",
                method: 'GET',
                data: {
                    park_id: park,
                    from: date_from,
                    to: date_to,
                    ride_id: ride,
                },
                success: function (data) {
                    console.log(data)
                    $('#items').html(' ');
                    $('#items').html(data);

                }
            });
        });
    </script>
@endpush
@section('footer')
    @include('admin.datatable.scripts')
@endsection
