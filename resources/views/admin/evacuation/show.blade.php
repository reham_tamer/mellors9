@extends('admin.layout.app')

@section('title')
    Show Evacuation
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Show Evacuation</h4>
                <a class="input-group-btn" href="{{ route('admin.evacuations.index') }}">
                    <button type="button" class="btn waves-effect waves-light btn-primary">back</button>
                </a>
             
<div class="row">
    <div class="col-xs-6">
        <div class="form-group form-float">
            <label class="form-label">Evacuation time</label>
            <div class="form-line">
                <input type="time" name="time" value="{{$evacuation->time}}" class="ml-4 form-control" required disabled>
            </div>
        </div>
    </div>
    <div class="col-xs-6" id="rides">

        <div class="form-group form-float">
            <label class="form-label">Total Rider</label>
            <div class="form-line">
                <input type="number" name="total_rider"  value="{{$evacuation->total_rider}}" class="ml-4 form-control" required disabled>
            </div>
        </div>
    </div>
 

    <div class="col-xs-6" id="rides">
        <div class="form-group form-float">
            <label class="form-label">First Rider Off</label>
            <div class="form-line">
                <input type="time" name="first_rider_off" value="{{$evacuation->first_rider_off}}" class="ml-4 form-control" required disabled >
            </div>
        </div>
    </div>
    <div class="col-xs-6" id="rides">
        <div class="form-group form-float">
            <label class="form-label">Last Rider Off</label>
            <div class="form-line">
                <input type="time" name="last_rider_off" value="{{$evacuation->last_rider_off}}" class="ml-4 form-control" required disabled>
            </div>
        </div>
    </div>
    <div class="col-xs-4" id="rides">
        <div class="form-group form-float">
            <label class="form-label">Abnormal</label>
            <div class="form-line">
                <input type="checkbox" name="abnormal" class="ml-4"  @if($evacuation->abnormal == 1) checked @endif disabled>
            </div>
        </div>
    </div>
    
    <div class="col-xs-4" id="rides">
        <div class="form-group form-float">
            <label class="form-label">Longer than normal</label>
            <div class="form-line">
                <input type="checkbox" name="longer_than_normal" class="ml-4"  @if($evacuation->longer_than_normal == 1) checked @endif disabled>
            </div>
        </div>
    </div>
    <div class="col-xs-4" id="rides">
        <div class="form-group form-float">
            <label class="form-label">Medics required</label>
            <div class="form-line">
                <input type="checkbox" name="medics_required" class="ml-4"  @if($evacuation->medics_required == 1) checked @endif disabled>
            </div>
        </div>
    </div>
    <div class="col-xs-4" id="rides">
        <div class="form-group form-float">
            <label class="form-label">Civil defense involved</label>
            <div class="form-line">
                <input type="checkbox" name="civil_defense_involved" class="ml-4"  @if($evacuation->civil_defense_involved == 1) checked @endif disabled>
            </div>
        </div>
    </div>
    <div class="col-xs-4" id="rides">
        <div class="form-group form-float">
            <label class="form-label">Vip guest involved</label>
            <div class="form-line">
                <input type="checkbox" name="vip_guest_involved" class="ml-4"  @if($evacuation->vip_guest_involved == 1) checked @endif disabled>
            </div>
        </div>
    </div>
    <div class="col-xs-4" id="rides">
        <div class="form-group form-float">
            <label class="form-label">Customer service issue</label>
            <div class="form-line">
                <input type="checkbox" name="customer_service_issue" class="ml-4"  @if($evacuation->customer_service_issue == 1) checked @endif disabled>
            </div>
        </div>
    </div>
    <div class="col-xs-4" id="rides">
        <div class="form-group form-float">
            <label class="form-label">Property damage</label>
            <div class="form-line">
                <input type="checkbox" name="property_damage" class="ml-4"  @if($evacuation->property_damage == 1) checked @endif disabled>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <label class="form-label">First Aid</label>
            <div class="form-line">
                {!! Form::textArea("first_aid",$evacuation->first_aid,['class'=>'form-control  ', 'disabled' => 'disabled'])!!}
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <label class="form-label">  Evacuation Details</label>
            <div class="form-line">
                {!! Form::textArea("evacuation_details",$evacuation->evacuation_details,['class'=>'form-control ', 'disabled' => 'disabled']) !!}
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <label class="form-label">Customer Service Gesture</label>
            <div class="form-line">
                {!! Form::textArea("customer_service_gesture",$evacuation->customer_service_gesture,['class'=>'form-control  ', 'disabled' => 'disabled'])!!}
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group form-float">
            <label class="form-label"> Portal Overview</label>
            <div class="form-line">
                {!! Form::textArea("portal_overview",$evacuation->portal_overview,['class'=>'form-control  ', 'disabled' => 'disabled'])!!}
            </div>
        </div>
    </div>
   
        <div class="col-xs-12">

            @if($evacuation->image)
                <div class="flex-img">
                    <a >
                        <img class="img-preview" src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($evacuation->image),now()->addMinutes(30)) }}"
                             style="height: 400px; width: 400px"></a>
                </div>
            @endif
        </div>

    </div>

@endsection
