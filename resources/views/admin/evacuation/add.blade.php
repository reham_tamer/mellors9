@extends('admin.layout.app')

@section('title')
    Add New Evacuation
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30"> Add New Evacuation Form </h4>
                {!! Form::open([
                    'route' => 'admin.evacuations.store',
                    'class' => 'form phone_validate form-horizontal',
                    'method' => 'Post',
                    'enctype' => 'multipart/form-data',
                    'files' => true,
                ]) !!}
                @csrf
                <div class="row">
                    @if(request()->is('evacuations/create'))
                    <div class="col-xs-12">
                        <div class="form-group form-float">
                            <label class="form-label">Park</label>
                            <div class="form-line">
                                {!! Form::select('park_id', auth()->user()->parks->pluck('name','id'),null, array('class' => 'form-control col-lg-6 select2')) !!}
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="col-xs-6">
                        <div class="form-group form-float">
                            <label class="form-label">Evacuation time</label>
                            <div class="form-line">
                                <input type="time" name="time" class="ml-4 form-control"
                                value="{{ !request()->is('evacuations/create') ? (isset($stoppage) ? $stoppage->time_slot_start : '') : '' }}" required>

                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6" id="rides">

                        <div class="form-group form-float">
                            <label class="form-label">Total Rider</label>
                            <div class="form-line">
                                <input type="number" name="total_rider" class="ml-4 form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-6" id="rides">
                        <div class="form-group form-float">
                            <label class="form-label">First Rider Off</label>
                            <div class="form-line">
                                <input type="time" name="first_rider_off" class="ml-4 form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6" id="rides">
                        <div class="form-group form-float">
                            <label class="form-label">Last Rider Off</label>
                            <div class="form-line">
                                <input type="time" name="last_rider_off" class="ml-4 form-control" required>
                            </div>
                        </div>
                    </div>
                     <div class="col-xs-4" id="rides">
                        <div class="form-group form-float">
                            <label class="form-label">Normal</label>
                            <div class="form-line">
                                <input type="checkbox" name="normal" class="ml-4" >
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4" id="rides">
                        <div class="form-group form-float">
                            <label class="form-label">Abnormal</label>
                            <div class="form-line">
                                <input type="checkbox" name="abnormal" class="ml-4" >
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4" id="rides">
                        <div class="form-group form-float">
                            <label class="form-label">Longer than normal</label>
                            <div class="form-line">
                                <input type="checkbox" name="longer_than_normal" class="ml-4" >
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4" id="rides">
                        <div class="form-group form-float">
                            <label class="form-label">Medics required</label>
                            <div class="form-line">
                                <input type="checkbox" name="medics_required" class="ml-4" >
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4" id="rides">
                        <div class="form-group form-float">
                            <label class="form-label">Civil defense involved</label>
                            <div class="form-line">
                                <input type="checkbox" name="civil_defense_involved" class="ml-4" >
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4" id="rides">
                        <div class="form-group form-float">
                            <label class="form-label">Vip guest involved</label>
                            <div class="form-line">
                                <input type="checkbox" name="vip_guest_involved" class="ml-4" >
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4" id="rides">
                        <div class="form-group form-float">
                            <label class="form-label">Customer service issue</label>
                            <div class="form-line">
                                <input type="checkbox" name="customer_service_issue" class="ml-4" >
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4" id="rides">
                        <div class="form-group form-float">
                            <label class="form-label">Property damage</label>
                            <div class="form-line">
                                <input type="checkbox" name="property_damage" class="ml-4" >
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group form-float">
                            <label class="form-label">First Aid</label>
                            <div class="form-line">
                                {!! Form::textArea("first_aid",null,['class'=>'form-control   ', 'rows' => 2])!!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group form-float">
                            <label class="form-label">  Evacuation Details</label>
                            <div class="form-line">
                                {!! Form::textArea("evacuation_details", null, ['class' => 'form-control', 'rows' => 2]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group form-float">
                            <label class="form-label">Customer Service Gesture</label>
                            <div class="form-line">
                                {!! Form::textArea("customer_service_gesture",null,['class'=>'form-control   ', 'rows' => 2])!!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group form-float">
                            <label class="form-label"> Portal Overview</label>
                            <div class="form-line">
                                {!! Form::textArea("portal_overview",null,['class'=>'form-control   ', 'rows' => 2])!!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group form-float">
                            <label class="form-label"> Image</label>
                            <div class="form-line">
                                <input type="file" class="form-control" name="image">
                            </div>
                        </div>
                    </div>

                </div>
                @if(isset($park_time_id))
                <input type="hidden" name="park_time_id" value="{{ $park_time_id }}" >
                @endif
                 <input type="hidden" name="stoppage_id" value="{{ !request()->is('evacuations/create') ? (isset($stoppage) ? $stoppage->id : '') : '' }}" >
                <div class="  aligne-center contentbtn">
                    <button class="btn btn-primary waves-effect" type="submit">Save</button>
                </div>

                {!! Form::close() !!}
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->
@endsection
@push('scripts')
    {!! JsValidator::formRequest(\App\Http\Requests\Dashboard\Accident\EvacuationRequest::class, '#form') !!}
    <script>


    </script>
@endpush
