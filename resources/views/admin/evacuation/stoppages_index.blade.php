@extends('admin.layout.app')

@section('title')
    Evacuation Stoppages 
@endsection

@section('content')
    <div class="card-box">
       
            <a class="input-group-btn" href="{{ route('admin.evacuations.create') }}">
                <button type="button" class="btn waves-effect waves-light btn-primary">Add Evacuation To Park
                 </button>
            </a>
            <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1" aria-sort="ascending">ID
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Ride
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Park
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Stoppage time
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                   Stoppage type
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Add Evacuation
                                </th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach ($items as $item)
                                <tr role="row" class="odd" id="row-{{ $item->id }}">
                                    <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                    <td>{{ $item->ride->name}}</td>
                                    <td>{{ $item->park->name }}</td>
                                    <td>{{ $item->time_slot_start }}</td>
                                    <td>{{ $item->stopageSubCategory->name ?? '' }} </td>
                                    <td>
                                            <a href="{{ route('admin.addEvacuationRide', $item->id) }}"
                                               class="btn btn-info">Add</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
@endsection
@section('footer')
    @include('admin.datatable.scripts')
@endsection
