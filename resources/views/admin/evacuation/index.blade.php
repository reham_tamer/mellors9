@extends('admin.layout.app')

@section('title')
    Evacuation
@endsection

@section('content')
    <div class="card-box">
        <form class="formSection" action="{{url('/search_evacuation/')}}" method="GET">

            @csrf
            <div class="row">
                <div class='col-md-4'>
                    <div class="form-group">
                        <label for="middle_name">Time Slot Date </label>
                        {!! Form::date('date',request('date'),['class'=>'form-control','id'=>'date_to']) !!}
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <label for="middle_name">Park </label>
                        {!! Form::select('park_id', $userparks, request('park_id'), [
                            'class' => 'form-control select2 Parks',
                            'placeholder' => 'Choose Park...',
                        ]) !!}
                    </div>
                </div>
                <div class='col-md-2 mtButton'>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary save_btn waves-effect">Show</button>
                    </div>
                </div>
            </div>
            {!!Form::close() !!}
            <br>
            <a class="input-group-btn" href="{{ route('admin.evacuationStoppages') }}">
                <button type="button" class="btn waves-effect waves-light btn-primary">Add Evacuation
                 </button>
            </a>
            <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1" aria-sort="ascending">ID
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Date
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Time
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Total Rider
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Park / Ride
                                </th>


                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Process
                                </th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach ($items as $item)
                                <tr role="row" class="odd" id="row-{{ $item->id }}">
                                    <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                    <td>{{ $item->submit_date}}</td>
                                    <td>{{ $item->time }}</td>
                                    <td>{{ $item->total_rider?? 'It is park evacuation' }}</td>
                                    <td>{{ $item->park->name }}
                                        @if($item->ride)
                                        / {{ $item->ride?->name }}
                                        @endif
                                    <td>

                                        @if(auth()->user()->can('investigation-edit'))

                                            <a href="{{ route('admin.evacuations.edit', $item) }}"
                                               class="btn btn-info">Edit</a>
                                        @endcan
{{--                                        @if(auth()->user()->can('investigation-list'))--}}

                                  <a href="{{ route('admin.evacuations.show', $item->id) }}"
                                              class="btn btn-info">show</a>
{{--                                        @endcan
 --}}                                       
                                            {!!Form::open( ['route' => ['admin.evacuations.destroy',$item->id] ,'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                                            {!!Form::close() !!}
                                            @if(auth()->user()->can('investigation-delete'))
                                            <a class="btn btn-danger" data-name=""
                                               data-url="{{ route('admin.evacuations.destroy', $item) }}"
                                               onclick="delete_form(this)">
                                               Delete
                                            </a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
@endsection
@section('footer')
    @include('admin.datatable.scripts')
@endsection
