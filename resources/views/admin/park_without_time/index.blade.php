@extends('admin.layout.app')

@section('title')
    All Zones 
@endsection

@section('content')

    <div class="card-box">
        <form class="formSection" action="{{url('/search_park_without_times')}}" method="GET">

            <div class="form-group">
                <label for="zone_id">Zone</label>
                <select name="zone_id" id="zone_id" class="form-control">
                    <option value="">Select Zone</option>
                    @foreach ($allZones as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="date">Date</label>
                <input type="date" name="date" id="date" class="form-control" value="{{ $date }}">
            </div>
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
        
        @if(isset($zones) && $zones->isNotEmpty())
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Pre-opening Inspection</th>
                        <th>Pre-closing Inspection</th>
                        <th>Hourly Inspection</th>
                        <th>Weekly Inspection</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($zones as $item)
                        <tr id="row-{{ $item->id }}">
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->name }}</td>
                            <td>
                                @if (auth()->user()->can('show_preopen_list'))
                                    @if(in_array($item->id, $open_data_exist))
                                        <a href="{{ url('show_zone_preopen_list/' . $item->id . '/' . $date) }}" class="btn btn-success">
                                            <i class="fa fa-info"></i> Show
                                        </a>
                                        <?php
                                            $info = \App\Models\ZoneInspectionInfo::where('opened_date', $date)
                                                ->where('zone_id', $item->id)
                                                ->where('type', 'preopening')
                                                ->first();
                                        ?>
                                        @if($info && $info->status == 'pending')
                                            <a href="{{ url('preopenApprove/' . $item->id . '/' . $date) }}" class="btn btn-xs btn-danger">
                                                <i class="fa fa-check"></i> Approve
                                            </a>
                                        @elseif($info && $info->status == 'approved')
                                            <span class="btn btn-xs btn-info">Verified</span>
                                        @endif
                                    @else
                                        Not Added Yet
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if (auth()->user()->can('show_preopen_list'))
                                    @if(in_array($item->id, $close_data_exist))
                                        <a href="{{ url('show_zone_preclose_list/' . $item->id . '/' . $date) }}" class="btn btn-success">
                                            <i class="fa fa-info"></i> Show
                                        </a>
                                        <?php
                                            $info = \App\Models\ZoneInspectionInfo::where('opened_date', $date)
                                                ->where('zone_id', $item->id)
                                                ->where('type', 'preclosing')
                                                ->first();
                                        ?>
                                        @if($info && $info->status == 'pending')
                                            <a href="{{ url('preclosApprove/' . $item->id . '/' . $date) }}" class="btn btn-xs btn-danger">
                                                <i class="fa fa-check"></i> Approve
                                            </a>
                                        @elseif($info && $info->status == 'approved')
                                            <span class="btn btn-xs btn-info">Verified</span>
                                        @endif
                                    @else
                                        Not Added Yet
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if (auth()->user()->can('show_preopen_list'))
                                    @if(in_array($item->id, $daily_data_exist))
                                        <a href="{{ url('show_zone_daily_list/' . $item->id . '/' . $date) }}" class="btn btn-success">
                                            <i class="fa fa-info"></i> Show
                                        </a>
                                    @else
                                        Not Added Yet
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if (auth()->user()->can('show_preopen_list'))
                                    @if(in_array($item->id, $weekly_data_exist))
                                        <a href="{{ url('show_zone_weekly_list/' . $item->id . '/' . $date) }}" class="btn btn-success">
                                            <i class="fa fa-info"></i> Show
                                        </a>
                                        <?php
                                            $info = \App\Models\ZoneInspectionInfo::where('opened_date', $date)
                                                ->where('zone_id', $item->id)
                                                ->where('type', 'inspection_list')
                                                ->first();
                                        ?>
                                        @if($info && $info->status == 'pending')
                                            <a href="{{ url('weeklyApprove/' . $item->id . '/' . $date) }}" class="btn btn-xs btn-danger">
                                                <i class="fa fa-check"></i> Approve
                                            </a>
                                        @elseif($info && $info->status == 'approved')
                                            <span class="btn btn-xs btn-info">Verified</span>
                                        @endif
                                    @else
                                        Not Added Yet
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>No data found for the selected criteria.</p>
        @endif
        </div>


@endsection


@section('footer')
    @include('admin.datatable.scripts')
@endsection
