<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="name">Name</label>
            {!! Form::text('name',null,['class'=>'form-control','id'=>'name','placeholder'=>'name']) !!}
            @error('name')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="first_name">First Name</label>
            {!! Form::text('first_name',null,['class'=>'form-control','id'=>'first_name','placeholder'=>'first_name']) !!}
            @error('first_name')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="middle_name">Middle Name</label>
            {!! Form::text('middle_name',null,['class'=>'form-control','id'=>'middle_name','placeholder'=>'middle_name']) !!}
            @error('middle_name')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="last_name">Last Name</label>
            {!! Form::text('last_name',null,['class'=>'form-control','id'=>'last_name','placeholder'=>'last_name']) !!}

            @error('last_name')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="email">Email</label>
            {!! Form::email('email',null,['class'=>'form-control','id'=>'email','placeholder'=>'email']) !!}
            @error('email')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="user_number">Employee Number</label>
            {!! Form::number('user_number',null,['class'=>'form-control','id'=>'user_number','placeholder'=>'Employee Number'])
            !!}
            @error('user_number')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="phone">Phone Number</label>
            {!! Form::text('phone',null,['class'=>'form-control','id'=>'phone','placeholder'=>'phone number']) !!}
            @error('phone')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="password">Password</label>
            {!! Form::password('password',['class'=>'form-control','id'=>'password','placeholder'=>'password']) !!}
            @error('password')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="password_confirmation">Password Confirmation</label>
            {!!
            Form::password('password_confirmation',['class'=>'form-control','id'=>'password_confirmation','placeholder'=>'password
            confirmation']) !!}
            @error('password_confirmation')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label>Roles :</label>
            {!! Form::select('roles', array_combine($roles, $roles), $userRole ?? null, ['class' => 'form-control select2', 'id' => 'role', 'placeholder' => 'Choose Role...']) !!}
            @error('roles')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6"  @if(@$userRole != 'Operations/Operator' && @$userRole != 'Breaker') hidden="" @endif id="code">
        <div class="form-group">
            <label for="phone">Code</label>
            {!! Form::text('code',null,['class'=>'form-control','id'=>'code','placeholder'=>'enter code']) !!}
            @error('code')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    {{-- <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label>Department :</label>
            {!! Form::select('department_id',$departments,null,array('class' => 'form-control select2')) !!}
            @error('department_id')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div> --}}

    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label>Time Zone :</label>
            <select name="time_zone" class="select2">
                @foreach (DateTimeZone::listIdentifiers() as $timezone)
                    <option value="{{ $timezone }}"
                            @if(old('time_zone', $user->time_zone ?? '') === $timezone) selected @endif>
                        {{ $timezone }}
                    </option>
                @endforeach
            </select>
            @error('time_zone')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label>Status :</label>
            <select name="status" class="select2">

                <option value="1" @if(isset($user) && $user?->status ==1 )selected @endif>Active</option>
                <option value="0" @if(isset($user) && $user?->status ==0 )selected @endif>Not active</option>

            </select>
            @error('status')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>

    <div class="col-xs-12">
        <div class="form-group">
            <button type="submit" class="btn waves-effect waves-light btn-primary">Save</button>
        </div>
    </div>
</div>


@push('scripts')

    <script>

        $("#role").change(function (e) {
            var val = $(this).val();
            console.log(val)
            if (val == 'Operations/Operator' || val == 'Breaker') {
              $('#code').show();
            }else {
                $('#code').hide();
            }

        });
    </script>

@endpush
