@extends('admin.layout.app')

@section('title', 'Update User Roles')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Assign Branch & Park & Zone & Ride To User </h4>

                {!! Form::model($user, [
                    'route' => ['admin.rolesUpdate', $user->id],
                    'method' => 'POST',
                    'enctype' => 'multipart/form-data',
                    'files' => true,
                    'id' => 'form',
                ]) !!}
                <div class="row">
                    <h3> All Branches</h3>
                    <br>
                    <div class="col-xs-12 col-sm-12 col-md-12" id="branchs">
                        <div class="form-group">
                            <h3>Branch </h3>
                            <div class="row col-md-12" id="branch1">
                                @foreach ($branches as $branche)
                                    @if ($userRole == 'Operations/Zone supervisor' || $userRole == 'Operations/Operator')
                                        <div class="ml-1 col-md-2">
                                            <label
                                                class="ml-2">{{ Form::radio('branch_id[]', $branche->id, in_array($branche->id, $user->branchs?->pluck('id')->toArray()) ? true : false, ['class' => 'name ml-2 checkbox_roles branch']) }}
                                                {{ $branche->name }}</label>
                                        </div>
                                    @else
                                        <div class="ml-1 col-md-2">
                                            <label
                                                class="ml-2">{{ Form::checkbox('branch_id[]', $branche->id, in_array($branche->id, $user->branchs?->pluck('id')->toArray()) ? true : false, ['class' => 'name ml-2 checkbox_roles branch']) }}
                                                {{ $branche->name }}</label>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="ml-3">
                                    @error('branch_id')
                                        <div class="invalid-feedback" style="color: #ef1010">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">

                                <div class="form-group">

                                    <h3> All Parks</h3>
                                    <br>
                                    <div class="row" id="parks1">
                                        @if ($userRole == 'Operations/Zone supervisor' || $userRole == 'Operations/Operator')
                                             
                                            @foreach ($parks as $value)
                                                <div class="col-md-4">
                                                    @isset($list)
                                                        <label>{{ Form::radio('park_id[]', $value->id, in_array($value->id, $list) ? true : false, ['class' => 'name checkbox_roles parks']) }}
                                                            {{ $value->name }}</label>
                                                    @else
                                                        <label>{{ Form::radio('park_id[]', $value->id, false, ['class' => 'name checkbox_roles parks']) }}
                                                            {{ $value->name }}</label>
                                                    @endisset
                                                </div>
                                            @endforeach
                                        @else
                                            @foreach ($parks as $value)
                                                <div class="col-md-4">
                                                    @isset($list)
                                                        <label>{{ Form::checkbox('park_id[]', $value->id, in_array($value->id, $list) ? true : false, ['class' => 'name checkbox_roles parks']) }}
                                                            {{ $value->name }}</label>
                                                    @else
                                                        <label>{{ Form::checkbox('park_id[]', $value->id, false, ['class' => 'name checkbox_roles parks']) }}
                                                            {{ $value->name }}</label>
                                                    @endisset
                                                </div>
                                            @endforeach
                                        @endif


                                        @error('park_id')
                                            <div class="invalid-feedback" style="color: #ef1010">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>

                                </div>
                                <input type="hidden" name="user_id" value="{{ $user_id }}" id="userid">
                                <input type="hidden" name="roleUser" value="{{ $userRole }}" id="roleUser">
                                <div class="col-xs-12 col-sm-12 col-md-12" id="zones">

                                    <div class="form-group">

                                        <h3> All Zones</h3>
                                        <br>
                                        <div class="row">

                                            @foreach ($zones as $value)
                                                <div class="col-md-4">
                                                    @if ($userRole == 'Operations/Zone supervisor' || $userRole == 'Operations/Operator')
                                                        @isset($list)
                                                            <label>{{ Form::radio('zone_id[]', $value->id, in_array($value->id, $listZone) ? true : false, ['class' => 'name checkbox_roles zone1']) }}
                                                                {{ $value->name }}</label>
                                                        @else
                                                            <label>{{ Form::radio('zone_id[]', $value->id, false, ['class' => 'name checkbox_roles zone1']) }}
                                                                {{ $value->name }}</label>
                                                        @endisset
                                                    @else
                                                        @isset($list)
                                                            <label>{{ Form::checkbox('zone_id[]', $value->id, in_array($value->id, $listZone) ? true : false, ['class' => 'name checkbox_roles zone1']) }}
                                                                {{ $value->name }}</label>
                                                        @else
                                                            <label>{{ Form::checkbox('zone_id[]', $value->id, false, ['class' => 'name checkbox_roles zone1']) }}
                                                                {{ $value->name }}</label>
                                                        @endisset
                                                    @endif

                                                </div>
                                            @endforeach

                                            @error('zone_id')
                                                <div class="invalid-feedback" style="color: #ef1010">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                    </div>
                                </div>


                                <div class="col-xs-12 col-sm-12 col-md-12" id="riders">
                                    @if ($userRole == 'Operations/Operator')
                                        <div class="form-group">

                                            <h3> All Rides</h3>
                                            <br>
                                            <div class="row">

                                                @foreach ($rides as $rideCollection)
                                                    @foreach ($rideCollection as $ride)
                                                        <div class="col-md-4">
                                                            @isset($listRide)
                                                                <label>{{ Form::radio('ride_id[]', $ride->id, in_array($ride->id, $listRide) ? true : false, ['class' => 'name checkbox_roles']) }}
                                                                    {{ $ride->name }}</label>
                                                            @else
                                                                <label>{{ Form::radio('ride_id[]', $ride->id, false, ['class' => 'name checkbox_roles']) }}
                                                                    {{ $ride->name }}</label>
                                                            @endisset

                                                        </div>
                                                    @endforeach
                                                @endforeach
                                                @error('ride_id')
                                                    <div class="invalid-feedback" style="color: #ef1010">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    @elseif ($userRole == 'Operations/Zone supervisor')
                                    @else
                                        <div class="form-group">

                                            <h3> All Rides</h3>
                                            <br>
                                            <div class="row">

                                                @foreach ($rides as $rideCollection)
                                                    @foreach ($rideCollection as $ride)
                                                        <div class="col-md-4">
                                                            @isset($listRide)
                                                                <label>{{ Form::checkbox('ride_id[]', $ride->id, in_array($ride->id, $listRide) ? true : false, ['class' => 'name checkbox_roles']) }}
                                                                    {{ $ride->name }}</label>
                                                            @else
                                                                <label>{{ Form::checkbox('ride_id[]', $ride->id, false, ['class' => 'name checkbox_roles']) }}
                                                                    {{ $ride->name }}</label>
                                                            @endisset

                                                        </div>
                                                    @endforeach
                                                @endforeach
                                                @error('ride_id')
                                                    <div class="invalid-feedback" style="color: #ef1010">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    @endif
                                </div>


                                <div class="col-xs-12 aligne-center contentbtn">
                                    <button class="btn btn-primary waves-effect" type="submit">Save</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div><!-- end col -->
                </div>

            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        var roleUser = $('#roleUser').val();

        $(document).on("click", ".parks", function() {

            var selected = []; // initialize array
            $('div#parks1 input').each(function() {
                if ($(this).is(":checked")) {
                    selected.push($(this).attr('value'));
                }
            });
            var user_id = $('#userid').val();
            console.log(selected)

            $.ajax({
                url: "{{ route('admin.getZone') }}",
                method: 'GET',
                data: {
                    arr: selected,
                    user_id: user_id,
                    roleUser: roleUser

                },
                success: function(data) {
                    console.log(data)
                    $('#zones').html(' ');
                    $('#zones').html(data);
                    $('#riders').html(' ');
                }
            });
        });
        $(document).on("click", ".zone1", function() {

            var selected = []; // initialize array
            $('div#zones input').each(function() {
                if ($(this).is(":checked")) {
                    selected.push($(this).attr('value'));
                }
            });
            var user_id = $('#userid').val();
            console.log(selected)
            console.log(roleUser)
            $.ajax({
                url: "{{ route('admin.getRiders') }}",
                method: 'GET',
                data: {
                    arr: selected,
                    user_id: user_id,
                    roleUser: roleUser

                },
                success: function(data) {
                    console.log(data)
                    $('#riders').html(' ');
                    $('#riders').html(data);
                }
            });
        });
        $(document).on("click", ".branch", function() {

            var user_id = $('#userid').val();
            var selected = [];
            var selected = []; // initialize array
            $('div#branch1 input').each(function() {
                if ($(this).is(":checked")) {
                    selected.push($(this).attr('value'));
                }
            });
            console.log(selected)
            $.ajax({
                url: "{{ route('admin.getParkBranch') }}",
                method: 'GET',
                data: {
                    arr: selected,
                    user_id: user_id,
                    roleUser: roleUser

                },
                success: function(data) {

                    $('#parks1').html(' ');
                    $('#parks1').html(data);
                    $('#zones').html(' ');

                    $('#riders').html(' ');
                }
            });
        });
    </script>
@endpush
