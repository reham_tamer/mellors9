@foreach ($parks as $value)
    <div class="col-md-4">
        @if ($roleUser == 'Operations/Zone supervisor' || $roleUser == 'Operations/Operator')
            @isset($list)
                <label>{{ Form::radio('park_id[]', $value->id, in_array($value->id, $list) ? true : false, ['class' => 'name checkbox_roles parks']) }}
                    {{ $value->name }}</label>
            @else
                <label>{{ Form::radio('park_id[]', $value->id, false, ['class' => 'name checkbox_roles parks']) }}
                    {{ $value->name }}</label>
            @endisset
        @else
            @isset($list)
                <label>{{ Form::checkbox('park_id[]', $value->id, in_array($value->id, $list) ? true : false, ['class' => 'name checkbox_roles parks']) }}
                    {{ $value->name }}</label>
            @else
                <label>{{ Form::checkbox('park_id[]', $value->id, false, ['class' => 'name checkbox_roles parks']) }}
                    {{ $value->name }}</label>
            @endisset
        @endif


    </div>
@endforeach

@error('park_id')
    <div class="invalid-feedback" style="color: #ef1010">
        {{ $message }}
    </div>
@enderror
