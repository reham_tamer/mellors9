@foreach ($branches as $branche)
    <div class="col col-xs-6 col-md-4 col-lg-6">
        <div class="card-box">
            <h4 class="header-title m-t-0 m-b-0">{{ $branche->name }}</h4>

            <div class="row" id="parks{{ $branche->id }}"
                 data-branch="{{ $branche->id }}">
                @if ($userRole == 'Operations/Zone supervisor' || $userRole == 'Operations/Operator')
                    @foreach ($parks->where('branch_id', $branche->id) as $value)
                        <div class="col-md-12 card-box">

                            <div class="col-md-4">
                                @isset($list)
                                    <label class="text-danger">{{ Form::radio('park_id[]', $value->id, in_array($value->id, $list) ? true : false, ['class' => 'name checkbox_roles parks', 'data-branch' => $branche->id]) }}
                                        {{ $value->name }}</label>
                                @else
                                    <label class="text-danger">{{ Form::radio('park_id[]', $value->id, false, ['class' => 'name checkbox_roles parks', 'data-branch' => $branche->id]) }}
                                        {{ $value->name }}</label>
                                @endisset
                            </div>
                            @if ($userRole != 'Park Admin')

                            <div class="row col-md-12" id="zones{{ $value->id }}"
                            >

                                @foreach ($zones->where(
                                            'park_id',
                                            $value->id,
                                        ) as $zone)
                                    <div class="col-md-4">
                                        @if ($userRole == 'Operations/Zone supervisor' || $userRole == 'Operations/Operator')
                                            @isset($list)
                                                <label class="text-primary">{{ Form::radio('zone_id[]', $zone->id, in_array($zone->id, $listZone) ? true : false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id ,'data-zone' =>$zone->id]) }}
                                                    {{ $zone->name }}</label>
                                            @else
                                                <label class="text-primary">{{ Form::radio('zone_id[]', $zone->id, false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id, 'data-zone' =>$zone->id]) }}
                                                    {{ $zone->name }}</label>
                                            @endisset
                                        @else
                                            @isset($list)
                                                <label class="text-primary">{{ Form::checkbox('zone_id[]', $zone->id, in_array($zone->id, $listZone) ? true : false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id,'data-zone' =>$zone->id]) }}
                                                    {{ $zone->name }}</label>
                                            @else
                                                <label class="text-primary">{{ Form::checkbox('zone_id[]', $zone->id, false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id,'data-zone' =>$zone->id]) }}
                                                    {{ $zone->name }}</label>
                                            @endisset
                                        @endif

                                    </div>

                                    <div class="row col-md-12" id="rides{{$zone->id}}"
                                         data-branch="{{ $branche->id }}">


                                        @foreach ($rides->where('zone_id',
                                                $zone->id,
                                            ) as $ride)
                                            <div class="col-md-2">
                                                @if ($userRole == 'Operations/Operator')
                                                    @isset($listRide)
                                                        <label>{{ Form::radio('ride_id[]', $ride->id, in_array($ride->id, $listRide) ? true : false, ['class' => 'name checkbox_roles','data-zone' =>$zone->id]) }}
                                                            {{ $ride->name }}</label>
                                                    @else
                                                        <label>{{ Form::radio('ride_id[]', $ride->id, false, ['class' => 'name checkbox_roles','data-zone' =>$zone->id]) }}
                                                            {{ $ride->name }}</label>
                                                    @endisset
                                                @elseif ($userRole == 'Operations/Zone supervisor')
                                                @else
                                                    @isset($listRide)
                                                        <label>{{ Form::checkbox('ride_id[]', $ride->id, in_array($ride->id, $listRide) ? true : false, ['class' => 'name checkbox_roles','data-zone' =>$zone->id]) }}
                                                            {{ $ride->name }}</label>
                                                    @else
                                                        <label>{{ Form::checkbox('ride_id[]', $ride->id, false, ['class' => 'name checkbox_roles','data-zone' =>$zone->id]) }}
                                                            {{ $ride->name }}</label>
                                                    @endisset
                                                @endif
                                            </div>
                                        @endforeach

                                    </div>
                                @endforeach

                            </div>
                            @endif
                        </div>
                    @endforeach
                @else
                    @foreach ($parks->where('branch_id', $branche->id) as $value)
                        <div class="col-md-6">
                            @isset($list)
                                <label>{{ Form::checkbox('park_id[]', $value->id, in_array($value->id, $list) ? true : false, ['class' => 'name checkbox_roles parks', 'data-branch' => $branche->id]) }}
                                    {{ $value->name }}</label>
                            @else
                                <label>{{ Form::checkbox('park_id[]', $value->id, false, ['class' => 'name checkbox_roles parks', 'data-branch' => $branche->id]) }}
                                    {{ $value->name }}</label>
                            @endisset
                        </div>
                        @if ($userRole != 'Park Admin')

                        <div class="card-box col-md-12" id="park{{$value->id}}">
                            <div class="row" id="zones{{ $value->id }}"
                                 data-branch="{{ $branche->id }}">

                                @foreach ($zones->where(
                                            'park_id',
                                            $value->id,
                                        ) as $value)
                                    <div class="col-md-4">
                                        @if ($userRole == 'Operations/Zone supervisor' || $userRole == 'Operations/Operator')
                                            @isset($list)
                                                <label>{{ Form::radio('zone_id[]', $value->id, in_array($value->id, $listZone) ? true : false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id,'data-zone' =>$value->id]) }}
                                                    {{ $value->name }}</label>
                                            @else
                                                <label>{{ Form::radio('zone_id[]', $value->id, false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id,'data-zone' =>$value->id]) }}
                                                    {{ $value->name }}</label>
                                            @endisset
                                        @else
                                            @isset($list)
                                                <label>{{ Form::checkbox('zone_id[]', $value->id, in_array($value->id, $listZone) ? true : false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id,'data-zone' =>$value->id]) }}
                                                    {{ $value->name }}</label>
                                            @else
                                                <label>{{ Form::checkbox('zone_id[]', $value->id, false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id,'data-zone' =>$value->id]) }}
                                                    {{ $value->name }}</label>
                                            @endisset
                                        @endif

                                    </div>
                                @endforeach

                            </div>
                        </div>
                        @endif
                    @endforeach
                @endif
            </div>

        </div>
    </div>

@endforeach
