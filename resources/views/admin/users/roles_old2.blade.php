@extends('admin.layout.app')

@section('title', 'Update User Roles')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Assign Branch & Park & Zone & Ride To User </h4>

                {!! Form::model($user, [
                    'route' => ['admin.rolesUpdate', $user->id],
                    'method' => 'POST',
                    'enctype' => 'multipart/form-data',
                    'files' => true,
                    'id' => 'form',
                ]) !!}
                <div class="row">
                    <h3> All Branches</h3>
                    <br>
                    <div class="col-xs-12 col-sm-12 col-md-12" id="branchs">
                        <div class="form-group">
                            <h3>Branch </h3>
                            <div class="row col-md-12" id="branch1">
                                @foreach ($branches as $branche)
                                    @if ($userRole == 'Operations/Zone supervisor' || $userRole == 'Operations/Operator')
                                        <div class="ml-1 col-md-2">
                                            <label
                                                class="ml-2">{{ Form::radio('branch_id[]', $branche->id, in_array($branche->id, $user->branchs?->pluck('id')->toArray()) ? true : false, ['class' => 'name ml-2 checkbox_roles branch']) }}
                                                {{ $branche->name }}</label>
                                        </div>
                                    @else
                                        <div class="ml-1 col-md-2">
                                            <label
                                                class="ml-2">{{ Form::checkbox('branch_id[]', $branche->id, in_array($branche->id, $user->branchs?->pluck('id')->toArray()) ? true : false, ['class' => 'name ml-2 checkbox_roles branch']) }}
                                                {{ $branche->name }}</label>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="ml-3">
                                    @error('branch_id')
                                        <div class="invalid-feedback" style="color: #ef1010">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row" id="items">
                                @foreach ($branches as $branche)
                                    @if (in_array($branche->id, $user->branchs?->pluck('id')->toArray()))
                                        <div class="col col-xs-6 col-md-4 col-lg-6">
                                            <div class="card-box">
                                                <h4 class="header-title m-t-0 m-b-0">{{ $branche->name }}</h4>

                                                <h5> All Parks</h5>

                                                <div class="row" id="parks{{ $branche->id }}"
                                                    data-branch="{{ $branche->id }}">
                                                    @if ($userRole == 'Operations/Zone supervisor' || $userRole == 'Operations/Operator')
                                                        @foreach ($parks->where('branch_id', $branche->id) as $value)
                                                            <div class="col-md-4">
                                                                @isset($list)
                                                                    <label>{{ Form::radio('park_id[]', $value->id, in_array($value->id, $list) ? true : false, ['class' => 'name checkbox_roles parks', 'data-branch' => $branche->id]) }}
                                                                        {{ $value->name }}</label>
                                                                @else
                                                                    <label>{{ Form::radio('park_id[]', $value->id, false, ['class' => 'name checkbox_roles parks', 'data-branch' => $branche->id]) }}
                                                                        {{ $value->name }}</label>
                                                                @endisset
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        @foreach ($parks->where('branch_id', $branche->id) as $value)
                                                            <div class="col-md-6">
                                                                @isset($list)
                                                                    <label>{{ Form::checkbox('park_id[]', $value->id, in_array($value->id, $list) ? true : false, ['class' => 'name checkbox_roles parks', 'data-branch' => $branche->id]) }}
                                                                        {{ $value->name }}</label>
                                                                @else
                                                                    <label>{{ Form::checkbox('park_id[]', $value->id, false, ['class' => 'name checkbox_roles parks', 'data-branch' => $branche->id]) }}
                                                                        {{ $value->name }}</label>
                                                                @endisset
                                                            </div>
                                                        @endforeach
                                                    @endif


                                                    @error('park_id')
                                                        <div class="invalid-feedback" style="color: #ef1010">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>
                                                <h5> All Zones</h5>

                                                <div class="row" id="zones{{ $branche->id }}"
                                                    data-branch="{{ $branche->id }}">

                                                                                    @foreach ($zones->whereIn(
                                                            'park_id',
                                                            $parks->where('branch_id', $branche->id)
                                                                ?->pluck('id')
                                                                ?->toArray(),
                                                        ) as $value)
                                                        <div class="col-md-4">
                                                            @if ($userRole == 'Operations/Zone supervisor' || $userRole == 'Operations/Operator')
                                                                @isset($list)
                                                                    <label>{{ Form::radio('zone_id[]', $value->id, in_array($value->id, $listZone) ? true : false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id]) }}
                                                                        {{ $value->name }}</label>
                                                                @else
                                                                    <label>{{ Form::radio('zone_id[]', $value->id, false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id]) }}
                                                                        {{ $value->name }}</label>
                                                                @endisset
                                                            @else
                                                                @isset($list)
                                                                    <label>{{ Form::checkbox('zone_id[]', $value->id, in_array($value->id, $listZone) ? true : false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id]) }}
                                                                        {{ $value->name }}</label>
                                                                @else
                                                                    <label>{{ Form::checkbox('zone_id[]', $value->id, false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id]) }}
                                                                        {{ $value->name }}</label>
                                                                @endisset
                                                            @endif

                                                        </div>
                                                    @endforeach

                                                    @error('zone_id')
                                                        <div class="invalid-feedback" style="color: #ef1010">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>

                                                <h5> All Rides</h5>
                                                <div class="row" id="rides{{ $branche->id }}"
                                                    data-branch="{{ $branche->id }}">

                                                    {{-- @foreach ($rides as $rideCollection) --}}
                                                        @foreach ($rides->whereIn('park_id',
                                                    $parks->where('branch_id', $branche->id)
                                                        ?->pluck('id')
                                                        ?->toArray(),
                                                ) as $ride)
                                                            <div class="col-md-4">
                                                                @if ($userRole == 'Operations/Operator')
                                                                    @isset($listRide)
                                                                        <label>{{ Form::radio('ride_id[]', $ride->id, in_array($ride->id, $listRide) ? true : false, ['class' => 'name checkbox_roles']) }}
                                                                            {{ $ride->name }}</label>
                                                                    @else
                                                                        <label>{{ Form::radio('ride_id[]', $ride->id, false, ['class' => 'name checkbox_roles']) }}
                                                                            {{ $ride->name }}</label>
                                                                    @endisset
                                                                @elseif ($userRole == 'Operations/Zone supervisor')
                                                                @else
                                                                    @isset($listRide)
                                                                        <label>{{ Form::checkbox('ride_id[]', $ride->id, in_array($ride->id, $listRide) ? true : false, ['class' => 'name checkbox_roles']) }}
                                                                            {{ $ride->name }}</label>
                                                                    @else
                                                                        <label>{{ Form::checkbox('ride_id[]', $ride->id, false, ['class' => 'name checkbox_roles']) }}
                                                                            {{ $ride->name }}</label>
                                                                    @endisset
                                                                @endif
                                                            </div>
                                                        @endforeach
                                                    {{-- @endforeach --}}
                                                    @error('ride_id')
                                                        <div class="invalid-feedback" style="color: #ef1010">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>

                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <input type="hidden" name="user_id" value="{{ $user_id }}" id="userid">
                            <input type="hidden" name="roleUser" value="{{ $userRole }}" id="roleUser">
                            <div class="col-xs-12 aligne-center contentbtn">
                                <button class="btn btn-primary waves-effect" type="submit">Save</button>
                            </div>
                            {!! Form::close() !!}

                        </div>

                    </div><!-- end col -->
                </div>

            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        var roleUser = $('#roleUser').val();

        $(document).on("click", ".parks", function() {

            var selected = []; // initialize array
            var branch = $(this).data('branch');
            $('div#parks' + branch + ' input').each(function() {
                if ($(this).is(":checked")) {

                    selected.push($(this).attr('value'));

                }
            });


            var user_id = $('#userid').val();

            $.ajax({
                url: "{{ route('admin.getZone') }}",
                method: 'GET',
                data: {
                    arr: selected,
                    user_id: user_id,
                    roleUser: roleUser

                },
                success: function(data) {

                    $('#zones' + branch).html(' ');
                    $('#zones' + branch).html(data);
                    $('#rides' + branch).html(' ');

                }
            });
        });
        $(document).on("click", ".zone1", function(event) {

            var selected = []; // initialize array

            var branch = $(this).data('branch');
            $('div#zones' + branch + ' input').each(function() {
                if ($(this).is(":checked")) {

                    selected.push($(this).attr('value'));

                }
            });
            console.log(selected)
            console.log(branch)
             
            //   console.log($('.zone1').parent().parent())

          

            var user_id = $('#userid').val();

            $.ajax({
                url: "{{ route('admin.getRiders') }}",
                method: 'GET',
                data: {
                    arr: selected,
                    user_id: user_id,
                    roleUser: roleUser

                },
                success: function(data) {


                    $('#rides' + branch).html(' ');
                    $('#rides' + branch).html(data);
                }
            });
        });
        $(document).on("click", ".branch", function() {

            var user_id = $('#userid').val();
            var selected = [];
            var selected = []; // initialize array
            $('div#branch1 input').each(function() {
                if ($(this).is(":checked")) {
                    selected.push($(this).attr('value'));
                }
            });
            console.log(selected)
            $.ajax({
                url: "{{ route('admin.getParkBranch') }}",
                method: 'GET',
                data: {
                    arr: selected,
                    user_id: user_id,
                    roleUser: roleUser

                },
                success: function(data) {
                    console.log(data);
                    $('#items').html(' ');
                    $('#items').html(data);

                }
            });
        });
    </script>
@endpush
