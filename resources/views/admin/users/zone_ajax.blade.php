@foreach ($zones as $value)
    <div class="col-md-4">
        @if ($roleUser == 'Operations/Zone supervisor' || $roleUser == 'Operations/Operator')
            @isset($list)
                <label>{{ Form::radio('zone_id[]', $value->id, in_array($value->id, $listZone) ? true : false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id,'data-zone' =>$value->id]) }}
                    {{ $value->name }}</label>
            @else
                <label>{{ Form::radio('zone_id[]', $value->id, false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id,'data-zone' =>$value->id]) }}
                    {{ $value->name }}</label>
            @endisset
        @else
            @isset($list)
                <label>{{ Form::checkbox('zone_id[]', $value->id, in_array($value->id, $listZone) ? true : false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id,'data-zone' =>$value->id]) }}
                    {{ $value->name }}</label>
            @else
                <label>{{ Form::checkbox('zone_id[]', $value->id, false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id,'data-zone' =>$value->id]) }}
                    {{ $value->name }}</label>
            @endisset
        @endif


    </div>
    <div class="row col-md-12" id="rides{{$value->id}}"
          >
    </div>
@endforeach




