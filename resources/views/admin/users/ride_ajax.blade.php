@if ($roleUser == 'Operations/Operator')

    @foreach ($rides as $ride)
        <div class="col-md-4">
            @isset($listRide)
                <label>{{ Form::radio('ride_id[]', $ride->id, in_array($ride->id, $listRide) ? true : false, ['class' => 'name checkbox_roles']) }}
                    {{ $ride->name }}</label>
            @else
                <label>{{ Form::radio('ride_id[]', $ride->id, false, ['class' => 'name checkbox_roles']) }}
                    {{ $ride->name }}</label>
            @endisset

        </div>
    @endforeach
@elseif ($roleUser == 'Operations/Zone supervisor' || $roleUser == 'Operations/Zones Manager' )
@else

    @foreach ($rides as $ride)
        <div class="col-md-4">
            @isset($listRide)
                <label>{{ Form::checkbox('ride_id[]', $ride->id, in_array($ride->id, $listRide) ? true : false, ['class' => 'name checkbox_roles']) }}
                    {{ $ride->name }}</label>
            @else
                <label>{{ Form::checkbox('ride_id[]', $ride->id, false, ['class' => 'name checkbox_roles']) }}
                    {{ $ride->name }}</label>
            @endisset

        </div>
    @endforeach
@endif

