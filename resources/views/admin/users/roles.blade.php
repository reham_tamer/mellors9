@extends('admin.layout.app')

@section('title', 'Update User Roles')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Assign Branch & Park & Zone & Ride To User </h4>

                {!! Form::model($user, [
                    'route' => ['admin.rolesUpdate', $user->id],
                    'method' => 'POST',
                    'enctype' => 'multipart/form-data',
                    'files' => true,
                    'id' => 'form',
                ]) !!}
                <div class="row">
                    <h3> All Branches</h3>
                    <br>
                    <div class="col-xs-12 col-sm-12 col-md-12" id="branchs">
                        <div class="form-group">
                            <h3>Branch </h3>
                            <div class="row col-md-12" id="branch1">
                                @foreach ($branches as $branche)
                                    @if ($userRole == 'Operations/Zone supervisor' || $userRole == 'Operations/Operator')
                                        <div class="ml-1 col-md-2">
                                            <label
                                                class="ml-2">{{ Form::radio('branch_id[]', $branche->id, in_array($branche->id, $user->branchs?->pluck('id')->toArray()) ? true : false, ['class' => 'name ml-2 checkbox_roles branch']) }}
                                                {{ $branche->name }}</label>
                                        </div>
                                    @else
                                        <div class="ml-1 col-md-2">
                                            <label
                                                class="ml-2">{{ Form::checkbox('branch_id[]', $branche->id, in_array($branche->id, $user->branchs?->pluck('id')->toArray()) ? true : false, ['class' => 'name ml-2 checkbox_roles branch']) }}
                                                {{ $branche->name }}</label>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="ml-3">
                                    @error('branch_id')
                                    <div class="invalid-feedback" style="color: #ef1010">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row" id="items">
                                @foreach ($branches as $branche)
                                    @if (in_array($branche->id, $user->branchs?->pluck('id')->toArray()))
                                        <div class="col col-xs-6 col-md-4 col-lg-6">
                                            <div class="card-box">
                                                <h4 class="header-title m-t-0 m-b-0">{{ $branche->name }}</h4>

                                                <div class="row" id="parks{{ $branche->id }}"
                                                     data-branch="{{ $branche->id }}">

                                                        @foreach ($parks->where('branch_id', $branche->id) as $value)
                                                            <div class="col-md-12 card-box">

                                                            <div class="col-md-4">
                                                                @if ($userRole == 'Operations/Zone supervisor' || $userRole == 'Operations/Operator')
                                                                @isset($list)
                                                                    <label class="text-danger">{{ Form::radio('park_id[]', $value->id, in_array($value->id, $list) ? true : false, ['class' => 'name checkbox_roles parks', 'data-branch' => $branche->id]) }}
                                                                        {{ $value->name }}</label>
                                                                @else
                                                                    <label class="text-danger">{{ Form::radio('park_id[]', $value->id, false, ['class' => 'name checkbox_roles parks', 'data-branch' => $branche->id]) }}
                                                                        {{ $value->name }}</label>
                                                                @endisset

                                                                @else
                                                                @isset($list)
                                                                    <label class="text-danger">{{ Form::checkbox('park_id[]', $value->id, in_array($value->id, $list) ? true : false, ['class' => 'name checkbox_roles parks', 'data-branch' => $branche->id]) }}
                                                                        {{ $value->name }}</label>
                                                                @else
                                                                    <label class="text-danger">{{ Form::checkbox('park_id[]', $value->id, false, ['class' => 'name checkbox_roles parks', 'data-branch' => $branche->id]) }}
                                                                        {{ $value->name }}</label>
                                                                @endisset
                                                                @endif
                                                            </div>
                                                                @if ($userRole != 'Park Admin')

                                                                <div class="row col-md-12" id="zones{{ $value->id }}"
                                                                     >

                                                                    @foreach ($zones->where(
                                                                                'park_id',
                                                                                $value->id,
                                                                            ) as $zone)
                                                                        <div class="col-md-4">
                                                                            @if ($userRole == 'Operations/Zone supervisor' || $userRole == 'Operations/Operator' )
                                                                                @isset($list)
                                                                                    <label class="text-primary">{{ Form::radio('zone_id[]', $zone->id, in_array($zone->id, $listZone) ? true : false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id ,'data-zone' =>$zone->id]) }}
                                                                                        {{ $zone->name }}</label>
                                                                                @else
                                                                                    <label class="text-primary">{{ Form::radio('zone_id[]', $zone->id, false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id, 'data-zone' =>$zone->id]) }}
                                                                                        {{ $zone->name }}</label>
                                                                                @endisset
                                                                            @else
{{--                                                                                @dd($listRide)--}}
                                                                                @isset($list)
                                                                                    <label class="text-primary">{{ Form::checkbox('zone_id[]', $zone->id, in_array($zone->id, $listZone)  || in_array($zone->id, $listRideZone)  ? true : false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id,'data-zone' =>$zone->id]) }}
                                                                                        {{ $zone->name }}</label>
                                                                                @else
                                                                                    <label class="text-primary">{{ Form::checkbox('zone_id[]', $zone->id, false, ['class' => 'name checkbox_roles zone1', 'data-branch' => $branche->id,'data-zone' =>$zone->id]) }}
                                                                                        {{ $zone->name }}</label>
                                                                                @endisset
                                                                            @endif

                                                                        </div>

                                                                            <div class="row col-md-12" id="rides{{$zone->id}}"
                                                                                 data-branch="{{ $branche->id }}">

                                                                                @foreach ($rides->where('zone_id',
                                                                                        $zone->id,
                                                                                    ) as $ride)
                                                                                    <div class="col-md-2">
                                                                                        @if ($userRole == 'Operations/Operator')
                                                                                            @isset($listRide)
                                                                                                <label>{{ Form::radio('ride_id[]', $ride->id, in_array($ride->id, $listRide) ? true : false, ['class' => 'name checkbox_roles','data-zone' =>$zone->id]) }}
                                                                                                    {{ $ride->name }}</label>
                                                                                            @else
                                                                                                <label>{{ Form::radio('ride_id[]', $ride->id, false, ['class' => 'name checkbox_roles','data-zone' =>$zone->id]) }}
                                                                                                    {{ $ride->name }}</label>
                                                                                            @endisset
                                                                                        @elseif ($userRole == 'Operations/Zone supervisor' || $userRole == 'Operations/Zones Manager'  || $userRole == 'Operations/Zones Manager Without Time')

                                                                                        @else
                                                                                            @isset($listRide)
                                                                                                <label>{{ Form::checkbox('ride_id[]', $ride->id, in_array($ride->id, $listRide) ? true : false, ['class' => 'name checkbox_roles','data-zone' =>$zone->id]) }}
                                                                                                    {{ $ride->name }}</label>
                                                                                            @else
                                                                                                <label>{{ Form::checkbox('ride_id[]', $ride->id, false, ['class' => 'name checkbox_roles','data-zone' =>$zone->id]) }}
                                                                                                    {{ $ride->name }}</label>
                                                                                            @endisset
                                                                                        @endif
                                                                                    </div>
                                                                                @endforeach

                                                                        </div>
                                                                    @endforeach

                                                                </div>
                                                                @endif
                                                            </div>
                                                        @endforeach

                                                </div>

                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <input type="hidden" name="user_id" value="{{ $user_id }}" id="userid">
                            <input type="hidden" name="roleUser" value="{{ $userRole }}" id="roleUser">
                            <div class="col-xs-12 aligne-center contentbtn">
                                <button class="btn btn-primary waves-effect" type="submit">Save</button>
                            </div>
                            {!! Form::close() !!}

                        </div>

                    </div><!-- end col -->
                </div>

            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        var roleUser = $('#roleUser').val();

        $(document).on("click", ".parks", function () {

            var park = $(this).attr('value');
            var user_id = $('#userid').val();

            $.ajax({
                url: "{{ route('admin.getZone') }}",
                method: 'GET',
                data: {
                    arr: $(this).attr('value'),
                    user_id: user_id,
                    roleUser: roleUser

                },
                success: function (data) {

                    $('#zones' + park).html(' ');
                    $('#zones' + park).html(data);

                }
            });
        });
        $(document).on("click", ".zone1", function (event) {

            var selected = []; // initialize array

            var branch = $(this).data('branch');
            var zone = $(this).data('zone');
            console.log(branch)


            var user_id = $('#userid').val();

            $.ajax({
                url: "{{ route('admin.getRiders') }}",
                method: 'GET',
                data: {
                    arr: $(this).attr('value'),
                    user_id: user_id,
                    zone_id :zone,
                    roleUser: roleUser

                },
                success: function (data) {


                    $('#rides' + zone).html(' ');
                    $('#rides' + zone).html(data);
                }
            });
        });
        $(document).on("click", ".branch", function () {

            var user_id = $('#userid').val();
            var selected = [];
            var selected = []; // initialize array
            $('div#branch1 input').each(function () {
                if ($(this).is(":checked")) {
                    selected.push($(this).attr('value'));
                }
            });
            console.log(selected)
            $.ajax({
                url: "{{ route('admin.getParkBranch') }}",
                method: 'GET',
                data: {
                    arr: selected,
                    user_id: user_id,
                    roleUser: roleUser

                },
                success: function (data) {
                    console.log(data);
                    $('#items').html(' ');
                    $('#items').html(data);

                }
            });
        });
    </script>
@endpush
