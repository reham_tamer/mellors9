@extends('admin.layout.app')
@section('Update','update Zone PreOpening Elements'.$zone->name)

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Update Zone PreOpening Elements </h4>
                <a class="input-group-btn" href="{{route('admin.zone_inspection_lists.index')}}">
                    <button type="button" class="btn waves-effect waves-light btn-primary">back</button>
                </a>
                            {!!Form::model($zone, ['route' => ['admin.updatZonePreopenElements' , $zone->id] , 'method' => 'post','enctype'=>"multipart/form-data",'files' => true,'id'=>'form']) !!}
                            @include('admin.zone_preopening.form')
                            {!!Form::close() !!}
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->
@endsection
@push('scripts')
    {!! JsValidator::formRequest(\App\Http\Requests\Dashboard\InspectionList\ZoneInspectionListRequest::class, '#form'); !!}
@endpush
