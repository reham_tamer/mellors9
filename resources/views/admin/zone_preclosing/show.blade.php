@extends('admin.layout.app')

@section('title')
   Zone PreClosing List 
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <input type="button" value="Print Report" id="printDiv" class="btn btn-primary printBtn"></input>
            
        </div>


        <div class="col-xs-12 printable_div " id="myDivToPrint">
            <div class="report-head">
                <div class="report-body">
                    <h3 class="report-title">Zone PreClosing List </h3>
                </div>
                <div class="report-logo">
                    <img src="{{ asset('/_admin/assets/images/logo1.png') }}" alt="Mellors-img" title="Mellors"
                        class="image">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 ">
                    <div class="row">
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                                <tr role="row">
                                    <th class="type-header">Zone: </th>
                                    <th>{{ $list->zone->name }}</th>
                                    <th class="type-header">Park:</th>
                                    <th>{{ $list->park->name }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 ">
                    <div class="row">
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                                @php
                                  /*   use Carbon\Carbon;
                                    $approved = '';
                                    if ($list->approve == 1) {
                                        $approved = Carbon::parse($list->approved_at);
                                    } */

                                    $created = Carbon::parse($list->created_at);
                                @endphp
                                <tr role="row">
                                    <th class="type-header">Created Date:</th>
                                    <th>{{ $list->opened_date }}</th>
                                    <th class="type-header">Created Time:</th>
                                    <th>{{ $created?->format('H:i') }}</th>
                                </tr>
                                 
                            </thead>
                          
                        </table>

                    </div>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 ">
                    @if (isset($items) && count($items) > 0)
                        <div class="row">
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                    <tr role="row" class="type-header">
                                        <th style="text-align: center;">ID</th>
                                        <th>PreClosing List Items </th>
                                        <th>Status</th>
                                        <th>Comment</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  
                             @foreach ($items as $item)
                                 <tr role="row" class="odd">
                                <td >{{ $item->id }}</td>
                                <td>{{ $item->inspection_list->name }}</td>
                               
                                <td>  
                                    {{ $item->status }}
                                </td>
                                
                                <td>  
                                    {{ $item->comment }}
                                </td>
                            </tr>
                           
                        @endforeach
                                </tbody>
                            </table>

                        </div>

                        <table style="border-color: #0b0b0b"
                            class="table table-striped table-bordered dt-responsive nowrap">
                            <tbody>
                                <tr>
                                    <td style="border-color: #0b0b0b">Created by:</td>
                                    <td style="border-color: #0b0b0b">{{ $list->created_by?->name }}</td>
                                    <td>
                                        <a>
                                        <img class="img-preview"
                                        src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($list->image),now()->addMinutes(30)) }}"
                                        style="height: 40px; width: 40px">
                                         </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-color: #0b0b0b">Reviewed by</td>
                                    @if ($list->status === 'approved')
                                        <td style="border-color: #0b0b0b">{{ $list->approve_by?->name }}</td>

                                        @else
                                        <td style="border-color: #0b0b0b"> Not Reviewed yet</td>

                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    @else
                        <label>No questions</label>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script language="javascript">
        $('#printDiv').click(function() {
            $('#myDivToPrint').show();
            window.print();
            return false;
        });
    </script>
@endpush
