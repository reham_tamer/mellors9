{{--@include('admin.common.errors')--}}
<div class="row">
    <div class="col-xs-12">
        <div class="form-group form-float">
            <label class="form-label">Branch Name</label>
            <div class="form-line">
                {!! Form::text("name",null,['class'=>'form-control','placeholder'=>' Branch name'])!!}
                @error('name')
                <div class="invalid-feedback" style="color: #ef1010">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
        <div class="form-group form-float">
            <div class="form-group">
                <label>Country :</label>
                {!! Form::select('country_id', $countries,@$branch?$branch->country_id:null, array('class' => 'form-control select2')) !!}
                @error('country_id')
                <div class="invalid-feedback" style="color: #ef1010">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
    </div>

    <div class="col-xs-12 aligne-center contentbtn">
        <button class="btn btn-primary waves-effect" type="submit">Save</button>
    </div>
</div>
