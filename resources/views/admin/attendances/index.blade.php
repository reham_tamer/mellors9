@extends('admin.layout.app')

@section('title')
    All Attendants
@endsection
@section('content')

    <div class="card-box">
        <p class="text-muted font-14 mb-3">
            <a href="{{ route('admin.attendances.create') }}" class="btn btn-info">Add New Attendant</a>
        </p>

        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <table id="users-datatable" class="table table-striped table-bordered dt-responsive nowrap users-datatable" >
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Name
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Code
                            </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Park
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Status
                            </th>

                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Add Time
                            </th>


                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Process
                            </th>
                        </tr>
                        </thead>


                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')

    @include('admin.datatable.scripts')

    <script type="text/javascript">
        $(function () {

            var table = $('#users-datatable').DataTable({
                processing: false,
                serverSide: false,
                ajax: "{{ route('admin.attendances.index') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'code', name: 'code'},
                    {data: 'park', name: 'park',  orderable: false, searchable: false},
                    {data: 'status', name: 'status'},
                    {data: 'add_time', name: 'add_time',  orderable: false, searchable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
@endsection

