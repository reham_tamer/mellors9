<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="name">Name</label>
            {!! Form::text('name',null,['class'=>'form-control','id'=>'name','placeholder'=>'name']) !!}
            @error('name')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="first_name">Code</label>
            {!! Form::text('code',null,['class'=>'form-control','id'=>'code','placeholder'=>'code']) !!}
            @error('code')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="name">Last Name</label>
            {!! Form::text('last_name',null,['class'=>'form-control','id'=>'last_name','placeholder'=>'last name']) !!}
            @error('name')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="name">National Id</label>
            {!! Form::number('national_id',null,['class'=>'form-control','id'=>'national_id','placeholder'=>'national id']) !!}
            @error('national_id')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label>Park :</label>
            {!! Form::select('park_id', $parks,@$userPark?$userPark:null, array('class' => 'form-control select2', 'id' => 'park',)) !!}
            @error('park_id')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
   {{--  <div class='col-md-6'>
        <div class="form-group">
            <label for="last_name">Select Ride</label>
            {!! Form::select('ride_id[]', $rides??[], $attendanceRide??[], [
                'class' => 'form-control ride form-select',
                'id' => 'ride',
                'placeholder' => 'Choose Ride',
                'multiple'
            ]) !!}

        </div>
    </div> --}}
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label>Status :</label>
            <select name="status" class="select2">

                <option value="1" @if(isset($user) && $user?->status ==1 )selected @endif>Active</option>
                <option value="0" @if(isset($user) && $user?->status ==0 )selected @endif>Not active</option>

            </select>
            @error('status')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <button type="submit" class="btn waves-effect waves-light btn-primary">Save</button>
        </div>
    </div>
</div>


@push('scripts')

    <script type="text/javascript">
        $("#park").change(function () {
            $.ajax({
                url: "{{ route('admin.getParkRides') }}?park_id=" + $(this).val(),
                method: 'GET',
                success: function (data) {
                    $('#ride').html(data.html);
                }
            });
        });
    </script>

@endpush
