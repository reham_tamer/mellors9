@extends('admin.layout.app')
@section('title')
    Add Attendants Time
@stop
@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Add Attendants Time </h4>

                        {!!Form::open( ['route' => 'admin.attendances.store_time' ,'class'=>'form phone_validate', 'method' => 'Post', 'enctype'=>"multipart/form-data",'class'=>'form-horizontal','files' => true,'id'=>'form']) !!}
                        <div class="row">
                            <input type="hidden" name="attendance_id" value="{{ $user_id }}" >
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label>Park :</label>
                                    {!! Form::select('park_id', $parks,null, array('class' => 'form-control select2','id'=>'park','placeholder' => 'Select Park...',)) !!}
                                    @error('park_id')
                                    <div class="invalid-feedback" style="color: #ef1010">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Select Ride :</label>
                                    {!! Form::select('ride_id', [], null, ['class' => 'form-control select2', 'placeholder' => 'Select park first...', 'id' => 'ride']) !!}
                                    @error('ride_id')
                                    <div class="invalid-feedback" style="color: #ef1010">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div> 
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="name">Time Slot Date</label>
                                    {!! Form::date('open_date',null,['class'=>'form-control']) !!}
                                    @error('open_date')
                                    <div class="invalid-feedback" style="color: #ef1010">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                        
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="name">Start Date & Time </label>
                                    {!! Form::datetimeLocal('date_time_start', null, ['class' => 'form-control', 'placeholder' => 'Start Date Time']) !!}
                                    @error('date_time_start')
                                    <div class="invalid-feedback" style="color: #ef1010">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="name">End Date & Time </label>
                                    {!! Form::datetimeLocal('date_time_end', null, ['class' => 'form-control', 'placeholder' => 'Start Date Time']) !!}
                                   @error('date_time_end')
                                    <div class="invalid-feedback" style="color: #ef1010">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                          
                        
                                              
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <button type="submit" class="btn waves-effect waves-light btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                        

                        {!!Form::close() !!}
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->

@endsection
@push('scripts')
    {!! JsValidator::formRequest(\App\Http\Requests\Dashboard\User\StoreRequest::class, '#form'); !!}


    <script type="text/javascript">
        $("#park").change(function () {
            $.ajax({
                url: "{{ route('admin.getParkRides') }}?park_id=" + $(this).val(),
                method: 'GET',
                success: function (data) {
                    $('#ride').html(data.html);
                }
            });
        });
        </script>
    @endpush
