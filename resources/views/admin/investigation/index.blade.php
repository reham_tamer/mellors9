@extends('admin.layout.app')

@section('title')
Incident Investigation QMS-F-14
@endsection

@section('content')
    <div class="card-box">
        <form class="formSection" action="{{url('/search_investigation/')}}" method="GET">

            @csrf
            <div class="row">
                <div class='col-md-4'>
                    <div class="form-group">
                        <label for="middle_name">Time Slot Date </label>
                        {!! Form::date('date',\Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date_to']) !!}
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <label for="middle_name">Park </label>
                        {!! Form::select('park_id', $userparks, null, [
                            'class' => 'form-control select2 Parks',
                            'placeholder' => 'Choose Park...',
                        ]) !!}              
                    </div>
                </div>
                <div class='col-md-2 mtButton'>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary save_btn waves-effect">Show</button>
                    </div>
                </div>
            </div>
            {!!Form::close() !!}
            <br>
        <a class="input-group-btn" href="{{ route('admin.investigation.create') }}">
            <button type="button" class="btn waves-effect waves-light btn-primary">Add Incident Investigation Form QMS-F-14</button>
        </a>
        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1" aria-sort="ascending">ID
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Date
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Name of Person Involved
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Location
                                </th>
                                @if(auth()->user()->can('addStatment'))

                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Witness Statement
                            </th>
                            @endcan
                            @if(auth()->user()->can('investigation-approve-manager'))

                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                            	Manager Verfication
                            </th>
                            @endcan
                            @if(auth()->user()->can('investigation-approve-director'))

                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1">
                                Director Verfication
                            </th>
                            @endcan
                                <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                    colspan="1">
                                    Process
                                </th>
                            </tr>
                        </thead>

                        <tbody>

                            @foreach ($items as $item)
                                <tr role="row" class="odd" id="row-{{ $item->id }}">
                                    <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                    <td>{{ $item->date}}</td>
                                    <td>{{ $item->value['person_name'] }}</td>
                                    <td>{{ $item->park->name }} / {{ $item->ride->name ?? $item->text }} </td>
                                    @if(auth()->user()->can('addStatment'))
                                    <td>
                                        <a href="{{ url('show_statment/' . $item->id) }}"
                                           class="btn btn-primary"><i class="fa fa-plus"></i>  Witness Statment </a>
                                      </td>
                                      @endcan
                                      @if(auth()->user()->can('investigation-approve-manager'))

                                    <td>
                                        @if($item->status=='pending')
                                        <a href="{{url('investigation/'.$item->id.'/approve')}}"
                                        class="btn btn-xs btn-danger"><i class="fa fa-check"></i> Approve</a>
                                        @endif
                                        @if($item->status=='approved')
                                       <span>Verified</span>
                                          @endif
                                    </td>
                                    @endcan
                                    @if(auth()->user()->can('investigation-approve-director'))
                                   <td>
                                       @if($item->director_approve=='pending')
                                       <a href="{{url('investigation/'.$item->id.'/approve_director')}}"
                                       class="btn btn-xs btn-danger"><i class="fa fa-check"></i> Approve</a>
                                       @endif
                                       @if($item->director_approve=='approved')
                                      <span>Verified</span>
                                         @endif
                                   </td>
                                   @endcan
                                        {!! Form::open([
                                        'route' => ['admin.investigation.destroy', $item->id],
                                        'id' => 'delete-form' . $item->id,
                                        'method' => 'Delete',
                                    ]) !!}
                                    {!! Form::close() !!}
                                    <td>
                                        @if(auth()->user()->can('investigation-edit'))

                                        <a href="{{ route('admin.investigation.edit', $item) }}" class="btn btn-info">Edit</a>
                                       @endcan
                                       @if(auth()->user()->can('investigation-list'))

                                        <a href="{{ route('admin.investigation.show', $item->id) }}" class="btn btn-info">show</a>
                                       @endcan
                                        @if(auth()->user()->can('investigation-delete'))
                                        <a class="btn btn-danger" data-name=""
                                            data-url="{{ route('admin.investigation.destroy', $item) }}"
                                            onclick="delete_form(this)">
                                            Delete
                                        </a>
                                        @endcan
                                       </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    @include('admin.datatable.scripts')
@endsection
