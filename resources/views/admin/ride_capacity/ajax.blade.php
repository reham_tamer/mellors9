<div class="font-bold m-3 text-center">
    @if($outRang)
        <span class="font-bold m-3 text-danger">Filter out of range</span>
    @else
        <span class="font-bold m-3 text-danger">Total Count</span> : @if (isset($items))
            {{$items->count()}}
        @else
            0
        @endif
    @endif
</div>
@if(isset($items) && count($items) >0)
    <div class="form-group left row col-md-12">
        <a class="btn btn-info" href="{{ route('admin.export.capacity',$items->pluck('id')) }}">Export File Csv</a>

    </div>
<div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
    <div class="row">

        <div class="col-xs-12 " >
            <div class="col-xs-12 ">

            </div>
            <div class="col-xs-12">
                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1" aria-sort="ascending">ID
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Ride
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Availabilty Capacity
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Preopening checkList Capacity
                        </th>

                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                            Final Capacity
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                           Time Slot Date
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                            colspan="1">
                           Updating Time
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    @if (isset($items))
                        @foreach ($items as $item)
                            <tr role="row" class="odd" id="row-{{ $item->id }}">
                                <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                <td>{{ $item->ride->name ?? '' }}</td>
                                <td>{{ $item->ride_availablity_capacity ?? 'Not yet' }}</td>

                                <td>
                                    {{ $item->preopening_capacity ?? 'Not yet' }}
                                </td>
                                <td>
                                    {{ $item->final_capacity ?? 'Not yet' }}
                                </td>
                                <td>{{ $item->date }}</td>
                                <td>{{ $item->time }}</td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>


            </div>
        </div>
    </div>

</div>
@endif
