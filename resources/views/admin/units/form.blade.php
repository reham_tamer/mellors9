{{--@include('admin.common.errors')--}}
<div class="row">
<div class="col-xs-12">
<div class="form-group form-float">
    <label class="form-label">Owner</label>
    <div class="form-line">
        {!! Form::text("owner",null,['class'=>'form-control','placeholder'=>'Owner'])!!}
        @error('owner')
        <div class="invalid-feedback" style="color: #ef1010">
            {{ $message }}
        </div>
        @enderror
    </div>
</div>
</div>
<div class="col-xs-12">
    <div class="form-group form-float">
        <label class="form-label">Unit NO.</label>
        <div class="form-line">
            {!! Form::text("unit_no",null,['class'=>'form-control','placeholder'=>' Unit NO'])!!}
            @error('unit_no')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    </div> 
    <div class="col-xs-12">
        <div class="form-group form-float">
            <label class="form-label">Address</label>
            <div class="form-line">
                {!! Form::text("address",null,['class'=>'form-control','placeholder'=>' Address'])!!}
                @error('adress')
                <div class="invalid-feedback" style="color: #ef1010">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
        </div>
<div class="col-xs-12">
    <div class="form-group form-float">
        <label class="form-label">City/Zip (Optional)</label>
        <div class="form-line">
            {!! Form::text("city",null,['class'=>'form-control','placeholder'=>' City/Zip'])!!}
            @error('city')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    </div>

<div class="col-xs-12 aligne-center contentbtn">
    <button class="btn btn-primary waves-effect" type="submit">Save</button>
</div>
</div>
