@extends('admin.layout.app')

@section('title')
Units
@endsection

@section('content')

    <div class="card-box">

        <p class="text-muted font-14 mb-3">
            <a href="{{ route('admin.units.create') }}" class="btn btn-info">Add New Unit</a>
        </p>

        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Unit NO.
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Owner
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Availability
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Move In
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Initial Inspection
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Move Out 
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Process
                            </th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach ($items as $item)
                            <tr role="row" class="odd" id="row-{{ $item->id }}">
                                <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                <td>{{ $item->unit_no }}</td>
                                <td>{{ $item->owner }}</td>
                                <td>{{ $item->available }}</td>
                                <td>
                                    <a href="{{ url('add_move_in/' . $item->id) }}"
                                    class="btn btn-success"> <i class="fa fa-plus"></i> Move In</a>
                                </td>
                                <td>
                                    <a href="{{ url('add_intial_check/' . $item->id) }}"
                                    class="btn btn-warning">  <i class="fa fa-plus"></i> Initial Inspection</a>
                                </td>
                                <td>
                                    <a href="{{ url('add_move_out/' . $item->id) }}"
                                    class="btn btn-danger"> <i class="fa fa-plus"></i> Move Out</a>
                                </td>
                                {!!Form::open( ['route' => ['admin.units.destroy',$item->id] ,'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                                {!!Form::close() !!}
                                <td>
                                    @if(auth()->user()->can('units-edit'))
                                        <a href="{{ route('admin.units.edit', $item) }}"
                                           class="btn btn-info">Edit</a>
                                    @endif
                                        @if(auth()->user()->can('units-delete'))

                                        <a class="btn btn-danger" data-name="{{ $item->name }}"
                                           data-url="{{ route('admin.units.destroy', $item) }}"
                                           onclick="delete_form(this)">
                                            Delete
                                        </a>
                                        @endif

                                </td>

                            </tr>

                        @endforeach

                        </tbody>
                    </table>


                </div>
            </div>

        </div>
    </div>


@endsection
@section('footer')
    @include('admin.datatable.scripts')
@endsection



