@extends('admin.layout.app')

@section('title','Update Ride Cycle ')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Update Cycle </h4>
                {!!Form::model($item , ['route' => ['admin.rides-cycles.update' , $item->id] , 'method' => 'PATCH','enctype'=>"multipart/form-data",'files' => true,'id'=>'form']) !!}
                <div class="form-group">
                    <input type="hidden" name="ride_id" value="{{$ride_id}}" >
                    <input type="hidden" name="cycle_id" value="{{$item->id}}" >
                    <input type="hidden" name="park_time_id" value="{{$park_time_id}}" >
                    </div>
                @include('admin.rides_cycles.form')
                {!!Form::close() !!}
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->
@endsection
@push('scripts')
{!! JsValidator::formRequest(\App\Http\Requests\Dashboard\Ride\RideCycleRequest::class, '#form'); !!}
@endpush
