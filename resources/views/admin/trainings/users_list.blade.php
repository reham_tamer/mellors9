@extends('admin.layout.app')

@section('title')
    Training Users Lists
@endsection

@section('content')

    <div class="card-box">
       

        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                User No.
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                User Name
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                User Type
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Is User Complete Training ?
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                               Is Reviewed By Supervisor ?
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                               Is Reviewed By Operation Manager ?
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Show / Print
                            </th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach ($users as $item)
                            <tr role="row" class="odd" id="row-{{ $item->id }}">
                                <td tabindex="0" class="sorting_1">{{ $loop->iteration }}</td>
                                <td>{{ $item->userable_id }}</td>
                                <td>{{ $item->userable->name }}</td>
                                <td>{{ $item->userable_type == 'App\Models\Attendance' ? 'Attendant' :'Operator'}}</td>
                                <td>
                                    <span style="color: {{ $item->user_signature ? 'green' : 'red' }}">
                                        {{ $item->user_signature ? 'Yes' : 'Not Completed Yet' }}
                                    </span>
                                </td>
                                <td>
                                    <span style="color: {{ $item->supervisor_signature ? 'green' : 'red' }}">
                                        {{ $item->supervisor_signature ? 'Yes' : 'Not Reviewed Yet' }}
                                    </span>
                                </td>
                                <td>
                                    <span style="color: {{ $item->operation_manager_signature ? 'green' : 'red' }}">
                                        {{ $item->operation_manager_signature ? 'Yes' : 'Not Reviewed Yet' }}
                                    </span>
                                </td>
                                
                                {!!Form::open( ['route' => ['admin.training.destroy',$item->id] ,'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                                {!!Form::close() !!}

                                <td>
                                    <a href="{{ route('admin.showUserChecklist', ['user_id' => $item->userable_id, 'training_id' => $item->training_id]) }}"
                                        class="btn btn-info">Show</a>
                                     
                                </td>

                            </tr>

                        @endforeach

                        </tbody>
                    </table>


                </div>
            </div>

        </div>
    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
    
    $("#user_type").change(function () {
            $.ajax({
                url: "{{ route('admin.getListType') }}?list_type=" + $(this).val(),
                method: 'GET',
                success: function (data) {
                    $('#list_type').html(data.html);
                }
            });
        });
        $("#park").change(function () {
            $.ajax({
                url: "{{ route('admin.getParkRides') }}?park_id=" + $(this).val(),
                method: 'GET',
                success: function (data) {
                    $('#ride').html(data.html);
                }
            });
        });
    </script>
@endpush
@section('footer')
    @include('admin.datatable.scripts')
@endsection

