@extends('admin.layout.app')

@section('title')
    Training Users 
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <input type="button" value="Print Report" id="printDiv" class="btn btn-primary printBtn"></input>
           
        </div>

        <div class="col-xs-12 printable_div " id="myDivToPrint">
            <div class="report-head">
                <div class="report-body">
                    <h3 class="report-title">{{ $training->checklist->name }} </h3>
                </div>
                <div class="report-logo">
                    <img src="{{ asset('/_admin/assets/images/logo1.png') }}" alt="Mellors-img" title="Mellors"
                        class="image">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 ">
                    <div class="row">
                         <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                                <tr role="row">
                                    <th class="type-header">Name of Employee: </th>
                                    <th>{{ $trainingUser->userable_type == 'App\Models\Attendance' ? $trainingUser->attendance->name : $trainingUser->operator?->name }}</th>
                                    <th class="type-header">Mellors I.D Number:</th>
                                    <th>{{ $trainingUser->userable_type == 'App\Models\Attendance' ? $trainingUser->attendance->code : $trainingUser->operator?->code }}</th>
                                </tr>
                                <tr role="row">
                                    <th class="type-header">Training Start Date: </th>
                                    <th>{{ $training->start_date }}</th>
                                    <th class="type-header">Training Completion Date: </th>
                                    <th>{{ $training->end_date }}</th>
                                </tr>
                                <tr role="row">
                                    <th class="type-header">Attraction Name: </th>
                                    <th>{{ $training->ride->name }}</th>
                                    <th class="type-header">Location of Event: </th>
                                    <th>{{ $training->park->name }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>

                </div>
              
                <div class="col-xs-12 col-sm-12 col-md-12 ">
                    @if (isset($items) && count($items) > 0)
                        <div class="row">
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                    @php
                                        $groupedItems = $items->groupBy('trainingItems.type');
                                       // dd( $groupedItems);
                                    @endphp

                                    @foreach ($groupedItems as $Type => $group)
                                       
                                    @if($Type )

                                    <thead>
                                        <tr role="row" class="type-header">
                                            <th style="text-align: center;">{{ $Type }}</th>
                                            <th>Attraction {{ $training->checklist->type == 'operator'? 'Operator': 'Attendant' }} Initials
                                            </th>
                                            <th>Date Trained
                                            </th>
                                            <th>Trainee Initials 
                                            </th>
                                        </tr>
                                    </thead>
                                    @endif
                                    <tbody>
                                        @foreach ($group as $value)
                                            <tr>
                                                <td>{{ $value->trainingItems->name_en ?? '' }} 
                                                    <br>
                                                    {{ $value->trainingItems->name_ar ?? '' }}
                                                </td>
                                                <td>  
                                                     @if ($value->date == null)
                                                    Not Checked Yet
                                                @else
                                                    &#x2705; 
                                                @endif
                                             </td>
                                                <td>{{ $value->date?? 'Not Checked Yet' }} </td>
                                                <td>  
                                                    @if ($value->trainee_signature == null)
                                                   Not Checked Yet
                                               @else
                                               <a>
                                                   <img class="img-preview"
                                                   src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($value->trainee_signature),now()->addMinutes(30)) }}"
                                                   style="height: 40px; width: 40px"></a>
                                               @endif
                                            </td>
                                            </tr>
                                          
                                        @endforeach
                                    </tbody>
                                    @endforeach
                            </table>

                        </div>
                        @else
                        <label>No questions</label>
                    @endif
                       
                        <table style="border-color: #0b0b0b"
                        class="table table-striped table-bordered dt-responsive nowrap">
                        <tbody>
                            <tr> <td colspan="3" style="border-color: #0b0b0b">
                                <h3>Mellors Entertainment Trainee </h3>
                                <p>

                                     I, the undersigned, hereby confirm that I have completed all training related to the Site Induction Package, including the 
                                    additional training package required for the role of Attraction Attendant/Queue Assistant/Usher.        
                                
                                </p>
                                <p>
                                    I also confirm that I have received comprehensive training on and fully understand my responsibilities regarding the 
                                    following documentation: 
                                </p>
                                <ul>
                                    <li>
                                        Attraction Attendant Roles and Responsibilities 
                                    </li>
                                    <li>
                                        Pre-Operating Checks  

                                    </li>
                                     <li>
                                        Accident/Incident and Near Miss Reporting Procedures                                      
                                    </li>
                                    <li>
                                         Attraction Specific Emergency Evacuation Procedure  
                                    </li>
                                    <li>
                                        Attraction Attendant Manual  
                                    </li>
                                    <li>
                                        Safety Restraint Procedure 
                                    </li>
                                    <li>
                                        Hand Signals 
                                    </li>
                                    <li>
                                        Lost Property/Children Procedure 
                                   </li>
                                    <li>
                                        Disabled Passenger Procedure 
                                    </li>
                                    <li>
                                        HSG 175 
                                    </li>
                                    <li>
                                            ISO 17842
                                    </li>
                                    <li>
                                           BS EN 13814
                                    </li>
                                </ul>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-color: #0b0b0b">Trainee Name : {{ $trainingUser->userable_type == 'App\Models\Attendance' ? $trainingUser->attendance->name : $trainingUser->operator?->name }}</td>
                                <td style="border-color: #0b0b0b"> Trainee  Signature : <a>
                                    <img class="img-preview"
                                    src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($trainingUser->user_signature),now()->addMinutes(30)) }}"
                                    style="height: 40px; width: 40px"></a></td>
                                <td style="border-color: #0b0b0b"> 
                                    Date :{{ $trainingUser->user_date }}
                                </td>
                            
                            </tr>
                            <tr> 
                                <td colspan="3" style="border-color: #0b0b0b">
                                  <h3>Mellors Entertainment Supervisor</h3>
                                     <p>
                                        I have trained out the full contents of these training packages mentioned above, and the trainee has confirmed their 
                                        understanding of their role regarding the documentation stated above. 
                                       </p>
                                     <p>

                                        I can confirm that the training was carried out using the hours in the competence management system QMS-Pr-35. The 
                                        trainee mentioned above is aware they must complete all tasks to all Health and Safety Regulations and Guidelines, and 
                                        to all Standards and Policies as laid out by Mellors Group. 
                                                                         </p>
                                     <h3 style="text-align: center">
                                        I am recommending that the trainee named above is ready to undertake a workplace competence assessment 
                                        (after their minimum training hours have been completed).                                      </h3>
                                </td>
                            </tr>
                           
                            <tr>
                                <td style="border-color: #0b0b0b">Trainer Name : {{ $training->supervisor?->name }}</td>
                                <td style="border-color: #0b0b0b">Signature : 
                                <a>
                                <img class="img-preview"
                                src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($trainingUser->supervisor_signature),now()->addMinutes(30)) }}"
                                style="height: 40px; width: 40px"></a>
                            </td>
                            <td style="border-color: #0b0b0b"> 
                                Date :{{ $trainingUser->supervisor_date }}
                            </td>
                            </tr>
                            <tr> 
                                <td colspan="3" style="border-color: #0b0b0b">
                                  <h3>Mellors Entertainment Appointed Person</h3>
                                     <p>

                                        I the undersigned, confirm that the person named as the trainer on this documentation has assured me that the employee 
                                        named as the trainee on this documentation, has received the correct training to enable them to perform their role related 
                                        tasks as mentioned in this document.                                  </td>
                            </tr>
                            <tr>
                                <td style="border-color: #0b0b0b">Name : {{ $training->manager?->name }}</td>
                                <td style="border-color: #0b0b0b">Signature : 
                                <a>
                                <img class="img-preview"
                                src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($trainingUser->operation_manager_signature),now()->addMinutes(30)) }}"
                                style="height: 40px; width: 40px"></a>
                            </td>
                            <td style="border-color: #0b0b0b"> 
                                Date :{{ $trainingUser->operation_manager_date }}
                            </td>
                            </tr>
                        </tbody>
                    </table> 
              
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script language="javascript">
        $('#printDiv').click(function() {
            $('#myDivToPrint').show();
            window.print();
            return false;
        });
    </script>
@endpush
