@extends('admin.layout.app')
@section('title','Update Training '.$training->name)
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">  {{ $training->name }} </h4>
                <a class="input-group-btn" href="{{route('admin.training.index')}}">
                    <button type="button" class="btn waves-effect waves-light btn-primary">back</button>
                </a>
                {!!Form::model($training , ['route' => ['admin.training.update' , $training->id] , 'method' => 'PATCH','enctype'=>"multipart/form-data",'files' => true,'id'=>'form']) !!}
                @include('admin.trainings.form')
                {!!Form::close() !!}
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->
@endsection

