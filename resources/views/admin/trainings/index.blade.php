@extends('admin.layout.app')

@section('title')
    Trainings
@endsection

@section('content')

    <div class="card-box">
        <form class="formSection" action="{{url('/training_search/')}}" method="GET">

            <div class="row">
                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="last_name">Select Park</label>
                        {!! Form::select('park_id', $user_parks, request('park_id') ?? null, [
                            'class' => 'form-control park',
                            'id' => 'park',
                            'placeholder' => 'Choose Park',
                        ]) !!}
                    </div>
                </div>
                <div class='col-md-5'>
                    <div class="form-group">
                        <label for="last_name">Select Ride</label>
                        {!! Form::select('ride_id', [], request('ride_id') ?? null, [
                            'class' => 'form-control ride',
                            'id' => 'ride',
                            'placeholder' => 'Choose Ride',
                        ]) !!}
                    </div>
                </div>
                <div class='col-md-3'>
                    <div class="form-group">
                        <label for="last_name">Select User Type</label>
                        {!! Form::select('user_type',['attendance'=>'Attendants','operator'=>'operator'],request('user_type') ?? null, array('class' => 'form-control select2','id'=>'user_type','placeholder'=>'Choose User Type') ) !!}
                    </div>
                </div>
                <div class='col-md-3'>
                    <div class="form-group">
                        <label for="last_name">Select CheckList Type</label>
                        {!! Form::select('list_type',[],request('list_type') ?? null, array('class' => 'form-control select2','id'=>'list_type','placeholder'=>'Choose CheckList Type') ) !!}
                    </div>
                </div>
                <div class='col-md-3'>
                    <div class="form-group">
                        <label for="middle_name">Date  </label>
                        {!! Form::date('start_date',request('start_date') ?? \Carbon\Carbon::now()->toDate(),['class'=>'form-control ','id'=>'date']) !!}
                    </div>
                </div>
                

                <div class='col-md-2 mtButton'>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary save_btn waves-effect">Show</button>
                    </div>
                </div>
            </div>
        {!!Form::close() !!}

        <div id="datatable-buttons_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1"
                                colspan="1" aria-sort="ascending">ID
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Park
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Ride
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                User Type
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Training Type
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Start date
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                End Date
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Supervisor
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Operations Manager
                            </th>


                            <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1">
                                Process
                            </th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach ($items as $item)
                            <tr role="row" class="odd" id="row-{{ $item->id }}">
                                <td tabindex="0" class="sorting_1">{{ $item->id }}</td>
                                <td>{{ $item->park->name }}</td>
                                <td>{{ $item->ride?->name }}</td>
                                <td>{{ $item->user_type == 'attendance' ? 'Attendant' :'Operator'}}</td>
                                <td>{{ $item->type}}</td>
                                <td>{{ $item->start_date}}</td>
                                <td>{{ $item->end_date }}</td>
                                <td>{{ $item->supervisor?->name }}</td>
                                <td>{{ $item->manager?->name }}</td>

                                {!!Form::open( ['route' => ['admin.training.destroy',$item->id] ,'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                                {!!Form::close() !!}

                                <td>
                                    <a href="{{ route('admin.training.edit', $item) }}"
                                       class="btn btn-info">Edit</a>
                                    <a href="{{ route('admin.showUsersLists', $item) }}"
                                       class="btn btn-info">Show</a>
                                    <a class="btn btn-danger" data-name="{{ $item->park->name }}"
                                       data-url="{{ route('admin.training.destroy', $item) }}"
                                       onclick="delete_form(this)">
                                        Delete
                                    </a>

                                </td>

                            </tr>

                        @endforeach

                        </tbody>
                    </table>


                </div>
            </div>

        </div>
    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
    
    $("#user_type").change(function () {
            $.ajax({
                url: "{{ route('admin.getListType') }}?list_type=" + $(this).val(),
                method: 'GET',
                success: function (data) {
                    $('#list_type').html(data.html);
                }
            });
        });
        $("#park").change(function () {
            $.ajax({
                url: "{{ route('admin.getParkRides') }}?park_id=" + $(this).val(),
                method: 'GET',
                success: function (data) {
                    $('#ride').html(data.html);
                }
            });
        });
    </script>
@endpush
@section('footer')
    @include('admin.datatable.scripts')
@endsection

