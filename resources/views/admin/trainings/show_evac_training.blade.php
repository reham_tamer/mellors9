@extends('admin.layout.app')

@section('title')
    Training Users 
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <input type="button" value="Print Report" id="printDiv" class="btn btn-primary printBtn"></input>
           
        </div>

        <div class="col-xs-12 printable_div " id="myDivToPrint">
            <div class="report-head">
                <div class="report-body">
                    <h3 class="report-title">{{ $training->checklist->name }} </h3>
                </div>
                <div class="report-logo">
                    <img src="{{ asset('/_admin/assets/images/logo1.png') }}" alt="Mellors-img" title="Mellors"
                        class="image">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 ">
                    <div class="row">
                         <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                                <tr role="row">
                                    <th class="type-header">Name of Trainee: </th>
                                    <th colspan="3">{{ $trainingUser->userable_type == 'App\Models\Attendance' ? $trainingUser->attendance->name : $trainingUser->operator?->name }}</th>
                                     </tr>
                                <tr role="row">
                                    <th class="type-header">Training Start Date: </th>
                                    <th>{{ $training->start_date }}</th>
                                    <th class="type-header">Training Completion Date: </th>
                                    <th>{{ $training->end_date }}</th>
                                </tr>
                                <tr role="row">
                                    <th class="type-header">Name of Trainer: </th>
                                    <th>{{ $training->supervisor?->name }}</th>
                                    <th class="type-header">Location of Event: </th>
                                    <th>{{ $training->park->name }}</th>
                                </tr>
                                <tr role="row">
                                    <td colspan="4" class="type-header" style="text-align: center">
                                     <p>   NOTE:<span style="color: red"> It is required for the employee to complete all the training specified in this training programme before the individual is allowed
                                       to evacuate an attraction alongside other competent members of management staff.
                                    </span> </p>
                                    </td>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>

                </div>
              
                <div class="col-xs-12 col-sm-12 col-md-12 ">
                    @if (isset($items) && count($items) > 0)
                        <div class="row">
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                    @php
                                        $groupedItems = $items->groupBy('trainingItems.type');
                                       // dd( $groupedItems);
                                    @endphp

                                    @foreach ($groupedItems as $Type => $group)
                                       
                                    @if($Type )

                                    <thead>
                                        <tr role="row" class="type-header">
                                            <th style="text-align: center;">{{ $Type }}</th>
                                            <th>Trainee Initials 
                                            </th>
                                            <th>Trainers Initials
                                            </th>
                                        </tr>
                                    </thead>
                                    @endif
                                    <tbody>
                                        @foreach ($group as $value)
                                            <tr>
                                                <td>{{ $value->trainingItems->name_en ?? '' }} 

                                                </td>
                                                <td>  
                                                     @if ($value->trainee_signature == null)
                                                    Not Checked Yet
                                                @else
                                                <a>
                                                    <img class="img-preview"
                                                    src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($value->trainee_signature),now()->addMinutes(30)) }}"
                                                    style="height: 40px; width: 40px"></a>
                                                @endif
                                             </td>
                                                <td>
                                                    @if ($value->trainer_signature == null)
                                                    Not Checked Yet
                                                @else
                                                <a>
                                                    <img class="img-preview"
                                                    src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($value->trainer_signature),now()->addMinutes(30)) }}"
                                                    style="height: 10px; width: 10px">
                                                </a>
                                                @endif
                                                 </td>
                                            </tr>
                                          
                                        @endforeach
                                    </tbody>
                                    @endforeach
                            </table>

                        </div>
                        @else
                        <label>No questions</label>
                    @endif
                    <div class="col-xs-12 col-sm-12 col-md-12 ">
                        @if (isset($trainingUserRides) && count($trainingUserRides) > 0)
                            <div class="row">
                                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                        <thead>
                                            <tr role="row" class="type-header">
                                                <th style="text-align: center;">Ride Name</th>
                                                <th>Trainee Initials 
                                                </th>
                                                <th>Trainers Initials
                                                </th>
                                                <th>
                                                    Instructed
                                                </th>
                                                <th>
                                                    Unassisted
                                                </th>
                                                <th>
                                                    Date Training was Completed
                                                </th>
                                            </tr>
                                        </thead>
                                        @endif
                                        <tbody>
                                            @foreach ($trainingUserRides as $ride)
                                            <tr>
                                                <td>
                                                    {{ $ride->ride->name }}
                                                </td>
                                                <td>
                                                    <a>
                                                        <img class="img-preview"
                                                        src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($ride->trainee_signature),now()->addMinutes(30)) }}"
                                                        style="height: 40px; width: 40px">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a>
                                                        <img class="img-preview"
                                                        src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($ride->trainer_signature),now()->addMinutes(30)) }}"
                                                        style="height: 40px; width: 40px">
                                                    </a>
                                                </td>
                                                <td>
                                                @if ($ride->instructed == 1)
                                                    &#x2705; 
                                                @endif
                                                </td>
                                                <td>
                                                @if ($ride->instructed == 0)
                                                    &#x2705; 
                                                @endif
                                                </td>
                                                <td>{{ $ride->date }}</td>
                                            </tr>
                                                
                                            @endforeach

                                        </tbody>
                                </table>
                            </div>


                         <table style="border-color: #0b0b0b"
                            class="table table-striped table-bordered dt-responsive nowrap">
                            <tbody>
                                <tr> <td colspan="3" style="border-color: #0b0b0b">
                                    <h3>Mellors Entertainment Trainee </h3>
                                    <p>
                                        I the undersigned, confirm that I have received all the training regarding the INSTRUCTED and UNASSISTED PROCESS and the additional training for the emergency evacuations. I can also confirm that I have received training on, and I understand my role regarding the documentation stated below:
                                    </p>
                                    <ul>
                                        <li>
                                            	Ride Risk Assessment (For all attractions mentioned above)
                                        </li>
                                        <li>
                                            	Ride Stoppages 
                                        </li>
                                         <li>
                                            	Accident Reporting Procedures 
                                        </li>
                                        <li>
                                            	Ride Specific Emergency Evacuation Procedure for all attractions mentioned above (role specific information)
                                        </li>
                                        <li>
                                            	Ride Operations Manual (For all attractions mentioned above)
                                        </li>
                                        <li>
                                            	Ride Maintenance Manual (For all attractions mentioned above)
                                        </li>
                                        <li>
                                            	Safety Restraint Procedure 
                                        </li>
                                        <li>
                                            	HSG 175
                                        </li>
                                        <li>
                                            	ISO 17842
                                        </li>
                                        <li>
                                            	EN 13814
                                        </li>
                                    </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-color: #0b0b0b">Trainee Name : {{ $trainingUser->userable_type == 'App\Models\Attendance' ? $trainingUser->attendance->name : $trainingUser->operator?->name }}</td>
                                    <td style="border-color: #0b0b0b"> Trainee  Signature : <a>
                                        <img class="img-preview"
                                        src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($trainingUser->user_signature),now()->addMinutes(30)) }}"
                                        style="height: 40px; width: 40px"></a></td>
                                    <td style="border-color: #0b0b0b"> 
                                        Date :{{ $trainingUser->user_date }}
                                    </td>
                                
                                </tr>
                                <tr> 
                                    <td colspan="3" style="border-color: #0b0b0b">
                                      <h3>Mellors Entertainment Zone Supervisor/Manager</h3>
                                         <p>
                                            I have been informed by the trainers mentioned (signed and trainer initials on this document) that have initialed this document, that the full contents of these training packages mentioned above, have been trained out correctly (as per the relevant evacuation operation manuals and this document), and the trainee has confirmed their understanding of their role regarding the documentation stated above.
                                         </p>
                                         <p>
                                            I can confirm that the training was carried out using the instructed and unassisted process. The trainee mentioned above is aware they must complete all tasks to all Health and Safety Regulations and Guidelines, and to all standards and policies as laid out by Mellors Entertainment (Kuwait). I can confirm that the trainee was correctly trained on the attractions that are mentioned below using the specific operations manual for that attraction.
                                         </p>
                                         <h3 style="text-align: center">
                                            I am recommending that the trainee named above is ready to undertake a workplace competence assessment.
                                         </h3>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td style="border-color: #0b0b0b">Trainer Name : {{ $training->supervisor?->name }}</td>
                                    <td style="border-color: #0b0b0b">Signature : 
                                    <a>
                                    <img class="img-preview"
                                    src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($trainingUser->supervisor_signature),now()->addMinutes(30)) }}"
                                    style="height: 40px; width: 40px"></a>
                                </td>
                                <td style="border-color: #0b0b0b"> 
                                    Date :{{ $trainingUser->supervisor_date }}
                                </td>
                                </tr>
                                <tr> 
                                    <td colspan="3" style="border-color: #0b0b0b">
                                      <h3>Mellors Entertainment Appointed Person</h3>
                                         <p>
                                            I the undersigned, confirm that the person named as the zone supervisor on this documentation has assured me that the employee named as the trainee on this documentation, has received the correct training to enable them to perform their role related tasks as mentioned in this document.                                          </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-color: #0b0b0b">Operation Manager : {{ $training->manager?->name }}</td>
                                    <td style="border-color: #0b0b0b">Signature : 
                                    <a>
                                    <img class="img-preview"
                                    src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($trainingUser->operation_manager_signature),now()->addMinutes(30)) }}"
                                    style="height: 40px; width: 40px"></a>
                                </td>
                                <td style="border-color: #0b0b0b"> 
                                    Date :{{ $trainingUser->operation_manager_date }}
                                </td>
                                </tr>
                            </tbody>
                        </table> 
                  
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script language="javascript">
        $('#printDiv').click(function() {
            $('#myDivToPrint').show();
            window.print();
            return false;
        });
    </script>
@endpush
