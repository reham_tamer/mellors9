{{--@include('admin.common.errors')--}}
<div class="row">
    <div class="col-lg-6">
            <div class="form-group">
        <label for="middle_name">Start Date  </label>
        {!! Form::date('start_date',isset($training ) ? $training?->start_date : \Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date']) !!}
    </div>
</div>
<div class="col-lg-6">
    <div class="form-group">
        <label for="middle_name">End Date  </label>
        {!! Form::date('end_date',isset($training ) ? $training?->end_date : \Carbon\Carbon::now()->toDate(),['class'=>'form-control','id'=>'date_to']) !!}
    </div>
</div>
</div>

<div class="row">
<div class="col-lg-6">
<div class="form-group form-float">
    <label class="form-label">Park :</label>
    <div class="form-line">
        {!! Form::select('park_id',$parks,isset($training ) ? $training?->park_id : null, array('class' => 'form-control park','id'=>'park','placeholder'=>'Choose Park', 'required' => 'required') ) !!}
        @error('park_id')
        <div class="invalid-feedback" style="color: #ef1010">
            {{ $message }}
        </div>
        @enderror
    </div>
</div>
</div>
<div class="col-lg-6">
    <div class="form-group form-float">
        <label class="form-label">User Type :</label>
        <div class="form-line">
            {!! Form::select("user_type", ['' => 'Choose...', 'operator' => 'Operator', 'attendance' => 'Attendant'], null, ['class' => 'form-control' ,'id'=>'user_type']) !!}
            @error('user_type')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
</div>
</div>
<div class="row">


    <div class="col-lg-6">
        <div class="form-group form-float">
            <label class="form-label">Training CheckList :</label>
            <div class="form-line">
                {!! Form::select("training_checklist_id", $checklists??[], null, ['class' => 'form-control', 'id'=>'checklist']) !!}
                @error('training_checklist_id')
                <div class="invalid-feedback" style="color: #ef1010">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
    </div>

    <div class="col-lg-6" id="ride-container">
        <div class="form-group form-float">
            <label class="form-label">Rides:</label>
            <div class="form-line">
                {!! Form::select('ride_id', $rides??[],isset($training ) ? $training?->ride_id : null, array('class' => 'form-control ride','id'=>'ride','placeholder'=>'Choose Ride')) !!}
                @error('ride_id')
                <div class="invalid-feedback" style="color: #ef1010">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
    </div>
</div>

 <div class="row">
    <div class="col-lg-6">
        <div class="form-group form-float">
            <label class="form-label">Supervisor :</label>
            <div class="form-line">
                {!! Form::select("supervisor_id", $supervisors??[] ,isset($training ) ? $training?->supervisor_id : null , ['class' => 'form-control ', 'id' => 'supervisor']) !!}
                @error('supervisor_id')
                <div class="invalid-feedback" style="color: #ef1010">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group form-float">
            <label class="form-label">Operations Manager :</label>
            <div class="form-line">
                {!! Form::select("manager_id", $managers??[] ,isset($training ) ? $training?->manager_id : null ,['class' => 'form-control ', 'id' => 'manager']) !!}
                @error('manager_id')
                <div class="invalid-feedback" style="color: #ef1010">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
    </div>
</div>

<div class="row">
<div class="col-lg-12">
    <div class="form-group form-float">
        <label class="form-label">Users :</label>
        <div class="form-line">
            {!! Form::select("user_ids[]", $users??[], $usersItems??[] , ['class' => 'form-control select2', 'id' => 'user_list', 'multiple' => 'multiple']) !!}
            @error('user_ids[]')
            <div class="invalid-feedback" style="color: #ef1010">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
</div>
</div>

<div class="col-lg-12 aligne-center contentbtn" >
    <button class="btn btn-primary waves-effect" type="submit">Save</button>
</div>


@push('scripts')

    <script type="text/javascript">
        $("#park").change(function () {
            $.ajax({
                url: "{{ route('admin.getParkRides') }}?park_id=" + $(this).val(),
                method: 'GET',
                success: function (data) {
                    $('#ride').html(data.html);
                }
            });
        });
        $("#user_type").change(function () {
            let userType = $(this).val();
             let park_id =$('#park').val();
            $.ajax({
                url: "{{ route('admin.getTrainingChecklists') }}?type=" + userType + "&park_id=" + park_id,
                method: 'GET',
                success: function (data) {
                    $('#checklist').html(data.html);
                }
            });
        });
        $("#user_type").change(function () {
        let userType = $(this).val();
        let park_id = $('#park').val();

        $.ajax({
            url: "{{ route('admin.getUsersByType') }}?type=" + userType + "&park_id=" + park_id,
            method: 'GET',
            success: function (response) {
                $('#user_list').html(response.html); // Update the user list container
            }
        });
    });

    $("#checklist").change(function () {
        let checklist = $(this).val();
        $.ajax({
            url: "{{ route('admin.getChecklistType') }}?checklist=" + checklist ,
            method: 'GET',
            success: function (response) {

                if (response.training_type === 'evacuation') {
                $("#ride-container").hide();
            } else {
                $("#ride-container").show(); 
            } 
            }
        });
    });

    $("#park").change(function () {
        let userType = 'manager';
        let park_id = $(this).val();

        $.ajax({
            url: "{{ route('admin.getUsersByType') }}?type=" + userType + "&park_id=" + park_id,
            method: 'GET',
            success: function (response) {
                $('#manager').html(response.html); // Update the user list container
            }
        });
    });
    $("#park").change(function () {
        let userType = 'supervisor';
        let park_id = $(this).val();

        $.ajax({
            url: "{{ route('admin.getUsersByType') }}?type=" + userType + "&park_id=" + park_id,
            method: 'GET',
            success: function (response) {
                $('#supervisor').html(response.html); // Update the user list container
            }
        });
    });

    $(document).ready(function() {
    $('.select2').select2({
        placeholder: "Select users",
        allowClear: true,
        width: '100%',
        closeOnSelect: false
    });

    $('.select2').on('select2:select', function (e) {
        var selected = $(this).val();
        if (selected.length > 100) { // Example limit
            alert("You can only select up to 100 users.");
            $(this).val(selected.slice(0, 100)).trigger('change');
        }
    });
});

    </script>

@endpush
