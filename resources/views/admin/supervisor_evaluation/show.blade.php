@extends('admin.layout.app')

@section('title')
Show Zone Supervisor Performance Evaluation
@endsection

@section('content')
    <div class="row">
        
        <div class="col-xs-12">
            <input type="button" value="Print Report" id="printDiv" class="btn btn-primary printBtn"></input>
           
        </div>


        <div class="col-xs-12 printable_div " id="myDivToPrint">
            <div class="report-head">
                <div class="report-body">
                    <h3 class="report-title">Zone Supervisor Performance Evaluation</h3>
                </div>
                <div class="report-logo">
                    <img src="{{ asset('/_admin/assets/images/logo1.png') }}" alt="Mellors-img" title="Mellors"
                        class="image">
                </div>
            </div>
            <div class="row">
                       
                <div class="col-xs-12 col-sm-12 col-md-12 ">
                    <div class="row">
                    <div class='responsive-wrapper'>
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                               
                                <tr role="row">
                                    <th class="type-header">Employee Name:</th>
                                    <th>{{ $list->supervisor?->name }}</th>
                                    <th class="type-header"> Reviewer Name:</th>
                                    <th>{{  $list->addedBy?->name}}</th>
                                </tr>
                                <tr role="row">
                                    <th class="type-header">Employee ID :</th>
                                    <th>{{ $list->supervisor?->user_number }}</th>
                                    <th class="type-header"> Reviewer ID :</th>
                                    <th>{{  $list->addedBy?->user_number}}</th>

                                </tr>
                                <tr role="row">
                                    <th class="type-header">Department:</th>
                                    <th>Operations</th>
                                    <th class="type-header">Department:</th>
                                    <th>Operations</th>

                                </tr>
                                <tr role="row">
                                    <th class="type-header">Zone Name :</th>
                                    <th>{{ $list->supervisor?->zones()->first()->name }}</th>
                                    <th class="type-header">Job Title:</th>
                                    <th>{{ $list->addedBy?->getRoleNames()->first() }}</th>
                                </tr>
                                <tr role="row">
                                    <th class="type-header">Event Name:</th>
                                    <th>{{ $list->park?->name }}</th>
                                    <th class="type-header">Review Period</th>
                                    <th>
                                        {{ $list->date }}

{{--                                     {{ $list->approve == 'approved' ? $list->approved_at . ' By: ' . optional($list->approvedBy)->name : 'Not Approved Yet' }}
 --}}                                    </th>
                                </tr>
                            </thead>
                        </table>

                    </div>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 ">
                    @if (isset($items) && count($items) > 0)
                        <div class="row">
                    <div class='responsive-wrapper'>
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                    <tr role="row" class="type-header">
                                        <th style="text-align: center;">CRITERIA & FUNCTIONAL SKILLS  </th>
                                        <th> Score</th>
                                        <th> Comment</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $groupedItems = $items->groupBy('question.type');
                                    @endphp

                                    @foreach ($groupedItems as $questionType => $group)
                                    @if($questionType != 'Scoring System & Attributes')
                                        <tr>
                                            <td colspan="4" class="eval-header">
                                                <strong> {{ $questionType }}</strong>
                                            </td>
                                        </tr>
                                        @foreach ($group as $value)
                                            <tr>
                                                <td>{{ $value->question->name ?? '' }}</td>
                                                <td>{{ $value->score ?? 'N/A' }}</td>
                                                <td>{{ $value->comment?? 'N/A' }} </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                    <tr role="row" class="type-header">
                                        <th style="text-align: center;">Scoring System & Attributes </th>
                                        <th> Score</th>
                                        <th> Description</th>
                                        <th> Remarks / Feedback</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $groupedItems = $items->groupBy('question.type');
                                    @endphp

                                    @foreach ($groupedItems as $questionType => $group)
                                    @if($questionType == 'Scoring System & Attributes')
                                    @php
                                    $i = 5;
                                    $rowCount = count($group); 

                                @endphp
                               @foreach ($group as $index => $value)
                               <tr>
                                   <td>{{ $value->question->name ?? '' }}</td>
                                   <td>{{ $i }}</td>
                                   <td>{{ $value->question->description ?? 'N/A' }}</td>
                                   @if ($index == 0)
                                       <td rowspan="{{ $rowCount }}">{{ $list->remarks ?? 'N/A' }}</td>
                                   @endif
                               </tr>
                               @php
                                   $i --;
                               @endphp
                           @endforeach
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>

                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                    <tr role="row" class="type-header">
                                        <th style="text-align: center;">Scoring System & Attributes </th>
                                        <th> Score</th>
                                        <th> Description</th>
                                        <th> Remarks / Feedback</th>
                                    </tr>
                                </thead>
                                <tbody>
                                 
                                    <tr>
                                        <td>Outstanding</td>
                                        <td>5</td>
                                        <td>Supervisor Performance represents an extraordinary level of achievement and commitment in terms of quality skill and knowledge. Operator at this level should have demonstrated exceptional job mastery in all areas of responsibility	</td>
                                        <td rowspan="5">
                                            {{ $list->remarks }}                             
                                        </td>
                                    </tr>
                                    <tr>
                                     <td>Exceeds Requirements	</td>
                                     <td>4</td>
                                     <td>Supervisor performance Exceeded expectations . All goals and objectivises were achieved above the  requirements of the role.
                                     </td>
                                 </tr>
                                 <tr>
                                    <td>Meets Requirements	</td>
                                    <td>3</td>
                                    <td>Supervisor performance met expectations in terms of quality of work. Most critical goals were met 

                                    </td>
                                </tr>
                                <tr>
                                    <td>Need Improvement	</td>
                                    <td>2</td>
                                    <td>Supervisor performance failed to meet expectations, and/or reasonable progress toward critical goals was not achieved  

                                    </td>
                                </tr>
                                <tr>
                                    <td>Unsatisfactory</td>
                                    <td>1</td>
                                    <td> Supervisor performance was consistently below expectations, and/or reasonable progress toward critical goals was not achieved  
                                        
                                    </td>
                                </tr>
                                     </tbody>
                            </table>
                        
                            <table style="border-color: #0b0b0b"
                            class="table table-striped table-bordered dt-responsive nowrap">
                            <tbody>
                                <tr>
                                    <th  style="border-color: #0b0b0b; background-color: #c8e1f8">Supervisor/Reviewer name :</td>
                                    <td style="border-color: #0b0b0b">{{ $list->addedBy?->name }}</td>
                                </tr>
                                <tr>
                                    <th style="border-color: #0b0b0b; background-color: #c8e1f8">Date : </td>
                                    <td style="border-color: #0b0b0b">{{ $list->date }}</td>
                                </tr>
                                <tr>
                                    <th style="border-color: #0b0b0b; background-color: #c8e1f8">Signature :</td>
                                    <td style="border-color: #0b0b0b">
                                    <a >
                                    <img class="img-preview"
                                    src="{{ Storage::disk('s3')->temporaryUrl('images/'.baseName($list->signature),now()->addMinutes(30)) }}"
                                    style="height: 40px; width: 40px"></a>
                                </td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="border-color: #0b0b0b"
                        class="table table-striped table-bordered dt-responsive nowrap">
                        <tbody>
                            <tr>
                                <th  style="border-color: #0b0b0b; background-color: #c8e1f8 " >Total Score:</td>
                                <td style="border-color: #0b0b0b;">{{ $list->total_score }}</td>
                            </tr>
                        </tbody>
                    </table>
                        <table style="border-color: #0b0b0b"
                        class="table table-striped table-bordered dt-responsive nowrap">
                        <tbody>
                            <tr>
                                <th colspan="5"  style="border-color: #0b0b0b;  background-color: #c8e1f8 ; text-align: center; " >Brackets Based on Total Score</td>
                            </tr>
                            <tr>
                                <th style="text-align: center; border-color: #0b0b0b; background-color: #76ee42">Outstanding</th>
                                <th style="text-align: center; border-color: #0b0b0b; background-color: #348013">Exceeds</th>
                                <th style="text-align: center; border-color: #0b0b0b; background-color: #f59a4b">Meets</th>
                                <th style="text-align: center; border-color: #0b0b0b; background-color: #a00707">Improvement</th>
                                <th style="text-align: center; border-color: #0b0b0b; background-color: #f80404">Unsatisfactory</th>
                            </tr>
                            <tr style="text-align: center">
                                <td style="border-color: #0b0b0b; background-color: rgb(221, 183, 155)">136 - 165</td>
                                <td style="border-color: #0b0b0b; background-color: rgb(221, 183, 155)">106 - 135</td>
                                <td style="border-color: #0b0b0b; background-color: rgb(221, 183, 155)">76 - 105</td>
                                <td style="border-color: #0b0b0b; background-color: rgb(221, 183, 155)">46 - 75</td>
                                <td style="border-color: #0b0b0b; background-color: rgb(221, 183, 155)">0 - 45</td>

                            </tr>
                        </tbody>
                    </table>
                        </div>
                        </div>

                    @else
                        <label>No questions</label>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script language="javascript">
        $('#printDiv').click(function() {
            $('#myDivToPrint').show();
            window.print();
            return false;
        });
    </script>
@endpush
