@extends('admin.layout.app')

@section('title')
    Update Supervisor Performance Evaluation
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12 printable_div " id="myDivToPrint">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 ">
                    <div class="row">
                        <div class='responsive-wrapper'>
                            <table id="datatable-buttons"
                                   class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>

                                {{--                                <tr role="row">--}}
                                {{--                                    <th class="type-header">Employee Name:</th>--}}
                                {{--                                    <th>{{ $evaluation->supervisor?->name }}</th>--}}
                                {{--                                    <th class="type-header">Supervisor / Reviewer Name:</th>--}}
                                {{--                                    <th>{{  $evaluation->addedBy?->name}}</th>--}}
                                {{--                                </tr>--}}


                                {{--                                <tr role="row">--}}
                                {{--                                    <th class="type-header">Ride Name :</th>--}}
                                {{--                                    <th>{{ $evaluation->ride?->name }}</th>--}}
                                {{--                                    <th class="type-header">Job Title:</th>--}}
                                {{--                                    <th>Operations/Zone supervisor</th>--}}

                                {{--                                </tr>--}}
                                <tr role="row">
                                    <th class="type-header">Event Name:</th>
                                    <th>{{ $evaluation->park?->name }}</th>
                                    <th class="type-header">Review Period</th>
                                    <th>
                                        {{ $evaluation->date }}
                                        {{--                                         {{ $evaluation->approve == 'approved' ? $evaluation->approved_at . ' By: ' . optional($evaluation->approvedBy)->name : 'Not Approved Yet' }}
                                         --}}                                    </th>

                                </tr>
                                </thead>
                            </table>

                        </div>
                    </div>

                </div>
                {!!Form::model($evaluation , ['route' => ['admin.supervisor-evaluation.update' , $evaluation->id] ,
                'method' => 'PATCH','enctype'=>"multipart/form-data",'files' => true,'id'=>'form']) !!}
                <div class=" col-xs-12 col-sm-12 col-md-12 mb-2 ">
                    <div class="col-md-6 mb-2 p-2">
                        <label class="form-label">Supervisor</label>
                        <div class="form-line">
                            {!! Form::select('supervisor_id', $users,$evaluation->supervisor_id, array('class' => 'form-control select2')) !!}
                            @error('supervisor_id')
                            <div class="invalid-feedback" style="color: #ef1010">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="form-label">Rides</label>
                        <div class="form-line">
                            {!! Form::select('ride_id', $rides,$evaluation->ride_id, array('class' => 'form-control select2')) !!}
                            @error('ride_id')
                            <div class="invalid-feedback" style="color: #ef1010">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <br><br><br><br><br><br>



                <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                    @if (isset($evaluation->lists) && count($evaluation->lists) > 0)
                        <div class="row">
                            <div class='responsive-wrapper'>
                                <table id="datatable-buttons"
                                       class="table table-striped table-bordered dt-responsive nowrap">
                                    <thead>
                                    <tr role="row" class="type-header">
                                        <th style="text-align: center;">CRITERIA & FUNCTIONAL SKILLS</th>
                                        <th> Score</th>
                                        <th> Comment</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $groupedItems = $evaluation->lists->groupBy('question.type');
                                    @endphp


                                    @foreach ($groupedItems as $questionType => $group)
                                        @if($questionType != 'Scoring System & Attributes')
                                            <tr>
                                                <td colspan="4" class="eval-header">
                                                    <strong> {{ $questionType }}</strong>
                                                </td>
                                            </tr>
                                            @foreach ($group as $value)
                                                <tr>
                                                    <td>{{ $value->question->name ?? '' }}
                                                        <input type="hidden"
                                                               name="question_id[]"
                                                               value="{{$value->question->id}}">
                                                    </td>
                                                    <td>
                                                        <input type="number"
                                                               name="score[]"
                                                               value="{{$value->score}}">

                                                    </td>
                                                    <td>
                                                        <textarea cols="50" rows="6"
                                                                  name="comment[]">{{ $value->comment }} </textarea>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endforeach



                                    </tbody>
                                </table>

                                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                    <thead>
                                        <tr role="row" class="type-header">
                                            <th style="text-align: center;">Scoring System & Attributes </th>
                                            <th> Score</th>
                                            <th> Description</th>
                                            <th> Remarks / Feedback</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     
                                        <tr>
                                            <td>Outstanding</td>
                                            <td>5</td>
                                            <td>Supervisor Performance represents an extraordinary level of achievement and commitment in terms of quality skill and knowledge. Operator at this level should have demonstrated exceptional job mastery in all areas of responsibility	</td>
                                            <td rowspan="5">
                                                <textarea cols="50" rows="5"
                                                name="remarks">{{ $evaluation->remarks }}
                                             </textarea>
                                 
                                            </td>
                                        </tr>
                                        <tr>
                                         <td>Exceeds Requirements	</td>
                                         <td>4</td>
                                         <td>Supervisor performance Exceeded expectations . All goals and objectivises were achieved above the  requirements of the role.
                                         </td>
                                     </tr>
                                     <tr>
                                        <td>Meets Requirements	</td>
                                        <td>3</td>
                                        <td>Supervisor performance met expectations in terms of quality of work. Most critical goals were met 

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Need Improvement	</td>
                                        <td>2</td>
                                        <td>Supervisor performance failed to meet expectations, and/or reasonable progress toward critical goals was not achieved  

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Unsatisfactory</td>
                                        <td>1</td>
                                        <td> Supervisor performance was consistently below expectations, and/or reasonable progress toward critical goals was not achieved  
                                            
                                        </td>
                                    </tr>
                                         </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="col-xs-12 aligne-center contentbtn">
                            <button class="btn btn-primary waves-effect" form="form" type="submit">Save</button>
                        </div>
                    @else
                        <label>No questions</label>
                    @endif
                </div>
                {!!Form::close() !!}
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script language="javascript">
        $('#printDiv').click(function () {
            $('#myDivToPrint').show();
            window.print();
            return false;
        });
    </script>
@endpush
