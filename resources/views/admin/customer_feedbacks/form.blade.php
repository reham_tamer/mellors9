{{--@include('admin.common.errors')--}}
<div class="row">
    <div class="col-xs-12">
        <div class="form-group form-float">
            <label class="form-label">Ride</label>
            <div class="form-line">
                {!! Form::select('ride_id', $rides,null, array('class' => 'form-control ')) !!}
                @error('ride_id')
                <div class="invalid-feedback" style="color: #ef1010">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group form-float">
                <label class="form-label">Customer Feedback Type</label>
                <div class="form-line">
                    <input type="radio" name="type" value="Complaint" class="ml-3" id="complaint_type" > Negative
                    <input type="radio" name="type" value="Positive" class="ml-3" id="positive_type" > Positive
                    <input type="radio" name="type" value="Suggestion" class="ml-3" id="sugg"  > Suggestion
                    <input type="radio" name="type" value="Other " class="ml-3" id="other" > Other
                </div>
            </div>
        </div>

        <div class="col-xs-12 " id="complaint" hidden>
            <div class="form-group form-float">
                <label class="form-label">Negative Items</label>
                <div class="form-line">
                    <select name="complaint_id" class="form-control positive_type">
                        <option value=""> Choose....</option>
                        @foreach ($complaints as $value )
                           <option value="{{ $value->id }}"> {{ $value->name }} </option>
                        @endforeach
                        </select>
                    @error('type')
                    <div class="invalid-feedback" style="color: #ef1010">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-xs-12 " id="positive" hidden>
            <div class="form-group form-float">
                <label class="form-label">Positive Items</label>
                <div class="form-line">
                    <select name="complaint_id" class="form-control positive_type">
                        <option value=""> Choose....</option>
                        @foreach ($positive as $value )
                           <option value="{{ $value->id }}"> {{ $value->name }} </option>
                        @endforeach
                        </select>
                    @error('type')
                    <div class="invalid-feedback" style="color: #ef1010">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-xs-12 " id="other_items" hidden>
            <div class="form-group form-float">
                <label class="form-label">Other Items</label>
                <div class="form-line">
                    <select name="complaint_id" class="form-control other_type">
                        <option value=""> Choose....</option>
                        @foreach ($other as $value )
                           <option value="{{ $value->id }}"> {{ $value->name }} </option>
                        @endforeach
                        </select>
                    @error('type')
                    <div class="invalid-feedback" style="color: #ef1010">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-lg-12 form-group " id="subCat"  hidden>
            <label class="block">Customer Feedback Sub Category :</label>
    
            <div class="">
    
            {!! Form::select("feedback_subcategory_id",[],null,
                ['class'=>'form-control js-example-basic-single ms select2 subCategory','id'=>'subCategory','placeholder'=>'Choose Main Category first'])!!}
                    @error('feedback_subcategory_id')
                <div class="invalid-feedback" style="color: #ef1010">
                    {{ $message }}
                </div>
                @enderror
    
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group form-float">
                <label class="form-label">Comment</label>
                <div class="form-line">
                    {!! Form::textArea("comment",null,['class'=>'form-control  ','placeholder'=>' comment'])!!}
                    @error('comment')
                    <div class="invalid-feedback" style="color: #ef1010">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
        </div>
      {{--   <div class="col-xs-12">
            <div class="form-group form-float">
                <label class="form-label">Date</label>
                <div class="form-line">
                    {!! Form::date("date",null,['class'=>'form-control','placeholder'=>' comment'])!!}
                    @error('date')
                    <div class="invalid-feedback" style="color: #ef1010">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
        </div> --}}

        <div class="form-group">
            <label for="name"> Upload Customer Feedback Images </label>
            {!! Form::file('image[]' , [
                                      "class" => "form-control  file_upload_preview",
                                      "multiple" => "multiple",
                                      "data-preview-file-type" => "text"
                                                  ]) !!}
        </div>

        <div class="col-xs-12 aligne-center contentbtn">
            <button class="btn btn-primary waves-effect" type="submit">Save</button>
        </div>
    </div>
    @push('scripts')

    <script>
        $('#complaint_type').click(function () {
            $('#complaint').show();
            $('#positive').hide();
            $('#subCat').show();
            $('#other_items').hide();


        }) ;
        $('#other').click(function () {
            $('#complaint').hide();
            $('#positive').hide();
            $('#subCat').hide();
            $('#other_items').show();

            


        }) ;
        $('#sugg').click(function () {
            $('#complaint').hide();
            $('#positive').hide();
            $('#subCat').hide();
            $('#other_items').hide();



        }) ;

        $('#positive_type').click(function () {
            $('#positive').show();
            $('#subCat').show();
            $('#complaint').hide();
            $('#other_items').hide();


        }) ;
      
    </script>
    <script type="text/javascript">
        $('.positive_type').change(function() {
            var val = $(this).val();
            console.log(val);
            $.ajax({
                type: "post",
                url: "{{ route('admin.getFeedbackSubCategories') }}",
                data: {
                    'feedback_category_id': val,
                    '_token': "{{ @csrf_token() }}"
                },
                success: function(data) {
                    var options = '<option disabled>Choose Main Category</option>';
                    $.each(data.subCategory, function(key, value) {
                        options += '<option value="' + value.id + '">' + value.name +
                            '</option>';
        
                    });
                    $("#subCategory").empty().append(options);
                }
            });
        });
    </script>
    @endpush