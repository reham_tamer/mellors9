<!DOCTYPE html>
<html lang="en">
<body>
<div class="card-body pt-5">
    <div>
        <p> Last update of queues </p>
    </div>
    <div class="cursor-pointer">
        <i class="fa fa-file"></i>
        <div class="col-sm-12 col-12">
            <a href="{{$path}}" download=""> <span class="align-middle ms-1">queue.xlsx</span></a>
        </div>
    </div>
</div>
</body>
</html>
