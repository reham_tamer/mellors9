<?php
namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ObservationCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
      */
    public function run()
    {

        $category = [
          /*   [
                'name' => 'Staff Topic',
            ],
            [
                'name' => 'Manpower Topic',
            ],
            [
                'name' => 'Ride Hire Topic',
            ],
            [
                'name' => 'Ride Improvement',
            ],
            [
                'name' => 'Ops Improvement',
            ],
            [
                'name' => 'Site Improvement',
            ],
            [
                'name' => 'Safety Topic',
            ],
            [
                'name' => 'Cleanliness',
            ],
            [
                'name' => 'Other',
            ],
 */

        ];
        \DB::table('observation_categories')->insert($category);
    }
}
