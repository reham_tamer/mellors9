<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(PermissionsTableSeeder::class);
        $this->call(TrainingChecklistTableSeeder::class);
        $this->call(TrainingChecklistItemTableSeeder::class);
//         $this->call(RoleTableSeeder::class);
//         $this->call(UserSeeder::class);
//         $this->call(GeneralQuestionSeeder::class);
//         $this->call(CheckListQuestionSeeder::class);
//         $this->call(ObservationCategoryTableSeeder::class);
//         $this->call(EvaluationQuestionSeeder::class);

    }
}