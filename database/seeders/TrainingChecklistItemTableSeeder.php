<?php
namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TrainingChecklistItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
      */
    public function run()
    {

        $items = [
            [
                'type' => 'Description of Task (On Attraction Theory)',
                'training_checklist_id' => '1',
                'name_en' =>'I have been instructed on and fully comprehend the attraction orientation, including the identification   of hazardous areas (both operational and non-operational) and designated restricted zone  ',
                'name_ar' => 'لقد تم توجيهي وأفهم تمامًا توجهات اللعبة، بما في ذلك تحديد
                              المناطق الخطرة )سواء التشغيلية أو غير التشغيلية( والمنطقة
                              المحظورة المحددة .'
             ], 
               [
                'type' => 'Description of Task (On Attraction Theory)',
                'training_checklist_id' => '1',
                'name_en' =>'I have been instructed on and fully comprehend the attraction operational risk assessment concept and can access it, if I need to (also where to find this document).',
                'name_ar' => 'لقد تم توجيهي وأفهم تمامًا مفهوم تقييم المخاطر التشغيلية للعبة
                              وأدرك أنه يمكنني الوصول إلى هذا التقييم عند الحاجة )وأعرف
                              مكان العثور على هذا المستند( .'
             ], 
             [
                'type' => 'Description of Task (On Attraction Theory)',
                'training_checklist_id' => '1',
                'name_en' =>'
                          I have been instructed on and fully comprehend the 
                          concept of the attraction operations manual, and I   
                          can access it, if I need to (also where to find this 
                          document). ',
                'name_ar' => 'لقد تم توجيهي وأفهم تمامًا مفهوم دليل تشغيل اللعبة ويمكنني
                            الوصول إليه عند الحاجة )وأعرف مكان العثور على هذا المستند( .'
               ], 
             [
                'type' => 'Description of Task (On Attraction Theory)',
                'training_checklist_id' => '1',
                'name_en' =>'
                          I have been instructed on and fully comprehend the 
                          company procedures regarding attending persons 
                          with disabilities. ',
                'name_ar' => 'لقد تم توجيهي وأفهم تمامًا إجراءات الشركة فيما يتعلق بحضور
                              الأشخاص ذوي الاحتياجات الخاصة .'
               ], 
             [
                'type' => 'Description of Task (On Attraction Theory)',
                'training_checklist_id' => '1',
                'name_en' =>' 
                          I have been instructed on and fully comprehend the 
                          company reporting procedures regarding accidents, 
                          near misses, incidents, complaints, confrontations, 
                          and suspicious activity. ',
                'name_ar' => 'لقد تم توجيهي وأفهم تمامًا إجراءات الإبلاغ عن الحوادث، الحوادث
                              الوشيكة والوقائع العامة ، الشكاوى، المواجهات، والأنشطة المشبوهة .'
              ], 
             [
                'type' => 'Description of Task (On Attraction Theory)',
                'training_checklist_id' => '1',
                'name_en' =>'I have been instructed on and fully comprehend the company procedures regarding attending to guests in the queue.',
                'name_ar' => 'لقد تم توجيهي وأفهم تمامًا إجراءات الشركة المتعلقة بحضور
                            الضيوف في الطابور .'
              ], 
             [
                'type' => 'Description of Task (On Attraction Theory)',
                'training_checklist_id' => '1',
                'name_en' =>'
                          I have been instructed on and fully comprehend the 
                          signing-in and out procedures for the attraction ',
                'name_ar' => 'لقد تم إرشادي وأفهم تمامًا إجراءات التسجيل دخولًا وخروجًا
                              الخاصة باللعبة .'  
              ], 
             [
                'type' => 'Description of Task (On Attraction Theory)',
                'training_checklist_id' => '1',
                'name_en' =>'I have been instructed on and fully comprehend how
                            to attend to a distressed guest in line with company procedures.',
                'name_ar' => 'لقد تم إرشادي وأفهم تمامًا كيف
                              لرعاية الضيف المنكوب بما يتماشى مع إجراءات الشركة.'
              ], 
             [
                'type' => 'Description of Task (On Attraction Theory)',
                'training_checklist_id' => '1',
                'name_en' =>'I have been instructed on and fully comprehend that I am to inform the attraction operator (for that shift) if I or anyone else needs first aid assistance.',
                'name_ar' => 'لقد تلقيت تعليمات وأدركت تمامًا أنني يجب أن أبلغ مشغل
                              اللعبه )لهذه الوردية( إذا كنت أنا أو أي شخص آخر بحاجة
                              إلى مساعدة الإسعافات الأولية.'
               ], 
             [
                'type' => 'Description of Task (On Attraction Theory)',
                'training_checklist_id' => '1',
                'name_en' =>'I have been instructed on and fully comprehend the pre-opening and closing procedures and checks regarding this attraction.',
                'name_ar' => 'لقد تم إرشادي وفهمي الكامل لإجراءات ما قبل الافتتاح
                              والإغلاق والفحوصات المتعلقة بهذه اللعبة .'
               ], 
             [
                'type' => 'Description of Task (Practical Demonstration)',
                'training_checklist_id' => '1',
                'name_en' =>'I have been instructed on and fully comprehend that if any other person is trying to gain access to the attraction (except Mellors personnel), then I MUST ask for identification, their name and what they are doing at this location. Before letting them inside the entrance or exit gates, I must inform the Mellors Coordinator/Manager/Supervisor to get authorisation.',
                'name_ar' => 'لقد تم إرشادي وأفهم تمامًا أنه إذا حاول أي شخص آخر
                              الوصول إلى اللعبة (باستثناء موظفي Mellors) ، فيجب
                              عليّ طلب بطاقة التعريف، اسمه والغرض من وجوده في هذا
                              الموقع. قبل السماح لهم بالدخول عبر بوابات الدخول أو
                              الخروج، يجب عليّ إبلاغ منسق/مدير/مشرف Mellors
                              للحصول على إذن .'
               ], 
             [
                'type' => 'Description of Task (Practical Demonstration)',
                'training_checklist_id' => '1',
                'name_en' =>'I have received instructions, demonstrated my knowledge, and fully comprehend the emergency evacuation (site and attraction specific) and the procedures regarding a fire.',
                'name_ar' => 'لقد تلقيت التعليمات، وأثبتت معرفتي، وأفهم تمامًا إجراءات الإخلاء
                                الطارئ )الموقع واللعبة المحددة( والإجراءات الخاصة بالحريق .'
              ], 
             [
                'type' => 'Description of Task (Practical Demonstration)',
                'training_checklist_id' => '1',
                'name_en' =>'I have received instructions, demonstrated my knowledge, and fully comprehend the specific guest rider restrictions for this attraction (heights, ages, health etc).',
                'name_ar' => 'لقد تلقيت التعليمات، وأثبتت معرفتي، وأفهم تمامًا القيود الخاصة
                              بركوب الضيوف لهذه اللعبة )الطول، العمر، الصحة، إلخ( .'
             ], 
             [
                'type' => 'Description of Task (Practical Demonstration)',
                'training_checklist_id' => '1',
                'name_en' =>'I have received instructions, demonstrated my knowledge, and fully comprehend the restrictions regarding people not being permitted to bring personal items/possessions on the attraction whilst it is in operation.',
                'name_ar' => 'لقد تلقيت التعليمات، وأثبتت معرفتي، وأفهم تمامًا القيود المتعلقة
                              بعدم السماح بإحضار الأغراض الشخصية أثناء تشغيل اللعبة .'
               ],
             [
                'type' => 'Description of Task (Practical Demonstration)',
                'training_checklist_id' => '1',
                'name_en' =>'I have received instructions, demonstrated my knowledge, and fully comprehend the operation of the attraction access and egress gates and that I am to safely control guest access points.',
                'name_ar' => 'لقد تلقيت التعليمات، وأثبتت معرفتي، وأفهم تمامًا كيفية تشغيل
                              بوابات الدخول والخروج والتحكم بأمان في نقاط وصول الضيوف .'
                ],  
             [
                'type' => 'Description of Task (Practical Demonstration)',
                'training_checklist_id' => '1',
                'name_en' =>'I have received instructions, demonstrated my 
                              knowledge, and fully comprehend how to place 
                              guests correctly regarding safety and weight 
                              distribution (for the specific attraction) within the 
                              gondola/attraction seating equipment. ',
                'name_ar' => 'لقد تلقيت التعليمات، وأثبتت معرفتي، وأدركت تمامًا كيفية وضع
                              الضيوف بشكل صحيح فيما يتعلق بالأمان وتوزيع الوزن داخل
                              مقاعد/عربات اللعبة'
               ],  
             [
                'type' => 'Description of Task (Practical Demonstration)',
                'training_checklist_id' => '1',
                'name_en' =>'I have received instructions, demonstrated my knowledge, and fully comprehend how to close the restraint and how to complete the necessary safety checks correctly (in line with the operations manual and safety restraint procedure).',
                'name_ar' => 'لقد تلقيت التعليمات، وأثبتت معرفتي، وأفهم تمامًا كيفية إغلاق نظام
                            القيد وكيفية إجراء فحوصات السلامة اللازمة بشكل صحيح )بما
                            يتماشى مع دليل التشغيل وإجراءات نظام الأمان('
               ],  
             [
                'type' => 'Description of Task (Practical Demonstration)',
                'training_checklist_id' => '1',
                'name_en' =>'I have received instructions, demonstrated my knowledge, and fully comprehend the hand signals and safe dispatch procedure',
                'name_ar' => 'لقد تلقيت التعليمات، وأثبتت معرفتي، وأفهم تمامًا إشارات اليد
                              وإجراءات الإرسال الآمن .'
               ],  
             [
                'type' => 'Description of Task (Practical Demonstration)',
                'training_checklist_id' => '1',
                'name_en' =>'I have received instructions, demonstrated my knowledge, and fully comprehend the position I should be located and where guests should be located whilst the attraction is in operation',
                'name_ar' => 'لقد تلقيت التعليمات، وأثبتت معرفتي، وأدركت تمامًا الموقع الذي
                              يجب أن أتواجد فيه والمكان الذي يجب أن يتواجد فيه الضيوف أثناء
                              تشغيل اللعبة .'
                ],  
             [
                'type' => 'Description of Task (Practical Demonstration)',
                'training_checklist_id' => '1',
                'name_en' =>'I have received instructions, demonstrated my knowledge, and fully comprehend that I will inform the guests to always keep their arms and legs inside the vehicle, and ensure that they hold onto the restraints/safety bars before the commencement of every cycle.',
                'name_ar' => 'لقد تلقيت تعليمات، وأثبتت معرفتي، وأدرك تمامًا أنني
                              سأبلغ الضيوف بضرورة إبقاء أذرعهم وأرجلهم دائمًا داخل
                              العربة، والتأكد من تمسكهم بالقيود/قضبان الأمان قبل بدء كل
                              دورة.'
                ],  
             [
                'type' => 'Description of Task (Practical Demonstration)',
                'training_checklist_id' => '1',
                'name_en' =>'I have received instructions, demonstrated my knowledge, and fully comprehend that I have a responsibility to be observant (i.e., be aware of guest locations and watch the attraction for abnormalities and distressed guests) whilst the attraction is in operation.',
                'name_ar' => 'لقد تلقيت تعليمات، وأثبتت معرفتي، وأدرك تمامًا أنني أتحمل
                              مسؤولية الالتزام )أي أن أكون على دراية بمواقع الضيوف
                              ومراقبة اللعبة بحثًا عن الحالات غير الطبيعية والضيوف
                              المتعثرين( أثناء تشغيل اللعبة .'
              ],  
             [
                'type' => 'Description of Task (Practical Demonstration)',
                'training_checklist_id' => '1',
                'name_en' =>'I have received instructions, demonstrated my knowledge, and fully comprehend the restrictions regarding the use of mobile devices whilst on duty.',
                'name_ar' => 'لقد تلقيت تعليمات، وأثبتت معرفتي، وأدركت تمامًا القيود
                              المتعلقة باستخدام الأجهزة المحمولة أثناء العمل.'
               ],
          
             [
               'type' => 'Description of Task (On Attraction Theory)',
               'training_checklist_id' => '2',
               'name_en' => 'I have received training and I understand the ride orientation, including ride hazardous areas (operational and non-operational) and restricted areas. ',
               'name_ar' => ''
             ], 
             [
             'type' => 'Description of Task (On Attraction Theory)',
             'training_checklist_id' => '2',
             'name_en' => 'I have received training and I have read and understood the ride operational risk assessment (also where to find this document). ',
             'name_ar' => ''
           ], 
           [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '2',
            'name_en' => 'I have received training and I have read and understood the ride operations manual (also where to find this document).',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '2',
            'name_en' => 'I have received training and I understand the company reporting procedures regarding accidents, near misses, incidents, complaints, confrontation, and suspicious activity.',
            'name_ar' => ''
          ],
       
          [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '2',
            'name_en' => 'I have received training and I fully understand the pre-opening, closing procedures and checks regarding this attraction.',
            'name_ar' => ''
          ],
       
          [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '2',
            'name_en' => 'I have received training and I understand the operation of the ride access and egress gates and that I am to safely control customer access points.',
            'name_ar' => ''
          ],
       
          [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '2',
            'name_en' => 'I have received training and I understand how to place customers correctly regarding safety and weight distribution (for the specific ride) within the gondola/ride seating equipment.',
            'name_ar' => ''
          ],
       
          [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '2',
            'name_en' => 'I have received training and I understand how to attend to a distressed customer in line with company procedures and the in house training I have received.',
            'name_ar' => ''
          ],
       
          [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '2',
            'name_en' => 'I have received training and I understand that I MUST evacuate the attraction in line with the specific attraction operations manual and I MUST not deviate from this procedure.',
            'name_ar' => ''
          ],
       
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '2',
            'name_en' => 'I have received training, demonstrated and I understand how to close the restraint systems and how to complete the necessary safety checks correctly (in line with the operations manual).',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '2',
            'name_en' => 'I have received training, demonstrated and I understand the position I should be located and where my staff and customers should be located whilst the ride is in operation',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '2',
            'name_en' => 'I have received training, demonstrated and I understand the procedures regarding a ride breakdown, including informing management and recording the details (time, date, issues/failure).',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '2',
            'name_en' => 'I have received training, demonstrated and I understand the circumstances in which to implement the emergency stop procedures.',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '2',
            'name_en' => 'I have received training, demonstrated and I understand what equipment is required to evacuate customers from this ride safely and as per the operations, maintenance manuals and emergency evacuation procedure.',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '2',
            'name_en' => 'I have received training, demonstrated and I understand that I am to call the operations manager before I proceed with any evacuation if the attraction needs evacuating (excluding fire).',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '2',
            'name_en' => 'I have received training, demonstrated and I understand the emergency evacuation procedure (site, my duties according to my role during the evacuation and attraction specific) and the procedures regarding fire. ',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training and I understand the ride orientation, including ride hazardous areas (operational and non-operational) and restricted areas. ',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training and I have read and understood the ride operational risk assessment (also where to find this document).',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training and I have read and understood the ride operations manual(also where to find this document).',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training and I understand the company reporting procedures regarding accidents, near misses, incidents, complaints, confrontation, and suspicious activity.',
            'name_ar' => ''
          ], [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training and I fully understand the pre-opening, closing procedures and checks regarding this attraction.',
            'name_ar' => ''
          ], [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training and I understand the operation of the ride access and egress gates and that I am to safely control customer access points.',
            'name_ar' => ''
          ], [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training and I understand how to place customers correctly regarding safety and weight distribution (for the specific ride) within the gondola/ride seating equipment.',
            'name_ar' => ''
          ], [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training and I understand how to attend to a distressed customer in line with company procedures and the in house training I have received.',
            'name_ar' => ''
          ], [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training and I understand that I MUST evacuate the attraction in line with the specific attraction operations manual and I MUST not deviate from this procedure.',
            'name_ar' => ''
          ], [
            'type' => 'Description of Task (On Attraction Theory)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training and I understand that I MUST inform the local emergency services/civil defence if I cannot manage to safely evacuate guests due to excessive time limit or unforeseen circumstances or technical errors with the attraction functionality, that are beyond my competency level.',
            'name_ar' => ''
          ], [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training, demonstrated and I understand all the ride controls and their functions.',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training, demonstrated and I understand how to close the restraint systems and how to complete the necessary safety checks correctly (in line with the operations manual).',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training, demonstrated and I understand the position I should be located and where my staff and customers should be located whilst the ride is in operation',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training, demonstrated and I understand how to instruct, supervise, and ensure the ,ride operators (if supervisor or evac team), ride attendants and queue assistants operate in a safe manner.',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training, demonstrated and I understand the starting and stopping procedures for the ride (as per the operations manual).',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training, demonstrated and I understand the procedures regarding a ride breakdown, including informing management and recording the details (time, date, issues/failure).',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training, demonstrated and I understand the circumstances in which to implement the emergency stop procedures.',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training, demonstrated and I understand what equipment is required to evacuate customers from this ride safely and as per the operations, maintenance manuals and emergency evacuation procedure.',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training, demonstrated and I understand that I am to call the operations manager before I proceed with any evacuation if the attraction needs evacuating (excluding fire).',
            'name_ar' => ''
          ],
          [
            'type' => 'Description of Task (Practical Demonstration)',
            'training_checklist_id' => '3',
            'name_en' => 'I have received training, demonstrated and I understand the emergency evacuation procedure (site, my duties according to my role during the evacuation and attraction specific) and the procedures regarding fire. ',
            'name_ar' => ''
          ],

        ];
        foreach ($items as $item) {
         $exists = \DB::table('training_checklist_items')
                   ->where('name_en', $item['name_en'])
                   ->exists();

       if (!$exists) {
         \DB::table('training_checklist_items')->insert($items);
      }
    }
   }

}
