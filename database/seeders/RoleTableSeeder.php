<?php
namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // \DB::table('roles')->delete();

        \DB::table('roles')->insert(
            array(
             /*     0 =>
                 array(
                     'id' => 1,
                     'name' => 'Super Admin',
                     'guard_name' => 'web',
                     'created_at' => NULL,
                     'updated_at' => NULL,
                 ),

                 1 =>
                 array(
                     'id' => 2,
                     'name' => 'zone supervisor',
                     'guard_name' => 'web',
                     'created_at' => NULL,
                     'updated_at' => NULL,
                 ),
                 2 =>
                 array(
                     'id' => 3,
                     'name' => 'Technical',
                     'guard_name' => 'web',
                     'created_at' => NULL,
                     'updated_at' => NULL,
                 ),
                 3 =>
                 array(
                     'id' => 4,
                     'name' => 'Operation',
                     'guard_name' => 'web',
                     'created_at' => NULL,
                     'updated_at' => NULL,
                 ),
                 4 =>
                 array(
                     'id' => 5,
                     'name' => 'Skill Games',
                     'guard_name' => 'web',
                     'created_at' => NULL,
                     'updated_at' => NULL,
                 ),
                 5 =>
                 array(
                     'id' => 6,
                     'name' => 'Maintenance',
                     'guard_name' => 'web',
                     'created_at' => NULL,
                     'updated_at' => NULL,
                 ),
                 6 =>
                 array(
                     'id' => 7,
                     'name' => 'Health & Safety',
                     'guard_name' => 'web',
                     'created_at' => NULL,
                     'updated_at' => NULL,
                 ),
                 7 =>
                 array(
                     'id' => 8,
                     'name' => 'Ride & Ops',
                     'guard_name' => 'web',
                     'created_at' => NULL,
                     'updated_at' => NULL,
                 ),
                 8 =>
                 array(
                     'id' => 9,
                     'name' => 'Branch Admin',
                     'guard_name' => 'web',
                     'created_at' => NULL,
                     'updated_at' => NULL,
                 ), */
                /*  9 =>
                 array(
                     'id' => 10,
                     'name' => 'Park Admin',
                     'guard_name' => 'web',
                     'created_at' => NULL,
                     'updated_at' => NULL,
                 ),
                 10 =>
                array(
                     'id' => 11,
                    'name' => 'Client',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
                11 =>
               array(
                    'id' => 12,
                    'name' => 'Health & Safty Supervisor',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               12 =>
               array(
                    'id' => 13,
                    'name' => 'Management/General Manager',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),

            13 =>
               array(
                    'id' => 14,
                    'name' => 'Management/CEO',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),

            14 =>
               array(
                    'id' => 15,
                    'name' => 'Operations/Operator',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),

               15 =>
               array(
                    'id' => 16,
                    'name' => 'Operations/Zone supervisor',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               16 =>
               array(
                    'id' => 17,
                    'name' => 'Operations/Zones Manager',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               17 =>
               array(
                    'id' => 18,
                    'name' => 'Management/Operations Manager',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               18 =>
               array(
                    'id' => 19,
                    'name' => 'Management/Director',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               19 =>
               array(
                    'id' => 20,
                    'name' => 'Management/Chief',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               20 =>
               array(
                    'id' => 21,
                    'name' => 'Maintenance/Worker',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               21 =>
               array(
                    'id' => 22,
                    'name' => 'Maintenance/Supervisor',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               22 =>
               array(
                    'id' => 23,
                    'name' => 'Maintenance/Manager',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               23 =>
               array(
                    'id' => 24,
                    'name' => 'Maintenance/Director',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               24 =>
               array(
                    'id' => 25,
                    'name' => 'Maintenance/Chief',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               25 =>
               array(
                    'id' => 26,
                    'name' => 'Technical Services/Engineer',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               26 =>
               array(
                    'id' => 27,
                    'name' => 'Technical Services/Manager',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               27 =>
               array(
                    'id' => 28,
                    'name' => 'Technical Services/Director',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               28 =>
               array(
                    'id' => 29,
                    'name' => 'Health & safety/Supervisor',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               29 =>
               array(
                    'id' => 30,
                    'name' => 'Health & safety/Manager',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
               30 =>
               array(
                    'id' => 31,
                    'name' => 'Health & safety/Director',
                    'guard_name' => 'web',
                    'created_at' => NULL,
                    'updated_at' => NULL,
               ),
                31 =>
                    array(
                        'id' => 31,
                        'name' => 'Breaker',
                        'guard_name' => 'web',
                        'created_at' => NULL,
                        'updated_at' => NULL,
                    ), */
            )
        );
    }
}
