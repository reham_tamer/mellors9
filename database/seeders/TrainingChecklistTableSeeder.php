<?php
namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TrainingChecklistTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
      */
    public function run()
    {

        $training_checklists = [
             [ //1
                'name' => 'TMS-G-20 Attraction Attendant Cross-Training Checklist',
                'type' => 'attendance',
                'training_type'=>'training'

             ],
             
              [ //2
                'name' => 'TMS-G-08A Emergency Evacuation Training Checklist (Attraction Attendants)',
                'type' => 'attendance',
                'training_type'=>'evacuation'
             ],
             [ //3
                'name' => 'TMS-G-08 Emergency Evacuation Training Checklist',
                'type' => 'operator',
                'training_type'=>'evacuation'

             ], 

        ];
        foreach ($training_checklists as $checklist) {
            $existingRecord = DB::table('training_checklists')
                                ->where('name', $checklist['name'])
                                ->first();

            if ($existingRecord) {
                DB::table('training_checklists')
                    ->where('id', $existingRecord->id) 
                    ->update([
                        'type' => $checklist['type'],
                        'training_type' => $checklist['training_type']
                    ]);
            } else {
                DB::table('training_checklists')->insert($checklist);
            }
        }
    }
}