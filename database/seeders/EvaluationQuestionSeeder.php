<?php
namespace Database\Seeders;

use App\Models\EvaluationQuestion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EvaluationQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EvaluationQuestion::query()->truncate();

        $questions = [
            ['name' => 'Completing all  pre-opening checks (handrails, safety gates, height board, safety signage, and swiper).', 'type' => 'Pre Opening Checks  (Max 15 Marks)' , 'max_score' => 5 , 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Ensuring correct number of attendants are present (report any short fall).', 'type' => 'Pre Opening Checks  (Max 15 Marks)', 'max_score' => 5, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Removing covers and cleaning the ride- general cleaning (wipe & clean front of house, gates, seats, gondolas, etc.).', 'type' => 'Pre Opening Checks  (Max 15 Marks)', 'max_score' => 5, 'evaluation_to' => 'operator','description' =>null],

            ['name' => 'Ensuring that all riders meet the rules and regulations for the ride.', 'type' => 'Operations (Max 45 Marks)', 'max_score' => 3 , 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Ensuring the ride is presentable throughout their duty.', 'type' => 'Operations (Max 45 Marks)', 'max_score' => 2 , 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Operating the ride as instructed and to the safety requirements.', 'type' => 'Operations (Max 45 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Reporting any unusual circumstances to the Supervisor or Manager through the radio.', 'type' => 'Operations (Max 45 Marks)', 'max_score' => 2, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Ensuring no unauthorized personnel are in or around the ride (payboxes, back of house).', 'type' => 'Operations (Max 45 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Following and enforcing Health and Safety regulations.', 'type' => 'Operations (Max 45 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Being fully conversant with the EMERGENCY EVACUATION procedure.', 'type' => 'Operations (Max 45 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Completing all paperwork for the days operation.', 'type' => 'Operations (Max 45 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Inputting and using the iPad device correctly regarding documentation and statistics.', 'type' => 'Operations (Max 45 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],

            ['name' => 'Ensuring the attendants report to the ride on time with the correct uniform.', 'type' => 'Managing the Attendants (Max 40 Marks)', 'max_score' => 2, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Managing the attendants with cleaning duties and pre -opening checks.', 'type' => 'Managing the Attendants (Max 40 Marks)', 'max_score' => 4, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Scheduling and documenting breaks for the attendants.', 'type' => 'Managing the Attendants (Max 40 Marks)', 'max_score' => 5, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Monitoring and ensuring that the attendants are performing their duties correctly.', 'type' => 'Managing the Attendants (Max 40 Marks)', 'max_score' => 6, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Ensuring the attendants are standing in the correct positions (loading and ride cycle).', 'type' => 'Managing the Attendants (Max 40 Marks)', 'max_score' => 3, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Ensuring the attendants are interacting with customers in a professional manner and assisting customers on and off the rides when required.', 'type' => 'Managing the Attendants (Max 40 Marks)', 'max_score' => 2, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Ensuring the attendants follow Health & Safety Regulations (Smoking, back of house access etc).', 'type' => 'Managing the Attendants (Max 40 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Ensuring the attendants clean the ride and cover the gondolas at the end of their duty.', 'type' => 'Managing the Attendants (Max 40 Marks)', 'max_score' => 5, 'evaluation_to' => 'operator','description' =>null],

            ['name' => 'Empathy - Does the operator consistently demonstrate the ability to understand and acknowledge others emotions and perspectives?', 'type' => 'Soft Skills (Max 50 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Problem Solving - Does the operator consistently demonstrate the ability to identify the cause of an issue or complaint and effectively resolve it?', 'type' => 'Soft Skills (Max 50 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Communication - Does the operator consistently demonstrate positive body language and use clear, concise language when informing customers about company products and policies in person?', 'type' => 'Soft Skills (Max 50 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Active Listening - Does the operator listen to the customer’s questions and concerns and respond appropriately to address them and facilitate a solution?', 'type' => 'Soft Skills (Max 50 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Technical Knowledge -  Is the operator familiar with technical and industry knowledge to help customers make informed decisions and troubleshoot any issues?', 'type' => 'Soft Skills (Max 50 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Patience - Does the operator demonstrate compassion and patience to help deliver a positive customer experience, and their presence and actions prevent worsening a bad situation?', 'type' => 'Soft Skills (Max 50 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Tenancy - The operator remains persistent throughout difficult situations and guides the customer through the process when they need assistance.', 'type' => 'Soft Skills (Max 50 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Adaptability - The operator demonstrates the ability to adapt to various situations and maintains good communication with the customer.', 'type' => 'Soft Skills (Max 50 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Resourcefulness - The operator demonstrates the ability to find innovative and quick ways to solve problems, decreasing time spent with each customer while maintaining consistent service standards.', 'type' => 'Soft Skills (Max 50 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],
            ['name' => 'Positive Attitude - The operator maintains a positive attitude throughout their interactions with customers in the park.', 'type' => 'Soft Skills (Max 50 Marks)', 'max_score' => 1, 'evaluation_to' => 'operator','description' =>null],

            ['name' => 'Outstanding', 'type' => 'Scoring System & Attributes', 'max_score' => 5, 'evaluation_to' => 'operator' ,'description' => 'Operator Performance represents an extraordinary level of achievement and commitment in terms of quality , skill and knowledge. Operator at this level should have demonstrated exceptional job mastery in all areas of responsibility '],
            ['name' => 'Exceeds Requirements', 'type' => 'Scoring System & Attributes', 'max_score' => 4, 'evaluation_to' => 'operator' ,'description' => 'Operator performance Exceeded expectations . All goals and objectivises were achieved above the  requirements of the role.'],
            ['name' => 'Meets Requirements', 'type' => 'Scoring System & Attributes', 'max_score' => 3, 'evaluation_to' => 'operator' ,'description' => 'Operator performance met expectations in terms of quality of work. Most critical goals were met '],
            ['name' => 'Need Improvement', 'type' => 'Scoring System & Attributes', 'max_score' => 2, 'evaluation_to' => 'operator' ,'description' => 'Operator performance failed to meet expectations, and/or reasonable progress toward critical goals was not achieved'],
            ['name' => 'Unsatisfactory', 'type' => 'Scoring System & Attributes', 'max_score' => 1, 'evaluation_to' => 'operator' ,'description' => 'Operator performance was consistently below expectations, and/or reasonable progress toward critical goals was not achieved  '],

            //supervisor
            ['name' => 'Ensuring attractions in assigned zone are ready for daily operations before members of the public arrive .', 'type' => 'Opening & Closing (Max 30 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Ensuring all operators have carried out pre-operational ride checks required within the assigned zone area .', 'type' => 'Opening & Closing (Max 30 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Ensuring your zone is aesthetically ready for the public. Does it look appealing to the guests.', 'type' => 'Opening & Closing (Max 30 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Ensure that the handover sheets have been signed by the correct authority level in the relevant departments', 'type' => 'Opening & Closing (Max 30 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Ensuring accuracy of the opening procedures checklist by sampling daily .', 'type' => 'Opening & Closing (Max 30 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Supervise the closing down procedure at the end of the day within the assigned zone area.', 'type' => 'Opening & Closing (Max 30 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],

            ['name' => 'Reporting any faults to the Operations Manager, documenting issues ,solutions and waiting for confirmation from Management that the attraction can open to the public.', 'type' => 'Zone Management  (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Monitor staffing levels and performance in the assigned zone area, Rectifying problems and liaising with the Operations Manager.', 'type' => 'Zone Management  (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Ensure that operators and Attendants is implementing and enforcing the ride restrictions, guidelines and rules of operation with the staff and for the public.', 'type' => 'Zone Management  (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Ensuring Operators and Attendants maintain/perform as instructed and to the safety requirements..', 'type' => 'Zone Management  (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Ensuring all new or transferred employees have been adequately trained to safely perform their work on their assigned ride and documenting all training.', 'type' => 'Zone Management  (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Conducting continuous inspections to identify and correct operational hazards within the assigned zone area.', 'type' => 'Zone Management  (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Assisting in accident / incident investigation as requested by the safety officer', 'type' => 'Zone Management  (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Responding to all emergency situations and assisting in all emergency evacuations of rides. (If required)', 'type' => 'Zone Management  (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Providing attendants and operators with the  necessary information needed to effectively perform their duties.', 'type' => 'Zone Management  (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Following up on tasks delegated to Attendants and Operators to assure proper completion.', 'type' => 'Zone Management  (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Resolving and communicating concerns, occurrences or problems that may arise within the assigned zone area to the Operations Manager.', 'type' => 'Zone Management  (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],


            ['name' => 'Coaching - Does the supervisor demonstrate the ability to develop the staff performance by offering feedback and demonstrating the desired skills and expected work ethic.', 'type' => 'Leadership Skills (Max 25 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Motivating -  Does the supervisor encourages the staff to work harder or extend themselves beyond their comfort zone to accomplish big goals.', 'type' => 'Leadership Skills (Max 25 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Delegating - Does  the supervisor demonstrate the ability to assign tasks and responsibilities to the proper team members  to achieve a common goal', 'type' => 'Leadership Skills (Max 25 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Vision & Foresight - Does the supervisor have the ability to see what is likely to happen in the future and to take appropriate action .', 'type' => 'Leadership Skills (Max 25 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Conflict Management - Does the supervisor identify, prevent, minimize, and solve conflicts in the workplace.', 'type' => 'Leadership Skills (Max 25 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],


            ['name' => 'Empathy - Does the supervisor demonstrate the ability to understand another person’s emotions and perspective.', 'type' => 'Soft Skills (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Problem Solving - when an issue or complaint arises, does the supervisor demonstrate the ability to figure out why they are experiencing the problem and how to fix it.', 'type' => 'Soft Skills (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Communication - When speaking with customers in person, does the supervisor demonstrate they have positive body language and are they using clear and concise language to inform the customers of company products and policies.', 'type' => 'Soft Skills (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Active Listening - Does the supervisor listen to customer’s questions and concerns and responds in a way that makes them feel heard, therefore paving the way forward to a solution.', 'type' => 'Soft Skills (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Technical Knowledge -  Is the supervisor familiar with technical and industry knowledge to help customers make informed decisions and troubleshoot any issues.', 'type' => 'Soft Skills (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Patience - Does the supervisor demonstrate compassion and patience to help deliver a positive customer experience. Does their presence and actions prevent making a bad situation worse.', 'type' => 'Soft Skills (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Tenancy - Does the supervisor remain persistent throughout difficult situations and do they walk the customer through the process when they need assistance.', 'type' => 'Soft Skills (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Adaptability - Does the supervisor demonstrate the ability to adapt to various situations, as in do they maintain good communication with the customer.', 'type' => 'Soft Skills (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Resourcefulness - Does the supervisor demonstrate the ability to find innovative and quick ways to solve the problem, this can decrease time with each customer so that you can help more customers in a day (service standards must remain consistent).', 'type' => 'Soft Skills (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Pressure Management - Does the supervisor demonstrate  the ability to work in stressful situations while maintaining their full professional standards.', 'type' => 'Soft Skills (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],
            ['name' => 'Positive Attitude - Does the supervisor attitude remain positive throughout their interaction with the customers in the park.', 'type' => 'Soft Skills (Max 55 Marks)', 'max_score' => 5, 'evaluation_to' => 'supervisor','description' =>null],

            ['name' => 'Outstanding', 'type' => 'Scoring System & Attributes', 'max_score' => 5, 'evaluation_to' => 'supervisor' ,'description' =>'Supervisor Performance represents an extraordinary level of achievement and commitment in terms of quality skill and knowledge. Operator at this level should have demonstrated exceptional job mastery in all areas of responsibility'],
            ['name' => 'Exceeds Requirements', 'type' => 'Scoring System & Attributes', 'max_score' => 4, 'evaluation_to' => 'supervisor' ,'description' => 'Supervisor performance Exceeded expectations . All goals and objectivises were achieved above the  requirements of the role.'],
            ['name' => 'Meets Requirements', 'type' => 'Scoring System & Attributes', 'max_score' => 3, 'evaluation_to' => 'supervisor' ,'description' => 'Supervisor performance met expectations in terms of quality of work. Most critical goals were met '],
            ['name' => 'Need Improvement', 'type' => 'Scoring System & Attributes', 'max_score' => 2, 'evaluation_to' => 'supervisor' ,'description' => 'Supervisor performance failed to meet expectations, and/or reasonable progress toward critical goals was not achieved '],
            ['name' => 'Unsatisfactory', 'type' => 'Scoring System & Attributes', 'max_score' => 1, 'evaluation_to' => 'supervisor' ,'description' => 'Supervisor performance was consistently below expectations, and/or reasonable progress toward critical goals was not achieved '],
        ];

        DB::table('evaluation_questions')->insert($questions);
    }
}

