<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class CheckListQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \DB::table('check_list_questions')->delete();

        $questions = [
            ['type' => 'pm',
             'name' => 'Has the weather alert been prepared and posted in the relevant WhatsApp groups at least 1 hour before opening?',
             'question_type'=>'Pre-Opening Checks'
        ,
        'is_rides_question'=>'0'
        ],
        ['type' => 'pm',
        'name' => 'Has the operator/tech sheets been signed off by ALL relevant parties prior to pre-checks being carried out?',
        'question_type'=>'Pre-Opening Checks'
        ,
        'is_rides_question'=>'0'
        ],
        ['type' => 'pm',
        'name' => 'Are the rides fences being secured and stable?',
        'question_type'=>'Pre-Opening Checks',
        'is_rides_question'=>'0'
        ],
        ['type' => 'pm',
        'name' => 'Is the PA and CCTV system available with the required rides (If applicable)?',
        'question_type'=>'Pre-Opening Checks',
        'is_rides_question'=>'0'
        ],
        ['type' => 'pm',
        'name' => 'Are the ride entrances, exits and walkways free from slips, trips and fall hazards?',
        'question_type'=>'Pre-Opening Checks',
        'is_rides_question'=>'0'
        ],
        ['type' => 'pm',
        'name' => 'Is good housekeeping being maintained in and around the ride?',
        'question_type'=>'Pre-Opening Checks',
        'is_rides_question'=>'0'
        ],
        ['type' => 'pm',
        'name' => 'Is there adequate signage available on the rides and are they in good condition?',
        'question_type'=>'Pre-Opening Checks',
        'is_rides_question'=>'0'
        ],
        ['type' => 'pm',
        'name' => 'Is there any H&S observations noticed, reported and documented?',
        'question_type'=>'Operational Checks',
        'is_rides_question'=>'0'
        ],
        ['type' => 'pm',
        'name' => 'Are there any incidents/accident/near miss occurred, reported and documented?',
        'question_type'=>'Operational Checks',
        'is_rides_question'=>'0'
        ],
        ['type' => 'pm',
        'name' => 'Any statements taken for the incidents/accidents/near misses?',
        'question_type'=>'Operational Checks',
        'is_rides_question'=>'0'
        ],
        ['type' => 'pm',
        'name' => 'Any first aid was required for the employee or guest?',
        'question_type'=>'Operational Checks',
        'is_rides_question'=>'0'
        ],
        ['type' => 'pm',
        'name' => 'Was there any abnormal ride evacuations?',
        'question_type'=>'Operational Checks',
        'is_rides_question'=>'0'
        ], 
        ['type' => 'pm',
        'name' => 'Have any electrical hazards been observed?',
        'question_type'=>'Operational Checks',
        'is_rides_question'=>'0'
        ],

        ['type' => 'pm',
        'name' => 'Have any fire hazards been observed?',
        'question_type'=>'Operational Checks',
        'is_rides_question'=>'0'
        ],

        ['type' => 'pm',
        'name' => 'Guests related incident/accident/near miss reported to the Sela/MES HSE group?',
        'question_type'=>'Operational Checks',
        'is_rides_question'=>'0'
        ],

        ['type' => 'pm',
        'name' => 'Any work permits obtained during operational hours?',
        'question_type'=>'Operational Checks',
        'is_rides_question'=>'0'
        ],

        ['type' => 'pm',
        'name' => 'Any training/toolbox talks undertaken, and attendance registers been taken?',
        'question_type'=>'Operational Checks',
        'is_rides_question'=>'0'
        ],

        ['type' => 'pm',
        'name' => 'Any regulatory body visited the site during operational hours?',
        'question_type'=>'Operational Checks',
        'is_rides_question'=>'0'
        ],

        ['type' => 'pm',
        'name' => 'Has there been any violations observed for the attendants, operator, supervisor or third party (Sela/media etc) during operational hours?',
        'question_type'=>'Operational Checks',
        'is_rides_question'=>'0'
        ],

        ['type' => 'pm',
        'name' => 'Pick 5 rides and observe 3 cycles each. Have observations been documented and given to the relevant department.',
        'question_type'=>'Operational Checks',
        'is_rides_question'=>'1'
        ],

        ['type' => 'pm',
        'name' => 'Are the on-site paramedics team, ER team arriving in a acceptable timeframe (no more than 5 minutes)',
        'question_type'=>'Operational Checks',
        'is_rides_question'=>'0'
        ],

        ['type' => 'pm',
        'name' => 'Is the H&S duty report compiled and has it been sent to the concerned personnel?',
        'question_type'=>'Post operations checks',
        'is_rides_question'=>'0'
        ],

        ['type' => 'pm',
        'name' => 'Are all the relevant documents been inputted on the server?',
        'question_type'=>'Post operations checks',
        'is_rides_question'=>'0'
        ],

        ['type' => 'pm',
        'name' => 'Is there any red flag issue raised on the duty report?',
        'question_type'=>'Post operations checks',
        'is_rides_question'=>'0'
        ],

        ['type' => 'am',
        'name' => 'Have ALL work permits/equipment checklists have been correctly filled out before the commencement of the work.',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Are ALL fire extinguishers placed at the right locations, charged correctly, and easily accessible?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Are the hazardous chemicals being stored correctly in the appropriate container/location?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Are there any flammable & combustible materials placed in the ride vicinity?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
       
        ['type' => 'am',
        'name' => 'Have all plant equipment drivers (including staff) got the correct credentials?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Third-party certificates for employees have been updated (Mention in comments if anything expire within a month).',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Are good housekeeping practices being maintained in all areas? ',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Are there any non-routine works carried out such us (lifting, excavation, demolition, etc)?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Is LOTO being enforced by management?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Is LOTO being implemented by staff members?.',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Is there any third-party training or Inspections being conducted? ',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Have any electrical hazards been observed?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Have any fire hazards been observed?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Are the welfare facilities adequate, clean, and tidy (Drinking water, restroom, dining area)?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Have any training/toolbox talks undertaken, and attendance registers have been taken?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Has any regulatory body visited the site?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Have there been any violations/non-conformances observed from Mellors staff during the shift?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'If the question above is a yes, has this been reported to the line manager of the individual/individuals?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Pick 5 rides and observe working behaviours/safety culture/implementation. Have H&S observations been documented and given to the relevant department?.',
        'question_type'=>'',
        'is_rides_question'=>'1'
        ],
        ['type' => 'am',
        'name' => 'Has there been any accidents, incidents, or near misses during the shift?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Has there been any red flag issue raised on the shift?.',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Are ALL harnesses being handed back to the relevant container/store after the shift?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ],
        ['type' => 'am',
        'name' => 'Are staff wearing the correct PPE for the tasks they are undertaking?',
        'question_type'=>'',
        'is_rides_question'=>'0'
        ]


        ];

        DB::table('check_list_questions')->insert($questions);
    }
}
 
