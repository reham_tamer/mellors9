<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_feedbacks', function (Blueprint $table) {
            $table->foreignId('feedback_subcategory_id')->nullable()->constrained('feedback_subcategory');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_feedbacks', function (Blueprint $table) {
            //
        });
    }
};
