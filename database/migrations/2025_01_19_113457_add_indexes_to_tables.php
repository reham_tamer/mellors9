<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('rides', function (Blueprint $table) {
            $table->index('deleted_at');
            $table->index('ride_cat');

        });

        Schema::table('park_times', function (Blueprint $table) {
            $table->index('date');
        });
        Schema::table('ride_cycles', function (Blueprint $table) {
            $table->index('ride_id','park_time_id');

        });
        Schema::table('ride_stoppages', function (Blueprint $table) {
            $table->index('created_at');
            $table->index('ride_id','park_time_id');

        });

        Schema::table('queues', function (Blueprint $table) {
            $table->index('ride_id','park_time_id');
        }); 

        Schema::table('ride_cycles', function (Blueprint $table) {
            $table->index(
                ['riders_count', 'number_of_disabled', 'number_of_vip', 'number_of_ft'], 
                'ride_cycles_riders_index'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tables', function (Blueprint $table) {
            //
        });
    }
};
