<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_operational_infos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('daily_operational_id')->constrained('daily_operationals')->cascadeOnDelete();
            $table->foreignId('check_list_question_id')->constrained('check_list_questions')->cascadeOnDelete();
            $table->boolean('answer')->default(0);
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_operational_infos');
    }
};
