<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('availability_stoppages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ride_id')->nullable()->constrained('rides');
            $table->foreignId('park_id')->nullable()->constrained('parks');
            $table->foreignId('game_time_id')->nullable()->constrained('game_times');
            $table->date('date');
            $table->foreignId('stopage_sub_category_id')->nullable()->constrained('stopage_sub_categories');
            $table->foreignId('stopage_category_id')->nullable()->constrained('stopage_categories');
            $table->text('comment')->nullable();
            $table->foreignId('user_id')->nullable()->constrained('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('availability_stoppages');
    }
};
