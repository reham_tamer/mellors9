<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units_properties', function (Blueprint $table) {
            $table->id();
            $table->foreignId('unit_id')->nullable()->constrained('units');
            $table->foreignId('tenant_id')->nullable()->constrained('users');
            $table->date('move_in_date')->nullable();
            $table->foreignId('move_in_inspected_by_id')->nullable()->constrained('users');
            $table->date('move_out_date')->nullable();
            $table->foreignId('move_out_inspected_by_id')->nullable()->constrained('users');
            $table->date('initial_inspection_date')->nullable();
            $table->foreignId('initial_inspection_by_id')->nullable()->constrained('users');
            $table->json('move_in')->nullable();
            $table->json('initial_inspection')->nullable();
            $table->json('move_out')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units_properties');
    }
};
