<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('preopening_lists', function (Blueprint $table) {
            $table->dropColumn(['opened_date','lists_type']);

            Schema::table('preopening_lists', function (Blueprint $table) {
                $table->dropForeign('preopening_lists_ride_id_foreign');
            });
                Schema::table('preopening_lists', function (Blueprint $table) {
                $table->dropColumn('ride_id');
            });
            Schema::table('preopening_lists', function (Blueprint $table) {
                $table->dropForeign('preopening_lists_park_id_foreign');
            });
                Schema::table('preopening_lists', function (Blueprint $table) {
                $table->dropColumn('park_id');
            });
            Schema::table('preopening_lists', function (Blueprint $table) {
                $table->dropForeign('preopening_lists_zone_id_foreign');
            });
                Schema::table('preopening_lists', function (Blueprint $table) {
                $table->dropColumn('zone_id');
            });
            Schema::table('preopening_lists', function (Blueprint $table) {
                $table->dropForeign('preopening_lists_park_time_id_foreign');
            });
                Schema::table('preopening_lists', function (Blueprint $table) {
                $table->dropColumn('park_time_id');
            });
            Schema::table('preopening_lists', function (Blueprint $table) {
                $table->dropForeign('preopening_lists_created_by_id_foreign');
            });
                Schema::table('preopening_lists', function (Blueprint $table) {
                $table->dropColumn('created_by_id');
            });

            $table->foreignId('preopen_info_id')->constrained('preopen_infos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('preopening_lists', function (Blueprint $table) {
            //
        });
    }
};

