<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('training_user_rides')) {

            Schema::create('training_user_rides', function (Blueprint $table) {

                $table->id();
                $table->foreignId("training_id");
//            $table->foreignId('training_id')->nullable()->constrained('trainings')->cascadeOnDelete();
                $table->foreignId('ride_id')->nullable()->constrained('rides')->cascadeOnDelete();
                $table->foreignId('attendance_id')->nullable()->constrained('attendances')->cascadeOnDelete();
                $table->string('trainee_signature')->nullable();
                $table->string('trainer_signature')->nullable();
                $table->date('date')->nullable();
                $table->timestamps();
            });
        }


//        Schema::table('training_user_checklists', function (Blueprint $table) {
//
//            $table->string('trainee_signature')->nullable();
//            $table->string('trainer_signature')->nullable();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_user_rides');
    }
};
