<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('general_incidents', function (Blueprint $table) {
            $table->foreignId('game_time_id')->nullable()->constrained('game_times')->nullOnDelete();

        });
        Schema::table('observations', function (Blueprint $table) {
            $table->foreignId('game_time_id')->nullable()->constrained('game_times')->nullOnDelete();

        });
        Schema::table('queues', function (Blueprint $table) {
            $table->foreignId('game_time_id')->nullable()->constrained('game_times')->nullOnDelete();

        });
        Schema::table('ride_cycles', function (Blueprint $table) {
            $table->foreignId('game_time_id')->nullable()->constrained('game_times')->nullOnDelete();

        });
        Schema::table('ride_stoppages', function (Blueprint $table) {
            $table->foreignId('game_time_id')->nullable()->constrained('game_times')->nullOnDelete();

        });
        Schema::table('customer_feedbacks', function (Blueprint $table) {
            $table->foreignId('game_time_id')->nullable()->constrained('game_times')->nullOnDelete();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tables', function (Blueprint $table) {
            //
        });
    }
};
