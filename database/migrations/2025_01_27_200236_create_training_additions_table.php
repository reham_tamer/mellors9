<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_additions', function (Blueprint $table) {
            $table->id();
            $table->integer('training_id');
            $table->morphs('userable');
            $table->date('date')->nullable();
            $table->string('trainee_signature')->nullable();
            $table->longText('trainer_comment')->nullable();
            $table->longText('addition')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_additions');
    }
};
