<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trainings', function (Blueprint $table) {
            $table->foreignId('park_id')->nullable()->constrained('parks')->cascadeOnDelete();
            $table->foreignId('ride_id')->nullable()->constrained('rides')->cascadeOnDelete();
            $table->string('user_type')->nullable()->comment('attendance | operator');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_checklist_items', function (Blueprint $table) {
            //
        });
    }
};
