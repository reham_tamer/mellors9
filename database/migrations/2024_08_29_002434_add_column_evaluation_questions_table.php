<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluation_questions', function (Blueprint $table) {
            $table->string('evaluation_to')->nullable();
            $table->text('description')->nullable();
        });
        Schema::table('evaluation_infos', function (Blueprint $table) {
            $table->dropForeign(['evaluation_question_id']);
            $table->foreignId('evaluation_question_id')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evaluation_questions', function (Blueprint $table) {
            //
        });
    }
};
