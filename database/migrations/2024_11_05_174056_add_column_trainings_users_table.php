<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('training_users', function (Blueprint $table) {
            $table->string('user_signature')->nullable();
            $table->string('operation_manager_signature')->nullable();
            $table->string('supervisor_signature')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_checklist_items', function (Blueprint $table) {
            //
        });
    }
};
