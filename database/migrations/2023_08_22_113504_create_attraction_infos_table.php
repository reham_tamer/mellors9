<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attraction_infos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('created_by_id')->constrained('users');
            $table->foreignId('approved_by_id')->constrained('users');
            $table->foreignId('ride_id')->constrained('rides');
            $table->foreignId('park_time_id')->constrained('park_times');
            $table->foreignId('park_id')->constrained('parks');
            $table->foreignId('zone_id')->constrained('zones');
            $table->enum('type',['inspection_list','preopening','preclosing']);
            $table->text('operator_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attraction_infos');
    }
};
