<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zone_inspections', function (Blueprint $table) {
            $table->id();
            $table->string('status');
            $table->string('comment')->nullable();
            $table->string('is_checked');
            $table->foreignId('zone_inspection_infos_id');
            $table->foreignId('inspection_list_id');
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zone_inspections');
    }
};
