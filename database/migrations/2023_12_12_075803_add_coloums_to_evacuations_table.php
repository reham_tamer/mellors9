<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evacuations', function (Blueprint $table) {
            $table->boolean('longer_than_normal')->nullable();
            $table->boolean('medics_required')->nullable();
            $table->boolean('civil_defense_involved')->nullable();
            $table->boolean('vip_guest_involved')->nullable();
            $table->boolean('customer_service_issue')->nullable();
            $table->boolean('property_damage')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evacuations', function (Blueprint $table) {
            //
        });
    }
};
