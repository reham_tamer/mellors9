<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evacuations', function (Blueprint $table) {
            $table->id();
            $table->time('time');
            $table->integer('total_rider');
            $table->time('first_rider_off')->nullable();
            $table->time('last_rider_off')->nullable();
            $table->boolean('abnormal')->nullable();
            $table->text('first_aid')->nullable();
            $table->text('evacuation_details')->nullable();
            $table->text('customer_service_gesture')->nullable();
            $table->text('portal_overview')->nullable();
            $table->date('submit_date')->nullable();
            $table->string('image')->nullable();
            $table->integer('created_by');
            $table->foreignId('park_id')->nullable()->constrained('parks');
            $table->foreignId('game_time_id')->nullable()->constrained('game_times');
            $table->foreignId('stoppage_id')->nullable()->constrained('ride_stoppages');
            $table->foreignId('ride_id')->nullable()->constrained('rides');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evacuations');
    }
};
