<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preopen_infos', function (Blueprint $table) {
            $table->id();
            $table->enum('type',['inspection_list','preopening','preclosing'])->default('inspection_list');
            $table->string('image')->nullable();
            $table->foreignId('operator_id')->nullable()->constrained('users');
            $table->foreignId('approved_by_id')->nullable()->constrained('users');
            $table->foreignId('park_id')->constrained('parks');
            $table->foreignId('ride_id')->constrained('rides');
            $table->foreignId('zone_id')->constrained('zones');
            $table->foreignId('park_time_id')->nullable()->constrained('park_times');
            $table->date('opened_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preopen_infos');
    }
};
