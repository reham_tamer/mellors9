<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('queue_ride_numbers', function (Blueprint $table) {
            $table->foreignId('game_time_id')->nullable()->constrained('game_times')->cascadeOnDelete();

        });
        Schema::table('cycle_ride_numbers', function (Blueprint $table) {
            $table->foreignId('game_time_id')->nullable()->constrained('game_times')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('queue_ride_numbers');
    }
};
