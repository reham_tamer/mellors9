<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incident_images', function (Blueprint $table) {
            $table->id();
            $table->foreignId('general_incident_id')->constrained('general_incidents')->cascadeOnDelete();
            $table->string('image');
            $table->timestamps();
        });
        Schema::table('general_incidents', function (Blueprint $table) {
            $table->string('signature')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incident_images');
    }
};
