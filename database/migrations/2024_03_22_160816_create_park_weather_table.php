<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('park_weather', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('park_time_id');
            $table->foreign('park_time_id')->references('id')->on('park_times')->onDelete('cascade');
            $table->dateTime('datetime');
            $table->decimal('temperature', 8, 2);
            $table->decimal('windspeed_avg', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('park_weather');
    }
};
